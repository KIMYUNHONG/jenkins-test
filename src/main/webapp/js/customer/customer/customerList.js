/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	Common.showLoading();
	var $viewAllCustomerFlag = $('#viewAllCustomerFlag');	// 미사용포함 checkbox
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $addCustomerBtn = $('#addCustomerBtn');			// 거래처 등록
	var $resetBtn = $('#resetBtn');		// 검색조건 초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $contractStatusList = $("input[name=contractStatusList]");
	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');

	var previousMonthDay = new Date().addDate(0, -1, 0);
	var nextMonthDay = new Date().addDate(0, 1, 0);
	var $searchForm = $('#searchForm');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var customerColumns = [
		{header: '거래처구분', name: 'customerTypeText', width: 100, sortable: true, align: 'center'},
		{header: '거래처', name: 'customerShortName', minWidth: 135, sortable: true, align: 'left'},
		{header: '담당부서', name: 'representName', width: 100, sortable: true, align: 'center'},
		{header: '영업담당자', name: 'salesUserName', width: 100, sortable: true, align: 'center'},
		{header: '설치장소', name: 'locationCnt', width: 90, sortable: true, align: 'right', numberFormat: true},
		{header: '장비', name: 'deviceCnt', width: 90, sortable: true, align: 'right', numberFormat: true},
		{header: '거래처담당자', name: 'chargeName', width: 100, sortable: true, align: 'center'},
		{header: '직책', name: 'chargeRankText', width: 100, sortable: true, align: 'center'},
		{header: '휴대폰번호', name: 'chargePhone', width: 110, sortable: true, align: 'center'},
		{header: '전화번호', name: 'customerPhone', width: 110, sortable: true, align: 'center'},
		{header: '이메일', name: 'emailText', width: 180, sortable: true, align: 'left'},
		{header: '가입요청', name: 'confirmNoCnt', width: 60, sortable: true, align: 'right'},
		{header: '계약상태', name: 'contractStatusText', width: 71, sortable: true, align: 'center'},
		{header: '계약기간', name: 'contractPeriodText', width: 180, sortable: true, align: 'center'},
		{
			header: '자료',
			name: 'existFileYn',
			width: 60,
			sortable: false,
			align: 'center',
			renderer: Grid.iconRenderer({
					class: 'ic_data'
				},
				function () {
					var row = (customerGrid.getFocusedCell()['rowKey'] || 0);
					var record = customerGrid.getRow(row);
					openFileDownloadPopup("CUSTOMER", record.customerSeq);
				})
		},
		{header: '등록자', name: 'regName', width: 100, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', width: 100, sortable: true, align: 'center'},
	];

	var customerGrid = Grid.createGrid({
		id: 'customerGrid',
		columns: customerColumns,
		data: []
	});

	// 거래처 로우 선택시 이벤트 처리
	customerGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null || result.columnName == "existFileYn") {	 // 자료인경우 자료 팝업을 띄우기위해
			return false;
		}
		Common.showLoading();
		var record = customerGrid.getRow(row);
		location.href = '/customer/customer/customerDetail?customerSeq=' + record["customerSeq"] + '&' + $searchForm.serializeListParameter();
	});

	getCustomerList();

	// 코드 그룹 데이터 로드
	function getCustomerList() {
		Common.showLoading();
		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "customerList" : "?" + $searchForm.serializeListParameter());
		// 기간 종류를 선택안하고 날짜를 선택하거나, 기간종류만 선택하고 날짜를 선택안할시
		if ((Valid.isNotEmpty(parameter.dateType) && (Valid.isEmpty(parameter.searchStartDate) || Valid.isEmpty(parameter.searchEndDate)))
			|| (Valid.isEmpty(parameter.dateType) && (Valid.isNotEmpty(parameter.searchStartDate) || Valid.isNotEmpty(parameter.searchEndDate)))) {
			alert("기간을 선택해주세요.");
			Common.hideLoading();
			return false;
		}

		parameter = $.extend({}, parameter, {
			viewAllCustomerFlag: 'N',
			isAgencyMenu: 'N',
			excludeCustomerSeq: 1
		});
		Common.callAjax("/customer/customer/api/getCustomerList", parameter, function (result) {
			customerGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getCustomerList();
	});

	// 검색조건 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {

		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = customerColumns;

		var option = {
			fileName: '거래처리스트'
		};
		Common.excelDownload("/customer/customer/excel/getCustomerList", parameter, option);
	});

	// 등록 버튼 이벤트 처리
	$addCustomerBtn.on('click', function () {
		Common.showLoading();
		location.replace('/customer/customer/customerWrite');
	});

	//체크박스 전체 클릭시 이벤트 처리
	$('#total_chk').on('click', function () {
		$contractStatusList.prop("checked", $("#total_chk").is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$contractStatusList.on('click', function () {
		var totalCnt = $contractStatusList.length;
		var chkCnt = $("input[name=contractStatusList]:checked").length;

		// 전체 체크박스 개수와 선택된 체크박스 개수가 동일한경우 '전체' 체크
		$("#total_chk").prop("checked", (chkCnt == totalCnt));
	});

	function openFileDownloadPopup(domain, domainSeq) {

		Common.openPopup('/common/downloadFilePopup', {
			parameter: {
				domain: domain,
				domainSeq: domainSeq
			}
		}, function (result) {
			var $popup = Common.getPopupTemplate("자료", result, {width: 480});
			$('#popupList').append($popup);
			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	}
});