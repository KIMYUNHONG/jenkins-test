/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $customerModifyBtn = $('#customerModifyBtn');		// 수정 버튼
	var $customerListBtn = $('#customerListBtn');			// 목록 버튼
	var customerSeq = $('input[name="customerSeq"]').val();	// 거래처_SEQ(KEY)
	var queryString = $('input[name="queryString"]').val();

	var chargeColumns = [
		{header: '성명', name: 'name', minWidth: 200, sortable: true, align: 'center'},
		{header: '아이디', name: 'userId', minWidth: 200, sortable: true, align: 'center'},
		{header: '직책', name: 'rankText', minWidth: 150, sortable: true, align: 'center'},
		{header: '휴대폰번호', name: 'phone', minWidth: 240, sortable: true, align: 'center'},
		{header: '전화번호', name: 'telephone', minWidth: 240, sortable: true, align: 'center'},
		{header: '이메일', name: 'emailText', minWidth: 280, sortable: true, align: 'left'},
		{header: '대표담당', name: 'representYnText', minWidth: 100, sortable: true, align: 'center'},
		{header: '상태', name: 'useYnText2', minWidth: 100, sortable: true, align: 'center'},
		{
			header: '비고', name: 'confirmYn', minWidth: 139, sortable: true, align: 'center',
			renderer: Grid.buttonRenderer({text: '가입승인', class: 'btn_type2'}, function () {
					var row = (chargeGrid.getFocusedCell()['rowKey'] || 0);
					var record = chargeGrid.getRow(row);
					if (confirm("해당 거래처 담당자의 가입을 승인 하시겠습니까?")) {
						Common.showLoading();
						setUserComfirm(record.userSeq, record);
					}
				}
			)
		}
	];

	var chargeGrid = Grid.createGrid({
		id: 'chargeGrid',
		columns: chargeColumns,
		data: []
	});

	// 수정 버튼 이벤트 처리
	$customerModifyBtn.on('click', function () {
		Common.showLoading();
		location.href = '/customer/customer/customerWrite?' + queryString;
	});

	// 목록 버튼 이벤트 처리
	$customerListBtn.on('click', function () {
		Common.showLoading();
		location.href = '/customer/customer/customerList?' + queryString;
	});

	//이메일 select 박스 변경 이벤트 처리
	$("select[name='emailDomain']").on('change', function () {
		var selectValue = $(this).val();
		var $emailDomainInput = $('input[name="emailDomain"]');
		$emailDomainInput.val(select);

		if (Valid.isEmpty(selectValue)) {
			$emailDomainInput.attr('disabled', false);
		} else {
			$emailDomainInput.attr('disabled', true);
		}
	});

	getChargeList();

	// 코드 그룹 데이터 로드
	function getChargeList() {
		var parameter = {customerSeq: customerSeq};
		Common.callAjax("/customer/customer/api/getChargeList", parameter, function (result) {
			chargeGrid.resetData(result);
			Common.hideLoading();
		});
	}

	function setUserComfirm(userSeq) {
		var parameter = {userSeq: userSeq};
		Common.callAjax("/customer/customer/api/setChargeConfirm", parameter, function (result) {
			alert("가입 승인이 완료되었습니다.");
			getChargeList();
		});
	}
});