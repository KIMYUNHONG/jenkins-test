/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();
	var $customerSaveBtn = $('#customerSaveBtn');			// 수정 버튼
	var $customerListBtn = $('#customerListBtn');			// 목록 버튼
	var customerSeq = $('input[name="customerSeq"]').val();	// 거래처_SEQ(KEY)
	var $file = $('#file');
	var $fileList = $('#fileList');
	var $selectEmailDomain = $('select[name="selectEmailDomain"]');

	var $contractStartDt = $('#contractStartDt');
	var $contractEndDt = $('#contractEndDt');

	var queryString = $('input[name="queryString"]').val();

	var chargeColumns = [
		{header: '성명', name: 'name', minWidth: 200, sortable: true, align: 'center'},
		{header: '아이디', name: 'userId', minWidth: 200, sortable: true, align: 'center'},
		{header: '직책', name: 'rankText', minWidth: 150, sortable: true, align: 'center'},
		{header: '휴대폰번호', name: 'phone', minWidth: 240, sortable: true, align: 'center'},
		{header: '전화번호', name: 'telephone', minWidth: 240, sortable: true, align: 'center'},
		{header: '이메일', name: 'emailText', minWidth: 280, sortable: true, align: 'left'},
		{header: '대표담당', name: 'representYnText', minWidth: 100, sortable: true, align: 'center'},
		{header: '상태', name: 'useYnText2', minWidth: 100, sortable: true, align: 'center'},
		{
			header: '수정', name: 'modifyBtn', minWidth: 139, sortable: true, align: 'center',
			renderer: Grid.buttonRenderer({text: '수정', class: 'btn_type3'}, function () {
				var row = (chargeGrid.getFocusedCell()['rowKey'] || 0);
				var record = chargeGrid.getRow(row);
				openWriteCustomerChargePopup(record);
			})
		}
	];

	var chargeGrid = Grid.createGrid({
		id: 'chargeGrid',
		columns: chargeColumns,
		data: []
	});

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($contractStartDt.val()) ? new Date(changeDateFormat($contractStartDt.val(), '/')) : new Date(),
			input: '#contractStartDt',
			container: '#contractStartDtWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($contractEndDt.val()) ? new Date(changeDateFormat($contractEndDt.val(), '/')) : new Date(),
			input: '#contractEndDt',
			container: '#contractEndDtWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	//화면load시 이메일 readonly 처리
	checkEmailDomain($selectEmailDomain);

	$selectEmailDomain.on('change', function () {
		checkEmailDomain($selectEmailDomain);
	});

	existFile();

	// 저장 버튼 이벤트 처리
	$customerSaveBtn.on('click', function () {
		if (confirm('저장 하시겠습니까?')) {
			chargeGrid.blur();

			var parameter = $('#customerForm').serializeObject();
			parameter.chargeList = chargeGrid.getData();

			// 아이디가 null 이 아니라 ''로 저장될때가 있어서 처리
			$.each(parameter.chargeList, function (index, value) {
				Valid.isEmpty(value["userId"]) ? delete value["userId"] : function () {
				};
			});

			Common.showLoading();
			Common.callAjax("/customer/customer/api/setCustomer", parameter, function (result) {
				if (result.customerSeq == null || result.customerSeq <= 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					Common.hideLoading();
					return false;
				}
				alert("저장이 완료 되었습니다.");

				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('customerSeq') != -1 ? queryString : 'customerSeq=' + (result.customerSeq);
				location.href = '/customer/customer/customerDetail?' + param;
			});
		}
	});

	// 파일 추가 버튼 이벤트 처리
	$file.on('change', function () {
		if ($($fileList.find('li')).length >= 5) {
			alert("파일은 최대 5개까지 등록 가능합니다.");
			return false;
		}
		Common.fileUpload($file, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$file.val(null);
			var $html = $('\t\t\t\t\t\t\t<li><a href="/uploadFile/api/download/' + result['uploadFileKey'] + '"' +
				' class="icon file" target="_blank" download="' + result['originalFileName'] + '">' + result['originalFileName'] + '</a>' +
				'<button type="button" class="btn_del"></button>' +
				'<input type="hidden" name="uploadFileKey" value="' + result['uploadFileKey'] + '"></li>\n');

			$fileList.append($html);
			existFile();
		});
	});
	// 파일 삭제 이벤트 처리
	$fileList.on('click', '.btn_del', function () {
		$(this).parent('li').remove();
		existFile();
	});

	// 목록 버튼 이벤트 처리
	$customerListBtn.on('click', function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();
			location.href = (customerSeq > 0) ? '/customer/customer/customerDetail?' + queryString : '/customer/customer/customerList';
		}
	});

	//이메일 select 박스 변경 이벤트 처리
	$("select[name='selectEmailDomain']").on('change', function () {
		var $this = $(this).val();
		var $emailDomain = $('input[name="emailDomain"]');

		$emailDomain.val($this);
		$emailDomain.prop('readonly', Valid.isNotEmpty($this));
	});

	getChargeList();

	// 코드 그룹 데이터 로드
	function getChargeList() {
		if (customerSeq == null || customerSeq <= 0) {
			Common.hideLoading();
			return false;
		}

		var parameter = {customerSeq: customerSeq};
		Common.callAjax("/customer/customer/api/getChargeList", parameter, function (result) {
			chargeGrid.resetData(result);
			Common.hideLoading();
		});
	}

	// 우편번호 검색 팝업
	$('#openSearchAddressPopup').on('click', function () {
		Common.openPopup('/common/searchAddressPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("우편번호 찾기", result, {width: 500, bodyClass: 'nospace'});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);

			DaumAddress.createAddressPopup('daumAddress', {
				popup: $popup,
				address: 'address',
				addressDetail: 'addressDetail'
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	// 영업담당자 팝업 처리
	$('#openSalesUserListPopupBtn').on('click', function () {
		Common.openPopup('/common/salesUserListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("영업담당자", result, {width: 600});
			$('#popupList').append($popup);

			var salesUserColumns = [
				{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
				{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
				{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
				{header: '소속', name: 'customerName', width: 220, sortable: true, align: 'left'},
			];

			var salesUserGrid = Grid.createGrid({
				id: 'salesUserGrid',
				columns: salesUserColumns,
				data: [],
				bodyHeight: 300
			});

			getSalesUserList();

			function getSalesUserList() {
				var parameter = $popup.find('#searchForm').serializeObject();

				Common.callAjax("/standard/user/api/getSalesUserList", parameter, function (result) {
					salesUserGrid.resetData(result);
					$('#salesUserTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			salesUserGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = salesUserGrid.getRow(row);

				$('input[name="salesUserName"]').val(record.name);
				$('input[name="salesUserId"]').val(record.userId);
				$('input[name="salesCustomerSeq"]').val(record.customerSeq);
				$popup.remove();
			});

			$popup.find('#searchSalesUserBtn').on('click', function () {
				getSalesUserList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	// 담당자 추가/수정 팝업 처리
	$('#openWriteChargePopupBtn').on('click', function () {
		openWriteCustomerChargePopup({})
	});

	// 담당자 추가/수정 팝업 로직
	function openWriteCustomerChargePopup(record) {

		Common.openPopup('/customer/customer/customerChargeWritePopup', {parameter: Common.excludeNullValue(record)}, function (result) {
			var $popup = Common.getPopupTemplate("거래처담당자", result, {width: 635});
			$('#popupList').append($popup);

			var $isChecked = $popup.find('input[name="isChecked"]');
			var $userId = $popup.find('input[name="userId"]');
			var $selectEmailDomainPopup = $popup.find('select[name="selectEmailDomainPopup"]');
			var $chargeForm = $popup.find('#chargeForm');

			var $emailId = $popup.find('input[name="emailId"]');
			var $emailDomain = $popup.find('input[name="emailDomain"]');

			// 아이디 변경시 중복체크 플래그 해제
			$userId.on('change', function () {
				$isChecked.val(false);
			});

			// 중복체크 버튼 이벤트
			$popup.find('#duplicateCheckBtn').on('click', function () {

				var userId = $popup.find('input[name="userId"]').val();
				for (var i = 0, length = chargeGrid.getRowCount(); i < length; i++) {
					console.log(chargeGrid.getRow(i));
					if (Valid.isNotEmpty(userId) && userId == chargeGrid.getRow(i).userId) {
						alert(userId + '는 이미 사용중인 아이디입니다.');
						return false;
					}
				}

				Common.callAjax("/standard/user/api/getUserChk", {userId: $userId.val()}, function (result) {
					alert(result.msg);
					$isChecked.val(result.result);
				}, {async: false});
				Common.resizeHeightPopup($popup);
			});

			checkEmailDomain($selectEmailDomainPopup);

			//이메일 select 박스 변경 이벤트 처리(POPUP)
			$popup.find("select[name='selectEmailDomainPopup']").on('change', function () {
				var $this = $(this).val();
				$emailDomain.val($this);
				$emailDomain.prop('readonly', Valid.isNotEmpty($this));
			});

			// 저장 버튼 이벤트
			$popup.find('#saveBtn').on('click', function () {

				if ($isChecked.val() != "true" && Valid.isNotEmpty($popup.find('input[name="userId"]').val())) {
					alert("아이디 중복체크를 해주세요.");
					return false;
				}

				var charge = $chargeForm.serializeObject();

				if (Valid.isNotEmpty(charge.emailId) || Valid.isNotEmpty(charge.emailDomain)) {
					if (!Valid.isLowerCaseAndNumber(charge.emailId, "담당자 이메일")) return false;
					if (!Valid.isLowerCaseAndNumber(charge.emailDomain.replace(/\./gi, ""), "담당자 이메일 도메인")) return false;
				}

				var phone = Common.getPhoneNumber('phone', $popup);
				var telephone = Common.getTelephoneNumber('telephone', $popup);

				if (Valid.isEmpty(charge.name)) {
					alert("성명을 입력해주세요.");
					return false;
				}
				if (Valid.isNotEmpty(phone)) {
					if (!Valid.isPhoneFormat(phone, "담당자 휴대폰번호")) return false;
				}
				if (Valid.isNotEmpty(telephone)) {
					if (!Valid.isPhoneFormat(telephone, "담당자 휴대폰번호")) return false;
				}

				if (confirm('저장 하시겠습니까?')) {
					$.each(charge, function (key, value) {
						record[key] = value;
					});

					// 그리드에 보여주기 위한 데이터 가공 처리
					record["useYnText2"] = record["useYn"] === 'Y' ? '활동' : '미활동';						// 활동여부
					record["representYnText"] = record["representYn"] === 'Y' ? 'O' : 'X';		// 대표담당
					record["rankText"] = Valid.isNotEmpty(record["rank"]) ? $popup.find('select[name="rank"]').find('option:selected').text() : '';	// 직책
					record["phone"] = Common.getPhoneNumber('phone', $popup);							// 휴대폰번호
					record["telephone"] = Common.getTelephoneNumber('telephone', $popup);					// 전화번호
					record["emailText"] = (Valid.isNotEmpty($emailId.val()) && Valid.isNotEmpty($emailDomain.val())) ?
						$emailId.val() + "@" + $emailDomain.val() : "";		// 이메일 도메인
					if (Valid.isEmpty(record._attributes)) {	// 신규
						if (record["rankText"] == "선택") {
							record["rankText"] = ""
						}
						chargeGrid.appendRow(record);
					} else {	// 수정
						chargeGrid.setRow(record._attributes.rowNum - 1, record);
					}
					$popup.remove();
				}
			});

			// 닫기, 취소 버튼 이벤트
			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	function checkEmailDomain($select) {
		$select.siblings('input[name="emailDomain"]').prop('readonly', !(Valid.isEmpty($select.val())));
	}

	// 파일 첨부 밑에 파일있으면 라인 긋고 없으면 제거
	function existFile() {
		if ($fileList.find('li').length == 0) {
			$fileList.removeClass('filename');
		} else {
			if (!$fileList.hasClass('filename')) {
				$fileList.addClass('filename');
			}
		}
	}
});