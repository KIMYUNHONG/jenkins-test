/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');				// 검색 버튼
	var $addLocationBtn = $('#addLocationBtn');		// 설치장소 등록
	var $resetBtn = $('#resetBtn');					// 검색조건 초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $searchForm = $('#searchForm');
	var $contractStatusList = $('input[name=contractStatusList]');
	var $total_chk = $("#total_chk");

	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var locationColumns = [
		{header: '지역', name: 'locationAreaText', width: 70, sortable: true, align: 'center'},
		{header: '거래처구분', name: 'customerTypeText', width: 80, sortable: true, align: 'center'},
		{header: '거래처', name: 'customerName', width: 100, sortable: true, align: 'left'},
		{header: '설치장소', name: 'locationName', width: 160, sortable: true, align: 'left'},
		{
			header: '주소',
			name: 'addressText',
			minWidth: 400,
			sortable: true,
			align: 'left',
			renderer: Grid.addressRenderer({class: 'test'}, function () {
				var row = (locationGrid.getFocusedCell()['rowKey'] || 0);
				var record = locationGrid.getRow(row);
				var parameter = {
					locationSeq: record.locationSeq
				};
				openLocationMapPopup(parameter);
			})
		},
		{header: '장비수', name: 'deviceCnt', width: 65, sortable: true, align: 'right'},
		{header: '영업담당자', name: 'salesUserName', width: 90, sortable: true, align: 'center'},
		{header: '보수담당자', name: 'asUserName', width: 90, sortable: true, align: 'center'},
		{header: '계약상태', name: 'contractStatusText', width: 80, sortable: true, align: 'center'},
		{header: '계약기간', name: 'contractPeriodText', width: 160, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', width: 70, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', width: 80, sortable: true, align: 'center'},
		{
			header: '상세',
			name: 'modifyBtn',
			width: 90,
			sortable: true,
			align: 'center',
			renderer: Grid.buttonRenderer({text: '상세', class: 'location-modify-btn btn_type3'}, function () {
				var row = (locationGrid.getFocusedCell()['rowKey'] || 0);
				var record = locationGrid.getRow(row);
				var parameter = {
					customerSeq: record.customerSeq,
					locationSeq: record.locationSeq
				};
				openWriteLocationPopup(parameter);
			}), noExcel: true
		}
	];

	var deviceColumns = [
		{header: 'No', name: 'no', width: 80, sortable: true, align: 'center'},
		{header: '장비번호', name: 'deviceId', width: 170, sortable: true, align: 'center'},
		{header: '상태', name: 'statusText', width: 150, sortable: true, align: 'center'},
		{header: '장비명', name: 'deviceNameText', width: 350, sortable: true, align: 'left'},
		{header: '관리번호', name: 'manageNo', width: 170, sortable: true, align: 'center'},
		{header: '가로등번호', name: 'lampNo', width: 170, sortable: true, align: 'center'},
		{header: '보수담당자', name: 'asUserName', width: 150, sortable: true, align: 'center'},
		{header: '설치일', name: 'installDt', width: 150, sortable: true, align: 'center'},
		{header: '최근 유지보수일', name: 'lastMaintainDateText', width: 150, sortable: true, align: 'center'},
		{
			header: '상세',
			name: 'detailBtn',
			width: 100,
			sortable: true,
			align: 'center',
			renderer: Grid.buttonRenderer2({text: '상세', class: 'device-detail-btn btn_type3'}, function () {
				var row = (deviceGrid.getFocusedCell()['rowKey'] || 0);
				var record = deviceGrid.getRow(row);
				location.href = '/install/installDevice/installDeviceDetail?deviceSeq=' + record.deviceSeq;
			})
		}
	];

	var locationGrid = Grid.createGrid({
		id: 'locationGrid',
		columns: locationColumns,
		data: []
	});

	var deviceGrid = Grid.createGrid({
		id: 'deviceGrid',
		columns: deviceColumns,
		data: [],
		bodyHeight: 300
	});

	// 설치장소 그리드 select cell시 이벤트
	locationGrid.on('focusChange', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if (row == null) {
			return false;
		}

		var record = locationGrid.getRow(row);
		// 행변경 이벤트 감지
		if (row != prevRow) {
			getDeviceList(record.locationSeq);
		}
	});

	getLocationList();

	// 설치장소 리스트 조회
	function getLocationList() {
		Common.showLoading();
		var parameter = $searchForm.serializeObject();

		// 기간 종류를 선택안하고 날짜를 선택하거나, 기간종류만 선택하고 날짜를 선택안할시
		if ((Valid.isNotEmpty(parameter.dateType) && (Valid.isEmpty(parameter.searchStartDate) || Valid.isEmpty(parameter.searchEndDate)))
			|| (Valid.isEmpty(parameter.dateType) && (Valid.isNotEmpty(parameter.searchStartDate) || Valid.isNotEmpty(parameter.searchEndDate)))) {
			alert("기간을 선택해주세요.");
			Common.hideLoading();
			return false;
		}

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "locationList" : "?" + $searchForm.serializeListParameter());
		Common.callAjax("/customer/location/api/getLocationList", parameter, function (result) {
			locationGrid.resetData(result);
			if (result != null && result.length > 0) {
				locationGrid.focusAt(0, 0, false);
			} else {
				deviceGrid.resetData([]);
				$('#deviceTotalCount').text(0);
			}
			$('#locationTotalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// 설치장비 리스트 조회
	function getDeviceList(locationSeq) {
		Common.callAjax("/device/device/api/getInstallDeviceList", {locationSeq: locationSeq}, function (result) {
			if (result != null) {
				$.each(result, function (index, value) {
					value.no = index + 1;
				});
			}
			deviceGrid.resetData(result);
			$('#deviceTotalCount').text(result == null ? 0 : Common.addComma(result.length));
		});
	}

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getLocationList(false);
	});

	// 등록 버튼 이벤트 처리
	$addLocationBtn.on('click', function () {
		openWriteLocationPopup({});
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = locationColumns;

		var option = {
			fileName: '설치장소리스트'
		};
		Common.excelDownload("/customer/location/excel/getLocationList", parameter, option);
	});

	function openWriteLocationPopup(parameter) {

		// 설치장소 등록/수정 팝업 처리
		Common.openPopup('/customer/location/locationWritePopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("설치장소", result, {width: 640});
			$('#popupList').append($popup);

			var $contractStartDt = $popup.find('#contractStartDt');
			var $contractEndDt = $popup.find('#contractEndDt');

			var $openAsUserListPopupBtn = $popup.find('#openAsUserListPopupBtn');
			var $openCustomerListPopupBtn = $popup.find('#openCustomerListPopupBtn');
			var picker = Component.setRangePicker({
				startpicker: {
					date: Valid.isNotEmpty($contractStartDt.val()) ? new Date(changeDateFormat($contractStartDt.val(), '/')) : new Date(),
					input: '#contractStartDt',
					container: '#contractStartDtWrapper'
				},
				endpicker: {
					date: Valid.isNotEmpty($contractEndDt.val()) ? new Date(changeDateFormat($contractEndDt.val(), '/')) : new Date(),
					input: '#contractEndDt',
					container: '#contractEndDtWrapper'
				},
				format: "yyyy.MM.dd",
				language: 'ko'
			});

			$popup.find('#openSearchAddressPopup').on('click', function () {
				Common.openPopup('/common/searchAddressPopup', {}, function (result) {
					var $detailPopup = Common.getPopupTemplate("우편번호 찾기", result, {width: 500, bodyClass: 'nospace'});

					$detailPopup.find('.popup-close-btn').on('click', function () {
						$detailPopup.remove();
					});

					$('#popupList').append($detailPopup);

					DaumAddress.createAddressPopup('daumAddress', {
						popup: $detailPopup,
						address: 'address',
						addressDetail: 'addressDetail',
						sido: 'sido',
						sigungu: 'sigungu',
						latitude: 'locationLatitude',
						longitude: 'locationLongitude',
					}, function () {

						var parameter = {
							areaText: $popup.find('input[name="locationAreaText"]').val(),
							areaDetailText: $popup.find('input[name="locationAreaDetailText"]').val()
						};

						// 선택한 주소정보를 통해서 담당자 조회
						Common.callAjax("/standard/areaCharge/api/getAreaChargeByAreaName", parameter, function (result) {
							// 지역 담당자가 존재하는 경우 set
							if (result != null) {
								$popup.find('input[name="asUserName"]').val(result.asUserName1);
								$popup.find('input[name="asUserId"]').val(result.asUserId1);
								$popup.find('input[name="asCustomerSeq"]').val(result.asCustomerSeq1);
							}
						});
					});

					Common.resizeHeightPopup($detailPopup);
				});

				Common.resizeHeightPopup($popup);
			});

			// AS 담당자 버튼 처리
			$openAsUserListPopupBtn.on('click', function () {
				openAsUserListPopup();
			});
			$openCustomerListPopupBtn.on('click', function () {
				openCustomerListPopup();
			});

			// AS담당자 리스트 선택 팝업
			function openAsUserListPopup(parameter) {
				Common.openPopup('/common/asUserListPopup', {}, function (result) {
					var $detailPopup = Common.getPopupTemplate("보수담당자", result, {width: 600});
					$('#popupList').append($detailPopup);

					var asUserColumns = [
						{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
						{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
						{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
						{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
					];

					var asUserGrid = Grid.createGrid({
						id: 'asUserGrid',
						columns: asUserColumns,
						data: [],
						bodyHeight: 300
					});

					getAsUserList();

					function getAsUserList() {
						var parameter = {
							name: $detailPopup.find('input[name="name"]').val() || '',
							customerName: $detailPopup.find('input[name="customerName"]').val() || ''
						};
						Common.callAjax("/standard/user/api/getAsUserList", parameter, function (result) {
							asUserGrid.resetData(result);
							$detailPopup.find('#asUserTotalCount').text(result != null ? Common.addComma(result.length) : 0);
						});
					}

					// 로우 선택시 이벤트 처리
					asUserGrid.on('click', function (result) {
						var row = result['rowKey'];
						var prevRow = result['prevRowKey'];
						if (row == null) {
							return false;
						}
						var record = asUserGrid.getRow(row);

						$popup.find('input[name="asUserName"]').val(record.name);
						$popup.find('input[name="asUserId"]').val(record.userId);
						$popup.find('input[name="asCustomerSeq"]').val(record.customerSeq);
						$detailPopup.remove();
					});

					$detailPopup.find('#searchAsUserBtn').on('click', function () {
						getAsUserList();
					});

					$detailPopup.find('.popup-close-btn').on('click', function () {
						$detailPopup.remove();
					});

					Common.resizeHeightPopup($detailPopup);
				}, {})
			}

			// 거래처 리스트 선택 팝업
			function openCustomerListPopup(parameter) {
				Common.openPopup('/common/customerListPopup', {}, function (result) {
					var $detailPopup = Common.getPopupTemplate("거래처", result, {width: 600});
					$('#popupList').append($detailPopup);

					var customerColumns = [
						{header: '거래처구분', name: 'customerTypeText', width: 100, sortable: true, align: 'center'},
						{header: '거래처', name: 'customerShortName', width: 100, sortable: true, align: 'left'},
						{header: '거래처담당자', name: 'chargeName', width: 120, sortable: true, align: 'center'},
						{header: '영업담당자', name: 'salesUserName', width: 222, sortable: true, align: 'center'},
					];

					var customerGrid = Grid.createGrid({
						id: 'customerGrid',
						columns: customerColumns,
						data: [],
						bodyHeight: 300
					});

					getCustomerList();

					function getCustomerList() {
						var parameter = {
							name: $detailPopup.find('input[name="name"]').val() || '',
							customerName: $detailPopup.find('input[name="customerName"]').val() || '',
							customerType: $detailPopup.find('select[name="customerType"]').val() || '',
							viewAllCustomerFlag: 'Y',
							isAgencyMenu: 'N',
							excludeCustomerSeq: 1
						};
						Common.callAjax("/customer/customer/api/getCustomerList", parameter, function (result) {
							customerGrid.resetData(result);
							$detailPopup.find('#customerTotalCount').text(result != null ? Common.addComma(result.length) : 0);
						});
					}

					// 로우 선택시 이벤트 처리
					customerGrid.on('click', function (result) {
						var row = result['rowKey'];
						var prevRow = result['prevRowKey'];
						if (row == null) {
							return false;
						}
						var record = customerGrid.getRow(row);

						$popup.find('input[name="customerName"]').val(record.customerShortName);
						$popup.find('input[name="customerSeq"]').val(record.customerSeq);
						$popup.find('#salesUserName').text(record.salesUserName);
						$detailPopup.remove();
					});

					$detailPopup.find('#searchCustomerBtn').on('click', function () {
						getCustomerList();
					});

					$detailPopup.find('.popup-close-btn').on('click', function () {
						$detailPopup.remove();
					});

					Common.resizeHeightPopup($detailPopup);
				}, {})
			}

			// 영업담당자 팝업 처리
			$popup.find('#openSalesUserListPopupBtn').on('click', function () {
				Common.openPopup('/common/salesUserListPopup', {}, function (result) {
					var $detailPopup = Common.getPopupTemplate("영업담당자", result, {width: 600});
					$('#popupList').append($detailPopup);

					var salesUserColumns = [
						{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
						{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
						{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
						{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
					];

					var salesUserGrid = Grid.createGrid({
						id: 'salesUserGrid',
						columns: salesUserColumns,
						data: [],
						bodyHeight: 300
					});

					getSalesuserList();

					function getSalesuserList() {
						var parameter = {
							name: $detailPopup.find('input[name="name"]').val() || '',
							customerName: $detailPopup.find('input[name="customerName"]').val() || ''
						};
						Common.callAjax("/standard/user/api/getSalesUserList", parameter, function (result) {
							salesUserGrid.resetData(result);
						});
					}

					// 로우 선택시 이벤트 처리
					salesUserGrid.on('click', function (result) {
						var row = result['rowKey'];
						var prevRow = result['prevRowKey'];
						if (row == null) {
							return false;
						}
						var record = salesUserGrid.getRow(row);

						$popup.find('input[name="salesUserName"]').val(record.name);
						$popup.find('input[name="salesUserId"]').val(record.userId);
						$popup.find('input[name="salesCustomerSeq"]').val(record.customerSeq);
						$detailPopup.remove();
					});

					$detailPopup.find('#searchSalesUserBtn').on('click', function () {
						getSalesuserList();
					});

					$detailPopup.find('.popup-close-btn').on('click', function () {
						$detailPopup.remove();
					});
					Common.resizeHeightPopup($detailPopup);
				}, {})
			});

			$popup.find('#saveBtn').on('click', function () {
				if (confirm('저장 하시겠습니까?')) {
					Common.showLoading();
					Common.callAjax("/customer/location/api/setLocation", $popup.find('#locationForm').serializeObject(), function (result) {
						alert("저장이 완료 되었습니다.");
						getLocationList();
						$popup.remove();
					});
				}
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup)
		});
	}

	function openLocationMapPopup(parameter) {
		Common.openPopup('/customer/location/locationMapPopup', {parameter: parameter}, function (result) {
			var $detailPopup = Common.getPopupTemplate("설치장소", result, {width: 600});
			$('#popupList').append($detailPopup);

			$detailPopup.find('.popup-close-btn').on('click', function () {
				$detailPopup.remove();
			});
			Common.resizeHeightPopup($detailPopup);
		}, {})
	}

	//체크박스 전체 클릭시 이벤트 처리
	$total_chk.on('click', function () {
		$contractStatusList.prop("checked", $total_chk.is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$contractStatusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $contractStatusList.length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=contractStatusList]:checked").length
		$total_chk.prop("checked", chkCnt == totalCnt);
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});
});