/**
 *  Create By Lee DoWon on 2020-04-07 / 오후 2:30
 *  Email : jwh.19@uniwiz.co.kr
 **/

$(function () {

	Common.showLoading();
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $resetBtn = $('#resetBtn');       					// 검색조건 초기화 버튼
	var $systemPowerOnBtn = $('#systemPowerOnBtn');			// 전원 on 버튼
	var $systemPowerOffBtn = $('#systemPowerOffBtn');		// 전원 off 버튼
	var $infomationModifyBtn = $('#infomationModifyBtn');	// 기준 정보 설정 버튼	
	var $excelDownloadBtn = $("#excelDownloadBtn"); 		// 엑셀다운로드 버튼
	var $useNomalFlag = $('#useNomalFlag');					// 일반 장비 제외 checkbox
	var $infoSateBtn = $(".info_sate"); 			// 상태별 박스 아이콘
	var $searchForm = $('#searchForm');
	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');

	var previousMonthDay = new Date().addDate(0, -1, 0);
	var nextMonthDay = new Date().addDate(0, 1, 0);

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	$('#dateBtn1').on('click', function () {
		picker.setStartDate(new Date());
		picker.setEndDate(new Date());
	});
	$('#dateBtn2').on('click', function () {
		picker.setStartDate(new Date().addDate(0, 0, -7));
		picker.setEndDate(new Date());
	});
	$('#dateBtn3').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -1, 0));
		picker.setEndDate(new Date());
	});
	$('#dateBtn4').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -3, 0));
		picker.setEndDate(new Date());
	});

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		//getMonitorStatusData();
		getMonitorList();
	});

	// 검색조건 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	//체크박스 전체 클릭시 이벤트 처리
	$('input[name="total_chk"]').on('click', function () {
		$(this).closest("div").find('input[type="checkbox"]').prop('checked', $(this).is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$('.searchbox span input[type="checkbox"]').on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $(this).closest('div').find('span input[type="checkbox"]').length;
		// 체크박스 선택된 갯수
		var chkCnt = $(this).closest('div').find('span input[type="checkbox"]:checked').length;

		if (chkCnt == totalCnt) {
			$(this).closest('div').find('input[name="total_chk"]').prop("checked", true);
		} else {
			$(this).closest('div').find('input[name="total_chk"]').prop("checked", false);
		}
	});

	var monitorColumns = [
		{header: '지역', name: 'locationAreaText', width: 50, sortable: true, align: 'center'}
		, {header: '설치장소', name: 'locationName', width: 130, sortable: true, align: 'left'}
		, {header: '거래처 구분', name: 'customerTypeText', width: 85, sortable: true, align: 'left'}
		, {header: '거래처', name: 'customerName', width: 100, sortable: true, align: 'left'}
		, {header: '장비번호', name: 'deviceId', width: 140, sortable: true, align: 'center'}
		, {header: '전원', name: 'power', width: 50, sortable: true, align: 'center'}
		, {header: '상태', name: 'state', width: 50, sortable: true, align: 'center', formatter: ''}
		, {header: '통신', name: 'connection', width: 50, sortable: true, align: 'center', formatter: ''}
		, {header: '장비명', name: 'deviceNameText', width: 140, sortable: true, align: 'left'}
		, {header: '통신방식', name: 'protocolText', width: 70, sortable: true, align: 'center'}
		, {header: '총 운영일자', name: 'totalUseDate', width: 80, sortable: true, align: 'right'}
		, {header: '상부모터', name: 'moterTopDate', width: 70, sortable: true, align: 'right'}
		, {header: '하부모터', name: 'moterBottomDate', width: 70, sortable: true, align: 'right'}
		, {header: '램프', name: 'lampDate', width: 70, sortable: true, align: 'right'}
		, {header: '모뎀', name: 'modemDate', width: 70, sortable: true, align: 'right'}
		, {header: '가동기간', name: 'operateDateText', width: 160, sortable: true, align: 'center'}
		, {header: '가동시간', name: 'operateTimeText', width: 90, sortable: true, align: 'center'}
		, {header: '마지막 데이터 수신', name: 'receiveDate', width: 130, sortable: true, align: 'center'}
	];

	var monitorGrid = Grid.createGrid({
		id: 'monitorGrid'
		, rowHeaders: [
			{type: 'checkbox'}
		]
		, columns: monitorColumns
		, data: []
		, frozenCount: 8 		// 8개의 컬럼을 고정하고
		, frozenBorderWidth: 1 	// 고정 컬럼의 경계선 너비를 1px로 한다.
	});

	getMonitorStatusData();
	getMonitorList();

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = monitorColumns;

		// 상태 KEY에서 TEST로 변경
		parameter.gridColumnList[6].name = "stateText";

		var option = {
			fileName: '모니터'
		};
		Common.excelDownload("/monitor/monitor/excel/getMonitorList", parameter, option);
	});

	// 그리드 로우 선택시 이벤트 처리
	monitorGrid.on('click', function (result) {

		var columnName = result.columnName;

		//체크박스 클릭일 경우 이벤트 종료
		if (columnName == "_checked") {
			return;
		}

		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if (row == null) {
			return false;
		}
		Common.showLoading();
		var record = monitorGrid.getRow(row);

		location.href = '/monitor/monitor/monitorDetail?deviceSeq=' + record["deviceSeq"] + '&' + $searchForm.serializeListParameter();
	});

	function getMonitorList() {
		if ($('#searchStartDate').val() == '' && $('#searchEndDate').val() == '') {
			//picker.setStartDate(new Date().setDate(1));
			//picker.setEndDate(new Date().addDate(0,1,0).setDate(0));
		}
		Common.showLoading();

		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "monitorList" : "?" + $searchForm.serializeListParameter());

		// 기간 검색
		if ((Valid.isEmpty(parameter.searchStartDate) && Valid.isNotEmpty(parameter.searchEndDate))
			|| (Valid.isNotEmpty(parameter.searchStartDate) && Valid.isEmpty(parameter.searchEndDate))) {
			alert("기간을 선택해주세요.");
			Common.hideLoading();
			return false;
		}

		Common.callAjax("/monitor/monitor/api/getMonitorList", parameter, function (result) {

			// 체크 박스 체크
			var uncheckedAttr = {
				checkDisabled: true
			};

			$.each(result, function (index, value) {
				value._attributes = (value.modemId == null || value.modemId == '') ? uncheckedAttr : {};

				value.totalUseDate = Common.addComma(value.totalUseDate) + "일";
				value.moterTopDate = Common.addComma(value.moterTopDate) + "일";
				value.moterBottomDate = Common.addComma(value.moterBottomDate) + "일";
				value.lampDate = Common.addComma(value.lampDate) + "일";
				value.modemDate = Common.addComma(value.modemDate) + "일";
			});

			$("span[name='totalCount']").text(result.length);
			monitorGrid.resetData(result);

			for (var i = 0; i < result.length; i++) {
				// 전원
				switch (monitorGrid.getRow(i).power) {
					case 'ON':
						monitorGrid.addCellClassName(i, 'power', 'ic_power01');
						break;
					case 'OFF':
						monitorGrid.addCellClassName(i, 'power', 'ic_power02');
						break;
				}

				// 상태
				switch (monitorGrid.getRow(i).state) {
					case 'MT020001':
						monitorGrid.addCellClassName(i, 'state', 'ic_state01');
						break;
					case 'MT020002':
						monitorGrid.addCellClassName(i, 'state', 'ic_state02');
						break;
					case 'MT020003':
						monitorGrid.addCellClassName(i, 'state', 'ic_state03');
						break;
				}

				// 통신
				switch (monitorGrid.getRow(i).connection) {
					case '1':
						monitorGrid.addCellClassName(i, 'connection', 'ic_connection01');
						break;
					case '0':
						if (monitorGrid.getRow(i).modemId != null && monitorGrid.getRow(i).modemId != '') {
							monitorGrid.addCellClassName(i, 'connection', 'ic_connection02');
						} else {
							monitorGrid.addCellClassName(i, 'connection', 'ic_connection03');
						}

						break;
				}
			}
			Common.hideLoading();
		});
	}

	// 장비관리 데이터 로드(상태별)
	function getMonitorStatusData() {
		if ($('#searchStartDate').val() == '' && $('#searchEndDate').val() == '') {
			//picker.setStartDate(new Date().setDate(1));
			//picker.setEndDate(new Date().addDate(0,1,0).setDate(0));
		}
		Common.callAjax("/monitor/monitor/api/getMonitorStatusData", $searchForm.serializeObject(), function (result) {
			$('span[name="sumQty"]').text("0"); //초기화
			$.each(result, function (key, value) {
				$('#' + key.toUpperCase() + '_QTY').text(Common.addComma(value));
			})
		});
	}

	// 전원 ON 버튼 클릭
	$systemPowerOnBtn.on('click', function () {
		var checkedRowList = monitorGrid.getCheckedRows();
		if (checkedRowList.length === 0) {
			alert("장비를 선택해 주세요.");
			return false;
		}
		if (confirm("선택한 장비 전원을 ON하시겠습니까?")) {
			setSystemPower("1", checkedRowList);
		}
	});

	// 전원 OFF 버튼 클릭
	$systemPowerOffBtn.on('click', function () {
		var checkedRowList = monitorGrid.getCheckedRows();
		if (checkedRowList.length === 0) {
			alert("장비를 선택해 주세요.");
			return false;
		}
		if (confirm("선택한 장비 전원을 OFF하시겠습니까?")) {
			Common.showLoading();
			setSystemPower("0", checkedRowList);
		}
	});

	// 전원 컨트롤 처리 
	function setSystemPower(val, rows) {

		var check = (val == "0") ? "OFF" : "ON"
		var msg = "";

		if (check == "OFF") {
			msg = "가 진행 되지 않았습니다.";
		} else {
			msg = "이 진행되지 않았습니다.";
		}

		var parameter = {
			relay: val
			, devEUIList: []
			, seqList: []
			, protocolList: []
		};

		$.each(rows, function (index, value) {
			var deviceQueueItem = new Object();

			//parameter.devEUIList.push(value.devEUI);
			if (value.power != check) { // 전원 컨트롤 코드 세팅
				parameter.devEUIList.push(value.devEUI);
				parameter.seqList.push(value.deviceSeq);
				parameter.protocolList.push(value.protocolText);
			}
		});

		// 전원 컨트롤 장비 최종 검사
		if (parameter.devEUIList.length <= 0) {
			alert("신택한 리스트 중 전원 " + check + "할 장비가 없습니다.");
			Common.hideLoading();
			return false;
		}

		Common.callAjax("/monitor/monitor/api/setLoraPowerStat", parameter, function (result) {
			if (result > 0) {
				alert(result + "개의 장비 전원이 " + check + " 되었습니다.");
				getMonitorStatusData();
				getMonitorList();
			} else {
				alert("장비 전원 " + check + msg);
			}
			Common.hideLoading();
		});
	}

	// 기준정보 설정 버튼 클릭
	$infomationModifyBtn.on('click', function () {
		var checkedRowList = monitorGrid.getCheckedRows();
		if (checkedRowList.length === 0) {
			alert("장비를 선택해 주세요.");
			return false;
		}
		if (checkedRowList.length > 1) {
			alert("장비를 하나만 선택해주세요.");
			return false;
		}
		//기준정보 등록 팝업
		setProtocolPopup(checkedRowList[0]);
	});

	// 통신기준 정보관리 팝업 OPEN
	function setProtocolPopup(row) {

		var parameter = {
			deviceClassify: row.deviceClassify,
			deviceSeq: row.deviceSeq
		};

		Common.openPopup('/monitor/monitor/addProtocolPopup', {parameter: parameter}, function (result) {

			var $popup = Common.getPopupTemplate("통신상태 기준정보", result);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			$('#popupList').append($popup);

			//일자 디폴트 값으로 오늘날짜 설정되는게 아니라 db에서 가져와야 하면 아래주석 풀면됨
			var startDt = Component.setDatePicker({
				wrapper: 'operateStartDtWrapper',
				element: 'operateStartDt',
				date: row.deviceSeq == 0 ? new Date() : new Date(changeDateFormat(row.operateStartDt, '/'))
			});
			var endDt = Component.setDatePicker({
				wrapper: 'operateEndDtWrapper',
				element: 'operateEndDt',
				date: row.deviceSeq == 0 ? new Date() : new Date(changeDateFormat(row.operateEndDt, '/'))
			});

			$popup.find('#saveBtn').on('click', function () {
				if (confirm("저장하시겠습니까?")) {
					Common.callAjax("/monitor/monitor/api/setDeviceProtocol", $popup.find('#form').serializeObject(), function (result) {
						alert("저장이 완료 되었습니다.");
						$popup.remove();
						getMonitorStatusData();
						getMonitorList();
					});
				}
			});

			$popup.find('#cancelBtn').on('click', function () {
				if (confirm('취소 하시겠습니까?')) {
					$popup.remove();
				}
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 일반 장비 제외 checkbox 이벤트
	$useNomalFlag.on('change', function () {
		//getMonitorStatusData();
		getMonitorList();
	});

	// 상태별 검색 
	$infoSateBtn.on('click', function () {
		$("#searchForm")[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);

		if ($(this).data("type") != "07") {
			if ($(this).data("type") == "06") { // 일반장비 포함
				$("#" + $(this).data("value") + "").prop("checked", false);
				$("#nomalFlag").val("Y");
			} else {
				// 조건 체크 
				$("#nomalFlag").val("");
				$("#" + $(this).data("value") + "").closest("div").find("input[type='checkbox']").prop("checked", false);
				$("#" + $(this).data("value") + "").prop("checked", true);
			}

			// 데이터 바인딩
			//getMonitorStatusData();
			getMonitorList();

			$("#searchForm")[0].reset();

			picker.setStartDate(null);
			picker.setEndDate(null);
		}
	});
});