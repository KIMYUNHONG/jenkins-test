/**
 *  Create By Kim YunHong on 2020-04-22 / 오후 1:56
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	var $openDeviceStandardPopup = $("#openDeviceStandardPopup"); //장비 마스터정보 버튼
	var $infomationModifyBtn = $("#infomationModifyBtn"); //기준정보 변경 버튼
	var $reportTodayBtn = $("#reportTodayBtn"); // 리포트 당일 버튼
	var $reportWeekBtn = $("#reportWeekBtn");   // 리포트 1주일 버튼
	var $reportMonthBtn = $("#reportMonthBtn"); // 리포트 1개월 버튼

	var $power_deco = $("#power_deco"); // 전원 ON OFF 버튼
	var $top_power_deco = $("#top_power_deco"); // 상부 전원 ON OFF 버튼
	var $lamp_power_deco = $("#lamp_power_deco"); // LAMP 전원 ON OFF 버튼
	var $bottom_power_deco = $("#bottom_power_deco"); // 하부 전원 ON OFF 버튼

	var $reportSearchBtn = $("#reportSearchBtn"); // 리포트 조회 버튼
	var $listBtn = $('#listBtn');	  // 목록버튼

	var $moveInstallDeviceDetail = $("#moveInstallDeviceDetail"); // 설치장비 상세
	var $moveMaintainDetail = $("#moveMaintainDetail"); // 유지보수 상세
	var $openMaintainReportPopupBtn = $("#openMaintainReportPopupBtn"); // 유지보수 리포트
	var deviceSeq = $("#deviceSeq").val(); // 장비 시퀀스
	var queryString = $('input[name="queryString"]').val();

	// 설치장비 상세버튼 이벤트 처리
	$moveInstallDeviceDetail.on('click', function () {
		location.href = '/install/installDevice/installDeviceDetail?deviceSeq=' + deviceSeq;
	});

	// 유지보수 상세버튼 이벤트처리
	$moveMaintainDetail.on('click', function () {
		location.href = '/manage/maintain/maintainDetail?deviceSeq=' + deviceSeq;
	});	
	
	// 유지보수 리포트버튼 이벤트 처리
	$openMaintainReportPopupBtn.on('click', function () {
		var parameter = {
			deviceSeq: deviceSeq,
			searchType: "",
			searchYear: ""
		};

		Common.openPopup('/manage/maintain/maintainReportPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("유지보수 보고서", result, {width: 860});
			$('#popupList').append($popup);

			var $printPopup = $popup.find('#printPopup');

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			$printPopup.on('click', function () {
				$printPopup.hide();
				Common.showLoading();
				$('#printPage').printThis({
					afterPrint: function () {
						$printPopup.show();
						Common.hideLoading();
					}
				});
			});
			// 타이밍 이슈가 있어서 그냥 시간 줌
			setTimeout(function () {
				Common.resizeHeightPopup($popup);
			}, 10)
		});

	});

	// Report chart
	var chart1 = tui.chart; // 온도 차트
	var chart2 = tui.chart; // 습도 차트

	var $chart1 = $('#chart1')[0]; // 온도 차트
	var $chart2 = $('#chart2')[0]; // 습도 차트

	// 작동시간 차트
	var chartWrapper = document.getElementById('chart').getContext('2d');

	var operateStartTimeValue = $('input[name="operateStartTime"]').val() == null ? '0000' : $('input[name="operateStartTime"]').val();
	var operateEndTimeValue = $('input[name="operateEndTime"]').val() == null ? '0000' : $('input[name="operateEndTime"]').val();
	var startHour = operateStartTimeValue.substring(0, 2);
	var startMinute = operateStartTimeValue.substring(2, 4);
	var endHour = operateEndTimeValue.substring(0, 2);
	var endMinute = operateEndTimeValue.substring(2, 4);

	var startTime = ((Number(startHour))) + (startMinute == '00' ? 0 : 0.5) % 24;
	var endTime = ((Number(endHour))) + (endMinute == '00' ? 0 : 0.5) % 24;

	var gap = Math.abs(endTime - startTime);

	var data = [
		24 - parseFloat(gap % 24),
		parseFloat(gap % 24)
	];

	var chart = new Chart(chartWrapper, {
		type: 'pie',
		data: {
			labels: ['작동', '미작동'],
			datasets: [{
				backgroundColor: [
					"rgb(255, 186, 53)",
					"rgb(255,255 , 255)"
				],
				data: data
			}]
		},
		options: {
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			}
		}
	});
	$('#chart').css('transform', 'rotate(' + ((startHour) * 15 + (startMinute == '00' ? 0 : 7.5)) + 'deg)');
	var agent = navigator.userAgent.toLowerCase();

	// IE에서 차트 깨지는거 보정
	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1)) { // IE 일 경우
		$('.hour').find('canvas').css('position', 'absolute');
		$('.hour').find('canvas').css('left', '50%');
		$('.hour').find('canvas').css('top', '50%');
		$('.hour').find('canvas').css('width', '319px');
		$('.hour').find('canvas').css('height', '159px');
		$('#chart').css('margin-top', (Number($('#chart').css('height').replace('px', '')) / -2 + 'px'));
		$('#chart').css('margin-left', (Number($('#chart').css('width').replace('px', '')) / -2 + 'px'));
	}

	DaumAddress.createLocationStaticMap({
		id: 'detailMap',
		locationSeq: $('input[name="locationSeq"]').val(),
		deviceSeq: $('input[name="deviceSeq"]').val(),
	});

	//차트 조회버튼 클릭시 이벤트
	$reportSearchBtn.on('click', function () {
		getMonitorReportData();
	});

	// 모니터링 리포트 차트 DATA 조회
	// 모니터링 데이터가 있을때만 조회하도록 변경
	if (Valid.isNotEmpty(receiveDate)) {
		var picker = Component.setRangePicker({
			startpicker: {
				date: new Date(),
				input: '#searchStartDate',
				container: '#searchStartDateWrapper'
			},
			endpicker: {
				date: new Date(),
				input: '#searchEndDate',
				container: '#searchEndDateWrapper'
			},
			format: "yyyy.MM.dd",
			language: 'ko'
		});
		getMonitorReportData();
	}

	//장비규격 팝업
	$openDeviceStandardPopup.on("click", function () {
		var deviceMstSeq = $("#deviceMstSeq").val();
		var deviceSeq = $("#deviceSeq").val();
		var parameter = {deviceMstSeq: deviceMstSeq, deviceSeq: deviceSeq};

		Common.openPopup('/install/installDevice/deviceStandardPopup', {parameter: parameter}, function (result) {

			var $popup = Common.getPopupTemplate("장비정보", result, {width: 750});
			$('#popupList').append($popup);

			var deviceClassify = $popup.find("#deviceClassify").val();

			// 팝업 오픈시 유형에 에 해당하는 input만 보여지게 설정
			if (deviceClassify == "SD020002") {
				$popup.find('.' + 'SD020001').hide();
			} else {
				$popup.find('.' + 'SD020002').hide();
			}

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	//기준정보 변경 팝업
	$infomationModifyBtn.on("click", function () {
		//기준정보 등록 팝업 호출
		var deviceClassify = $("#deviceClassify").val();
		var deviceSeq = $("#deviceSeq").val()
		setProtocolPopup(deviceClassify, deviceSeq);
	});

	// 통신기준 정보관리 팝업 OPEN
	function setProtocolPopup(deviceClassify, deviceSeq) {

		var parameter = {
			deviceClassify: deviceClassify,
			deviceSeq: deviceSeq
		};

		Common.openPopup('/monitor/monitor/addProtocolPopup', {parameter: parameter}, function (result) {

			var $popup = Common.getPopupTemplate("통신상태 기준정보", result);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			$('#popupList').append($popup);

			var startDt = Component.setDatePicker({
				wrapper: 'operateStartDtWrapper',
				element: 'operateStartDt',
				date: deviceSeq == 0 ? new Date() : changeDateFormat(operateStartDt, '/')
			});
			var endDt = Component.setDatePicker({
				wrapper: 'operateEndDtWrapper',
				element: 'operateEndDt',
				date: deviceSeq == 0 ? new Date() : changeDateFormat(operateEndDt, '/')
			});

			$popup.find('#saveBtn').on('click', function () {
				if (confirm("저장하시겠습니까?")) {
					Common.callAjax("/monitor/monitor/api/setDeviceProtocol", $popup.find('#form').serializeObject(), function (result) {
						alert("저장이 완료 되었습니다.");
						$popup.remove();
						location.reload();
					});
				}
			});
			$popup.find('#cancelBtn').on('click', function () {
				if (confirm('취소 하시겠습니까?')) {
					$popup.remove();
				}
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// Report Data 조회
	function getMonitorReportData() {
		Common.callAjax("/monitor/monitor/api/getMonitorReportData", $("#reportForm").serializeObject(), function (result) {

			//차트 초기화
			$("#chart1").empty(); // 온도차트
			$("#chart2").empty(); // 습도차트

			//차트 data set
			//차트 data가 없어도 빈 차트 그려지도록 세팅
			var temperature = [0]; // 온도
			var humidity = [0];    // 습도
			var receiveDate = [0]; // 일자

			if (result.data.length > 1) {
				//실제 차트 data set 하기전 배열 초기화
				temperature = [];
				humidity = [];
				receiveDate = [];

				//차트 data set
				for (var i = 0; result.data.length > i; i++) {
					temperature.push(result.data[i].temperature);
					humidity.push(result.data[i].humidity);
					receiveDate.push(result.data[i].reportReceiveDateText);
				}
			}

			var chart1Theme = {series: {colors: ['#003973']}};// 온도 차트 컬러
			var chart2Theme = {series: {colors: ['#ff3600']}};// 습도 차트 컬러

			// 온도 차트
			var options1 = {
				chart: {
					width: 630,						// 차트 가로
					height: 320,					// 차트 높이
					title: '온도',	// 차트 설명
					format: '1,000'					// 차트 데이터 포맷
				},
				yAxis: {
					title: '',	// 세로축 설명
					min: 0,				// 세로축 최소값
					max: 50		// 세로축 최대값
				},
				xAxis: {
					title: ''	// 가로축 설명
				},
				series: {
					stack: 'normal',	// chart type
					barWidth: 40,		// chart bar 가로길이
					showLabel: false,	// chart bar 내에 숫자 표시 여부
				},
				tooltip: {
					align: 'left top',
					grouped: false		// 툴팁 묶어서 나올여부
				},
				legend: {
					showCheckbox: false,	// legend 체크박스 표시 여부
					align: 'bottom'			// series 설명 위치 (top, bottom)
				}
			};

			// 습도 차트
			var options2 = {
				chart: {
					width: 630,						// 차트 가로
					height: 320,					// 차트 높이
					title: '습도',	// 차트 설명
					format: '1,000'					// 차트 데이터 포맷
				},
				yAxis: {
					title: '',	// 세로축 설명
					min: 0,				// 세로축 최소값
					max: 100		// 세로축 최대값
				},
				xAxis: {
					title: ''	// 가로축 설명
				},
				series: {
					stack: 'normal',	// chart type
					barWidth: 40,		// chart bar 가로길이
					showLabel: false,	// chart bar 내에 숫자 표시 여부
				},
				tooltip: {
					align: 'left top',
					grouped: false		// 툴팁 묶어서 나올여부
				},
				legend: {
					showCheckbox: false,	// legend 체크박스 표시 여부
					align: 'bottom'			// series 설명 위치 (top, bottom)
				}
			};

			chart1.registerTheme('chart1Theme', chart1Theme);
			chart2.registerTheme('chart2Theme', chart2Theme);

			options1.theme = 'chart1Theme'; // 온도
			options2.theme = 'chart2Theme'; // 습도

			// 온도 차트 데이타
			var temperatureData = {
				categories: receiveDate,
				series: [
					{
						name: '온도',
						data: temperature
					}
				]
			};
			// 습도 차트 데이타
			var humidityData = {
				categories: receiveDate,
				series: [
					{
						name: '습도',
						data: humidity
					}
				]
			};
			chart1.lineChart($chart1, temperatureData, options1); // 온도차트 set
			chart2.lineChart($chart2, humidityData, options2);    // 습도차트 set
			
			// 알람 처리 
			if (result.alarm.length > 1) {
				var temp = $("#alarmList tfoot").html(); 
				
				console.log(temp);
				
				$("#alarmList tbody").html("");
				$.each(result.alarm, function (index, value) {
					html = temp.replace("#{viewDate}", value.viewDate);
					html = html.replace("#{viewPart}", value.viewPart);
					
					if(value.viewPart != "ALARM") {
						html = html.replace("#{useTime}", "-");
						html = html.replace("#{viewTime}", value.viewSTime);
					} else {
						if(value.useTime == 0) {
							html = html.replace("#{useTime}", "약" + 10 + "분");
						} else {
							html = html.replace("#{useTime}", Common.addComma(value.useTime) + "분");
						}
						
						html = html.replace("#{viewTime}", value.viewSTime + " ~ " + value.viewETime);
					} 
					
					$("#alarmList tbody").append(html);
				});
			} else {
				$("#alarmList tbody").html('<tr><td colspan="5" class="t_center no-data"><span>데이터가 존재 하지 않습니다.</span></td></tr>');
			}
		});
	}

	// 리포트 당일버튼 이벤트 처리
	$reportTodayBtn.on("click", function () {

		var picker = Component.setRangePicker({
			startpicker: {
				date: new Date(),
				input: '#searchStartDate',
				container: '#searchStartDateWrapper'
			},
			endpicker: {
				date: new Date(),
				input: '#searchEndDate',
				container: '#searchEndDateWrapper'
			},
			format: "yyyy.MM.dd",
			language: 'ko'
		});
		getMonitorReportData();
	});

	// 리포트 1주일 버튼 이벤트 처리
	$reportWeekBtn.on("click", function () {

		var picker = Component.setRangePicker({
			startpicker: {
				date: new Date().addDate(0, 0, -7),
				input: '#searchStartDate',
				container: '#searchStartDateWrapper'
			},
			endpicker: {
				date: new Date(),
				input: '#searchEndDate',
				container: '#searchEndDateWrapper'
			},
			format: "yyyy.MM.dd",
			language: 'ko'
		});
		getMonitorReportData();
	});

	// 리포트 1개월 버튼 이벤트 처리
	$reportMonthBtn.on("click", function () {

		var picker = Component.setRangePicker({
			startpicker: {
				date: new Date().addDate(0, -1, 0),
				input: '#searchStartDate',
				container: '#searchStartDateWrapper'
			},
			endpicker: {
				date: new Date(),
				input: '#searchEndDate',
				container: '#searchEndDateWrapper'
			},
			format: "yyyy.MM.dd",
			language: 'ko'
		});
		getMonitorReportData();
	});
	// 장비전원 ON / OFF 클릭
	$power_deco.on("click", function () {
		Common.showLoading();
		if ($("#power_deco").is(":checked")) {
			// 전원 ON
			if (confirm("장비 전원을 ON 하시겠습니까?")) {
				setSystemPower("1")
			} else {
				Common.hideLoading();
				$power_deco.prop("checked", false);
			}
		} else {
			// 전원 OFF
			if (confirm("장비 전원을 OFF 하시겠습니까?")) {
				setSystemPower("0")
			} else {
				Common.hideLoading();
				$power_deco.prop("checked", true);
			}
		}
	});

	// 상부 전원 ON / OFF 클릭
	$top_power_deco.on("click", function () {
		alert("상부 전원 클릭");
	});

	// LAMP 전원 ON / OFF 클릭
	$lamp_power_deco.on("click", function () {
		alert("LAMP 전원 클릭");
	});

	// 하부 전원 ON / OFF 클릭
	$bottom_power_deco.on("click", function () {
		alert("하부 전원 클릭");
	});









	// 전원 컨트롤 처리
	function setSystemPower(val) {

		var check = (val == "0") ? "OFF" : "ON"
		var msg = "";

		if (check == "OFF") {
			msg = "가 진행 되지 않았습니다.";
		} else {
			msg = "이 진행되지 않았습니다.";
		}

		var devEUI = $("#devEUI").val();
		var parameter = {
			relay: val
			, devEUIList: []
		};

		parameter.devEUIList.push(devEUI);

		Common.callAjax("/monitor/monitor/api/setLoraPowerStat", parameter, function (result) {

			if (result > 0) {
				alert("장비 전원이 " + check + " 되었습니다.");
				location.reload();
			} else {
				alert("장비 전원" + check + msg);
			}
			Common.hideLoading();
		});
	}

	// 목록버튼 이벤트 처리
	$listBtn.on('click', function () {
		Common.showLoading();
		location.href = "/monitor/monitor/monitorList?" + queryString;
	});
});

// kakao Map
// 인포윈도우를 표시하는 클로저를 만드는 함수입니다
function makeOverListener(map, marker, infowindow) {
	return function () {
		infowindow.open(map, marker);
	};
}

// kakao Map
// 인포윈도우를 닫는 클로저를 만드는 함수입니다
function makeOutListener(infowindow) {
	return function () {
		infowindow.close();
	};
}