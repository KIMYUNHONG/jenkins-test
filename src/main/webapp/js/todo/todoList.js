/**
 *  Create By Lee DoWon on 2020-04-22 / 오후 4:38
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $searchBtn = $('#searchBtn');
	var $resetBtn = $('#resetBtn');
	var $searchForm = $('#searchForm');

	var picker = Component.setRangePicker({
		startpicker: {
			date: '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var todoColumns = [
		{header: '업무 구분', name: 'domainStatusText', minWidth: 80, sortable: true, align: 'center'},
		{header: '제목', name: 'title', minWidth: 600, sortable: true, align: 'left'},
		{header: '요청자', name: 'requestUserName', minWidth: 70, sortable: true, align: 'center'},
		{header: '수신자', name: 'receiveUserName', minWidth: 70, sortable: true, align: 'center'},
		{header: '요청일', name: 'regDtText', minWidth: 80, sortable: true, align: 'center'},
		{header: '상태', name: 'handleYnText', minWidth: 80, sortable: true, align: 'center'},
	];

	var todoGrid = Grid.createGrid({
		id: 'todoGrid',
		columns: todoColumns,
		rowHeaders: ['rowNum'],
		data: []
	});

	getTodoList();

	todoGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null) {
			return false;
		}
		var record = todoGrid.getRow(row);
		if (record != null) {
			var status = record['domainStatus'];
			var url;
			switch(status){
				case "MR030001":	// 민원 신청
					url = '/manage/complain/complainDetail?complainSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "MR030002":	// 민원 접수
					url = '/manage/complain/complainDetail?complainSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "SC080001":	// 거래처 가입
					url = '/customer/customer/customerDetail?customerSeq=' + record['domainSeq2'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "IM050001":	// 설치요청 - 설치요청
					url = '/install/installRequest/installRequestDetail?installRequestSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "IM050002":	// 설치요청 - 요청접수
					url = '/install/installRequest/installRequestDetail?installRequestSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "IM050004":	// 설치요청 - 설치완료
					url = '/install/installRequest/installRequestDetail?installRequestSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
				case "IM050005":	// 설치요청 - 확인완료
					url = '/install/installRequest/installRequestDetail?installRequestSeq=' + record['domainSeq'];
					moveDetailPage(record['domainStatus'], record['domainSeq'], url);
					break;
			}
		}
		function moveDetailPage(status, seq, url){
			Common.showLoading();
			Common.callAjax("/todo/api/setTodoViewUserId", {domainStatus : status, domainSeq : seq}, function (result) {
				location.href = url;
			});
		}
	});

	$searchBtn.on('click', function () {
		getTodoList();
	});

	$resetBtn.on('click', function () {
		$('#searchForm')[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	function getTodoList() {

		Common.showLoading();
		Common.callAjax("/todo/api/getTodoList", $searchForm.serializeObject(), function (result) {
			todoGrid.resetData(result);

			if(result != null && result.length > 0){
				$.each(result, function(index, todo){
					if(todo.checkedYn == 'N'){
						$.each(todoColumns, function (columnIndex, columnValue) {
							todoGrid.addCellClassName(index,columnValue.name ,'bold');
						});
					}
				});
			}

			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}
});
