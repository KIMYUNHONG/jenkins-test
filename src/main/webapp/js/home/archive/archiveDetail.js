/**
 *  Create By Kim YunHong on 2020-03-19 / 오전 11:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $archiveModifyBtn = $('#archiveModifyBtn');		// 수정 버튼
	var $archiveListBtn = $('#archiveListBtn');			// 목록 버튼
	var $archiveDeleteBtn = $('#archiveDeleteBtn');			// 삭제 버튼
	var queryString = $('input[name="queryString"]').val();

	// 수정 버튼 이벤트 처리
	$archiveModifyBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/archive/archiveWrite?' + queryString;
	});

	// 목록 버튼 이벤트 처리
	$archiveListBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/archive/archiveList?' + queryString;
	});

	// 삭제버튼 이벤트 처리
	$archiveDeleteBtn.on('click', function () {
		if (confirm('삭제 하시겠습니까?')) {
			Common.callAjax("/home/board/api/deleteBoard", $("#archiveDetailForm").serializeObject(), function (result) {
				alert("삭제가 완료되었습니다.");
				location.href = "/home/archive/archiveList?" + queryString;
			});
		}
	});

});