/**
 *  Create By Kim YunHong on 2020-03-19 / 오전 11:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $("#resetBtn"); //초기화 버튼
	var $addArchiveBtn = $("#addArchiveBtn"); //등록 버튼
	var $searchForm = $("#searchForm");

	var archiveColumns = [
		{header: '등록일', width: 150, name: 'regDtText', sortable: true, align: 'center'},
		{header: '자료구분', width: 150, name: 'boardCategoryText', sortable: true, align: 'center'},
		{header: '제목', name: 'title', minWidth: 200, sortable: true, align: 'left'},
		{header: '등록자', name: 'regName', width: 130, sortable: true, align: 'center'},
		{
			header: '파일',
			name: 'existFileYn',
			width: 60,
			sortable: false,
			align: 'center',
			renderer: Grid.iconRenderer({class: 'ic_data'})
		},
		{header: '조회수', name: 'count', width: 100, sortable: true, align: 'center'}
	];

	var archiveGrid = Grid.createGrid({
		id: 'archiveGrid',
		columns: archiveColumns,
		data: []
	});

	getArchiveDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getArchiveDataList();
	});

	$resetBtn.on('click', function () {
		$('#searchForm')[0].reset();
	});

	// 자료실 데이터 로드
	function getArchiveDataList() {
		// 검색 Validation
		var parameter = $searchForm.serializeObject();
		Common.showLoading();
		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "archiveList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/home/archive/api/getArchiveList", parameter, function (result) {
			archiveGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// 등록 버튼 이벤트 처리
	$addArchiveBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/archive/archiveWrite?' + '&' + $searchForm.serializeListParameter();
	});

	// 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	// 그리드 로우 선택시 이벤트 처리
	archiveGrid.on('click', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if (row == null) {
			return false;
		}

		var record = archiveGrid.getRow(row);
		Common.showLoading();
		location.href = '/home/archive/archiveDetail?boardSeq=' + record["boardSeq"] + '&' + $searchForm.serializeListParameter();
	});

});