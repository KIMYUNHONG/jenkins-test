/**
 *  Create By Kim YunHong on 2020-03-16 / 오후 3:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $("#resetBtn"); //초기화 버튼
	var $addFaqBtn = $("#addFaqBtn"); //등록 버튼
	var $searchForm = $("#searchForm");

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getFaqDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	// 등록 버튼 이벤트 처리
	$addFaqBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/faq/faqWrite';
	});

	// 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	$searchForm.on('submit', function () {
		getFaqDataList();
		return false;
	});

	getFaqDataList(); // FAQ 데이터 로드

});

//FAQ 데이터 로드
function getFaqDataList() {
	Common.hideLoading();
	Common.callAjax("/home/faq/api/getFaqList", $("#searchForm").serializeObject(), function (result) {
		var html = '';
		for (var i = 0; i < result.length; i++) {
			html += '<li>';
			html += '<a onClick="openContent(this)">';
			html += '<span class="quiz">[' + result[i].boardCategoryText + '] ' + result[i].title + '</span>';
			html += '</a>';
			html += '<div class="answer">';
			html += '<p>' + result[i].content + '</p>';
			html += '</div>';
			html += '</li>';
		}
		$('#faqList').html(html);
		$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
	});
}

//카테고리 변경 이벤트
function changeCategory(target) {
	var cd = $(target).data('cd');
	$('#boardCategory').val(cd);
	getFaqDataList();

	// tab 처리
	$('.tab>li').each(function () {
		$(this).removeClass('on');
	});
	$(target).parent().addClass('on');
}

// 상세내용 펼치기 이벤트
function openContent(target) {
	var $li = $(target).parent();
	if ($li.hasClass('on')) {
		$li.removeClass('on');
	} else {
		$li.addClass('on');
	}
}