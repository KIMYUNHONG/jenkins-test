/**
 *  Create By Kim YunHong on 2020-03-24 / 오후 3:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

    var $faqModifyBtn = $('#faqModifyBtn');		// 수정 버튼
    var $faqListBtn = $('#faqListBtn');			// 목록 버튼
    var boardSeq = $('input[name="boardSeq"]').val();			// BOARD_SEQ(KEY)
    var $faqDeleteBtn = $('#faqDeleteBtn');			// 삭제 버튼

    // 수정 버튼 이벤트 처리
    $faqModifyBtn.on('click', function () {
        Common.showLoading();
        location.href = '/home/faq/faqWrite?' +'boardSeq=' + boardSeq;
    });

    // 목록 버튼 이벤트 처리
    $faqListBtn.on('click', function () {
        Common.showLoading();
        location.href = '/home/faq/faqList';
    });

    // 삭제버튼 이벤트 처리
    $faqDeleteBtn.on('click', function () {
        if(confirm('삭제 하시겠습니까?')){
            Common.callAjax("/home/board/api/deleteBoard", $("#faqDetailForm").serializeObject(), function (result) {
                alert("삭제가 완료되었습니다.");
                location.href="/home/faq/faqList";
            });
        }
    });
});