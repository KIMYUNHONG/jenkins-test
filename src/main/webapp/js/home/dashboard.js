$(function () {
	Common.showLoading();
	// ---- 고정 프로퍼티
	const DEVICE_MAP_MAX_LEVEL = 6;	// 장비별 마커뷰 최대레벨 1 ~ 6
	const MAP_MAX_LEVEL = 12;		// 축소가능한 카카오맵 최대 레벨 설정값
	const MAP_INIT_LEVEL = 10;		// 맵 처음 셋팅 레벨

	var customerChart = tui.chart;
	var monthChart = tui.chart;
	var $customerChart = $('#customerChart')[0];
	var $monthChart = $('#monthChart')[0];

	$('.currentYear').text('(' + new Date().getFullYear() + '년)');

	var $customerType = $('input[name="customerType"]');
	var $locationSelect = $('#locationSelect');
	var $deviceSelect = $('#deviceSelect');

	$customerType.on('change', function () {
		var val = $(this).val();
		var $userList = $('#userList');
		if (Valid.isEmpty(val)) {
			$userList.find('tr').show();
		} else {
			$userList.find('tr').hide();
			$('.' + val).show();
		}
	});

	var $deviceList = $('#deviceList');
	var $deviceListCnt = $('#deviceListCnt');
	var $scrollDiv = $('#scrollDiv');

	var map;
	var markers = [];
	var overlays = [];
	var infoWindows;
	var hoverMarker = {};
	var existZoomLevel = 0;
	var deviceListIndex = 0;

	$locationSelect.select2();
	$deviceSelect.select2();
	bindMap();
	bindMonitorData();
	initScrollPaging();

	// 차트 데이터 조회/바인드
	bindCustomerChart();
	bindMonthChart();

	/**
	 * 모니터링 지도 위 상태 전체 카운트 조회
	 */
	function bindMonitorData() {
		var bounds = map.getBounds();
		var swLatLng = bounds.getSouthWest();
		var neLatLng = bounds.getNorthEast();
		var depth = 2;
		var level = map.getLevel();
		if (Monitor.isSidoMap(level)) {
			depth = 0;
		} else {
			depth = Monitor.isSigunguMap(level) ? 1 : 2;
		}
		var parameter = {
			deviceMstSeq: $deviceSelect.val() || 0,
			swLat: swLatLng.Ha,
			swLng: swLatLng.Ga,
			neLat: neLatLng.Ha,
			neLng: neLatLng.Ga,
			depth: depth,
		};
		Common.callAjax("/monitor/monitor/api/getMonitorStatusData", parameter, function (result) {
			$.each(result, function (key, value) {
				$('#' + key.toUpperCase() + '_QTY').text(Common.addComma(value) + '대');
			});
		});
	}

	/**
	 * 맵에 그려진 마커, 인포윈도우, 등 컴포넌트 전체 제거
	 */
	function removeMapContent() {
		for (var i = 0, length = markers.length; i < length; i++) {
			markers[i].setMap(null);
			infoWindows[i].close();
		}
		for (i = 0, length = overlays.length; i < length; i++) {
			overlays[i].setMap(null);
		}
		markers = [];
		infoWindows = [];
		overlays = [];
	}

	/**
	 * 인포윈도우 전체 닫기
	 */
	function allCloseInfoWindow() {
		$.each(infoWindows, function (index, infowindow) {
			infowindow.close();
		});
	}


	// 스크롤 이벤트
	function initScrollPaging() {
		$scrollDiv.scroll(function () {   //스크롤이 최하단 으로 내려가면 리스트를 조회하고 page를 증가시킨다.
			if ($scrollDiv.scrollTop() >= ($deviceList.height() - ($scrollDiv.height())) - 100) {
				if (!Monitor.isDeviceMap(map.getLevel()) && deviceListIndex <= Number($deviceListCnt.text())) {
					$scrollDiv.unbind('scroll');
					setTimeout(function () {
						initScrollPaging();
					}, 1000);
					$('#list-loading').show();
					bindAreaMarker(map);
					bindMonitorData();
				}
			}
		});
	}

	/**
	 * 맵 좌측 장비 리스트 클리어
	 * @param result - 장비리스트 데이터
	 */
	function clearDeviceList(result) {
		$scrollDiv.animate({scrollTop: 0}, 10);
		deviceListIndex = 0;
		$deviceListCnt.text(0);
		$deviceList.html('');
		allCloseInfoWindow();
	}

	/**
	 * 맵 초기 설정 및 맵 관련 이벤트 바인딩
	 */
	function bindMap() {
		var mapContainer = document.getElementById('allViewMap'), // 지도를 표시할 div
			mapOption = {
				center: new kakao.maps.LatLng(37.56672508493643, 126.97860701094235),
				level: MAP_INIT_LEVEL // 지도의 확대 레벨
			};

		map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
		map.setMaxLevel(MAP_MAX_LEVEL);
		// 현재 위치 정보 받으면 지도 중심으로 잡아줌.
		navigator.geolocation.getCurrentPosition(function (pos) {
			map.setCenter(new kakao.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
		});

		var zoomControl = new kakao.maps.ZoomControl();
		map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);

		hoverMarker = Monitor.getHoverMarker('/img/common/map_here.gif');

		// 장비리스트에서 장비에 마우스호버시 맵에 호버마커 표시
		$deviceList.on('mouseenter', 'li', function () {
			var lat = $(this).find('input[name="latitude"]').val();
			var lng = $(this).find('input[name="longitude"]').val();
			hoverMarker.setPosition(new kakao.maps.LatLng(lat, lng));
			hoverMarker.setMap(map);
		});
		// 장비리스트에서 장비에 마우스호버 해제시 맵에 호버마커 제거
		$deviceList.on('mouseleave', 'li', function () {
			hoverMarker.setMap(null);
		});
		// 맵 장비리스트 클릭시 모니터링으로 이동
		$deviceList.on('click', 'li', function () {
			var $this = $(this);
			if ($this.hasClass('no-data')) {
				return false;
			}
			location.href = '/monitor/monitor/monitorDetail?deviceSeq=' + $this.find('input[name="deviceSeq"]').val();
		});

		kakao.maps.event.addListener(map, 'zoom_start', function () {
			removeMapContent();
			clearDeviceList();
		});
		kakao.maps.event.addListener(map, 'idle', function () {
			var level = map.getLevel();
			$('#list-loading').show();
			clearDeviceList();
			// 연속해서 확대/축소를 계속 하는경우 마커가 중복되어서 딜레이를 주고 바인드
			setTimeout(function () {
				if (existZoomLevel == 0 || (map.getLevel() == existZoomLevel)) {
					bindMonitorData();
					if (Monitor.isDeviceMap(level)) {
						bindDeviceMarker(map);
					} else {
						bindAreaMarker(map);
					}
				}
			}, existZoomLevel == level ? 300 : 700);
			existZoomLevel = level;
		});
		Common.hideLoading();
		bindAreaMarker(map);
		bindMonitorData();
	}

	// 장비 마커(줌레벨 6이하 인경우)
	function bindDeviceMarker(map) {
		var bounds = map.getBounds();
		var swLatLng = bounds.getSouthWest();
		var neLatLng = bounds.getNorthEast();
		var parameter = {
			deviceMstSeq: $deviceSelect.val() || 0,
			swLat: swLatLng.Ha,
			swLng: swLatLng.Ga,
			neLat: neLatLng.Ha,
			neLng: neLatLng.Ga,
			depth: 2,
			startIndex: 0,
			indexCount: 9999,
		};
		Common.callAjax("/home/dashboard/api/getDashboardDeviceList", parameter, function (result) {
			if (result != null && result.length > 0) {
				bindDeviceList(result);
				$deviceListCnt.text(result.length);
				$.each(result, function (index, value) {

					// 기존에 이미 지도에 찍혀있는 마크인지 체크
					var existFlag = false;
					$.each(markers, function (index, existMarker) {
						if (value.deviceSeq == existMarker.deviceSeq) {
							existFlag = true;
							return false;
						}
					})
					// 이미 지도에 표시되고있는 마커면 반복문 continue 처리
					if (existFlag) {
						return true;
					}
					var latlng = new kakao.maps.LatLng(value.latitude, value.longitude);

					// 지도를 클릭한 위치에 표출할 마커입니다
					var marker = new kakao.maps.Marker({
						position: latlng,
						image: Monitor.getDeviceStatusMarkerImage(value['monitorState'])
					});

					// 마커에 표시할 인포윈도우를 생성합니다
					var infowindow = Monitor.getInfoWindow(value['deviceId'], value['deviceNameText']);

					kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
					kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
					kakao.maps.event.addListener(marker, 'click', function () {
						location.href = '/monitor/monitor/monitorDetail?deviceSeq=' + value.deviceSeq;
					});

					marker.deviceSeq = value.deviceSeq;
					markers.push(marker);
					infoWindows.push(infowindow);
					marker.setMap(map);
				});

				// 이제 지도에 존재하지 않는 마커는 제거
				for (var i = markers.length - 1; i >= 0; i--) {
					$.each(result, function (index, value) {
						if (value.deviceSeq == markers[i].deviceSeq) {
							return false;
						}
						if (index == markers.length - 1) {
							markers[i].setMap(null);
							infoWindows[i].close();
							infoWindows.splice(i, 1);
							markers.splice(i, 1);
							return false;
						}
					});
				}
			} else {	// 장비가 아예 없는경우 초기화 처리.
				removeMapContent();
				clearDeviceList();
			}
		});
	}

	function bindDeviceListCnt(parameter) {
		Common.callAjax("/home/dashboard/api/getDashboardDeviceListCnt", parameter, function (result) {
			$deviceListCnt.text(result);
			if (result == 0) {
				clearDeviceList();
				var html =
					'<li class="no-data">' +
					'<span>등록된 장비가 없습니다</span>' +
					'</li>';
				$deviceList.append(html);
			}
		}, {async: true});
	}

	// 지역별 마커(줌레벨 7이상 인경우)
	function bindAreaMarker(map) {
		var bounds = map.getBounds();
		var swLatLng = bounds.getSouthWest();
		var neLatLng = bounds.getNorthEast();
		var level = map.getLevel();

		var parameter = {
			deviceMstSeq: $deviceSelect.val() || 0,
			swLat: swLatLng.Ha,
			swLng: swLatLng.Ga,
			neLat: neLatLng.Ha,
			neLng: neLatLng.Ga,
			depth: Monitor.isSigunguMap(level) ? 1 : 0,
			startIndex: deviceListIndex,
			indexCount: 100
		};

		// 처음 조회하는경우에만 조회
		if (deviceListIndex === 0) {
			Common.callAjax("/home/dashboard/api/getDeviceCntByArea", parameter, function (result) {
				$.each(result, function (index, value) {
					var customOverlay = Monitor.getCustomOverlay(value);
					customOverlay.setMap(map);
					overlays.push(customOverlay);
				})
			}, {async: true});
			bindDeviceListCnt(parameter);

		}
		Common.callAjax("/home/dashboard/api/getDashboardDeviceList", parameter, function (result) {
			if (result != null && result.length > 0) {
				bindDeviceList(result);
			}
			$('#list-loading').hide();
		}, {async: true});
		deviceListIndex += 100;
	}

	/**
	 * 장비 리스트 bind
	 * @param count
	 * @return {boolean}
	 */
	function bindDeviceList(list) {
		$.each(list, function (index, value) {
			var html =
				'<li>' +
				'\t<input type="hidden" name="deviceSeq" value="' + value.deviceSeq + '" />\n' +
				'\t<input type="hidden" name="modemId" value="' + Common.nvl(value.modemId, '') + '"/>' +
				'\t<input type="hidden" name="latitude" value="' + Common.nvl(value.latitude, '') + '"/>' +
				'\t<input type="hidden" name="longitude" value="' + Common.nvl(value.longitude, '') + '"/>' +
				'\t<div class="d_locate ' + Monitor.getDeviceConnectionClass(value['connectionYn'], value['modemId']) + '">' +
				'\t\t<span class="dv_state ' + Monitor.getDeviceStateClass(value['monitorState']) + '">' + value.monitorStateText + '</span>' +
				'\t\t<span class="dv_power ' + (value.powerYn).toLowerCase() + '" >' + value.powerYn + '</span>' +
				'\t\t<span class="locate_name">' + value.deviceId + '<span> (' + value.deviceNameText + ')</span></span>' +
				'\t</div>\n' +
				'\t<p class="d_info">' + value.deviceAddress + '</p>\n' +
				'\t<p class="d_data">' +
				'\t\t<span>관리번호 : ' + Common.nvl(value.manageNo, '') + '</span>' +
				'\t\t<span>가로등번호 : ' + Common.nvl(value.lampNo, '') + '</span>' +
				'\t</p>' +
				'</li>';
			$deviceList.append(html);
		});
		$('#list-loading').hide();
	}

	// 설치 지역 변경시 해당 지역으로 맵이동
	$locationSelect.on('select2:select', function (e) {
		var lat = e.params.data.element.dataset['lat'];
		var lng = e.params.data.element.dataset['lng'];
		if (Valid.isZero(lat) || Valid.isZero(lng)) {		// 위/경도 값이 없는경우 이동 X
			return false;
		}
		var level = map.getLevel();
		if ((Monitor.isSigunguMap(level) || Monitor.isSidoMap(level)) || level <= 3) {
			map.setLevel(5);
		}
		map.setCenter(new kakao.maps.LatLng(lat, lng));
	});

	// 장비필터 변경시 재조회
	$deviceSelect.on('select2:select', function (e) {
		var level = map.getLevel();
		$('#list-loading').show();
		removeMapContent();
		clearDeviceList();
		bindMonitorData();
		if (Monitor.isDeviceMap(level)) {
			bindDeviceMarker(map);
		} else {
			bindAreaMarker(map);
		}
	});

	function bindCustomerChart() {
		Common.callAjax("/home/dashboard/api/getCustomerCnt", {}, function (result) {
			var customerChartTheme = {
				series: {
					// [차트 설정] 거래처별현황 차트 컬러설정 (
					colors: [
						'#fbf28a', '#ffdb6c', '#ffba8c', '#ffa360', '#ff7d4a', '#fe5959', '#b3e064', '#9ed267', '#82bc43'
						, '#68b7f9', '#49a4f0', '#207cc9', '#106793', '#685084', '#7c7c7c'               // 차트 컬러
					]
				}
			};
			var customerData = [];
			var totalCnt = 0;
			if (result != null) {
				$.each(result, function (index, value) {
					var item = {
						name: value['customerTypeText'],
						data: value['cnt']
					}
					totalCnt += value['cnt'];
					customerData.push(item);
				});

				$.each(customerData, function (index, value) {
					value.data = (value.data / totalCnt * 100).toFixed(0);
				});
			}

			var data = {
				categories: ['거래처'],
				series: customerData
			};

			// [차트 설정] 거래처별 현황 차트설정
			var customerChartOption = {
				chart: {
					width: 630,						// 차트 가로
					height: 350,					// 차트 높이
					format: function (value, chartType, areaType, valuetype, legendName) {
						if (areaType === 'makingSeriesLabel') { // formatting at series area
							value = value + '%';
						}
						return value;
					}
				},
				series: {
					radiusRange: ['40%', '100%'],
					showLabel: true
				},
				tooltip: {
					suffix: '%'
				},
				legend: {
					showCheckbox: false,	// legend 체크박스 표시 여부
					align: 'bottom'			// series 설명 위치 (top, bottom, left, right)
				}
			};
			customerChart.registerTheme('customerChartTheme', customerChartTheme);
			customerChartOption.theme = 'customerChartTheme';

			customerChart.pieChart($customerChart, data, customerChartOption);

			if (totalCnt == 0) {
				var noData = '<div class="chartnodata"></div>';
				$('#customerChart').find('.tui-chart-series-custom-event-area').html(noData);
			}
		});
	}

	function bindMonthChart() {
		Common.callAjax("/home/dashboard/api/getDeviceStatusCnt", {}, function (result) {

			var currentMonth = new Date();
			var categories = [];
			for (var i = 4; i >= 0; i--) {
				categories.push(currentMonth.addDate(0, -i, 0).dateToYearMonth());
			}
			var data = {
				dm020001: [0, 0, 0, 0, 0],
				dm020002: [0, 0, 0, 0, 0],
				dm020003: [0, 0, 0, 0, 0],
				dm020004: [0, 0, 0, 0, 0],
				dm020005: [0, 0, 0, 0, 0],
				dm020006: [0, 0, 0, 0, 0]
			};

			var max = 0;
			$.each(result, function (index, value) {
				value.cnt = value.dm020001 + value.dm020002 + value.dm020003 + value.dm020004 + value.dm020005 + value.dm020006;
				if (value.cnt > max) {
					max = value.cnt;
				}
				$.each(categories, function (index2, value2) {
					if (value2 == value.month) {
						data.dm020001[index2] = value.dm020001;
						data.dm020002[index2] = value.dm020002;
						data.dm020003[index2] = value.dm020003;
						data.dm020004[index2] = value.dm020004;
						data.dm020005[index2] = value.dm020005;
						data.dm020006[index2] = value.dm020006;
					}
				});
			});

			var monthChartData = {
				categories: categories,
				series: [
					{
						name: '미설치',
						data: data.dm020001
					},
					{
						name: '설치',
						data: data.dm020002
					},
					{
						name: '탈거',
						data: data.dm020003
					},
					{
						name: '재설치',
						data: data.dm020004
					},
					{
						name: '폐기',
						data: data.dm020005
					},
					{
						name: '분실',
						data: data.dm020006
					}
				]
			};

			// [차트 설정] 월별 재고현황 크기/라벨 배치 설정
			var monthChartOption = {
				chart: {
					width: 630,						// 차트 가로
					height: 350,					// 차트 높이
					format: '1,000'					// 차트 데이터 포맷
				},
				yAxis: {
					min: 0,				// 세로축 최소값
					max: (max * 1.2 > 100) ? max * 1.2 : 100			// 세로축 최대값
				},
				series: {
					stack: 'normal',	// chart type
					barWidth: 40,		// chart bar 가로길이
					showLabel: false,	// chart bar 내에 숫자 표시 여부
				},
				legend: {
					showCheckbox: false,	// legend 체크박스 표시 여부
					align: 'bottom',			// series 설명 위치 (top, bottom, left, right)
				},
				tooltip: {
					grouped: true		// 툴팁 묶어서 나올여부
				},
			};
			monthChart.registerTheme('monthChartTheme', {
				series: {
					// [차트 설정] 월별 재고현황 컬러설정
					colors: [
						'#76b9f2', '#93dedb', '#ffd79b', '#b79dd5', '#00397a', '#6f6f6f' // 차트 컬러
					]
				}
			});
			monthChartOption.theme = 'monthChartTheme';
			monthChart.columnChart($monthChart, monthChartData, monthChartOption);
		});
	}
});

// kakao Map
// 인포윈도우를 표시하는 클로저를 만드는 함수입니다
function makeOverListener(map, marker, infowindow) {
	return function () {
		infowindow.open(map, marker);
	};
}

// kakao Map
// 인포윈도우를 닫는 클로저를 만드는 함수입니다
function makeOutListener(infowindow) {
	return function () {
		infowindow.close();
	};
}
