/**
 *  Create By Kim YunHong on 2020-03-16 / 오후 3:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $noticeSaveBtn = $("#noticeSaveBtn");
	var $noticeListBtn = $("#noticeListBtn");
	var boardSeq = $("input[name=boardSeq]").val();
	var $file = $('#file');
	var $fileList = $('#fileList');
	var queryString = $('input[name="queryString"]').val();
	existFile();

	// 저장 버튼 이벤트 처리
	$noticeSaveBtn.on('click', function () {
		if (confirm('저장 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/home/notice/api/setNotice", $('#form').serializeObject(), function (result) {
				alert("저장이 완료 되었습니다.");
				location.href = "/home/notice/noticeList?" + queryString;
			});
		}
	});

	// 취소 버튼 이벤트 처리
	$noticeListBtn.on('click', function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();
			if (boardSeq > 0) {
				location.href = '/home/notice/noticeDetail?' + queryString;
			} else {
				location.href = '/home/notice/noticeList?' + queryString;
			}
		}
	});

	// 파일 추가 버튼 이벤트 처리
	$file.on('change', function () {
		var fileName = $file[0].files[0].name;
		if (!(Valid.isImageFormat(fileName) || Valid.isPdfFormat(fileName))) {
			alert("이미지 또는 PDF파일만 등록이 가능합니다.");
			return false;
		}

		if ($($fileList.find('li')).length >= 5) {
			alert("파일은 최대 5개까지 등록 가능합니다.");
			return false;
		}
		Common.fileUpload($file, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$file.val(null);
			var $html = $('\t\t\t\t\t\t\t<li><a href="/uploadFile/api/download/' + result['uploadFileKey'] + '"' +
				' class="icon file" target="_blank" download="' + result['originalFileName'] + '">' + result['originalFileName'] + '</a>' +
				'<button type="button" class="btn_del"></button>' +
				'<input type="hidden" name="uploadFileKey" value="' + result['uploadFileKey'] + '"></li>\n');

			$fileList.append($html);
			existFile();
		});
	});

	// 파일 삭제 이벤트 처리
	$fileList.on('click', '.btn_del', function () {
		$(this).parent('li').remove();
		existFile();
	});

	// 파일 첨부 밑에 파일있으면 라인 긋고 없으면 제거
	function existFile() {
		if ($fileList.find('li').length == 0) {
			$fileList.removeClass('filename');
		} else {
			if (!$fileList.hasClass('filename')) {
				$fileList.addClass('filename');
			}
		}
	}

});