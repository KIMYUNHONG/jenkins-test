/**
 *  Create By Kim YunHong on 2020-03-16 / 오후 3:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $("#resetBtn"); //초기화 버튼
	var $addNoticeBtn = $("#addNoticeBtn"); //등록 버튼
	var $searchForm = $("#searchForm");
	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var noticeColumns = [
		{header: '등록일', width: 150, name: 'regDtText', sortable: true, align: 'center'},
		{header: '제목', name: 'title', minWidth: 200, sortable: true, align: 'left'},
		{
			header: '파일',
			name: 'existFileYn',
			width: 60,
			sortable: false,
			align: 'center',
			renderer: Grid.iconRenderer({class: 'ic_data'})
		},
		{header: '등록자', name: 'regName', width: 130, sortable: true, align: 'center'},
		{header: '조회수', name: 'count', width: 100, sortable: true, align: 'center'}
	];

	var noticeGrid = Grid.createGrid({
		id: 'noticeGrid',
		columns: noticeColumns,
		data: []
	});

	getNoticeDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getNoticeDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	// 공지사항 데이터 로드
	function getNoticeDataList() {
		if ($searchStartDate.val() == '' && $searchEndDate.val() == '') {
			picker.setStartDate(new Date().addDate(0, -1, 0));
			picker.setEndDate(new Date());
		}
		// 검색 Validation
		var parameter = $searchForm.serializeObject();
		Common.showLoading();
		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "noticeList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/home/notice/api/getNoticeList", parameter, function (result) {
			noticeGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// 등록 버튼 이벤트 처리
	$addNoticeBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/notice/noticeWrite?' + '&' + $searchForm.serializeListParameter();
	});

	// 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	// 그리드 로우 선택시 이벤트 처리
	noticeGrid.on('click', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if (row == null) {
			return false;
		}

		var record = noticeGrid.getRow(row);
		Common.showLoading();
		location.href = '/home/notice/noticeDetail?boardSeq=' + record["boardSeq"] + '&' + $searchForm.serializeListParameter();
	});

});