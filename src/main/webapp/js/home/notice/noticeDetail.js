/**
 *  Create By Kim YunHong on 2020-03-16 / 오후 3:04
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $noticeModifyBtn = $('#noticeModifyBtn');		// 수정 버튼
	var $noticeListBtn = $('#noticeListBtn');			// 목록 버튼
	var $noticeDeleteBtn = $('#noticeDeleteBtn');			// 삭제 버튼
	var queryString = $('input[name="queryString"]').val();

	// 수정 버튼 이벤트 처리
	$noticeModifyBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/notice/noticeWrite?' + queryString;
	});

	// 목록 버튼 이벤트 처리
	$noticeListBtn.on('click', function () {
		Common.showLoading();
		location.href = '/home/notice/noticeList?' + queryString;
	});

	// 삭제버튼 이벤트 처리
	$noticeDeleteBtn.on('click', function () {
		if (confirm('삭제 하시겠습니까?')) {
			Common.callAjax("/home/board/api/deleteBoard", $("#noticeDetailForm").serializeObject(), function (result) {
				alert("삭제가 완료되었습니다.");
				location.href = "/home/notice/noticeList?" + queryString;
			});
		}
	});
});