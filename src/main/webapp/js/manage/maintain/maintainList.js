/**
 *  Create By Kim YunHong on 2020-03-26 / 오전 09:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');
	var $resetBtn = $('#resetBtn');
	var $openAsUserListPopupBtn = $("#openAsUserListPopupBtn");
	var $excelDownloadBtn = $("#excelDownloadBtn");
	var $searchForm = $("#searchForm");
	var $statusList = $("input[name=statusList]");

	var picker = Component.setRangePicker({
		startpicker: {
			date: '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	//체크박스(민원유형) 전체 클릭시 이벤트 처리
	$('#total_chk').on('click', function () {
		$statusList.prop("checked", $("#total_chk").is(':checked'));
	});

	// 체크박스(민원유형) 클릭시 '전체' 항목 체크 이벤트
	$statusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=statusList]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=statusList]:checked").length;

		$("#total_chk").prop("checked", (chkCnt == totalCnt));
	});

	var maintainColumns = [
		{header: '지역', name: 'locationAreaText', minWidth: 80, sortable: true, align: 'center'},
		{header: '설치장소', name: 'locationName', minWidth: 80, sortable: true, align: 'left'},
		{header: '장비번호', name: 'deviceId', minWidth: 140, sortable: true, align: 'center'},
		{header: '차수', name: 'maintainNoText', minWidth: 50, sortable: true, align: 'center'},
		{header: '유형', name: 'maintainTypeTextConcat', minWidth: 180, sortable: true, align: 'center'},
		{header: '장비명', name: 'deviceNameText', minWidth: 120, sortable: true, align: 'center'},
		{header: '관리번호', name: 'manageNo', minWidth: 100, sortable: true, align: 'left'},
		{header: '가로등번호', name: 'lampNo', minWidth: 80, sortable: true, align: 'left'},
		{header: '거래처구분', name: 'customerTypeText', minWidth: 80, sortable: true, align: 'center'},
		{header: '거래처', name: 'customerName', minWidth: 80, sortable: true, align: 'left'},
		{header: '보수담당자', name: 'asUserName', minWidth: 80, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth: 80, sortable: true, align: 'center'}
	];

	var maintainGrid = Grid.createGrid({
		id: 'maintainGrid',
		columns: maintainColumns,
		data: []
	});

	// 그리드 로우 클릭시 디테일로 이동 처리
	maintainGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null) {
			return false;
		}
		Common.showLoading();
		var record = maintainGrid.getRow(row);
		location.href = '/manage/maintain/maintainDetail?deviceSeq=' + record.deviceSeq + '&' + $searchForm.serializeListParameter();
	});

	$searchBtn.on('click', function () {
		getMaintainList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	// AS 담당자 버튼 처리
	$openAsUserListPopupBtn.on('click', function () {
		openAsUserListPopup();
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {

		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = maintainColumns;

		var option = {
			fileName: '유지보수'
		};
		Common.excelDownload("/manage/maintain/excel/getMaintainList", parameter, option);
	});

	getMaintainList();

	// 민원신청 데이터 로드
	function getMaintainList() {
		if ($('#searchStartDate').val() == '' && $('#searchEndDate').val() == '') {
			picker.setStartDate(new Date().setDate(1));
			picker.setEndDate(new Date().addDate(0, 1, 0).setDate(0));
		}
		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		Common.showLoading();
		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "maintainList" : "?" + $searchForm.serializeListParameter());
		Common.callAjax("/manage/maintain/api/getMaintainList", parameter, function (result) {
			maintainGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// AS담당자 리스트 선택 팝업
	function openAsUserListPopup() {
		Common.openPopup('/common/asUserListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("보수담당자", result, {width: 600});
			$('#popupList').append($popup);

			var $searchAsUserForm = $popup.find('#searchAsUserForm');
			var asUserColumns = [
				{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
				{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
				{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
				{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
			];

			var asUserGrid = Grid.createGrid({
				id: 'asUserGrid',
				columns: asUserColumns,
				data: [],
				bodyHeight: 300
			});

			getAsUserList();

			function getAsUserList() {
				Common.callAjax("/standard/user/api/getAsUserList", $searchAsUserForm.serializeObject(), function (result) {
					asUserGrid.resetData(result);
					$popup.find('#asUserTotalCount').text(result != null ? Common.addComma(result.length) : 0);
				});
			}

			// 로우 선택시 이벤트 처리
			asUserGrid.on('click', function (result) {
				var row = result['rowKey'];
				if (row == null) {
					return false;
				}
				var record = asUserGrid.getRow(row);

				$popup.remove();
				$('input[name="asUserName"]').val(record.name);
				$('input[name="asUserId"]').val(record.userId);
			});

			$popup.find('#searchAsUserBtn').on('click', function () {
				getAsUserList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			Common.resizeHeightPopup($popup);
		}, {})
	}
});
