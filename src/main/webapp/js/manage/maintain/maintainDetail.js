/**
 *  Create By Kim YunHong on 2020-03-30 / 오후 02:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var deviceSeq = $('input[name="deviceSeq"]').val();
	var $moveMonitorDetail = $("#moveMonitorDetail"); // 모니터링 상세
	var $moveInstallDeviceDetail = $("#moveInstallDeviceDetail"); // 설치장비 상세
	var $openMaintainReportPopup = $('#openMaintainReportPopup'); // 유지보수 리포트

	var $searchForm = $('#searchForm');
	var $searchType = $('input[name="searchType"]');
	var $searchYear = $('select[name="searchYear"]');
	var queryString = $('input[name="queryString"]').val();

	var $btnToggle = $('.btn_toggle');

	// 모니터링 상세버튼 이벤트 처리
	$moveMonitorDetail.on('click', function () {
		location.href = '/monitor/monitor/monitorDetail?deviceSeq=' + deviceSeq;
	});

	// 설치장비 상세버튼 이벤트 처리
	$moveInstallDeviceDetail.on('click', function () {
		location.href = '/install/installDevice/installDeviceDetail?deviceSeq=' + deviceSeq;
	});

	// 최신순 / 등록순 정렬 변경시
	$searchType.on('change', function () {
		refreshDetailPage();
	});

	// 년도 변경시
	$searchYear.on('change', function () {
		refreshDetailPage();
	});

	$('#moveListBtn').unbind();
	$('#moveListBtn').on('click', function () {
		location.href = '/manage/maintain/maintainList?' + queryString;
	});

	function refreshDetailPage() {
		var parameter = $searchForm.serialize();
		var param = queryString;
		param = Common.removeStringParam(param, "deviceSeq");
		param = Common.removeStringParam(param, "searchYear");
		param = Common.removeStringParam(param, "searchType");
		location.href = '/manage/maintain/maintainDetail?' + parameter + '&' + param;
	}

	$btnToggle.on('click', function () {
		var $li = $(this).closest('li');
		if ($li.hasClass('on')) {
			$li.removeClass('on');
		} else {
			$li.addClass('on');
		}
	});

	// 유지보수 리포트버튼 이벤트 처리
	$openMaintainReportPopup.on('click', function () {
		var parameter = $searchForm.serialize();
		Common.openPopup('/manage/maintain/maintainReportPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("유지보수 보고서", result, {width: 860});
			$('#popupList').append($popup);

			var $printPopup = $popup.find('#printPopup');
			var $sendMail = $popup.find('#sendMail');

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			$printPopup.on('click', function () {
				$printPopup.hide();
				Common.showLoading();
				$('#printPage').printThis({
					afterPrint: function () {
						$printPopup.show();
						Common.hideLoading();
					}
				});
			});

			// 타이밍 이슈가 있어서 그냥 시간 줌
			setTimeout(function () {
				Common.resizeHeightPopup($popup);
			}, 10)

			$sendMail.on('click', function () {

				// 유지보수 리포트 팝업열었을때와 동일한 parameter를 가져감
				Common.openPopup('/manage/maintain/maintainReportSendMailPopup', {parameter: parameter}, function (result) {

					let $popup2 = Common.getPopupTemplate("메일발송", result, {width: 750});
					let $send = $popup2.find('#send');

					$popup2.find('.popup-close-btn').on('click', function () {
						$popup2.remove();
					});

					$('#popupList2').append($popup2);

					Common.resizeHeightPopup($popup2);

					$send.on('click', function () {
						Common.showLoading();
						let mailTitle = $popup2.find('#mailTitle').val();

						// 제목 미입력시 유지보수 보고서 디폴트 설정
						if(Valid.isEmpty(mailTitle)){
							$popup2.find('#mailTitle').val("유지보수 보고서");
						}

						Common.callAjax("/manage/maintain/api/maintainReportSendMail", $('#sendMailPage').serializeObject(), function (result) {
							alert("정상적으로 발송되었습니다.");
							Common.hideLoading();
							$popup2.remove();
						});

					});

				});
			});


		});
	});

});