/**
 *  Create By Kim YunHong on 2020-03-30 / 오후 05:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function(){
    var $openLocationListPopupBtn = $('#openLocationListPopupBtn');
    var $saveComplainBtn = $("#saveComplainBtn");
    var $cancelBtn = $("#cancelBtn");

    var $complainFile = $('#complainFile');
    var $complainFileList = $('#complainFileList');

    // 사진 클릭시 슬라이더 팝업 오픈
    $('.upload-image').on('click', function () {

        var parameter = {
            domain : 'COMPLAIN',
            key : $('input[name="complainSeq"]').val(),
            index : $(this).closest('li').index()
        };

        Common.openPopup('/common/imageSliderPopup', {parameter : parameter}, function (result) {
            var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto : true});
            $('#popupList').append($popup);

            $popup.find('.popup-close-btn').on('click', function () {
                $popup.remove();
            });
            Common.resizeHeightPopup($popup);
        });
    });

    // 민원저장 처리
    $saveComplainBtn.on('click', function () {
        if (confirm('저장 하시겠습니까?')) {
            Common.showLoading();
            Common.callAjax("/manage/complain/api/setComplain", $('#complainForm').serializeObject(), function (result) {
                alert("저장이 완료 되었습니다.");
                location.href = '/manage/complain/complainDetail?' + 'complainSeq=' + (result.complainSeq);
            });
        }
    });

    // 민원등록 취소처리
    $cancelBtn.on('click', function () {
        if (confirm('취소 하시겠습니까?')) {
            Common.showLoading();
            location.href = '/manage/complain/complainList';
        }
    });

    // 민원사진 버튼 처리
    $complainFile.on('change', function () {
        if($complainFileList.find('li').length >= 3){
            alert("사진은 최대 3개까지 등록 가능합니다.");
            return false;
        }
        Common.photoUpload($complainFile, function(result){
            if(result == null ){
                alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
            }

            $complainFile.val(null);
            var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
                '\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
                '\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
                '\t\t\t\t\t\t\t\t\t<input type="text" name="uploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
                '\t\t\t\t\t\t\t\t</li>\n');

            $complainFileList.append($html);
        });
    });

    // 파일 삭제 이벤트 처리
    $complainFileList.on('click', '.btn_img_del', function () {
        $(this).parent('li').remove();
    });
    
    // 설치장소 팝업 버튼 처리
    $openLocationListPopupBtn.on('click', function () {
        openLocationListPopup();
    });

    // 설치장소 팝업
    function openLocationListPopup(){
        Common.openPopup('/common/locationListPopup', {}, function (result) {
            var $popup = Common.getPopupTemplate("설치장소", result, {width : 860});
            $('#popupList').append($popup);

            var locationColumns = [
                {header: '지역', name: 'locationAreaText', width: 70, sortable: true, align: 'center'},
                {header: '설치장소', name: 'locationName', width: 100, sortable: true, align: 'left'},
                {header: '거래처구분', name: 'customerTypeText', width: 80, sortable: true, align: 'center'},
                {header: '거래처', name: 'customerName', width: 100, sortable: true, align: 'left'},
                {header: '주소', name: 'addressText', width: 300, sortable: true, align: 'left'},
                {header: '영업담당', name: 'salesUserName', width: 70, sortable: true, align: 'cen1ter'},
                {header: '설치장비수', name: 'deviceCnt', width: 80, sortable: true, align: 'right'},
            ];

            var locationGrid = Grid.createGrid({
                id: 'locationGrid',
                columns: locationColumns,
                data: [],
                bodyHeight: 300
            });

            getLocationList();

            function getLocationList (){
                Common.callAjax("/customer/location/api/getLocationList", $popup.find('#searchLocationForm').serializeObject(), function (result) {
                    locationGrid.resetData(result);
                    $popup.find('#locationTotalCount').text(result == null ? 0 : Common.addComma(result.length));
                });
            }

            // 로우 선택시 이벤트 처리
            locationGrid.on('click', function (result) {
                var row = result['rowKey'];
                var prevRow = result['prevRowKey'];
                if (row == null) {
                    return false;
                }
                var record = locationGrid.getRow(row);

                $('input[name="locationName"]').val(record.locationName + ' ' + record.address);
                $('input[name="locationSeq"]').val(record.locationSeq);
                $('input[name="receiptCustomerSeq"]').val(record.customerSeq);
                customerEmail = record.customerEmail;
                $popup.remove();
            });

            $popup.find('#searchLocationBtn').on('click', function () {
                getLocationList();
            });

            $popup.find('.popup-close-btn').on('click', function () {
                $popup.remove();
            });
            Common.resizeHeightPopup($popup);
        }, {})
    }

    //신청인 본인,  대리신청 체크박스 선택시 이벤트
    $("input[name='substituteYn']").change(function(){
       if($("input[name='substituteYn']:checked").val() == "Y"){
           $("#substitute").attr("disabled",true);
           $("#substitute").val("");
           $("input[name='email']").val(sessionUserEmail);
       }else{
           $("#substitute").attr("disabled",false);
           $("input[name='email']").val("");
       }
    });

    //신청인 대리신청 셀렉트박스 변경시 이벤트
    $("select[name='substitute']").change(function(e){
        if(Valid.isNotEmpty(this.value)){
            $("input[name='email']").val(customerEmail);
        }else{
            $("input[name='email']").val("");
        }

    });

});
