/**
 *  Create By Kim YunHong on 2020-03-26 / 오전 09:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');
	var $resetBtn = $('#resetBtn');
	var $excelDownloadBtn = $("#excelDownloadBtn");
	var $openAsUserListPopupBtn = $("#openAsUserListPopupBtn");
	var $addComplainBtn = $("#addComplainBtn");
	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');
	var $searchForm = $('#searchForm');
	var $complainTypeList = $("input[name=complainTypeList]");
	var $statusList = $("input[name=statusList]");
	var $searchForm = $('#searchForm');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	$('#dateBtn1').on('click', function () {
		picker.setStartDate(new Date());
		picker.setEndDate(new Date());
	});
	$('#dateBtn2').on('click', function () {
		picker.setStartDate(new Date().addDate(0, 0, -7));
		picker.setEndDate(new Date());
	});
	$('#dateBtn3').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -1, 0));
		picker.setEndDate(new Date());
	});
	$('#dateBtn4').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -3, 0));
		picker.setEndDate(new Date());
	});

	//체크박스(민원유형) 전체 클릭시 이벤트 처리
	$('#total_chk').on('click', function () {
		$complainTypeList.prop("checked", $("#total_chk").is(':checked'));
	});

	// 체크박스(민원유형) 클릭시 '전체' 항목 체크 이벤트
	$complainTypeList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=complainTypeList]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=complainTypeList]:checked").length;

		$("#total_chk").prop("checked", (chkCnt == totalCnt));
	});

	//체크박스(민원상태) 전체 클릭시 이벤트 처리
	$('#total_chk2').on('click', function () {
		if ($("#total_chk2").is(':checked')) {
			$statusList.prop("checked", true);
		} else {
			$statusList.prop("checked", false);
		}
	});

	// 체크박스(민원상태) 클릭시 '전체' 항목 체크 이벤트
	$statusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=statusList]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=statusList]:checked").length
		$("#total_chk2").prop("checked", (chkCnt == totalCnt));
	});

	var complainColumns = [
		{header: '신청일', name: 'receiptDt', minWidth: 80, sortable: true, align: 'center'},
		{header: '민원유형', name: 'complainTypeText', minWidth: 80, sortable: true, align: 'center'},
		{header: '제목', name: 'title', minWidth: 265, sortable: true, align: 'left'},
		{header: '지역', name: 'locationAreaText', width: 90, sortable: true, align: 'center'},
		{header: '설치장소', name: 'locationName', minWidth: 180, sortable: true, align: 'left'},
		{header: '거래처 구분', name: 'customerTypeText', minWidth: 80, sortable: true, align: 'left'},
		{header: '거래처', name: 'customerName', minWidth: 100, sortable: true, align: 'left'},
		{header: '신청구분', name: 'substituteText', minWidth: 100, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', minWidth: 80, sortable: true, align: 'center'},
		{header: '상태', name: 'statusText', minWidth: 80, sortable: true, align: 'center'},
		{header: '보수 담당자', name: 'asUserName', width: 120, sortable: true, align: 'center'}
	];

	var complainGrid = Grid.createGrid({
		id: 'complainGrid',
		columns: complainColumns,
		data: []
	});

	// 거래처 로우 선택시 이벤트 처리
	complainGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null) {
			return false;
		}
		Common.showLoading();
		var record = complainGrid.getRow(row);
		location.href = '/manage/complain/complainDetail?complainSeq=' + record["complainSeq"] + '&' + $searchForm.serializeListParameter();
	});

	$searchBtn.on('click', function () {
		getComplainList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
		$('input[type="hidden"]').val("");
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = complainColumns;

		var option = {
			fileName: '민원'
		};
		Common.excelDownload("/manage/complain/excel/getComplainList", parameter, option);
	});

	//민원신청 버튼 이벤트 처리
	$addComplainBtn.on('click', function () {
		Common.showLoading();
		location.href = '/manage/complain/complainWrite';
	});

	getComplainList();

	// 민원신청 데이터 로드
	function getComplainList() {
		if ($('#searchStartDate').val() == '' && $('#searchEndDate').val() == '') {
			picker.setStartDate(new Date().setDate(1));
			picker.setEndDate(new Date().addDate(0, 1, 0).setDate(0));
		}

		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		Common.showLoading();
		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "complainList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/manage/complain/api/getComplainList", parameter, function (result) {
			complainGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// AS 담당자 버튼 처리
	$openAsUserListPopupBtn.on('click', function () {
		openAsUserListPopup();
	});

	// AS담당자 리스트 선택 팝업
	function openAsUserListPopup(parameter) {
		Common.openPopup('/common/asUserListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("보수담당자", result, {width: 600});
			$('#popupList').append($popup);

			var $searchAsUserForm = $popup.find('#searchAsUserForm');
			var asUserColumns = [
				{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
				{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
				{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
				{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
			];

			var asUserGrid = Grid.createGrid({
				id: 'asUserGrid',
				columns: asUserColumns,
				data: [],
				bodyHeight: 300
			});

			getAsUserList();

			function getAsUserList() {
				Common.callAjax("/standard/user/api/getAsUserList", $searchAsUserForm.serializeObject(), function (result) {
					asUserGrid.resetData(result);
					$popup.find('#asUserTotalCount').text(result != null ? Common.addComma(result.length) : 0);
				});
			}

			// 로우 선택시 이벤트 처리
			asUserGrid.on('click', function (result) {
				var row = result['rowKey'];
				if (row == null) {
					return false;
				}
				var record = asUserGrid.getRow(row);

				$popup.remove();
				$('input[name="asUserName"]').val(record.name);
				$('input[name="asUserId"]').val(record.userId);
			});

			$popup.find('#searchAsUserBtn').on('click', function () {
				getAsUserList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			Common.resizeHeightPopup($popup);
		}, {})
	}
});
