/**
 *  Create By Kim YunHong on 2020-03-30 / 오후 02:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/



$(function(){

    var $saveBtn = $("#saveBtn");  // 저장
    var $cancleListBtn = $("#cancleListBtn");      // 취소
    var $complainFileList = $('.complainFileList');
    var $complainFile = $('.complainFile');
    var $addPartBtn = $(".addPartBtn");

    // 저장버튼 이벤트 처리
    $saveBtn.on('click', function () {

        var $tableList = $('.handling-table');
        var complainHandlingList = [];
        var complainSeq = $('input[name="complainSeq"]').val();
        var status = $('input[name="status"]').val();

        $.each($tableList, function (tableIndex, table) {
            var $table = $(table);
            complainHandlingList.push($table.find('form').serializeObject());
            var addpartList = [];
            $.each($table.find('.addpart-item'), function(index, item){
                var $item = $(item);
                var value = {};
                value["partType"] = $item.find('input[name="partType"]').val();
                value["partQty"] = $item.find('input[name="partQty"]').val();
                addpartList.push(value);
            });
            complainHandlingList[tableIndex].addPartList = addpartList;
        });

        var parameter = {
            handlingList: complainHandlingList,
            complainSeq : complainSeq,
            status : status
        };

        if (confirm('저장 하시겠습니까?')) {
            Common.showLoading();
            Common.callAjax("/manage/complain/api/setComplainHandle", parameter, function (result) {
                alert("저장이 완료 되었습니다.");
                location.href = '/manage/complain/complainDetail?' + 'complainSeq=' + (complainSeq);
            });
        }
    });

    // 취소버튼 이벤트 처리
    $cancleListBtn.on('click', function () {
        location.href = '/manage/complain/complainList';
    });

    // 추가부품 추가버튼 이벤트 처리
    $addPartBtn.on('click',function(){
        var $this = $(this);

        openAddPartListPopup($this);

    });

    // 사진 클릭시 슬라이더 팝업 오픈
    $('.upload-image').on('click', function () {

        var parameter = {
            domain : $(this).closest('ul').prop('id'),
            key : $('input[name="complainSeq"]').val(),
            index : $(this).closest('li').index()
        };

        Common.openPopup('/common/imageSliderPopup', {parameter : parameter}, function (result) {
            var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto : true});
            $('#popupList').append($popup);

            $popup.find('.popup-close-btn').on('click', function () {
                $popup.remove();
            });
            Common.resizeHeightPopup($popup);
        });
    });

    // 민원사진 버튼 처리
    $complainFile.on('change', function () {

        var $this = $(this);
        var $FileList = $this.siblings('.complainFileList');

        if($FileList.find('li').length >= 3){
            alert("사진은 최대 3개까지 등록 가능합니다.");
            return false;
        }
        Common.photoUpload($this, function(result){
            if(result == null ){
                alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
            }

            $this.val(null);
            var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
                '\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
                '\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
                '\t\t\t\t\t\t\t\t\t<input type="text" name="uploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
                '\t\t\t\t\t\t\t\t</li>\n');

            $FileList.append($html);
        });
    });

    // 파일 삭제 이벤트 처리
    $complainFileList.on('click', '.btn_img_del', function () {
        $(this).parent('li').remove();
    });



    //추가부품 팝업 OPEN
    function openAddPartListPopup($this) {
        Common.openPopup('/manage/complain/addPartListPopup?domainName=COMPLAIN_HANDLING&domainSeq=1&domainSeq2=1205', {}, function (result) {
            var $popup = Common.getPopupTemplate("부품교체", result, {width: 600});
            $('#popupList').append($popup);

            $popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
                $popup.remove();
            });

            $popup.find('#saveBtn').on('click', function () {
                var $addPartList = $popup.find('.add-part-item');
                $.each($addPartList, function (index, item) {
                    var $item = $(item);

                    if($item.find('input[name="partQty"]').val() > 0){

                        var html = '<span class="addpart-item">' +
                            '<span> ' + $item.find('input[name="partTypeText"]').val() + ' ' + $item.find('input[name="partQty"]').val() + '개' + ' </span>' +
                            '<input type="text" name="partType" value="' + $item.find('input[name="partType"]').val() + '" />  ' +
                            '<input type="text" name="partQty" value="' + $item.find('input[name="partQty"]').val() + '" />' +
                            '</span> ';
                        $this.siblings('.addpart-list').append(html);
                    }

                });

                $popup.remove();
            });

            $popup.find('.minusBtn').on('click',function(){
                var $this = $(this);
                var qty = $this.siblings('.qty').val() -1;
                if(qty < 0 ){return;}
                $this.siblings('.qty').val(qty);
            });

            $popup.find('.plusBtn').on('click',function(){
                var $this = $(this);
                var $target = $this.siblings('.qty');
                var qty = parseInt($target.val());
                qty = qty + 1;
                $target.val(qty)
            });

            Common.resizeHeightPopup($popup);
        }, {})
    }
});
