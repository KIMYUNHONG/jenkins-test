/**
 *  Create By Kim YunHong on 2020-03-30 / 오후 02:55
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $modifyBtn = $("#modifyBtn");  // 수정
	var $moveListBtn = $("#moveListBtn");      // 목록
	var $receiptBtn = $("#receiptBtn");      // 접수
	var $finishBtn = $("#finishBtn");        // 처리완료
	var $addListBtn = $("#addListBtn");        // 추가
	var $modifyListBtn = $("#modifyListBtn");      // 수정
	var $locationMapBtn = $("#locationMapBtn");  // 설치장소 지도 버튼
	var queryString = $('input[name="queryString"]').val();
	var $complainForm = $('#complainForm');

	// 처리내역 수정버튼 이벤트 처리
	$modifyListBtn.on('click', function () {
		var complainSeq = $("#complainSeq").val();
		if (confirm('수정 하시겠습니까?')) {
			var param = queryString;
			param = Common.removeStringParam(param, "complainSeq");
			param = Common.removeStringParam(param, "deviceSeqList");
			location.href = '/manage/complain/complainHandling?' +'complainSeq='+ complainSeq + '&'+  param;
		}
	});

	// 사진 클릭시 슬라이더 팝업 오픈
	$('.upload-image').on('click', function () {
		var $this = $(this);

		// 민원 본문 이미지 클릭시
		if (!$this.hasClass('complain-handle-img')) {
			console.log($this.closest('ul').prop('id'))
			openPhotoPopup($this.closest('ul').prop('id'), $('input[name="complainSeq"]').val(), 0, $this.closest('li').index());
		} else { // 민원 처리 내역 이미지 클릭시
			openPhotoPopup('COMPLAIN_HANDLE', $('input[name="complainSeq"]').val(), $this.siblings('input[name="complainHandleSeq"]').val(), 0);
		}
	});

	// 목록버튼 이벤트 처리
	$moveListBtn.on('click', function () {
		location.href = '/manage/complain/complainList?' + queryString;
	});

	// 수정버튼 이벤트 처리
	$modifyBtn.on('click', function () {
		var complainSeq = $("#complainSeq").val();
		if (confirm('수정 하시겠습니까?')) {
			var param = queryString;
			param = Common.removeStringParam(param, "complainSeq");
			location.href = '/manage/complain/complainWrite?' + 'complainSeq='+complainSeq + '&' +param;
		}
	});

	// 접수버튼 이벤트 처리
	$receiptBtn.on('click', function () {
		//접수처리
		if (confirm('민원을 접수 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/manage/complain/api/setReceiptComplain", $complainForm.serializeObject(), function (result) {
				alert("접수가 완료 되었습니다.");
				location.href = '/manage/complain/complainDetail?' + queryString;
			});
		}
	});

	// 처리완료 이벤트 처리
	$finishBtn.on('click', function () {
		//처리완료
		if (confirm('처리완료시 더 이상 장비정보를 변경 할 수 없습니다.\n' + '처리완료 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/manage/complain/api/setFinishComplain", $complainForm.serializeObject(), function (result) {
				alert("처리완료되었습니다.");
				location.href = '/manage/complain/complainDetail?' + queryString;
			});
		}
	});

	// 설치장소 지도 버튼클릭
	$locationMapBtn.on('click', function () {
		openLocationMapPopup({locationSeq: locationSeq})
	});

	$addListBtn.on('click', function () {
		openDeviceListPopup({locationSeq: locationSeq});
	});


	//설치장소 지도팝업 OPEN
	function openLocationMapPopup(parameter) {
		Common.openPopup('/customer/location/locationMapPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("설치장소", result, {width: 600});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	//설치장소 장비팝업 OPEN
	function openDeviceListPopup(parameter) {
		Common.openPopup('/manage/complain/complainDeviceListPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("민원처리", result, {width: 600});
			$('#popupList').append($popup);

			var deviceColumns = [
				{header: '장비번호', name: 'deviceId', minWidth: 150, sortable: true, align: 'center'},
				{header: '장비명', name: 'deviceNameText', minWidth: 120, sortable: true, align: 'left'},
				{header: '관리번호', name: 'manageNo', minWidth: 100, sortable: true, align: 'center'},
				{header: '가로등번호', name: 'lampNo', minWidth: 100, sortable: true, align: 'center'},
			];

			var deviceGrid = Grid.createGrid({
				id: 'deviceGrid',
				columns: deviceColumns,
				data: [],
				rowHeaders: ['checkbox'],
				bodyHeight: 300
			});

			getDeviceList();

			function getDeviceList() {
				var parameter = {
					locationSeq: $popup.find('input[name="locationSeq"]').val()
				};
				Common.callAjax("/device/device/api/getInstallDeviceListByLocation", parameter, function (result) {

					// 기존에 처리로 등록된 장비 리스트
					var $existDeviceList = $('input[name="deviceSeq"]');

					// 기존에 민원 처리한 장비는 리스트에 안나오도록 필터링
					var deviceList = [];
					if (result != null) {
						for (var i = result.length - 1; i >= 0; i--) {
							var existFlag = false;
							$.each($existDeviceList, function (existIndex, existItem) {
								if (result[i].deviceSeq == Number($(existItem).val())) {
									existFlag = true;
								}
							});
							if (!existFlag) {
								deviceList.push(result[i]);
							}
						}
					}
					deviceGrid.resetData(deviceList);
					$popup.find('deviceTotalCount').text(deviceList != null ? Common.addComma(deviceList.length) : 0);
				});
			}

			$popup.find('#addBtn').on('click', function () {
				var checkedRows = deviceGrid.getCheckedRows();
				if (checkedRows.length == 0) {
					alert("추가할 장비를 선택해주세요.");
					return false;
				}

				var deviceList = '';
				var complainSeq = $("#complainSeq").val();
				$.each(checkedRows, function (index, value) {
					deviceList += 'deviceSeqList=' + value.deviceSeq + '&';
				});

				deviceList = deviceList.substring(0, deviceList.length - 1);
				var param = queryString;
				location.href = '/manage/complain/complainHandling?' + deviceList + '&' + param;
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	function openPhotoPopup(domain, key, key2, index) {
		key2 = key2 == null ? 0 : key2;
		var parameter = {
			domain: domain,
			key: key,
			key2: key2,
			index: index
		};

		Common.openPopup('/common/imageSliderPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto: true});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	}

});
