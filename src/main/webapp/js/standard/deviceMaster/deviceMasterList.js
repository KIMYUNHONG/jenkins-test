/**
 *  Create By Kim Yun Hong on 2020-02-24 / 오후 5:34
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $viewAllDeviceMasterFlag = $('#viewAllDeviceMasterFlag');			// 미사용포함 checbkox
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $resetBtn = $('#resetBtn'); // 검색조건 초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼excelDownloadBtn

	var endDate = new Date();
	var startDate = new Date().addDate(-1, 0, 0);

	var picker = Component.setRangePicker({
		startpicker: {
			date: '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var deviceMasterColumns = [
		{header: '장비구분', minWidth: 150, name: 'deviceClassifyText', sortable: true, align: 'center'},
		{header: '장비명', minWidth: 300, name: 'deviceNameText', sortable: true, align: 'left'},
		{header: '모델명', minWidth: 300, name: 'modelName', sortable: true, align: 'left'},
		{header: '제조사구분', minWidth: 150, name: 'manufactureComClassifyText', sortable: true, align: 'center'},
		{header: '제조사', minWidth: 150, name: 'manufactureComText', sortable: true, align: 'center'},
		{header: '사용여부', name: 'useYnText', minWidth: 100, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', minWidth: 130, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth: 130, sortable: true, align: 'center'},
		{header: '상세',name: 'modifyBtn',minWidth: 130,sortable: true,	align: 'center',
			renderer: Grid.buttonRenderer({text: '상세', class: 'btn_type3'}, function () {
				var row = (deviceMasterGrid.getFocusedCell()['rowKey'] || 0);
				var record = deviceMasterGrid.getRow(row);
				setDeviceMasterPopup(record["deviceMstSeq"])
			}),
			noExcel: true
		}
	];

	var deviceMasterGrid = Grid.createGrid({
		id: 'deviceMasterGrid',
		columns: deviceMasterColumns,
		data: []
	});

	getDeviceMasterDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getDeviceMasterDataList();
	});

	// 검색조건 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$('#searchForm')[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	// 장비마스터 마사용 포함 이벤트
	$viewAllDeviceMasterFlag.on('change', function () {
		getDeviceMasterDataList();
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {

		var parameter = $('#searchForm').serializeObject();
		parameter.gridColumnList = deviceMasterColumns;

		var option = {
			fileName: '장비마스터'
		};
		Common.excelDownload("/standard/deviceMaster/excel/getDeviceMasterList", parameter, option);
	});

	// 장비마스터 데이터 로드
	function getDeviceMasterDataList() {

		// 검색 Validation
		var parameter = $('#searchForm').serializeObject();

		// 기간 종류를 선택안하고 날짜를 선택하거나, 기간종류만 선택하고 날짜를 선택안할시
		if ((Valid.isNotEmpty(parameter.dateType) && (Valid.isEmpty(parameter.searchStartDate) || Valid.isEmpty(parameter.searchEndDate)))
			|| (Valid.isEmpty(parameter.dateType) && (Valid.isNotEmpty(parameter.searchStartDate) || Valid.isNotEmpty(parameter.searchEndDate)))) {
			alert("기간을 선택해주세요.");
			return false;
		}

		Common.callAjax("/standard/deviceMaster/api/getDeviceMasterList", parameter, function (result) {
			deviceMasterGrid.resetData(result);
			$('#totalCount').text(result != null ? Common.addComma(result.length) : 0);
		});
	}

	// 등록 버튼 이벤트 처리
	$('#addDeviceMasterBtn').on('click', function () {
		setDeviceMasterPopup();
	});

	function setDeviceMasterPopup(seq) {

		seq = seq == null ? 0 : seq;
		var parameter = {deviceMstSeq: seq};
		var deviceMst = {};

		// seq가 0인경우 조회가 의미가 없어서 조회 X
		if (seq > 0) {
			Common.callAjax("/standard/deviceMaster/api/getDeviceMaster", parameter, function (result) {
				deviceMst = result;
			}, {async: false});
		}
		$('input[name="deviceMstSeq"]').val(seq);

		Common.openPopup('/standard/deviceMaster/addDeviceMasterPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비마스터", result);
			var $deviceClassify = $popup.find('input[name="deviceClassify"]');
			var deviceClassifyCode = $popup.find("#deviceClassifyCode").val();

			// 팝업 오픈시 유형에 에 해당하는 input만 보여지게 설정
			if (deviceClassifyCode == "SD020002") {
				$popup.find('.' + 'SD020001').hide();
			} else {
				$popup.find('.' + 'SD020002').hide();
			}

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);

			// 장비구분 라디오 버튼 클릭시 이벤트
			$deviceClassify.on('change', function () {
				bindTableComponentByDeviceClassify();
				$popup.find('select').val('');
				$popup.find('input[type=text]').val('');
			});

			// 장비구분 라디오 버튼 클릭시 장비구분에 해당하는 input만 보여주도록 처리
			function bindTableComponentByDeviceClassify() {
				var deviceClassify = $('input[name="deviceClassify"]:checked').val();
				$popup.find('.' + 'SD020001').hide();
				$popup.find('.' + 'SD020002').hide();
				$popup.find('.' + deviceClassify).show();
			}

			$popup.find('#deviceMasterSaveBtn').on('click', function () {
				if (confirm('저장 하시겠습니까?')) {
					Common.callAjax("/standard/deviceMaster/api/setDeviceMaster", $('#form').serializeObject(), function (result) {
						alert("저장이 완료 되었습니다.");
						$popup.remove();
						getDeviceMasterDataList();
					});
				}
			});
			$popup.find('#deviceMasterCancelBtn').on('click', function () {
				if (confirm('취소 하시겠습니까?')) {
					$popup.remove();
				}
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

});