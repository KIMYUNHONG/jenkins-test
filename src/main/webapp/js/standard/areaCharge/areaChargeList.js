/**
 *  Create By Lee DoWon on 2020-03-31 / 오전 10:18
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $resetBtn = $('#resetBtn');							// 초기화버튼
	var $saveAreaChargeBtn = $('#saveAreaChargeBtn');		// 저장 버튼
	var $openModifyAreaChargePopupBtn = $('#openModifyAreaChargePopupBtn');		// 일괄변경 팝업 버튼
	var $excelDownloadBtn = $('#excelDownloadBtn');
	var $searchForm = $('#searchForm');

	var asUserList = [];

	// 유지보수 담당자 전체 리스트 조회 api 호출
	Common.callAjax("/standard/user/api/getAsUserList", {}, function (result) {
		$.each(result, function(index, value){
			asUserList.push({code :  value.userId, codeName : value.name})
		});
		Common.hideLoading();
	}, {async : false});

	var picker = Component.setRangePicker({
		startpicker: {
			date: '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format : "yyyy.MM.dd",
		language : 'ko'
	});

	var editor = Grid.selectEditor(asUserList, {useEmptyValue: true, useEmptyValueText : '선택'});
	var areaChargeColumns = [
		{header: '시/도', name: 'areaText', minWidth:80, sortable: true, align: 'center'},
		{header: '구/군', name: 'areaDetailText', minWidth:100, sortable: true, align: 'center'},
		{header: '설치담당', name: 'installUserId', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor, hidden: true},
		{header: '보수담당1', name: 'asUserId1', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor},
		{header: '보수담당2', name: 'asUserId2', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor},
		{header: '등록자', name: 'uptName', minWidth:90, sortable: true, align: 'center'},
		{header: '등록일', name: 'uptDtText', minWidth:90, sortable: true, align: 'center'}
	];

	var areaChargeExcelColumns = [
		{header: '시/도', name: 'areaText', minWidth:80, sortable: true, align: 'center'},
		{header: '구/군', name: 'areaDetailText', minWidth:100, sortable: true, align: 'center'},
		{header: '설치담당', name: 'installUserName', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor},
		{header: '보수담당1', name: 'asUserName1', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor},
		{header: '보수담당2', name: 'asUserName2', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: editor},
		{header: '등록자', name: 'uptName', minWidth:90, sortable: true, align: 'center'},
		{header: '등록일', name: 'uptDtText', minWidth:90, sortable: true, align: 'center'}
	];

	var areaChargeGrid = Grid.createGrid({
		id: 'areaChargeGrid',
		columns: areaChargeColumns,
		rowHeaders: ['checkbox'],
		data: []
	});

	areaChargeGrid.on('editingFinish', function(data){
		areaChargeGrid.check(data.rowKey);
	});

	$openModifyAreaChargePopupBtn.on('click', function () {

		// 체크 박스 개수 확인
		var checkedRows = areaChargeGrid.getCheckedRows();
		if(checkedRows.length == 0 ){
			alert("변경할 지역을 선택해주세요");
			return false;
		}

		Common.openPopup('/standard/areaCharge/modifyAreaChargePopup', {parameter : {}}, function (result) {
			var $popup = Common.getPopupTemplate("담당자 일괄변경", result, {width: 360});
			$('#popupList').append($popup);

			$popup.find('#saveBtn').on('click', function () {

				var $installUserId = $popup.find('select[name="installUserId"]');
				var $asUserId1 = $popup.find('select[name="asUserId1"]');
				var $asUserId2 = $popup.find('select[name="asUserId2"]');

				if($installUserId.val() == '' && $asUserId1.val() == '' && $asUserId2.val() == '' ){
					alert('변경할 지역별 담당자를 선택해주세요.');
					return false;
				}

				$.each(checkedRows, function (index, record) {
					if($installUserId.val() != ''){
						record.installUserId = $installUserId.val();
						record.installUserName = $installUserId.find('option:selected').text();
					}
					if($asUserId1.val() != ''){
						record.asUserId1 = $asUserId1.val();
						record.asUserName1 = $asUserId1.find('option:selected').text();
					}
					if($asUserId2.val() != ''){
						record.asUserId2 = $asUserId2.val();
						record.asUserName2 = $asUserId2.find('option:selected').text();
					}
					areaChargeGrid.setRow(record._attributes.rowNum - 1, record);
				});

				if(confirm("저장 하시겠습니까?")) {
					setAreaChargeList();
					$popup.remove();
				}
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = areaChargeExcelColumns;

		var option = {
			fileName : '지역별담당자'
		};
		Common.excelDownload("/standard/areaCharge/excel/getAreaChargeList", parameter, option);
	})

	$saveAreaChargeBtn.on('click', function () {

		areaChargeGrid.blur();
		setTimeout(function () {
			setAreaChargeList();
		}, 100);
	});

	$searchBtn.on('click', function () {
		getAreaChargeList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	getAreaChargeList();

	// 지역별 담당자 리스트 조회 api 호출
	function getAreaChargeList() {
		Common.showLoading();
		var parameter = $searchForm.serializeObject();

		Common.callAjax("/standard/areaCharge/api/getAreaChargeList", parameter, function (result) {
			areaChargeGrid.resetData(result);
			$('#totalCount').text(result != null ? Common.addComma(result.length) : 0);
			Common.hideLoading();
		});
	}
	function setAreaChargeList(){

		// 체크 박스 개수 확인
		var checkedRows = areaChargeGrid.getCheckedRows();
		if(checkedRows.length == 0 ){
			alert("저장할 지역을 선택해주세요");
			return false;
		}

		Common.callAjax("/standard/areaCharge/api/setAreaChargeList", {updatedRows : checkedRows}, function (result) {
			alert(result + '건 저장이 완료되었습니다.');
			getAreaChargeList();
		});
	}

});