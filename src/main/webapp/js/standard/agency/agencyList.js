/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $viewAllCustomerFlag = $('#viewAllCustomerFlag');	// 미사용포함 checkbox
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $addCustomerBtn = $('#addCustomerBtn');			// 거래처 등록
	var $resetBtn = $('#resetBtn');                    // 검색조건 초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');
	var $searchForm = $('#searchForm');

	var previousMonthDay = new Date().addDate(0,-1,0);
	var nextMonthDay = new Date().addDate(0,1,0);

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format : "yyyy.MM.dd",
		language : 'ko'
	});

	var customerColumns = [
		{header: '대리점', name: 'customerShortName', minWidth: 180, sortable: true, align: 'left'},
		{header: '담당부서', name: 'representName', minWidth: 100, sortable: true, align: 'center'},
		{header: '영업담당자', name: 'salesUserName', minWidth: 100, sortable: true, align: 'center'},
		{header: '관리장소', name: 'locationCnt', minWidth: 100, sortable: true, align: 'right'},
		{header: '장비', name: 'deviceCnt', minWidth: 100, sortable: true, align: 'right'},
		{header: '대표 전화번호', name: 'customerPhone', minWidth: 120, sortable: true, align: 'center'},
		{header: '이메일', name: 'emailText', minWidth: 280, sortable: true, align: 'left'},
		{header: '계약기간', name: 'contractPeriodText', minWidth: 180, sortable: true, align: 'center'},
		{header: '자료', name: 'existFileYn', width: 60, sortable: false, align: 'center', renderer: Grid.iconRenderer({
					class: 'ic_data'
				},
				function() {
					var row = (customerGrid.getFocusedCell()['rowKey'] || 0);
					var record = customerGrid.getRow(row);
					openFileDownloadPopup("CUSTOMER", record.customerSeq);
				})
		},
		{header: '활동여부', name: 'useYnText', minWidth: 100, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', minWidth: 120, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth: 120, sortable: true, align: 'center'},
	];

	var customerGrid = Grid.createGrid({
		id: 'customerGrid',
		columns: customerColumns,
		data: []
	});

	// 거래처 로우 선택시 이벤트 처리
	customerGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null || result.columnName == "existFileYn") {	 // 자료인경우 자료 팝업을 띄우기위해
			return false;
		}
		var record = customerGrid.getRow(row);
		Common.showLoading();
		location.href = '/standard/agency/agencyDetail?customerSeq=' + record["customerSeq"] + '&' + $searchForm.serializeListParameter();;
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = customerColumns;
		parameter.isAgencyMenu = 'Y';

		var option = {
			fileName : '대리점'
		};
		Common.excelDownload("/customer/customer/excel/getCustomerList", parameter, option);
	});

	getCustomerList();

	// 코드 그룹 데이터 로드
	function getCustomerList() {

		// 검색 Validation
		var parameter = $searchForm.serializeObject();
		parameter.isAgencyMenu = 'Y';

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "agencyList" : "?" + $searchForm.serializeListParameter());

		// 기간 종류를 선택안하고 날짜를 선택하거나, 기간종류만 선택하고 날짜를 선택안할시
		if ((Valid.isNotEmpty(parameter.dateType) && (Valid.isEmpty(parameter.searchStartDate) || Valid.isEmpty(parameter.searchEndDate)))
			|| (Valid.isEmpty(parameter.dateType) && (Valid.isNotEmpty(parameter.searchStartDate) || Valid.isNotEmpty(parameter.searchEndDate)))) {
			alert("기간을 선택해주세요.");
			return false;
		}

		Common.callAjax("/customer/customer/api/getCustomerList", parameter, function (result) {
			customerGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
		});
	}
	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getCustomerList();
	});

	// 검색버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
        picker.setStartDate(null);
        picker.setEndDate(null);
	});


	// 마사용 포함 이벤트 처리
	$viewAllCustomerFlag.on('change', function () {
		getCustomerList();
	});

	// 등록 버튼 이벤트 처리
	$addCustomerBtn.on('click', function () {
		Common.showLoading();
		location.href = '/standard/agency/agencyWrite';
	});

	function openFileDownloadPopup(domain, domainSeq){

		Common.openPopup('/common/downloadFilePopup', {parameter : {domain : domain, domainSeq : domainSeq}}, function (result) {
			var $popup = Common.getPopupTemplate("자료", result, {width: 480});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	}


});