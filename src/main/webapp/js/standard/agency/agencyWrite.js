/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $customerSaveBtn = $('#customerSaveBtn');		// 수정 버튼
	var $customerListBtn = $('#customerListBtn');			// 목록 버튼
	var customerSeq = $('input[name="customerSeq"]').val();			// 거래처_SEQ(KEY)
	var $file = $('#file');
	var $fileList = $('#fileList');
	var $selectEmailDomain = $('select[name="selectEmailDomain"]');
	var queryString = $('input[name="queryString"]').val();

	var picker = Component.setRangePicker({
		startpicker: {
			date: $('#contractStartDt').val() != '' ? new Date(changeDateFormat($('#contractStartDt').val(), '/')) : new Date(),
			input: '#contractStartDt',
			container: '#contractStartDtWrapper'
		},
		endpicker: {
			date: $('#contractEndDt').val() != '' ? new Date(changeDateFormat($('#contractEndDt').val(), '/')) : new Date(),
			input: '#contractEndDt',
			container: '#contractEndDtWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	//화면load시 이메일 readonly 처리
	checkEmailDomain($selectEmailDomain);

	$selectEmailDomain.on('change', function () {
		checkEmailDomain($selectEmailDomain);
	});

	// 저장 버튼 이벤트 처리
	$customerSaveBtn.on('click', function () {
		if (confirm('저장 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/customer/customer/api/setCustomer", $('#customerForm').serializeObject(), function (result) {
				if (result.customerSeq == null || result.customerSeq == 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					return false;
				}
				alert("저장이 완료 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('customerSeq') != -1 ? queryString : 'customerSeq=' + (result.customerSeq);
				location.href = '/standard/agency/agencyDetail?' + param;
			});
		}
	});

	// 파일 추가 버튼 이벤트 처리
	$file.on('change', function () {
		if ($($fileList.find('li')).length >= 5) {
			alert("파일은 최대 5개까지 등록 가능합니다.");
			return false;
		}
		Common.fileUpload($file, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$file.val(null);
			var $html = $('\t\t\t\t\t\t\t<li><a href="/uploadFile/api/download/' + result['uploadFileKey'] + '"' +
				' class="icon img" target="_blank">' + result['originalFileName'] + '</a>' +
				'<button type="button" class="btn_del"></button>' +
				'<input type="hidden" name="uploadFileKey" value="' + result['uploadFileKey'] + '"></li>\n');

			$fileList.append($html);

			if ($fileList.find('li').length == 0) {
				$fileList.removeClass('filename');
			} else {
				if (!$fileList.hasClass('filename')) {
					$fileList.addClass('filename');
				}
			}
		});
	});
	// 파일 삭제 이벤트 처리
	$fileList.on('click', '.btn_del', function () {
		var $this = $(this);

		$this.parent('li').remove();
		if ($fileList.find('li').length == 0) {
			$fileList.removeClass('filename');
		} else {
			if (!$fileList.hasClass('filename')) {
				$fileList.addClass('filename');
			}
		}
	});

	// 목록 버튼 이벤트 처리
	$customerListBtn.on('click', function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();
			if (customerSeq > 0) {
				location.href = '/standard/agency/agencyDetail?' + queryString;
			} else {
				location.href = '/standard/agency/agencyList?' + queryString;
			}
		}
	});

	//이메일 select 박스 변경 이벤트 처리
	$("select[name='selectEmailDomain']").on('change', function () {
		var $this = $(this).val();
		var $emailDomain = $('input[name="emailDomain"]');

		$emailDomain.val($this);
		$emailDomain.prop('readonly', Valid.isNotEmpty($this));
	});

	// 우편번호 검색 팝업
	$('#openSearchAddressPopup').on('click', function () {
		Common.openPopup('/common/searchAddressPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("우편번호 찾기", result, {width: 500, bodyClass: 'nospace'});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);

			DaumAddress.createAddressPopup('daumAddress', {
				popup: $popup,
				address: 'address',
				addressDetail: 'addressDetail'
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	// 영업담당자 팝업 처리
	$('#openSalesUserListPopupBtn').on('click', function () {
		Common.openPopup('/common/salesUserListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("영업담당자", result, {width: 600});
			$('#popupList').append($popup);

			var salesUserColumns = [
				{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
				{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
				{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
				{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
			];

			var salesUserGrid = Grid.createGrid({
				id: 'salesUserGrid',
				columns: salesUserColumns,
				data: [],
				bodyHeight: 300
			});

			getSalesUserList();

			function getSalesUserList() {
				var parameter = $popup.find('#searchForm').serializeObject();

				Common.callAjax("/standard/user/api/getSalesUserList", parameter, function (result) {
					salesUserGrid.resetData(result);
				});
			}

			// 로우 선택시 이벤트 처리
			salesUserGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = salesUserGrid.getRow(row);

				$('input[name="salesUserName"]').val(record.name);
				$('input[name="salesUserId"]').val(record.userId);
				$('input[name="salesCustomerSeq"]').val(record.customerSeq);
				$popup.remove();
			});

			$popup.find('#searchSalesUserBtn').on('click', function () {
				getSalesUserList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	function checkEmailDomain($select) {
		$select.siblings('input[name="emailDomain"]').prop('readonly', !($select.val() == ''));
	}
});