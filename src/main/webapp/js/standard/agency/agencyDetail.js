/**
 *  Create By Lee DoWon on 2020-02-25 / 오후 1:30
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $customerModifyBtn = $('#customerModifyBtn');		// 수정 버튼
	var $customerListBtn = $('#customerListBtn');			// 목록 버튼
	var customerSeq = $('input[name="customerSeq"]').val();			// 거래처_SEQ(KEY)

	var queryString = $('input[name="queryString"]').val();


	// 수정 버튼 이벤트 처리
	$customerModifyBtn.on('click', function () {
		Common.showLoading();
		location.href = '/standard/agency/agencyWrite?' + queryString;
	});

	// 목록 버튼 이벤트 처리
	$customerListBtn.on('click', function () {
		Common.showLoading();
		location.href = '/standard/agency/agencyList?' + queryString;
	});

});