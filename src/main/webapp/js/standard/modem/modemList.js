/**
 *  Create By Lee DoWon on 2020-03-27 / 오후 3:41
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $searchBtn = $('#searchBtn');					// 검색 버튼
	var $resetBtn = $('#resetBtn');						// 초기화버튼
	var $deleteModemBtn = $('#deleteModemBtn');			// 모뎀 삭제 버튼
	var $excelUploadBtn = $('#excelUploadBtn');			// 엑셀 업로드 버튼
	var $excelDownloadBtn = $('#excelDownloadBtn');			// 엑셀 다운로드 버튼
	var $searchForm = $('#searchForm');

	var modemColumns = [
		{header: '모뎀ID', name: 'modemId', minWidth: 120, sortable: true, align: 'center'},
		{header: '연결여부', name: 'connectYnText', minWidth: 80, sortable: true, align: 'center'},
		{header: '연결 장비번호', name: 'deviceId', minWidth: 200, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', minWidth: 120, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth: 100, sortable: true, align: 'center'},
	];

	var modemGrid = Grid.createGrid({
		id: 'modemGrid',
		columns: modemColumns,
		rowHeaders: ['checkbox'],
		data: []
	});

	$searchForm.on('submit', function(){
		getModemDataList();
		return false;
	});
	// 페이지 열리면 리스트 조회
	getModemDataList();

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	$searchBtn.on('click', function () {
		getModemDataList();
	});

	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = modemColumns;

		var option = {
			fileName: '모뎀'
		};
		Common.excelDownload("/standard/modem/excel/getModemList", parameter, option);
	});

	//체크박스 전체 클릭시 이벤트 처리
	$('#checkAll').on('click', function () {
		$("input[name=connectYn]").prop("checked", $("#checkAll").is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$('input[name=connectYn]').on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=connectYn]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=connectYn]:checked").length;

		$("#checkAll").prop("checked", (chkCnt == totalCnt));
	});

	$deleteModemBtn.on('click', function () {

		// 그리드에 체크된 리스트 조회
		var checkedRowList = modemGrid.getCheckedRows();

		if (checkedRowList.length === 0) {
			alert("삭제할 모뎀을 선택해주세요.");
			return false;
		}
		if (confirm("모뎀ID를 삭제하시겠습니까?")) {
			deleteModemList(checkedRowList);
		}
	});

	$excelUploadBtn.on('click', function () {

		Common.openPopup('/standard/modem/modemUploadPopup', {parameter: {}}, function (result) {
			var $popup = Common.getPopupTemplate("장비정보", result, {width: 750});
			$('#popupList').append($popup);

			var $uploadFile = $popup.find('#uploadFile');

			$uploadFile.on('change', function () {

				var fileName = $uploadFile[0].files[0].name;
				if (!(Valid.isExcelFormat(fileName))) {
					alert("xls, xlsx 파일만 등록이 가능합니다.");
					$uploadFile.val('');
					return false;
				}

				Common.fileUpload($uploadFile, function (result) {
					if (result == null) {
						alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
					}
					$popup.find('input[name="uploadFileKey"]').val(result.uploadFileKey);
					$popup.find('input[name="originalFileName"]').val(result.originalFileName);
				});
			});

			$popup.find('#saveBtn').on('click', function () {
				Common.showLoading();
				var parameter = $popup.find('#uploadFrom').serializeObject();
				Common.callAjax("/standard/modem/api/setModemListByExcel", parameter, function (result) {
					alert("업로드하신 " + result.totalRow + "개의 데이터중 " + result.insertRow + "개 등록되었습니다.");
					$popup.remove();
					getModemDataList();
				});
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});

			Common.resizeHeightPopup($popup);
		});
	});


	// 코드 그룹 데이터 로드
	function getModemDataList() {
		Common.showLoading();
		var parameter = $('#searchForm').serializeObject();

		Common.callAjax("/standard/modem/api/getModemList", parameter, function (result) {

			var uncheckedAttr = {
				checkDisabled: true
			};

			// 체크박스 사용여부 걸기
			// 연결이 된 모뎀장비는 체크박스 체크 불가
			$.each(result, function (index, value) {
				value._attributes = value.connectYnText == 'O' ? uncheckedAttr : {};
			});

			modemGrid.resetData(result);
			$('#modemTotalCount').text(result != null ? Common.addComma(result.length) : 0);
			Common.hideLoading();
		});
	}

	// 모뎀 삭제 함수
	function deleteModemList(rows) {

		// 파라미터 준비
		var parameter = {
			modemIdList: []
		};

		$.each(rows, function (index, value) {
			parameter.modemIdList.push(value.modemId);
		});

		Common.callAjax("/standard/modem/api/deleteModemList", parameter, function (result) {
			alert("삭제되었습니다.");
			getModemDataList();
		});
	}

});


