/**
 *  Create By Lee DoWon on 2020-02-18 / 오후 1:34
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $viewAllCodeGroupFlag = $('#viewAllCodeGroupFlag');	// 코드그룹 미사용포함 checkbox
	var $viewAllCodeFlag = $('#viewAllCodeFlag');			// 코드 미사용포함 checbkox
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $codeGroupAddBtn = $('#codeGroupAddBtn');			// 코드그룹 등록
	var $codeGroupSaveBtn = $('#codeGroupSaveBtn');			// 코드그룹 저장
	var $resetBtn = $('#resetBtn');		// 초기화버튼

	var $codeAddBtn = $('#codeAddBtn');			// 코드그룹 등록
	var $codeSaveBtn = $('#codeSaveBtn');			// 코드그룹 저장

	var codeList = {};

	// 필요한 데이터들 조회
	Common.getCodeList(["SC01", "S002"], function (result) {
		Component.setSelectByCodeList(result, {codeGroup: "SC01", name: "codeType"});
		codeList = result;
	}, {async: false});

	var codeGroupColumns = [
		{header: '사용여부', name: 'useYn', width:80, sortable: true, align: 'center', formatter: 'listItemText', editor: Grid.oxSelectEditor()},
		{header: '분류', name: 'codeType', width:100, sortable: true, formatter: 'listItemText'
			, editor: Grid.selectEditor(codeList["SC01"], {useEmptyValue: true, useEmptyValueText : '선택'})},
		{header: '순서', name: 'orderSeqText', width:70, sortable: true, align: 'right', editor: Grid.numberEditor()},
		{header: '코드그룹', name: 'codeGroup', width:90, sortable: true, align: 'center'},
		{header: '코드그룹명', name: 'codeGroupName', width:120, sortable: true, editor: 'text'},
		{header: '비고', name: 'etc', width:125, sortable: true, align: 'left', editor: 'text'},
		{header: '수정일', name: 'uptDtText', width:80, align: 'center', sortable: true},
		{header: '수정자', name: 'uptName', width:70, align: 'center', sortable: true},
		{header: '수정가능여부', name: 'modifyYnText', width:80, align: 'center'}
	];
	var codeColumns = [
		{header: '사용여부', width:70, name: 'useYn', sortable: true, align: 'center', formatter: 'listItemText', editor: Grid.oxSelectEditor()},
		{header: '순서', width:59, name: 'orderSeqText', sortable: true, align: 'right', editor: 'text'},
		{header: '코드', width:100, name: 'code', sortable: true},
		{header: '코드명', width:120, name: 'codeName', sortable: true, editor: 'text'},
		{header: '상위코드', width:120, name: 'parentCode', sortable: true},
		{header: '비고', name: 'etc', width:142, align:'left', sortable: true, editor: 'text'},
		{header: '수정일', name: 'uptDtText', width:100, sortable: true, align: 'center'},
		{header: '수정자', name: 'uptName', width:100, sortable: true, align: 'center'},
	];

	var codeGroupGrid = Grid.createGrid({
		id: 'codeGroupGrid',
		columns: codeGroupColumns,
		data: []
	});
	var codeGrid = Grid.createGrid({
		id: 'codeGrid',
		columns: codeColumns,
		data: []
	});

	// 코드그룹 그리드 select cell시 이벤트
	codeGroupGrid.on('focusChange', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if(row == null){
			return false;
		}

		var record = codeGroupGrid.getRow(row);
		// 행변경 이벤트 감지
		if (row != prevRow && (record["codeGroup"] != null && record["codeGroup"] != '')) {
			getCodeDataList(row);
		}
	});

	getCodeGroupDataList();

	// 코드 추가버튼 이벤트
	$codeAddBtn.on('click', function () {

		// 1. 그룹코드에 데이터가 없이 코드 추가 버튼을 누른경우 체크
		if (codeGroupGrid.getRowCount() <= 0) {
			return false;
		}

		var codeGroupRow = (codeGroupGrid.getFocusedCell()['rowKey'] || 0);
		var codeGroupRecord = codeGroupGrid.getRow(codeGroupRow);

		// 2. 추가하려는 코드가 코드그룹이 저장된 코드 그룹인지 확인
		// 새로 추가해서 저장이 안된 코드그룹은 코드를 매핑할 키(codeGroup)가 없어서 안됨.
		if(codeGroupRecord["codeGroup"] == null || codeGroupRecord["codeGroup"] == '') {
			return false;
		}

		var codeGridData = codeGrid.getData();
		var maxOrderSeq = 0;

		$.each(codeGridData, function (index, value) {
			if (value["orderSeqText"] != null && $.isNumeric(value["orderSeqText"]) && maxOrderSeq < Number(value["orderSeqText"])) {
				maxOrderSeq = Number(value["orderSeqText"]).toFixed(2);
			}
		});
		codeGrid.appendRow({
			useYn: 'Y',
			codeGroup: codeGroupRecord["codeGroup"],
			orderSeqText: (Number(maxOrderSeq) + 1).toFixed(2)
		});
		codeGrid.focusAt(codeGridData.length, 3, true);
	});

	$codeSaveBtn.on('click', function () {
		codeGrid.blur();
		Common.showLoading();
		var parameter = codeGrid.getModifiedRows();
		// 저장할 데이터가 없는경우
		if (parameter.createdRows.length == 0 && parameter.updatedRows.length == 0) {
			alert("변경된 데이터가 없습니다.");
			Common.hideLoading();
			return false;
		}

		Common.callAjax("/standard/code/api/setCode", parameter, function (result) {
			alert("저장이 완료 되었습니다.");
			Common.hideLoading();
			getCodeDataList();
		});
	});

	// 코드그룹 추가버튼 이벤트
	$codeGroupAddBtn.on('click', function () {

		var codeGroupGridData = codeGroupGrid.getData();
		var maxOrderSeq = 0;

		$.each(codeGroupGridData, function (index, value) {
			if (value["orderSeqText"] != null && $.isNumeric(value["orderSeqText"]) && maxOrderSeq < Number(value["orderSeqText"])) {
				maxOrderSeq = Number(value["orderSeqText"]).toFixed(2);
			}
		});
		codeGroupGrid.appendRow({useYn: 'Y', orderSeqText: (Number(maxOrderSeq) + 1).toFixed(2)});
		codeGroupGrid.focusAt(codeGroupGridData.length, 4, true);
	});

	// 코드그룹 저장버튼 이벤트
	$codeGroupSaveBtn.on('click', function () {
		codeGroupGrid.blur();
		Common.showLoading();
		var parameter = codeGroupGrid.getModifiedRows();

		// 저장할 데이터가 없는경우
		if (parameter.createdRows.length == 0 && parameter.updatedRows.length == 0) {
			alert("변경된 데이터가 없습니다.");
			Common.hideLoading();
			return false;
		}

		Common.callAjax("/standard/code/api/setCodeGroup", parameter, function (result) {
			alert("저장이 완료 되었습니다.");
			Common.hideLoading();
			getCodeGroupDataList();
		});
	});

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getCodeGroupDataList();
	});

	// 코드그룹 마사용 포함 이벤트
	$viewAllCodeGroupFlag.on('change', function () {
		getCodeGroupDataList();
	});

	// 코드 미사용 포함 이벤트
	$viewAllCodeFlag.on('change', function () {
		getCodeDataList();
	});

	$resetBtn.on('click', function () {
		$('#searchForm')[0].reset();
	});

	// 코드 그룹 데이터 로드
	function getCodeGroupDataList() {
		Common.showLoading();
		var parameter = $('#searchForm').serializeObject();

		Common.callAjax("/standard/code/api/getCodeGroupList", parameter, function (result) {
			codeGroupGrid.resetData(result);
			$('#codeGroupTotalCount').text(result != null ? Common.addComma(result.length) : 0);
			if (codeGroupGrid.getRowCount() > 0) {
				getCodeDataList(0);
			} else {
				Common.hideLoading();
				codeGrid.resetData([]);
			}
		});
	}

	// 코드 데이터 로드
	function getCodeDataList(row) {

		// 입력된 로우가 없으면 현재 포커스된 셀로
		row = row == null ? (codeGroupGrid.getFocusedCell()['rowKey'] || 0) : row;
		var record = codeGroupGrid.getRow(row);

		var parameter = {
			viewAllCodeFlag: $viewAllCodeFlag.prop('checked') ? 'Y' : '',
			codeGroup: record["codeGroup"]
		};
		Common.callAjax("/standard/code/api/getCodeListByGroupCode", parameter, function (result) {
			codeGrid.resetData(result);
			$('#codeTotalCount').text(result != null ? Common.addComma(result.length) : 0);
			Common.hideLoading();
		});
	}
	
});