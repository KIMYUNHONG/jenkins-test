/**
 *  Create By Lee DoWon on 2020-02-18 / 오후 1:34
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	Common.showLoading();
	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $resetBtn = $('#resetBtn');		// 초기화버튼
	var $saveAuthBtn = $('#saveAuthBtn');			// 권한 저장

	var $menu1Seq = $('select[name="menu1Seq"]');
	var $menu2Seq = $('select[name="menu2Seq"]');
	var $searchForm = $('#searchForm');

	var codeList = [];

	// 필요한 데이터들 조회
	Common.getCodeList(["SD01"], function (result) {
		codeList = result;
	}, {async: false});

	$menu1Seq.on('change', function () {
		$menu2Seq.val('');
		$.each($menu2Seq.find('option'), function (index, option) {
			var $option = $(option);
			if(Valid.isNotEmpty($option.val()) &&  $menu1Seq.val() != $option.data('parentSeq')){
				$option.hide();
			}else {
				$option.show();
			}
		});
	});

	var authColumns = [
		{header: '대메뉴', name: 'm1MenuName', minWidth:150, sortable: true, align: 'left'},
		{header: '중메뉴', name: 'm2MenuName', minWidth:150, sortable: true, align: 'left'},
		{header: '팜클(관리자)', name: 'menuAuth1', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: Grid.selectEditor(codeList["SD01"], {useEmptyValue: true, useEmptyValueText : '선택'})},
		{header: '팜클(직원)', name: 'menuAuth2', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: Grid.selectEditor(codeList["SD01"], {useEmptyValue: true, useEmptyValueText : '선택'})},
		{header: '유지보수담당', name: 'menuAuth3', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: Grid.selectEditor(codeList["SD01"], {useEmptyValue: true, useEmptyValueText : '선택'})},
		{header: '거래처담당', name: 'menuAuth4', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: Grid.selectEditor(codeList["SD01"], {useEmptyValue: true, useEmptyValueText : '선택'})},
		{header: '영업담당', name: 'menuAuth5', minWidth:120, sortable: true, formatter: 'listItemText', align: 'center'
			, editor: Grid.selectEditor(codeList["SD01"], {useEmptyValue: true, useEmptyValueText : '선택'})}
	];

	var authGrid = Grid.createGrid({
		id: 'authGrid',
		columns: authColumns,
		data: []
	});

	getAuthDataList();

	$saveAuthBtn.on('click', function () {
		Common.showLoading();
		authGrid.blur();
		var parameter = authGrid.getModifiedRows();
		// 저장할 데이터가 없는경우
		if (parameter.createdRows.length == 0 && parameter.updatedRows.length == 0) {
			alert("변경된 데이터가 없습니다.");
			return false;
		}

		Common.callAjax("/standard/menuAuth/api/setMenuAuthList", parameter, function (result) {
			alert("저장이 완료 되었습니다.");
			getAuthDataList();
		});
	});

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getAuthDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	// 권한 데이터 로드
	function getAuthDataList() {
		Common.showLoading();
		var parameter = $searchForm.serializeObject();

		Common.callAjax("/standard/menuAuth/api/getMenuAuthList", parameter, function (result) {
			authGrid.resetData(result);
			$('#authTotalCount').text(result != null ? Common.addComma(result.length) : 0);
			Common.hideLoading();
		});
	}

});