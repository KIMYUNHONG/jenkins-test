/**
 *  Create By Kim YunHong on 2020-02-27 / 오후 1:02
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

    var $searchBtn = $('#searchBtn');	// 검색 버튼
    var $resetBtn = $('#resetBtn');    // 검색조건 초기화 버튼
    var $searchForm = $('#searchForm');

    var appColumns = [
        {header: 'OS', width:100, name: 'osText', sortable: true, align: 'center' },
        {header: '버전', width:100, name: 'versionText', sortable: true, align: 'center'},
        {header: '버전명', width:100, name: 'versionName', sortable: true ,align: 'left'},
        {header: '배포', width:90, name: 'useYnText', sortable: true,align: 'center'},
        {header: '앱파일', width:200, name: 'originalFileName', sortable: true, align: 'left', renderer: Grid.fileDownloadRenderer({text : ''}, function(){
                var row = (appGrid.getFocusedCell()['rowKey'] || 0);
                var record = appGrid.getRow(row);
                var html = '<a id="down" href="/uploadFile/api/download/' + (record.appFileKey != null ? record.appFileKey : '#') +'"' + ' target="_blank"></a>';
                $(html).get(0).click();
                // appFileUri
            })},
        {header: '비고', name: 'etc', minWidth:400, sortable: true, align: 'left'},
        {header: '수정', name: 'modifyBtn', width: 100, sortable: true, align: 'center', renderer: Grid.buttonRenderer({text : '수정', class : 'btn_type3'}, function(){
                var row = (appGrid.getFocusedCell()['rowKey'] || 0);
                var record = appGrid.getRow(row);
                setAppPopup(record["appSeq"])
            })},
        {header: '등록일', name: 'regDtText', width:120, sortable: true, align: 'center'},
        {header: '등록자', name: 'regName', width:120, sortable: true, align: 'center'},

    ];

    var appGrid = Grid.createGrid({
        id: 'appGrid',
        columns: appColumns,
        data: []
    });

    getAppDataList();

    // 검색버튼 이벤트 처리
    $searchBtn.on('click', function () {
        getAppDataList();
    });

    // 검색조건 초기화 버튼 이벤트 처리
    $resetBtn.on('click', function () {
        $searchForm[0].reset();
    });


    // App 관리 데이터 로드
    function getAppDataList() {
        Common.callAjax("/standard/app/api/getAppList", $searchForm.serializeObject(), function (result) {
            console.log(result)
            appGrid.resetData(result);
            $('#totalCount').text(result != null ? Common.addComma(result.length) : 0);
        });
    }

    // 등록 버튼 이벤트 처리
    $('#addAppBtn').on('click', function () {
        setAppPopup();
    });



    function setAppPopup(seq){

        seq = seq == null ? 0 : seq;
        var parameter = { appSeq : seq};
        var title = seq == 0 ? "App 등록" : "App 수정";

        Common.openPopup('/standard/app/addAppPopup',{parameter : parameter}, function(result){
            var $popup = Common.getPopupTemplate(title, result, {width : 640});
            $popup.find('.popup-close-btn').on('click', function(){
                $popup.remove();
            });
            $('#popupList').append($popup);

            $popup.find('#appFile').on('change', function () {
                Common.fileUpload($popup.find('#appFile'), function(result){
                    if(result == null ){
                        alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
                    }

                    $popup.find('input[name="appFileKey"]').val(result.uploadFileKey);
                    $popup.find('input[name="originalFileName"]').val(result.originalFileName);
                });
            });

            $popup.find('#appSaveBtn').on('click', function(){
                var seq = $("#appSeq").val();
                seq = seq == null ? 0 : seq;
                var message = seq == 0 ? "저장 하시겠습니까?" : "수정 하시겠습니까?";
                var messageConf = seq == 0 ? "저장이 완료 되었습니다." : "수정이 완료 되었습니다.";
                if(confirm(message)) {
                    Common.callAjax("/standard/app/api/setApp", $('#form').serializeObject(), function (result) {
                        alert(messageConf);
                        $popup.remove();
                        getAppDataList();
                    });
                }
            });
            $popup.find('#appCancelBtn').on('click', function(){
                if(confirm('취소 하시겠습니까?')){
                    $popup.remove();
                }
            });
            Common.resizeHeightPopup($popup);
        }, {})
    }
});