/**
 *  Create By Kim Yun Hong on 2020-04-06 / 오후 5:34
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $('#resetBtn');     // 검색조건 초기화 버튼

	var $deviceClassify = $("#deviceClassify"); //장비유형
	var $deviceMst1 = $("#deviceMst1"); //포충기 탭
	var $deviceMst2 = $("#deviceMst2"); //기피제분사함 탭
	var $searchForm = $('#searchForm');

	Common.tabBind($('.tab'), $('.tab_conts'));

	//포충기
	var protocolColumns_1 = [
		{header: '기준년도', minWidth:100, name: 'baseYear', sortable: true, align: 'center'},
		{header: '시작', minWidth:100, name: 'operateStartDt', sortable: true, align: 'center'},
		{header: '종료', minWidth:100, name: 'operateEndDt', sortable: true, align: 'center' },
		{header: '점등', minWidth:60, name: 'operateStartTime', sortable: true, align: 'center'},
		{header: '소등', minWidth:60, name: 'operateEndTime', sortable: true, align: 'center'},
		{header: '하한', minWidth:60, name: 'temperatureLow', sortable: true ,align: 'center' , renderer: Grid.unitRenderer({unit : '℃'})},
		{header: '상한', minWidth:60, name: 'temperatureHigh', sortable: true ,align: 'center', renderer: Grid.unitRenderer({unit : '℃'})},
		{header: '하한', minWidth:60, name: 'humidityLow', sortable: true,align: 'center', renderer: Grid.unitRenderer({unit : '%'})},
		{header: '상한', minWidth:60, name: 'humidityHigh', sortable: true,align: 'center' , renderer: Grid.unitRenderer({unit : '%'})},
		{header: '하한', minWidth:60, name: 'voltageLow', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '상한', minWidth:60, name: 'voltageHigh', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '하한', minWidth:60, name: 'electricLow', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'electricHigh', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '하한', minWidth:60, name: 'property1Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'property1High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '하한', minWidth:60, name: 'property2Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'property2High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '하한', minWidth:60, name: 'property3Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'property3High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '등록자', name: 'regName', minWidth:100, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth:100, sortable: true, align: 'center'}
	];

	var protocolGrid_1 = Grid.createGrid({
		id: 'protocolGrid_1',
		complexColumns: [
			{header: '가동기간',name: 'operateDt',childNames: ['operateStartDt', 'operateEndDt']},
			{header: '가동시간',name: 'operateTime',childNames: ['operateStartTime', 'operateEndTime']},
			{header: '온도',name: 'temperature',childNames: ['temperatureLow', 'temperatureHigh']},
			{header: '습도',name: 'humidity',childNames: ['humidityLow', 'humidityHigh']},
			{header: '전체전압',name: 'voltage',childNames: ['voltageLow', 'voltageHigh']},
			{header: '전체전류',name: 'electric',childNames: ['electricLow', 'electricHigh']},
			{header: '상부 FAN 전류',name: 'property1',childNames: ['property1Low', 'property1High']},
			{header: 'Lamp 전류',name: 'property2',childNames: ['property2Low', 'property2High']},
			{header: '하부 Motor 전류',name: 'property3',childNames: ['property3Low', 'property3High']}
		],
		columns: protocolColumns_1,
		data: []
	});

	//기피제 분사함
	var protocolColumns_2 = [
		{header: '기준년도', minWidth:100, name: 'baseYear', sortable: true, align: 'center' },
		{header: '시작', minWidth:100, name: 'operateStartDt', sortable: true, align: 'center' },
		{header: '종료', minWidth:100, name: 'operateEndDt', sortable: true, align: 'center' },
		{header: '점등', minWidth:60, name: 'operateStartTime', sortable: true, align: 'center'},
		{header: '소등', minWidth:60, name: 'operateEndTime', sortable: true, align: 'center'},
		{header: '하한', minWidth:60, name: 'temperatureLow', sortable: true ,align: 'center' , renderer: Grid.unitRenderer({unit : '℃'})},
		{header: '상한', minWidth:60, name: 'temperatureHigh', sortable: true ,align: 'center', renderer: Grid.unitRenderer({unit : '℃'})},
		{header: '하한', minWidth:60, name: 'humidityLow', sortable: true,align: 'center', renderer: Grid.unitRenderer({unit : '%'})},
		{header: '상한', minWidth:60, name: 'humidityHigh', sortable: true,align: 'center' , renderer: Grid.unitRenderer({unit : '%'})},
		{header: '하한', minWidth:60, name: 'voltageLow', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '상한', minWidth:60, name: 'voltageHigh', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '하한', minWidth:60, name: 'electricLow', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'electricHigh', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '하한', minWidth:60, name: 'property1Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '상한', minWidth:60, name: 'property1High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '하한', minWidth:60, name: 'property2Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '상한', minWidth:60, name: 'property2High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'V'})},
		{header: '하한', minWidth:60, name: 'property3Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : '%'})},
		{header: '상한', minWidth:60, name: 'property3High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : '%'})},
		{header: '하한', minWidth:60, name: 'property4Low', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '상한', minWidth:60, name: 'property4High', sortable: true, align: 'center', renderer: Grid.unitRenderer({unit : 'A'})},
		{header: '등록자', name: 'regName', minWidth:100, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth:100, sortable: true, align: 'center'}
	];

	var protocolGrid_2 = Grid.createGrid({
		id: 'protocolGrid_2',
		complexColumns: [
			{header: '가동기간',name: 'operateDt',childNames: ['operateStartDt', 'operateEndDt']},
			{header: '가동시간',name: 'operateTime',childNames: ['operateStartTime', 'operateEndTime']},
			{header: '온도',name: 'temperature',childNames: ['temperatureLow', 'temperatureHigh']},
			{header: '습도',name: 'humidity',childNames: ['humidityLow', 'humidityHigh']},
			{header: '전체전압',name: 'voltage',childNames: ['voltageLow', 'voltageHigh']},
			{header: '전체전류',name: 'electric',childNames: ['electricLow', 'electricHigh']},
			{header: 'Battery 전압',name: 'property1',childNames: ['property1Low', 'property1High']},
			{header: 'Solar 전압',name: 'property2',childNames: ['property2Low', 'property2High']},
			{header: '약재잔량',name: 'property3',childNames: ['property3Low', 'property3High']},
			{header: 'Pump 전류',name: 'property4',childNames: ['property4Low', 'property4High']}
		],
		columns: protocolColumns_2,
		data: []
	});
	protocolGrid_2.on('onGridMounted', function(e){
		$('#tab2').hide();
	});

	getProtocolDataList();

	//탭 선택시 hidden deviceClassify에 장비유형 값 세팅 및 재조회
	$deviceMst1.on('click', function () {
		$deviceClassify.val("SD020001");
		getProtocolDataList();
	});

	//탭 선택시 hidden deviceClassify에 장비유형 값 세팅 및 재조회
	$deviceMst2.on('click', function () {
		$deviceClassify.val("SD020002");
		getProtocolDataList();
	});

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getProtocolDataList();
	});

	// 검색조건 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	// 통신기준정보관리 데이터 로드
	function getProtocolDataList() {

		var parameter = $searchForm.serializeObject();

		Common.callAjax("/standard/protocol/api/getProtocolList", parameter, function (result) {
			if(parameter.deviceClassify =='SD020001'){
				protocolGrid_1.resetData(result);
				$('#totalCount1').text(result != null ? result.length : 0);
			}else{
				protocolGrid_2.resetData(result);
				$('#totalCount2').text(result != null ? result.length : 0);
			}

		});
	}

	// 등록 버튼 이벤트 처리
	$("button[name='insertBtn']").on('click', function () {
		setProtocolPopup();
	});

	// 통신기준 정보관리 팝업 OPEN
	function setProtocolPopup(){

		var deviceClassify = $deviceClassify.val();
		var parameter = { deviceClassify : deviceClassify};
		var baseYear = $("select[name='baseYear']").val();

		Common.openPopup('/standard/protocol/addProtocolPopup',{parameter : parameter}, function(result){

			var $popup = Common.getPopupTemplate("통신상태 기준정보", result);

			$popup.find("input[name='baseYear']").val(baseYear);
			$popup.find("#titleBaseYear").text("기준년도 " + baseYear);

			$popup.find('.popup-close-btn').on('click', function(){
				$popup.remove();
			});

			$('#popupList').append($popup);

			var startDt1 = Component.setDatePicker({wrapper: 'operateStartDtWrapper', element: 'operateStartDt', date : ''});
			var endDt1 = Component.setDatePicker({wrapper: 'operateEndDtWrapper', element: 'operateEndDt', date : ''});

			$popup.find('#saveBtn').on('click', function(){
				if(confirm("신규 설치된 장비부터 적용 됩니다.")) {
					Common.callAjax("/standard/protocol/api/setProtocol", $popup.find('#form').serializeObject(), function (result) {
						alert("저장이 완료 되었습니다.");
						$popup.remove();
						getProtocolDataList();
					});
				}
			});

			$popup.find('#cancelBtn').on('click', function(){
				if(confirm('취소 하시겠습니까?')){
					$popup.remove();
				}
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

});