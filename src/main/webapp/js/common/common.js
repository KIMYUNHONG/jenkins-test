
var Common = {
};

Common.callAjax = function (url, parameter, successFunction, option) {
	option = option == null ? {} : option;
	option.async = option.async == null ? true : option.async;
	option.spinner = option.spinner || true;
	$.ajax({
		type: "POST",
		async : option.async,
		url: url,
		data:JSON.stringify(parameter),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			if (!data.header.result) {
				alert(data.header.message);
				Common.hideLoading();
				if(isMobile == 'true'){
					Native.endLoading();
				}
				if(isMobile && data.header.resultCode == 'GO_LOGIN'){
					Native.goLogin();
				}
				return;
			}
			successFunction(data.body);
		}, //success
		error: function(jqXHR, textStatus, errorThrown) {
			if (option.spinner) {
				//$("body").removeClass("spinner-show");
			}
			if(isMobile == 'true'){
				Native.endLoading();
			}
		},
		beforeSend: function(jqXHR, textStatus, errorThrown) {
			if (option.spinner) {
				//$("body").addClass("spinner-show");
			}
		},
		complete: function(jqXHR, textStatus, errorThrown) {
			if (option.spinner) {
				//$("body").removeClass("spinner-show");
			}
		}
	}); //ajax
};
Common.excelDownload = function (url, parameter, option) {
	Common.showLoading();

	var gridColumnList = [];
	$.each(parameter.gridColumnList, function (index, value) {
		if(value.noExcel == null){
			gridColumnList.push(value);
		}
	});
	parameter.gridColumnList = gridColumnList;
	option = option == null ? {} : option;
	option.async = option.async == null ? true : option.async;
	option.spinner = option.spinner || true;
	$.ajax({
		type: "POST",
		async : option.async,
		url: url,
		data:JSON.stringify(parameter),
		contentType: "application/json; charset=utf-8",
		xhrFields: { //response 데이터를 바이너리로 처리한다.
			responseType: 'blob'
		},
		timeout: 60000,	// 타임아웃 1분
		success: function(data) {
			var linkUrl = option.fileName + '_' + (new Date().datetimeToString(''))  + '.xlsx';

			var blob = new Blob([data]);
			if (navigator.msSaveBlob) {

				return navigator.msSaveBlob(blob, linkUrl);
			}
			else {
				var link = document.createElement('a');
				link.href = window.URL.createObjectURL(blob);
				link.download = (linkUrl);
				link.click();
			}
			Common.hideLoading();

		}, error : function(){
			alert("엑셀다운로드중 에러가 발생했습니다. 관리자에게 문의해주세요.");
			Common.hideLoading();
			return false;
		}, complete : function(data) {
			Common.hideLoading();
		}
	}); //ajax
};

Common.fileUpload = function($fileInput, successFunction){
	var form = new FormData();
	form.append("file", $fileInput[0].files[0]);
	$.ajax({
		url: "/uploadFile/api/upload",
		data: form,
		dataType: 'json',
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data) {
			if (!data.header.result) {
				alert(data.header.message);
				Common.hideLoading();
				return;
			}
			successFunction(data.body);
		}, //success
		error: function(jqXHR, textStatus, errorThrown) {

		}
	});
};
Common.photoUpload = function($fileInput, successFunction){

	if(!Valid.isImageFormat($fileInput[0].files[0].name)){
		alert("이미지파일(jpg, jpeg, gif, bmp, png)만 업로드가 가능합니다.");
		$file.val(null);
		return false;
	}

	var form = new FormData();
	form.append("file", $fileInput[0].files[0]);
	$.ajax({
		url: "/uploadFile/api/photoUpload",
		data: form,
		dataType: 'json',
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data) {
			if (!data.header.result) {
				alert(data.header.message);
				Common.hideLoading();
				return;
			}
			successFunction(data.body);
		}, //success
		error: function(jqXHR, textStatus, errorThrown) {

		}
	});
};

Common.openPopup = function(url, option, callbackFunction){
	var parameter = option['parameter'] || {};

	$.ajax({
		type: "GET",
		async : false,
		url: url,
		data: parameter,
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		dataType: "html",
		success: function(data) {
			callbackFunction(data);
		}, //success
		error: function(jqXHR, textStatus, errorThrown) {
			alert("데이터를 불러오는중 문제가 발생했습니다. 관리자에게 문의해주세요.");
			return false;
		},
		beforeSend: function(jqXHR, textStatus, errorThrown) {
			if (option.spinner) {
				//$("body").addClass("spinner-show");
			}
		},
		complete: function(jqXHR, textStatus, errorThrown) {
			if (option.spinner) {
				//$("body").removeClass("spinner-show");
			}
		}
	}); //ajax
};

Common.getPopupTemplate = function(title, content, options){

	options = options == null ? {} : options;
	options["width"] = options["width"] == null ? 780 : options["width"] ;
	options["isPhoto"] = options["isPhoto"] == null ? false : options["isPhoto"] ;
	options["wrapper"] = options["wrapper"] == null ? false : options["wrapper"] ;

	var width = $(window).width() / 2  - options["width"] / 2 ;

	if(isMobile == 'true'){
		width = 0;
	}

	var $html = $(
		'<div class="popup">' +
		'	<div class="popup-dim"></div>' +
		'	<div id="popupContent" class="' + (options['isPhoto'] ? 'photo-popup-content' : 'popup-content') + '" style="width:' + options["width"]  +'px; left:' + width +'px;">' +
		'		<div class="popup-header">' +
		'			<span class="popup-title"></span>' +
		'			<span class="popup-sub-title"></span>' +
		'			<button class="popup-close-btn" type="button"></button>' +
		'		</div>' +
		'		<div class="popup-body ' + (options["bodyClass"] || '') + '">' +
		'			<div class="scroll-y"></div>' +
		'		</div>' +
		'	</div>' +
		'</div>');

	$html.find('.popup-title').html(title);
	$html.find('.scroll-y').html(content);

	return $html;
};

Common.getCodeList = function (codeGroupList, callbackFunction, option) {
	var parameter = {codeGroupList : codeGroupList};
	Common.callAjax("/standard/code/api/getCodeListByGroupCodeList", parameter, function (result) {
		callbackFunction(result);
	}, option);
};

/**
 * 그리드 리스트를 자바리스트에서 받을 수 있게 컨버트
 * @param data	- 그리드
 * @return result
 */
 Common.convertToJavaListMap = function(data){
 	var result = [];

 	$.each(data, function (listKey, listValue){
 		$.each(listValue, function(index, value){
 			$.each(value, function(mapKey, mapValue){
 				if(mapValue != null){
					result.push(listKey + '[' + index + ']' + '.' + mapKey + '=' + (mapValue || '') );
				}
			})
		})
	});

 	return result;
 };

Common.excludeNullValue = function (map) {
	var result = {};
	$.each(map, function (key, value) {
		if (value != null && value != '') {
			result[key] = value;
		}
	});
	return result;
};

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] && (this.value != null && this.value != '')) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value);
		} else if(this.value != null && this.value != '') {
			o[this.name] = this.value;
		}
	});
	return o;
};

$.fn.serializeListParameter = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] && (this.value != null && this.value != '')) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value);
		} else if(this.value != null && this.value != '') {
			o[this.name] = this.value;
		}
	});
	return $.param(o).replace(/%5B%5D/g, '');
};

Common.showLoading = function(){
	$('#page-info-loading').show();
};

Common.hideLoading = function(){
	$('#page-info-loading').hide();
};

Common.resizeHeightPopup = function($popup){

	resizePopup($popup);

	$(window).off("resize");
	$.each($('#popupList').find('.popup'), function(index, value){
		$(window).resize(function(){
			resizePopup($(value));
		});
	});

};

function resizePopup($popup){
	var height =  window.innerHeight / 2 - $popup.find('#popupContent').height() / 2 ;
	$popup.find('#popupContent').css('top', (height) +'px');
	var width = $(window).width() / 2  - $popup.find('#popupContent').width() / 2 ;
	$popup.find('#popupContent').css('left', width +'px');

}

Common.tabBind = function ($tab, $tabCont) {

	var $tabList = $tab.find('li');
	var $tabContList = $tabCont.find('li');

	$tabList.on('click', function () {
		var $this = $(this);
		$tabList.removeClass('on');
		$tabContList.hide();
		$this.addClass('on');
		$tabContList.eq($this.index()).show();
	});
};

/**
 * 전화번호 합치기
 * @param wrapperName
 * @param $wrapper
 */
Common.getPhoneNumber = function (wrapperName, $wrapper) {
	var phone1 = $wrapper.find('select[name="' + (wrapperName + '1') + '"').val();
	var phone2 = $wrapper.find('input[name="' + (wrapperName + '2') + '"').val();
	var phone3 = $wrapper.find('input[name="' + (wrapperName + '3') + '"').val();
	return (Valid.isNotEmpty(phone1) && Valid.isNotEmpty(phone2) && Valid.isNotEmpty(phone3)) ? phone1 + '-' + phone2 + '-' + phone3 : '';
}

/**
 * 전화번호 합치기(telephone)
 * @param wrapperName
 * @param $wrapper
 */
Common.getTelephoneNumber = function (wrapperName, $wrapper) {
	var phone1 = $wrapper.find('input[name="' + (wrapperName + '1') + '"').val();
	var phone2 = $wrapper.find('input[name="' + (wrapperName + '2') + '"').val();
	var phone3 = $wrapper.find('input[name="' + (wrapperName + '3') + '"').val();
	return (Valid.isNotEmpty(phone1) && Valid.isNotEmpty(phone2) && Valid.isNotEmpty(phone3)) ? phone1 + '-' + phone2 + '-' + phone3 : '';
}

Common.nvl = function(value, nvlValue){
	return value == null ? nvlValue : value;
}

/**
 * 3자리마다 ,(콤마) 추가해주는 유틸
 * @param data (숫자형태 , ex. 123123123)
 * @returns {string}	(ex. 123,123,123)
 */
Common.addComma = function (data) {

	if (data == null || data === '' || data.length === 0) {
		return "";
	}
	var value = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return value;
};

/**
 * 3자리마다 ,(콤마) 추가해주는 유틸 * 소수점 버림!
 * @param data
 * @returns {string}
 */
Common.addCommaNoDeigits = function (data) {
	if (data == null || data === '' || data.length === 0) {
		return "";
	}
	var parts=data.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

/**
 * 달러화폐단위 .00이여도 보이게끔 추가
 * @param data
 * @returns {string}
 */
Common.addComma2 = function (data) {
    if (data == null || data === '' || data.length === 0) {
        return "";
    }

    return (data).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');  // 12,345.67

};

$(function(){
	// 헤더 메뉴 좌측 메뉴 클릭시 로딩바 구현
	$('nav, aside').on('click', 'a', function(){
		if($(this).attr("target") != "_blank") { // 새창 오픈 제외
			Common.showLoading();
		}
	});

	// 상단 검색버튼
	$('#mobileSearchAllBtn').on('click', function () {
		var parameter = {
			'pageName': 'getInstallDeviceList',
			'type': 'detailBridge'
		};
		Native.movePage(parameter);
	});

	// 상단 사이드 메뉴 버튼
	$('#mobileSideMenuBtn').on('click', function () {
		var parameter = {
			'pageName': 'sideMenu'
		};
		Native.movePage(parameter);
	});

	// 상단 사이드 메뉴 버튼
	$('#mobileBackBtn').on('click', function () {
		Native.backPage();
	});

	$('.btn_aside').on('click', function () {
		var $aside = $('aside');
		if ($aside.is(':visible')) {
			$('.wrap').css('left', '0px');
			$aside.hide();
		} else {
			$aside.show();
			$('.wrap').css('left', '195px');
		}
	});

	/**
	 * 검색 enter 처리
	 *   검색 form id = searchForm, 검색 버튼 id = searchBtn
	 */
	$("#searchForm input[type=text]").keypress(function(e) {
	    if (e.keyCode == 13){
	    	//if($.trim($(this).val()) != '') { // 검색어 입력시에만 검색 처리
	    		$("#searchBtn").trigger("click");
	    	//}
	    }
	});
});

/**
 * 모바일에서 백버튼 클릭시 호출 이벤트
 */
var clickedBackBtn = function(){
	if($('#popupList').find('div').length > 0){
		$('#popupList').find('div').remove();
	} else {
		Native.backPage();
	}
}

/**
 * URL에 특정Param 삭제 (String)
 */
Common.removeStringParam = function(param, parameterName){

	if(Valid.isEmpty(param)){
		return "";
	}
	if(param.indexOf(parameterName) === -1){
		return param;
	}

	while(param.indexOf(parameterName) !== -1){
		var firstIndex = param.indexOf(parameterName);
		var lastIndex = param.indexOf('&', firstIndex) == -1 ? param.length : param.indexOf('&', firstIndex);
		param = param.substring(0, firstIndex) + param.substring(lastIndex + 1, param.length);
	}

	return param;
};

/**
 * URL에 특정Param 삭제 (List)
 */
Common.removeListParam = function(param, parameterName){

	if(Valid.isEmpty(param)){
		return "";
	}
	if(param.indexOf(parameterName) === -1){
		return param;
	}

	var result ="";

	return result;
};