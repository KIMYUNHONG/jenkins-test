
var Component = {
};

Component.setSelectByCodeList = function (codeList, option){

	var $select = $('select[name="' + option["name"] + '"]');
	var list = codeList[option["codeGroup"]];
	var $html = '';

	$select.html('');

	option.defaultValue = option.defaultValue ? option.defaultValue : '';
	option.useEmptyValue = option.useEmptyValue == null ? true : option.useEmptyValue;
	option.useEmptyValueText = option.useEmptyValueText || '전체';

	if(option.useEmptyValue){
		$html += "<option value=''>" + option.useEmptyValueText + "</option>";
	}

	$.each(list, function(index, value){
		$html += "<option value=" + value['code'] + (value == option.defaultValue ? "selected" : "") +">" + (value['codeName'] || '') + "</option>\n";
	});

	$select.append($html);
};
/**
 *
 * @param codeList
 * @param option
 * 		- id : wrapper name
 * 		- codeGroup : 그룹코드
 * 		- defaultValue : 기본값
 * 		- useEmptyValue : '전체' 값 사용시
 */
Component.setRadioByCodeList = function (codeList, option){

	var $select = $('#' + option['id']);
	var list = codeList[option['codeGroup']];
	var $html = '';
	var index = 0;
console.log(option.defaultValue)
	option.defaultValue = option.defaultValue ? option.defaultValue : '';
	option.useEmptyValue = option.useEmptyValue || false;
	option.useEmptyValueText = option.useEmptyValueText || '전체';
	option.isDisabled = option.isDisabled || false;

	if(option['useEmptyValue']){
		$html += "<input type='radio' name='" + option['name']
			+ "' id='"+ (option['id'] + index) + "' value='' "+ (option.isDisabled ? "disabled" : "") +">" + option['useEmptyValueText']
			+ "<label for='" + (option['id'] + index) + "'></label>";
	}

	$.each(list, function(index, value){
		$html += "<input type='radio' name='" + option['name']
			+ "' id='"+ (option['id'] + index) + "' value='" + value['code'] + "' "
			+ (value == option['defaultValue'] ? "checked" : "") + (option.isDisabled ? "disabled" : "") + ">"
			+ "<label for='" + (option['id'] + index) + "'>" + (value['codeName'] || '') + "</label>\n";
	});

	$select.append($html);
};
Component.setDatePicker = function (option){

	option['format'] = option['format'] == null ? 'yyyy.MM.dd' : option['format'];
	option['date'] = option['date'] == null ? new Date() : option['date'];
	option['selectableRanges'] = option['selectableRanges'] ==  null ? null : option['selectableRanges'];



	var datePicker = new tui.DatePicker('#' + option['wrapper'], {
		date: option['date'],
		input: {
			element: '#' + option['element'],
			format: option['format']
		},
		language : 'ko',
		selectableRanges : option['selectableRanges']
	});

	$('#' + option['element']).siblings('span').on('click', function(){
		$('#' + option['element']).click();
	});

	option['defaultValue'] = $('#' + option['element']).val();

	if(option['defaultValue'] != null && option['defaultValue'] != '' ){
		datePicker.setDate(option['defaultValue']);
	}

	return datePicker;
};

Component.setRangePicker = function(options){

	var rangePicker = tui.DatePicker.createRangePicker(options);

	var startPickerInput = $(options.startpicker.input);
	var endPickerInput = $(options.endpicker.input);
	startPickerInput.siblings('span').on('click', function(){
		startPickerInput.click();
	});
	endPickerInput.siblings('span').on('click', function(){
		endPickerInput.click();
	});
	return rangePicker;
};
