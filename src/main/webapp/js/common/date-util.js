/**
 *  Create By Lee DoWon on 2020-02-18 / 오후 6:37
 *  Email : vin4195@uniwiz.co.kr
 **/

Date.prototype.dateToString = function (unit) {

	unit = unit == null ? '.' : unit;

	var date = this;

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();

	month = month >= 10 ? month : '0' + month;
	day = day >= 10 ? day : '0' + day;

	return year + unit + month + unit + day;

};

/**
 * 데이트 타입 포맷 변경(yyyy.mm.dd) 형태 년월일
 * @param format
 */
var changeDateFormat = function (date, format) {

	if(Valid.isEmpty(date) || date.length != 10){
		return new Date();
	}

	var year = date.substring(0,4);
	var month = date.substring(5,7);
	var day = date.substring(8,10);

	return year + format + month + format + day;
};

Date.prototype.dateToYearMonth = function (unit) {

	unit = unit == null ? '.' : unit;

	var date = this;

	var year = date.getFullYear();
	var month = date.getMonth() + 1;

	month = month >= 10 ? month : '0' + month;

	return year + unit + month;

};
Date.prototype.datetimeToString = function (unit) {

	unit = unit == null ? '.' : unit;

	var date = this;

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hour = date.getHours();
	var minute = date.getMinutes();

	month = month >= 10 ? month : '0' + month;
	day = day >= 10 ? day : '0' + day;
	hour = hour >= 10 ? hour : '0' + hour;
	minute = minute >= 10 ? minute : '0' + minute;

	return year + unit + month + unit + day + '_' + hour + minute;

};

/**
 *
 * @param unit
 * @return {*}
 */
Date.prototype.addDate = function (addYear, addMonth, addDay) {

	var date = this;
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();

	return new Date(year + addYear, month + addMonth, day + addDay);
};
