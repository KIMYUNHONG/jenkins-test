var Native = {

	isAndroid: function () {
		var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기
		if (varUA.match('android') != null) {
			return true;
		} else if (varUA.indexOf("iphone") > -1 || varUA.indexOf("ipad") > -1
			|| varUA.indexOf("ipod") > -1) {
			//IOS 일때 처리
		} else {
			//아이폰, 안드로이드 외 처리
		}
		return false;
	},

	/**
	 methodName : 네이티브 호출할 함수명
	 params : 네이티브에 전달할 파라미터
	 cbName : 네이티브에서 전달 받을 함수
	 */
	execute: function (methodName, params, cbName) {
		try {
			var callParams = {
				callMethod: methodName,
				reqParam: params,
				cbMethod: cbName == null ? "" : cbName.toString()
			}
			var callMsg = JSON.stringify(callParams);
			if (Native.isAndroid()) {
				window.AndroidCall.JSCall(callMsg);
			} else {

				// 아이폰 예외처리 (전화/sendCall, 파일 다운로드/openBrowser 통합)
				if(callParams.callMethod == 'sendCall' || callParams.callMethod == 'openBrowser'){

					if(callParams.callMethod == 'sendCall'){
						callParams.reqParam = {'url' : 'tel://' + callParams.reqParam.phoneNumber};
					}

					if(callParams.callMethod == 'openBrowser'){

					}

					callParams.callMethod = 'openUrl';
					var callMsg = JSON.stringify(callParams);
				}
				window.webkit.messageHandlers.jscall.postMessage(encodeURIComponent(callMsg));
			}
		} catch (e) {
			if (methodName != 'getInfo') {
				return false;
			}
		}
	},

	/**
	 *  위치 정하기
	 */
	setAddress: function (lat, lng, address) {
		Native.execute("setAddress", {
			lat: lat,
			lng: lng,
			address: address
		});
	},
	moveDeviceDetail: function (deviceSeq) {
		Native.execute("moveDeviceDetail", {
			deviceSeq: deviceSeq
		});
	},
	backPage: function () {
		Native.execute("backPage", "");
	},
	movePage: function (parameter) {
		Native.execute("movePage", parameter);
	},
	// 웹뷰 닫기
	closePage: function () {
		Native.execute("closePage", "");
	},
	// 네이티브 로딩 시작
	startLoading: function () {
		Native.execute("startLoading", "");
	},
	// 네이티브 로딩 종료
	endLoading: function () {
		Native.execute("endLoading", "");
	},
	// 로그인으로 보내기
	goLogin: function () {
		Native.execute("goLogin", "");
	},
	// 네이티브 전화 기능
	sendCall: function (phoneNumber) {
		Native.execute("sendCall", {'phoneNumber': phoneNumber});
	},
	// 네이티브 전화 기능
	openBrowser: function (url) {
		Native.execute("openBrowser", {'url': url});
	},
	getGps: function (callback) {
		Native.execute("getGps", "", callback);
	}
};