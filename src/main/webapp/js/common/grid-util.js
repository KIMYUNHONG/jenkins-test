/**
 *  Create By Lee DoWon on 2020-02-18 / 오후 6:37
 *  Email : vin4195@uniwiz.co.kr
 **/

var Grid = {};
var TuiGrid = tui.Grid;
TuiGrid.applyTheme('striped');

Grid.createGrid = function (option) {
	var grid;
	if (option != null && option.id != null) {

		option.columns = option.columns || [];
		option.data = option.data || [];
		option.pageOptions = option.pageOptions || {};
		option.scrollX = option.scrollX || false;
		option.scrollY = option.scrollY || true;
		option.resizable = option.resizable || true;
		option.frozenCount = option.frozenCount || 0;
		option.bodyHeight = option.bodyHeight || 397;
		option.header = {height: 38};
		if(	option.complexColumns != null){
			option.header.complexColumns = option.complexColumns;
			option.header.height = option.header.height * 2;
		}
		grid = new TuiGrid({
			el: document.getElementById(option.id), // Container element
			columns: option.columns,
			columnOptions: {
				resizable: option.resizable,
				frozenCount: option.frozenCount
			},
			selectionUnit : 'row',
			rowHeaders: option.rowHeaders,
			bodyHeight: option.bodyHeight,
			data: option.data,
			header : option.header,
			rowHeight: 38,
			minRowHeight: 38,
			pageOptions: option.pageOptions
		});
		grid.resetData([]);

	}

	grid.on('sort', function () {

	});

	return grid;
};

'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var numberEditor = function () {
	function numberEditor(props) {
		_classCallCheck(this, numberEditor);

		var el = document.createElement('input');

		el.type = 'text';
		el.value = String(props.value);

		this.el = el;
	}

	_createClass(numberEditor, [{
		key: 'getElement',
		value: function getElement() {
			return this.el;
		}
	}, {
		key: 'getValue',
		value: function getValue() {
			return this.el.value;
		}
	}, {
		key: 'mounted',
		value: function mounted() {
			this.el.select();
		}
	}]);

	return numberEditor;
}();

var buttonRenderer = function () {
	var options;
	function buttonRenderer(props) {
		_classCallCheck(this, buttonRenderer);

		var el = document.createElement('button');
	 	options = props.columnInfo.renderer.options;

		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? '' : options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		el.type = 'button';
		this.el = el;
		this.render(props);
	}

	_createClass(buttonRenderer, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			$el.text(options.text);
			$el.prop('class', options.class);
			$el.on('click', function(){
				options.callbackFunction();
			});
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
			if(Valid.isEmpty(props) || props.value == 'Y'){
				$(this.el).hide();
			}
		}
	}]);

	return buttonRenderer;
}();
var iconRenderer = function () {
	var options;
	function iconRenderer(props) {
		_classCallCheck(this, iconRenderer);

		var el = document.createElement('div');
	 	options = props.columnInfo.renderer.options;
		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? '' : options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		el.type = 'div';
		this.el = el;
		this.render(props);
	}

	_createClass(iconRenderer, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			$el.text(options.text);
			$el.prop('class', options.class);
			$el.on('click', function(){
				options.callbackFunction();
			});
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
			console.log(props)
			if(Valid.isEmpty(props) || props.value == 'N'){
				$(this.el).hide();
			}
		}
	}]);

	return iconRenderer;
}();

var buttonRenderer2 = function () {
	var options;
	function buttonRenderer2(props) {
		_classCallCheck(this, buttonRenderer2);

		var el = document.createElement('button');
	 	options = props.columnInfo.renderer.options;

		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? '' : options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		el.type = 'button';
		this.el = el;
		this.render(props);
	}

	_createClass(buttonRenderer2, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			$el.text(options.text);
			$el.prop('class', options.class);
			$el.on('click', function(){
				options.callbackFunction();
			});
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
		}
	}]);

	return buttonRenderer2;
}();

var addressRenderer = function () {
	var options;
	function addressRenderer(props) {
		_classCallCheck(this, addressRenderer);

		var el = document.createElement('div');
	 	options = props.columnInfo.renderer.options;

		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? 'tui-grid-cell-content' : 'tui-grid-cell-content ' + options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		el.type = 'span';
		this.el = el;
		this.render(props);
	}

	_createClass(addressRenderer, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			$el.prop('class', options.class);
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
			var $el = $(this.el);
			$el.append('<a href="#" class="ic_locate mr5"><i></i></a>');
			$el.append(String(props.value));
			$el.find('.ic_locate').on('click', function(){
				options.callbackFunction();
			});

		}
	}]);

	return addressRenderer;
}();

var fileDownloadRenderer = function () {
	var options;
	function fileDownloadRenderer(props) {
		_classCallCheck(this, fileDownloadRenderer);

		var el = document.createElement('div');
	 	options = props.columnInfo.renderer.options;

		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? 'tui-grid-cell-content' : 'tui-grid-cell-content ' + options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		this.el = el;
		this.render(props);
	}

	_createClass(fileDownloadRenderer, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			$el.prop('class', options.class);
			$el.prop('href', options.href);
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
			var $el = $(this.el);
			$el.append(String(props.value || ''));
			$el.on('click', function () {
				options.callbackFunction();
			});
		}
	}]);

	return fileDownloadRenderer;
}();


var unitRenderer = function () {
	var options;
	function unitRenderer(props) {
		_classCallCheck(this, unitRenderer);

		var el = document.createElement('div');
		options = props.columnInfo.renderer.options;

		options.text = options.text == null ? '' : options.text;
		options.class = options.class == null ? 'tui-grid-cell-content' : 'tui-grid-cell-content ' + options.class;
		options.callbackFunction = options.callbackFunction == null ? function(){} : options.callbackFunction;
		el.type = 'span';
		this.el = el;
		this.render(props);
	}

	_createClass(unitRenderer, [{
		key: 'getElement',
		value: function getElement() {
			var $el = $(this.el);
			return this.el;
		}
	}, {
		key: 'render',
		value: function render(props) {
			var $el = $(this.el);
			if(options.place == 'prefix'){
				$el.append(options.unit + String(props.value));
			}else {
				$el.append(String(props.value) + options.unit);
			}
		}
	}]);

	return unitRenderer;
}();

Grid.unitRenderer = function(options, callbackFunction){
	return {
		type: unitRenderer,
		options: {
			unit : options.unit,
			place : options.place != null ? options.place : 'suffix',
			callbackFunction : callbackFunction
		}
	};
};
Grid.addressRenderer = function(options, callbackFunction){
	return {
		type: addressRenderer,
		options: {
			text : options.text,
			class : options.class,
			callbackFunction : callbackFunction
		}
	};
};
Grid.fileDownloadRenderer = function(options, callbackFunction){
	return {
		type: fileDownloadRenderer,
		options: {
			text : options.text,
			class : options.class,
			callbackFunction : callbackFunction
		}
	};
};
Grid.buttonRenderer = function(options, callbackFunction){
	return {
		type: buttonRenderer,
		options: {
			text : options.text,
			class : options.class,
			callbackFunction : callbackFunction
		}
	};
};

Grid.buttonRenderer2 = function(options, callbackFunction){
	return {
		type: buttonRenderer2,
		options: {
			text : options.text,
			class : options.class,
			callbackFunction : callbackFunction
		}
	};
};
Grid.iconRenderer = function(options, callbackFunction){
	return {
		type: iconRenderer,
		options: {
			text : options.text,
			class : options.class,
			callbackFunction : callbackFunction
		}
	};
};

Grid.numberEditor = function(){
	return {
		type: numberEditor,
		options: {
			customTextEditorOptions: {}
		}
	}
}

Grid.selectEditor = function(data, option) {
	return {
		type: 'select',
		options: {
			listItems: Grid.encodeCodeToGridOption(data, option)
		}
	}
}

Grid.oxSelectEditor = function () {
	return {
		type: 'select',
		options: {
			listItems: [
				{text: 'O', value: 'Y'},
				{text: 'X', value: 'N'},
			]
		}
	}
};
/**
 * 공통코드 데이터를 그리드 editor option으로 변환
 * @param data
 * @returns {[]}
 */
Grid.encodeCodeToGridOption = function(data, option){
	var list = [];

	option.useEmptyValue = option.useEmptyValue || true;
	option.useEmptyValueText = option.useEmptyValueText || '전체';

	if(option.useEmptyValue){
		list.push({text:option.useEmptyValueText, value: ''});
	}

	$.each(data, function(index, code){
		list.push({text:code["codeName"], value: code["code"]});
	});

	return list;
};