var DaumAddress = {};

DaumAddress.createAddressPopup = function (wrapper, options, callBackFunction) {

	// wrapper - 지도 바인딩할 객체
	// options[popup] - 팝업 객체
	// options[address] - 주소 받을 객체
	// options[addressDetail] - 상세주소 받을 객체


	var $daumAddress = document.getElementById(wrapper);
	new daum.Postcode({
		oncomplete: function (data) {
			// 각 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var addr = '(' + data.zonecode + ') '; // 주소 변수
			var extraAddr = ''; // 참고항목 변수

			//사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
			if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
				addr += Valid.isEmpty(data.roadAddress) ? data.autoRoadAddress : data.roadAddress;
			} else { // 사용자가 지번 주소를 선택했을 경우(J)
				addr += Valid.isEmpty(data.jibunAddress) ? data.autoJibunAddress : data.jibunAddress;
			}

			// 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
			if (data.userSelectedType === 'R') {
				// 법정동명이 있을 경우 추가한다. (법정리는 제외)
				// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
				if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
					extraAddr += data.bname;
				}
				// 건물명이 있고, 공동주택일 경우 추가한다.
				if (data.buildingName !== '' || data.apartment === 'Y') {
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
				if (extraAddr !== '') {
					extraAddr = '(' + extraAddr + ')';
				}
				// 조합된 참고항목을 해당 필드에 넣는다.
				$('#' + options['addressDetail']).val(extraAddr);

			} else {
				$('#' + options['addressDetail']).val('');
			}

			$('#' + options['address']).val(addr);

			var geocoder = new kakao.maps.services.Geocoder();

			var searchKeyword = Valid.isEmpty(data.jibunAddress) ? data.autoJibunAddress : data.jibunAddress;
			geocoder.addressSearch(searchKeyword, function(result, status) {
				// 정상적으로 검색이 완료됐으면
				if (status === kakao.maps.services.Status.OK) {
					if(result.length > 0){
						$('#' + options['latitude']).val(result[0].y);
						$('#' + options['longitude']).val(result[0].x);
					}
					return false;
				}
			});

			// 시/도 , 시/군/구를 받게끔 필드가 설정되어있으면 셋팅
			if(options['sido'] != null &&  options['sigungu']){
				$('#' + options['sido']).val(data.sido);
				$('#' + options['sigungu']).val(data.sigungu);
			}

			options['popup'].remove();
			if(callBackFunction != null){
				callBackFunction();
			}
		}
	}).embed($daumAddress);


};

DaumAddress.createStaticMap = function (options) {

	var mapContainer = document.getElementById(options["id"]), // 지도를 표시할 div
		mapOption = {
			center: new kakao.maps.LatLng(options["lat"], options["lng"]), // 지도의 중심좌표
			level: 5 // 지도의 확대 레벨
		};

	var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

	// 지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
	// var zoomControl = new kakao.maps.ZoomControl();
	// map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);

	// 지도를 클릭한 위치에 표출할 마커입니다
	var marker = new kakao.maps.Marker({
		// 지도 중심좌표에 마커를 생성합니다
		position: map.getCenter()
	});

	marker.setMap(map);
	map.setDraggable(false);
	map.setZoomable(false);

};


// 설치장소 장비를 다 보여줘야할때 사용
DaumAddress.createLocationStaticMap = function (options) {

	var parameter = {locationSeq: options.locationSeq == '' ? 0 : options.locationSeq,
		statusList : ['DM020002', 'DM020004'] };
	var token = options.token == null ? '' : '?token='+options.token;
	Common.callAjax("/device/device/api/getInstallDeviceList" + token, parameter, function (result) {
			if(result != null && result.length > 0){

				var bounds = new kakao.maps.LatLngBounds();

				var mapContainer = document.getElementById(options["id"]), // 지도를 표시할 div
					mapOption = {
						center: new kakao.maps.LatLng(result[0]["latitude"], result[0]["longitude"]), // 지도의 중심좌표
						level: 9 // 지도의 확대 레벨
					};

				var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
				clusterer = new kakao.maps.MarkerClusterer({
					map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
					averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
					minLevel: 6 // 클러스터 할 최소 지도 레벨
				});

				var zoomControl = new kakao.maps.ZoomControl();
				map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);

				var markers = [];
				$.each(result, function (index, value) {
					var latlng = new kakao.maps.LatLng(value.latitude, value.longitude);

					var imageSrc = '/img/common/map_ty3_1.png', // 마커이미지의 주소입니다.
						imageSize = new kakao.maps.Size(27, 42), // 마커이미지의 크기입니다
						imageOption = {offset: new kakao.maps.Point(9, 42)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.


					var deviceSeq = $('#deviceSeq').val();

					// 모니터 상태별 아이콘 설정
					// 정상인경우
					if(value.monitorState == 'MT020001'){
						imageSrc = '/img/common/map_ty2';
					} else if(value.monitorState == 'MT020002'){
						imageSrc ='/img/common/map_ty1';
					} else {
						imageSrc ='/img/common/map_ty3_1';
					}

					imageSrc += (deviceSeq == value.deviceSeq) ? '_on.png' : '.png';

					// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
					var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);

					// 지도를 클릭한 위치에 표출할 마커입니다
					var marker = new kakao.maps.Marker({
						map: map,
						position: latlng,
						image: markerImage
					});
					// 마커에 표시할 인포윈도우를 생성합니다
					var infowindow = new kakao.maps.InfoWindow({
						content: '<h3 class="mapcode">' + value.deviceId +'</h3>' +  // 인포윈도우에 표시할 내용
							'<p  class="mapname">' + value.deviceNameText + '</p>'
					});

					markers.push(marker);
					kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
					kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));

					bounds.extend(latlng);
				});
				clusterer.addMarkers(markers);

				map.setBounds(bounds);
			} else {

				var mapContainer = document.getElementById(options["id"]), // 지도를 표시할 div
					mapOption = {
						center : new kakao.maps.LatLng(37.53105539245015, 126.98392330433975),
						level: 9 // 지도의 확대 레벨
					};

				var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
				clusterer = new kakao.maps.MarkerClusterer({
					map: map // 마커들을 클러스터로 관리하고 표시할 지도 객체
				});;

				var zoomControl = new kakao.maps.ZoomControl();
				map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
			}
	});
};

// 설치장소 장비를 다 보여줘야할때 사용
// 마커 알파벳 일때 사용
DaumAddress.createLocationAlphabetMap = function (options, mouseEventFunction) {

	var parameter = {locationSeq: options.locationSeq == '' ? 0 : options.locationSeq};
	var $list = options.$list;
	Common.callAjax("/device/device/api/getInstallDeviceListByLocation?token=" + options.token, parameter, function (result) {
		var markers = [];
		if (result != null) {
			var bounds = new kakao.maps.LatLngBounds();

			var mapContainer = document.getElementById(options["id"]), // 지도를 표시할 div
				mapOption = {
					center: new kakao.maps.LatLng(result[0]["latitude"], result[0]["longitude"]), // 지도의 중심좌표
					level: 9 // 지도의 확대 레벨
				};

			var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
			clusterer = new kakao.maps.MarkerClusterer({
				map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
				averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
				minLevel: 6 // 클러스터 할 최소 지도 레벨
			});

			var zoomControl = new kakao.maps.ZoomControl();
			map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);

			// 마커 이미지
			var MARKER_WIDTH = 27, // 기본, 클릭 마커의 너비
				MARKER_HEIGHT = 42, // 기본, 클릭 마커의 높이
				OFFSET_X = 9, // 기본, 클릭 마커의 기준 X좌표
				OFFSET_Y = MARKER_HEIGHT, // 기본, 클릭 마커의 기준 Y좌표
				OVER_MARKER_WIDTH = 27, // 오버 마커의 너비
				OVER_MARKER_HEIGHT = 42, // 오버 마커의 높이
				OVER_OFFSET_X = 9, // 오버 마커의 기준 X좌표
				OVER_OFFSET_Y = OVER_MARKER_HEIGHT, // 오버 마커의 기준 Y좌표
				SPRITE_MARKER_URL = '/img/common/map_ty3_all.png', // 스프라이트 마커 이미지 URL
				SPRITE_WIDTH = 81, // 스프라이트 이미지 너비
				SPRITE_HEIGHT = 336, // 스프라이트 이미지 높이
				SPRITE_GAP = 0; // 스프라이트 이미지에서 마커간 간격

			var markerSize = new kakao.maps.Size(MARKER_WIDTH, MARKER_HEIGHT), // 기본, 클릭 마커의 크기
				markerOffset = new kakao.maps.Point(OFFSET_X, OFFSET_Y), // 기본, 클릭 마커의 기준좌표
				overMarkerSize = new kakao.maps.Size(OVER_MARKER_WIDTH, OVER_MARKER_HEIGHT), // 오버 마커의 크기
				overMarkerOffset = new kakao.maps.Point(OVER_OFFSET_X, OVER_OFFSET_Y), // 오버 마커의 기준 좌표
				spriteImageSize = new kakao.maps.Size(SPRITE_WIDTH, SPRITE_HEIGHT); // 스프라이트 이미지의 크기

			$.each(result, function (index, value) {
				var latlng = new kakao.maps.LatLng(value.latitude, value.longitude);

				var gapX = (MARKER_WIDTH + SPRITE_GAP), // 스프라이트 이미지에서 마커로 사용할 이미지 X좌표 간격 값
					originY = (MARKER_HEIGHT + SPRITE_GAP) * index, // 스프라이트 이미지에서 기본, 클릭 마커로 사용할 Y좌표 값
					overOriginY = (OVER_MARKER_HEIGHT + SPRITE_GAP) * index, // 스프라이트 이미지에서 오버 마커로 사용할 Y좌표 값
					normalOrigin = new kakao.maps.Point(27, originY), // 스프라이트 이미지에서 기본 마커로 사용할 영역의 좌상단 좌표
					overOrigin = new kakao.maps.Point(0, overOriginY); // 스프라이트 이미지에서 클릭 마커로 사용할 영역의 좌상단 좌표

				addMarker(latlng, normalOrigin, overOrigin, index);
				bounds.extend(latlng);
				map.setBounds(bounds);
			});

			clusterer.addMarkers(markers);

			function addMarker(position, normalOrigin, overOrigin, index) {

				// 기본 마커이미지, 오버 마커이미지, 클릭 마커이미지를 생성합니다
				var normalImage = createMarkerImage(markerSize, markerOffset, normalOrigin),
					overImage = createMarkerImage(overMarkerSize, overMarkerOffset, overOrigin);

				// 마커를 생성하고 이미지는 기본 마커 이미지를 사용합니다
				var marker = new kakao.maps.Marker({
					map: map,
					position: position,
					image: normalImage
				});

				// 마커 객체에 마커아이디와 마커의 기본 이미지를 추가합니다
				marker.normalImage = normalImage;
				marker.index = index;
				// 마커에 mouseover 이벤트를 등록합니다

				$list.eq(index).find('h3').on('mouseover', function(){
					marker.setImage(overImage);
					mouseEventFunction('over', marker.index);
				});
				$list.eq(index).find('h3').on('mouseout', function(){
					marker.setImage(normalImage);
					mouseEventFunction('out', marker.index);
				});

				kakao.maps.event.addListener(marker, 'mouseover', function() {

					// 클릭된 마커가 없고, mouseover된 마커가 클릭된 마커가 아니면
					// 마커의 이미지를 오버 이미지로 변경합니다
					marker.setImage(overImage);
					mouseEventFunction('over', marker.index);
				});

				// 마커에 mouseout 이벤트를 등록합니다
				kakao.maps.event.addListener(marker, 'mouseout', function() {

					// 클릭된 마커가 없고, mouseout된 마커가 클릭된 마커가 아니면
					// 마커의 이미지를 기본 이미지로 변경합니다
					marker.setImage(normalImage);
					mouseEventFunction('out', marker.index);
				});

				// 마커에 click 이벤트를 등록합니다
				kakao.maps.event.addListener(marker, 'click', function() {

				});
				markers.push(marker);
			}

			// MakrerImage 객체를 생성하여 반환하는 함수입니다
			function createMarkerImage(markerSize, offset, spriteOrigin) {
				var markerImage = new kakao.maps.MarkerImage(
					SPRITE_MARKER_URL, // 스프라이트 마커 이미지 URL
					markerSize, // 마커의 크기
					{
						offset: offset, // 마커 이미지에서의 기준 좌표
						spriteOrigin: spriteOrigin, // 스트라이프 이미지 중 사용할 영역의 좌상단 좌표
						spriteSize: spriteImageSize // 스프라이트 이미지의 크기
					}
				);

				return markerImage;
			}
		}
	});
};