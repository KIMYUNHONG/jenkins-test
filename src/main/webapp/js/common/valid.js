
var Valid = {
};
/**
 * 객체가 비었는지 확인
 * @param value
 * @return {boolean} - true : null or '' or undefined, false : value
 */
Valid.isEmpty = function(value){
	return value === null || value === '' || value === undefined;
};
/**
 * 객체에 값이 있는지 확인
 * @param value
 * @return {boolean} - true : value, false : null or '' or undefined
 */
Valid.isNotEmpty = function(value){
	return !(value === null || value === '' || value === undefined);
};

/**
 * 객체가 비었거나, 0인지 확인
 * @param value
 * @return {boolean} - true : value, false : null or '' or undefined or 0
 */
Valid.isZero = function(value){
	return (value === null || value === '' || value === undefined) || (value === 0 || value === '0');
};

Valid.isLowerCaseAndNumber = function(value, valueName){
	var regExp = /^[a-z0-9+]*$/;

	if(Valid.isEmpty(value)){
		alert(valueName + "를 입력해주세요.");
		return false;
	}

	if(!regExp.test(value)){
		alert(valueName + "는 영문/숫자만 입력이 가능합니다.");
		return false;
	}
	return true;
};
Valid.isPhoneFormat = function(value, valueName){
	var regExp = /^\d{2,3}-\d{3,4}-\d{4}$/;

	if(Valid.isEmpty(value)){
		alert(valueName + "를 입력해주세요.");
		return false;
	}

	if(!regExp.test(value)){
		alert(valueName + "를 양식에 맞춰 입력해주세요.");
		return false;
	}

	return true;
};

Valid.isPhoneNumberFormat = function(value){
	var regExp = /^\d{2,3}-\d{3,4}-\d{4}$/;

	if(Valid.isEmpty(value)){
		alert(valueName + "를 입력해주세요.");
		return false;
	}

	if(!regExp.test(value)){
		alert("휴대폰번호는 -를 제외한 숫자 11자리만 입력해주시기 바랍니다.");
		return false;
	}

	return true;
};

Valid.isNumberFormat = function(value, valueName,required ){

	var regex = /^[0-9+]*$/;

	if(required){
		if(Valid.isEmpty(value)){
			alert(valueName + "를 입력해주세요.");
			return false;
		}
	}

	if(!regex.test(value)){
		alert(valueName+"은 숫자만 입력가능합니다.");
		return false;
	}

	return true;
};

Valid.isImageFormat = function (filename) {
	if(Valid.isEmpty(filename) || filename.length < 5){
		return false;
	}
	var ext = filename.substring(filename.length - 3, filename.length).toLowerCase();
	return (ext == 'jpg' || ext == 'jpeg' || ext == 'gif' || ext == 'bmp' || ext == 'png');
};
Valid.isPdfFormat = function (filename) {
	if(Valid.isEmpty(filename) || filename.length < 5){
		return false;
	}
	var ext = filename.substring(filename.length - 3, filename.length).toLowerCase();
	return ext == 'pdf';
};
Valid.isExcelFormat = function (filename) {
	if(Valid.isEmpty(filename) || filename.length < 5){
		return false;
	}
	var ext = filename.substring(filename.length - 4, filename.length).toLowerCase();
	return (ext == '.xls' || ext == 'xlsx');
};