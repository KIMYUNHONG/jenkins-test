var Monitor = {};

const STATE_OK = 'MT020001';	// 정상
const STATE_ALARM = 'MT020002';	// 알람
const STATE_OFF = 'MT020003';	// 꺼짐


const DEVICE_MAP_MIN_LEVEL = 1;	// 1-6레벨까지 장비별 마커맵
const DEVICE_MAP_MAX_LEVEL = 6;	// 1-6레벨까지 장비별 마커맵

const SIGUNGU_MAP_MIN_LEVEL = 7;	// 7-9레벨까지 시군구별 마커맵
const SIGUNGU_MAP_MAX_LEVEL = 9;	// 7-9레벨까지 시군구별 마커맵

const SIDO_MAP_MIN_LEVEL = 10;	// 10-12레벨까지 시도별 마커맵
const SIDO_MAP_MAX_LEVEL = 12;	// 10-12레벨까지 시도별 마커맵

/**
 * 장비 리스트 출력시 상태 클래스(CSS용) 획득(대시보드, 모바일 모니터링)
 * @param state	- 모니터링상태 코드
 * @return {string}
 */
Monitor.getDeviceStateClass = function (state) {
	if (Valid.isEmpty(state)) {
		return 'off';
	}
	switch (state) {	// 장비 상태 체크
		case STATE_OK:				// 정상
			return 'ok';
		case STATE_ALARM:			// 알림
			return 'notic';
		case STATE_OFF:				// 알림
			return 'off';
		default :					// 꺼짐
			return 'off';
	}
};

/**
 * 장비 리스트 출력시 통신상태 클래스(CSS용) 획득(대시보드, 모바일 모니터링)
 * @param connectionYn	- 통신 상태 Y/N
 * @param modemId	- 모뎀 ID
 * @return {string}
 */
Monitor.getDeviceConnectionClass = function (connectionYn, modemId) {
	if (Valid.isEmpty(connectionYn)) {
		return 'device';
	}
	if (connectionYn === 'Y') {	// 연결 ON
		return 'wifi';
	} else {	// 연결 OFF 시 모뎀이 설치된 장비인지 체크함.
		return (Valid.isEmpty(modemId) ? "device" : "wifino");
	}
};

/**
 * 현재 장비별 마커맵인지 여부 return
 * @param level - 현재 맵 zoom 레벨
 * @return {boolean}
 */
Monitor.isDeviceMap = function (level) {
	if (Valid.isZero(level)) {
		return false;
	}
	return DEVICE_MAP_MAX_LEVEL >= level && level >= DEVICE_MAP_MIN_LEVEL;
}
/**
 * 현재 시군구별 마커맵인지 여부 return
 * @param level - 현재 맵 zoom 레벨
 * @return {boolean}
 */
Monitor.isSigunguMap = function (level) {
	if (Valid.isZero(level)) {
		return false;
	}
	return SIGUNGU_MAP_MAX_LEVEL >= level && level >= SIGUNGU_MAP_MIN_LEVEL;
}
/**
 * 현재 시도별 마커맵인지 여부 return
 * @param level - 현재 맵 zoom 레벨
 * @return {boolean}
 */
Monitor.isSidoMap = function (level) {
	if (Valid.isZero(level)) {
		return false;
	}
	return SIDO_MAP_MAX_LEVEL >= level && level >= SIDO_MAP_MIN_LEVEL;
}

/**
 * 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
 * @param monitorState - 장비 모니터링 상태코드
 * @return {kakao.maps.MarkerImage}
 */
Monitor.getDeviceStatusMarkerImage = function (monitorState) {

	var imageSrc, // 마커이미지의 주소입니다.
		imageSize = new kakao.maps.Size(27, 42), // 마커이미지의 크기입니다
		imageOption = {offset: new kakao.maps.Point(9, 42)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.


	switch (monitorState) {
		case STATE_OK:
			imageSrc = '/img/common/map_ty2.png';
			break;
		case STATE_ALARM:
			imageSrc = '/img/common/map_ty1.png';
			break;
		case STATE_OFF:
			imageSrc = '/img/common/map_ty3_1.png';
			break;
		default:
			imageSrc = '/img/common/map_ty2.png';
			break;
	}

	return new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);

}
/**
 * 마커에 마우스 호버시 나올 인포윈도우 그리기
 * @param deviceId - 장비 번호
 * @param deviceNameText - 장비명
 * @return {kakao.maps.InfoWindow}
 */
Monitor.getInfoWindow = function (deviceId, deviceNameText) {
	return new kakao.maps.InfoWindow({
		content: '<h3 class="mapcode">' + Common.nvl(deviceId, '') + '</h3>' +  // 인포윈도우에 표시할 내용
			'<p  class="mapname">' + Common.nvl(deviceNameText, '') + '</p>'
	});
}

/**
 * 마우스호버시 표시될 마커 객체
 * @return {kakao.maps.Marker}
 */
Monitor.getHoverMarker = function (imgSrc) {

	var imageSrc = imgSrc, // 마커이미지의 주소입니다.
		imageSize = new kakao.maps.Size(24, 42), // 마커이미지의 크기입니다
		imageOption = {offset: new kakao.maps.Point(7, 42)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.

	// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
	var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);

	// 지도를 클릭한 위치에 표출할 마커입니다
	return new kakao.maps.Marker({
		position: new kakao.maps.LatLng(0, 0),
		image: markerImage
	});
}

/**
 * 지역별 카운트 지도 오버레이 객체
 * @param value - 지역 JSON 데이터(Area)
 * @return {kakao.maps.CustomOverlay}
 */
Monitor.getCustomOverlay = function(value){
	var content = '<div class ="map_local" ><span class="cityname">' + value.areaText +'</span><span class="d_num"> ' + value.cnt + '</span><span class="right"></span></div>';
	var position = new kakao.maps.LatLng(value.lat, value.lng);
	return new kakao.maps.CustomOverlay({
		position: position,
		content: content
	});
}