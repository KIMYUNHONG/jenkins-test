/**
 *  Create By Lee DoWon on 2020-02-17 / 오전 9:20
 *  Email : vin4195@uniwiz.co.kr
 **/


$(function () {

	var $regUserForm = $('#regUserForm');
	var $openCustomerListPopupBtn = $regUserForm.find('#openCustomerListPopupBtn');
	var $nextStepBtn = $regUserForm.find('#nextStepBtn');
	var $saveBtn = $regUserForm.find('#saveBtn');
	var $duplicateCheckBtn = $regUserForm.find('#duplicateCheckBtn');
	
	// 회사명 검색 버튼 처리
	$openCustomerListPopupBtn.on('click', function () {
		openCustomerListPopup();
	});
	
	// 회사명 검색 팝업
	function openCustomerListPopup(parameter) {
		Common.openPopup('/customerListPopup', {}, function (result) {
			var $detailPopup = Common.getPopupTemplate("회사명 검색", result, {width: 600});
			$('#popupList').append($detailPopup);


			$detailPopup.find('#searchForm').on('submit', function(){
				getCustomerList();
				return false;
			});

			
			function getCustomerList() {
				if($detailPopup.find('input[name="customerName"]').val() == null || $detailPopup.find('input[name="customerName"]').val().length < 2){
					alert("회사명을 두글자 이상 입력해주세요.");
					return false;
				}
				var parameter = {
					customerName: $detailPopup.find('input[name="customerName"]').val() || '',
					viewAllCustomerFlag: 'Y'
				};
				Common.callAjax("/customer/customer/api/getAllCustomerList", parameter, function (result) {
					$('#customerListTable').show();
					var html = '';
					for(var i=0;i<result.length;i++) {
						html += '<tr onClick="addCustomer(this);"';
						html += '		data-seq=\"' + result[i].customerSeq + '\"';
						html += '		data-name=\"' + result[i].customerShortName + '\"';
						html += '		data-type=\"' + result[i].customerType + '\"';
						html += '		data-num=\"' + result[i].businessNumber + '\">';
						html += '	<td><p>' + result[i].customerShortName + '</p></td>';
						html += '	<td class="t_center"><p>' + Common.nvl(result[i].businessNumber, '') + '</p></td>';
						html += '	<td class="t_center"><p>' + Common.nvl(result[i].representName, '') + '</p></td>';
						html += '</tr>';
					}
					if(result.length == 0){
						html +='<tr>' +
							'<td colspan="3" class="no-data">입력하신 거래처가 존재하지않습니다.</td>' +
							'</tr>'
					}
					$detailPopup.find('#customerTbody').html(html)
					$detailPopup.find('#customerTotalCount').text(result != null ? Common.addComma(result.length) : 0);
				});
				
			}

			if(isMobile != 'true'){
				Common.resizeHeightPopup($detailPopup);
			}
			$detailPopup.find('#searchCustomerBtn').on('click', function () {
				getCustomerList();
			});

			$detailPopup.find('.popup-close-btn').on('click', function () {
				$detailPopup.remove();
			});

		}, {})
	}

	$('.cancelBtn').on('click', function () {
		console.log(isMobile)
		if(isMobile == 'true'){
			Native.closePage();
		}else{
			location.href = '/login';
		}
	});


	
	// 다음 버튼 이벤트
	$nextStepBtn.on('click', function() {
		
		var agree = $('#agree').is(':checked');
		if(!agree){
			alert("이용약관을 동의해주세요.");
			return false;
		}
		var agree2 = $('#agree2').is(':checked');
		if(!agree2){
			alert("개인정보 수집 이용을 동의해주세요.");
			return false;
		}
		
		var customerSeq = $('#customerSeq').val();
		if(customerSeq == '' || customerSeq <= 0 ){
			alert("소속 회사를 선택하세요.");
			return false;
		}
		
		$('#step01').hide();
		$('#step02').show();
		// 대리점이거나 팜클인경우 대표담당이아닌 권한 표시
		if($('#customerType').val() == 'CM010001' || $('#customerType').val() == 'CM010005'){
			$('#representYn2').prop('checked', true);
			$('#customerForm').hide();
			$('#pharmcleForm').show();
			// 대리점 권한인경우 팜클(직원) 권한 숨김
			if($('#customerType').val() == 'CM010001'){
				$('#authRole1').hide();
				$('label[for="authRole1"]').hide();
				$('#authRole2').prop('checked', true);
			}
		}
		
	});
	
	//이메일 select 박스 변경 이벤트 처리(POPUP)
	$regUserForm.find("select[name='selectEmailDomainPopup']").on('change',function(){
		var $this = $(this).val();
		var $emailDomain = $('input[name="emailDomain"]');
		
		$emailDomain.val($this);
		$emailDomain.prop('readonly', Valid.isNotEmpty($this));
	});
	
	// 중복확인 버튼 이벤트
	$duplicateCheckBtn.on('click', function () {

		var $isChecked = $regUserForm.find('input[name="isChecked"]');
		var $userId = $regUserForm.find('input[name="userId"]');

		Common.callAjax("/standard/user/api/getUserChk", {userId : $userId.val()}, function (result) {
			alert(result.msg);
			$isChecked.val(result.result);
		}, {async : false});
	});
	
	// 가입완료 버튼 이벤트
	$saveBtn.on('click', function() {
		
		var agree = $('#agree').is(':checked');
		if(!agree){
			alert("이용약관을 동의해주세요.");
			return false;
		}
		var agree2 = $('#agree2').is(':checked');
		if(!agree2){
			alert("개인정보 수집 이용을 동의해주세요.");
			return false;
		}
		
		var customerSeq = $('#customerSeq').val();
		if(customerSeq == '' || customerSeq <= 0 ){
			alert("소속 회사를 선택하세요.");
			return false;
		}
		
		if($regUserForm.find('input[name="isChecked"]').val() != "true" || Valid.isEmpty($regUserForm.find('input[name="userId"]').val())){
			alert("아이디 중복체크를 해주세요.");
			return false;
		}

		var phone1 =  $regUserForm.find('select[name="phone1"]').val();
		var phone2 =  $regUserForm.find('input[name="phone2"]').val();
		var phone3 =  $regUserForm.find('input[name="phone3"]').val();
		var phone = phone1+'-'+phone2+'-'+phone3;

		var telephone1 = $regUserForm.find('input[name="telephone1"]').val();
		var telephone2 = $regUserForm.find('input[name="telephone2"]').val();
		var telephone3 = $regUserForm.find('input[name="telephone3"]').val();

		if(phone1 != '' && phone2 != '' && phone3 != ''){
			if(!Valid.isPhoneNumberFormat(phone)) return false;
			$regUserForm.find('input[name="phone"]').val(phone)
		}

		if(telephone1 != '' && telephone2 != '' && telephone3 != ''){
			var telephone = telephone1+'-'+telephone2+'-'+telephone3;
			if(!Valid.isPhoneFormat(telephone, "전화번호")) return false;
			$regUserForm.find('input[name="telephone"]').val(telephone)
		}

		if (confirm('가입 하시겠습니까?')) {
			Common.showLoading();
			var parameter = $('#regUserForm').serializeObject();
			Common.callAjax("/standard/user/api/regUser", parameter, function(result){
				alert("가입이 완료되었습니다.\n담당자 승인 후 서비스 이용이 가능합니다.");
				if(isMobile == "true"){
					Native.closePage();
				}else{
					location.href = '/login';
				}
			});
		}
		
	});
});

function addCustomer(target) {
	var customerSeq = $(target).data('seq')
	var customerShortName = $(target).data('name')
	var businessNumber = $(target).data('num')
	var customerType = $(target).data('type');
	$('#regUserForm').find('#customerName').val(customerShortName);
	$('#regUserForm').find('input[name="customerSeq"]').val(customerSeq);
	$('#regUserForm').find('#businessNumberText').text(businessNumber);
	$('#regUserForm').find('#customerNameText').text(customerShortName);
	$('#regUserForm').find('#customerType').val(customerType);

	$('#popupContent').find('.popup-close-btn').click();
}
