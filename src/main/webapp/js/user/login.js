/**
 *  Create By Lee DoWon on 2020-02-17 / 오전 9:20
 *  Email : vin4195@uniwiz.co.kr
 **/


$(function () {

	var $findIdBtn = $("#findIdBtn"); //아이디 찾기 버튼
	var $regUserBtn = $("#regUserBtn"); //회원가입 버튼

	// Cookey 아이디 저장 - 쿠키에 저장된 ID값을 set, 없으면 공백이 들어간다.
	var userId = getCookie("userId");
	$("input[name='userId']").val(userId);

	//Cookey에 저장된 ID가 있으면 아이디 저장을 체크 상태로 둔다
	if($("input[name='userId']").val() != ""){
		$("#saveId").attr("checked", true);
	}
	// 체크박스에 변화가 있다면 (체크를 풀었을 경우 Cookey 삭제처리.)
	$("#saveId").change(function(){
		if($("#saveId").is(":checked")){ // ID 저장하기 체크했을 때,
		}else{ // ID 저장하기 체크 해제 시,
			deleteCookie("userId");
		}
	});


	function deleteCookie(cookieName){
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() - 1);
		document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
	}

	function getCookie(cookieName) {
		cookieName = cookieName + '=';
		var cookieData = document.cookie;
		var start = cookieData.indexOf(cookieName);
		var cookieValue = '';
		if(start != -1){
			start += cookieName.length;
			var end = cookieData.indexOf(';', start);
			if(end == -1)end = cookieData.length;
			cookieValue = cookieData.substring(start, end);
		}
		return unescape(cookieValue);
	}

	$('form').on('submit', function(){

		var parameter = {
			userId : $('input[name="userId"]').val(),
			password : $('input[name="password"]').val()
		};

		Common.callAjax("/api/login", parameter, function (result) {
			
			if(result.returnUrl != null && result.returnUrl != "") {
				location.href = result.returnUrl;
			} else {
				location.href = "/";
			}

			// 로그인 성공시 ID 저장하기 체크했으면  Cookey에 ID값 저장
			if($("#saveId").is(":checked")){ // ID 저장하기 체크했을 때,
				var userInputId = $("input[name='userId']").val();
				setCookie("userId", userInputId, 7); // 7일 동안 쿠키 보관
			}else{ // ID 저장하기 체크 해제 시,
				deleteCookie("userId");
			}

			function setCookie(cookieName, value, exdays){
				var exdate = new Date();
				exdate.setDate(exdate.getDate() + exdays);
				var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
				document.cookie = cookieName + "=" + cookieValue;
			}
			
		});
		return false;
	});

	// 아이디찾기 버튼 이벤트 처리
	$findIdBtn.on('click', function () {
		openFindIdPopup();
	});
	
	// 회원가입 버튼 이벤트 처리
	$regUserBtn.on('click', function () {
		location.href = 'regUser';
	});

	function openFindIdPopup(){
		var parameter = {};
		Common.openPopup('/standard/user/findUserIdPopup',{parameter : parameter}, function(result){
			var $popup = Common.getPopupTemplate("아이디 찾기", result, {width : 580 });
			$popup.find('.popup-close-btn').on('click', function(){
				$popup.remove();
			});
			$('#popupList').append($popup);

			$popup.find('#findIdSaveBtn').on('click', function(){
					Common.callAjax("/standard/user/api/userFindId", $('#form').serializeObject(), function (result) {
						alert("아이디는 "+result.maskId+" 입니다.");
						$popup.remove();
					});
			});

			$popup.find("select[name='selectEmailDomainPopup']").siblings('input[name="emailDomain"]').prop('readonly', !($popup.find("select[name='selectEmailDomainPopup']").val() == ''));
			//이메일 select 박스 변경 이벤트 처리(POPUP)
			$popup.find("select[name='selectEmailDomainPopup']").on('change',function(){
				var $emailDomain = $popup.find('input[name="emailDomain"]');
				var $this = $(this).val();

				$emailDomain.val($this);
				$emailDomain.prop('readonly', Valid.isNotEmpty($this));
			});

			Common.resizeHeightPopup($popup);

			// 팝업생성 완료 후 팝업 높이조절
			$("#popupContent").css('height','270px');

		}, {})
	}

});