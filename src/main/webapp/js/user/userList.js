/**
 *  Create By Kim YunHong on 2020-02-26 / 오전 11:16
 *  Email : fbsghd123@uniwiz.co.kr
 **/


$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');						// 검색 버튼
	var $resetBtn = $('#resetBtn');                        // 검색조건 초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $searchForm = $('#searchForm');

	var userColumns = [
		{header: '사용권한', width: 80, name: 'authRoleText', sortable: true, align: 'center'},
		{header: '소속', width: 120, name: 'customerName', sortable: true, align: 'left'},
		{header: '성명', width: 100, name: 'name', sortable: true, align: 'center'},
		{header: '직급', width: 70, name: 'rankText', sortable: true, align: 'center'},
		{header: '아이디', width: 100, name: 'userId', sortable: true, align: 'center'},
		{header: '전화번호', width: 120, name: 'telephone', sortable: true, align: 'center'},
		{header: '핸드폰번호', width: 120, name: 'phone', sortable: true, align: 'center'},
		{header: '이메일', width: 150, name: 'emailText', sortable: true, align: 'center'},
		{header: '비고', name: 'etc', width: 180, sortable: true, align: 'left'},
		{header: 'App 사용', name: 'useAppYnText', width: 80, sortable: true, align: 'center'},
		{header: '최근접속일', name: 'loginDtText', width: 80, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', width: 90, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', width: 90, sortable: true, align: 'center'},
		{header: '상태', name: 'useYnText2', width: 80, sortable: true, align: 'center'},
		{header: '승인', name: 'confirmYn', minWidth: 100, sortable: true, align: 'center',
			renderer: Grid.buttonRenderer({text: '가입승인', class: 'btn_type2'}, function () {
				var row = (userGrid.getFocusedCell()['rowKey'] || 0);
				var record = userGrid.getRow(row);
				if (confirm("해당 거래처 담당자의 가입을 승인 하시겠습니까?")) {
					Common.showLoading();
					setUserComfirm(record.userSeq, record);
				}
			})
		},
		{header: '수정',name: 'modifyBtn',width: 80,sortable: true,	align: 'center',
			renderer: Grid.buttonRenderer2({text: '수정', class: 'btn_type3'}, function () {
				var row = (userGrid.getFocusedCell()['rowKey'] || 0);
				var record = userGrid.getRow(row);
				var confirmYn = record["confirmYn"];
				if (confirmYn != 'Y') {
					alert("승인되지 않은 유저입니다.");
					return;
				}
				setUserPopup(record["userSeq"])
			}),
			noExcel: true
		}
	];

	var userGrid = Grid.createGrid({
		id: 'userGrid',
		columns: userColumns,
		data: []
	});

	getUserDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getUserDataList();
	});

	// 검색조건 초기화 버튼 이벤트 처리
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});
	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {

		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = userColumns;

		var option = {
			fileName: '사용자'
		};
		Common.excelDownload("/standard/user/excel/getUserList", parameter, option);
	});

	// 등록 버튼 이벤트 처리
	$('#addUserBtn').on('click', function () {
		setUserPopup();
		$("#userIdChk").val("사용가능");
	});

	// 사용자관리 데이터 로드
	function getUserDataList() {
		Common.showLoading();
		Common.callAjax("/standard/user/api/getUserList", $searchForm.serializeObject(), function (result) {
			userGrid.resetData(result);
			$('#totalCount').text(result != null ? Common.addComma(result.length) : 0);
			Common.hideLoading();
		});
	}

	function setUserPopup(seq) {

		seq = seq == null ? 0 : seq;
		var parameter = {userSeq: seq};
		var user = {};

		// seq가 0인경우 조회가 의미가 없어서 조회 X
		if (seq > 0) {
			Common.callAjax("/standard/user/api/getUser", parameter, function (result) {
				user = result;
			}, {async: false});
		}
		$('input[name="userSeq"]').val(seq);

		Common.openPopup('/standard/user/addUserPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("사용자등록", result);
			var userIdChk;
			var userIdChkMsg;
			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);

			$popup.find("select[name='selectEmailDomainPopup']").siblings('input[name="emailDomain"]').prop('readonly', !($popup.find("select[name='selectEmailDomainPopup']").val() == ''));
			//이메일 select 박스 변경 이벤트 처리(POPUP)
			$popup.find("select[name='selectEmailDomainPopup']").on('change', function () {
				var $emailDomain = $popup.find('input[name="emailDomain"]');
				var $this = $(this).val();

				$emailDomain.val($this);
				$emailDomain.prop('readonly', Valid.isNotEmpty($this));
			});

			$popup.find('#userSaveBtn').on('click', function () {
				//화면 중복여부 체크
				if (userIdChk > 0) {
					alert(userIdChkMsg);
					return;
				}

				if (confirm('저장 하시겠습니까?')) {

					var parameter = $('#form').serializeObject();

					parameter.phone = Common.getTelephoneNumber('phone', $popup);
					parameter.telephone = Common.getTelephoneNumber('telephone', $popup);

					Common.callAjax("/standard/user/api/setUser", parameter, function (result) {
						alert("저장이 완료 되었습니다.");
						$popup.remove();
						getUserDataList();
					});
				}
			});
			$popup.find('#userCancelBtn').on('click', function () {
				if (confirm('취소 하시겠습니까?')) {
					$popup.remove();
				}
			});

			$popup.find('#userResetPwBtn').on('click', function () {
				if (confirm('비밀번호 초기화 하시겠습니까?')) {
					Common.callAjax("/standard/user/api/resetUserPw", $('#form').serializeObject(), function (result) {
						alert("비밀번호 초기화가 완료 되었습니다.");
						$popup.remove();
						getUserDataList();
					});
				}
			});

			//userId 중복체크
			$popup.find('input[name=userId]').change(function () {
				Common.callAjax("/standard/user/api/getUserChk", $("#form").serializeObject(), function (result) {
					if (!result.result) {
						$popup.find('#userIdChk').text(result.msg);
						$popup.find('#userIdChk').removeClass('txt_alert2').addClass('txt_alert');
					} else {
						$popup.find('#userIdChk').text(result.msg);
						$popup.find('#userIdChk').removeClass('txt_alert').addClass('txt_alert2');
					}
					userIdChk = result.result ? 0 : 1;
					userIdChkMsg = result.msg;
				});
			});

			Common.resizeHeightPopup($popup);
		}, {})
	}

	function setUserComfirm(userSeq, record) {
		Common.showLoading();
		var parameter = {userSeq: userSeq};
		Common.callAjax("/customer/customer/api/setChargeConfirm", parameter, function (result) {
			alert("가입 승인이 완료되었습니다.");
			getUserDataList();
		});
	}
});