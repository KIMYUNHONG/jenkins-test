/**
 *  Create By Kim YunHong on 2020-03-02 / 오후 5:01
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $deleteBtn = $('#deleteBtn');// 삭제버튼
	var $editBtn = $('#editBtn');	  // 수정버튼
	var $listBtn = $('#listBtn');	  // 목록버튼
	var $detail = $('#detailBtn'); //상세정보 버튼
	var queryString = $('input[name="queryString"]').val();

	// 삭제버튼 이벤트 처리
	$deleteBtn.on('click', function () {
		if (confirm('삭제 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/device/device/api/deleteDevice", $("#deviceDetailForm").serializeObject(), function (result) {
				alert("삭제가 완료되었습니다.");
				location.href = "/device/device/deviceList?" + queryString;
			});
		}
	});

	// 수정버튼 이벤트 처리
	$editBtn.on('click', function () {
		Common.showLoading();
		location.href = '/device/device/deviceWrite?' + queryString;
	});

	// 목록버튼 이벤트 처리
	$listBtn.on('click', function () {
		Common.showLoading();
		location.href = "/device/device/deviceList?" + queryString;
	});

	// 상세정보 버튼 이벤트 처리
	$detail.on('click', function () {
		openDetailPopup();
	});
	$("#barcode").barcode($('input[name="deviceId"]').val(), "code128", {barHeight: 28, fontSize: 11});

	function openDetailPopup() {

		var parameter = {deviceMstSeq: $("#deviceMstSeq").val()};

		Common.openPopup('/device/device/detailDeviceMasterPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비 상세정보", result);
			var deviceClassify = $popup.find("#deviceClassify").val();

			// 팝업 오픈시 유형에 에 해당하는 input만 보여지게 설정
			if (deviceClassify == "SD020002") {
				$popup.find('.' + 'SD020001').hide();
			} else {
				$popup.find('.' + 'SD020002').hide();
			}

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);
			Common.resizeHeightPopup($popup);

			// 팝업생성 완료 후 팝업 높이조절
			$("#popupContent").css('height', '300px');

		}, {})
	}
});