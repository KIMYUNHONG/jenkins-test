/**
 *  Create By Kim YunHong on 2020-03-02 / 오후 5:01
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $saveBtn = $('#deviceSaveBtn');	  // 저장버튼
	var $listBtn = $('#deviceListBtn');	  // 목록버튼
	var $selectBtn = $('#deviceSelectBtn');	  // 장비선택버튼
	var $openModemListPopupBtn = $('#openModemListPopupBtn');
	var queryString = $('input[name="queryString"]').val();

	if (deviceSeq == 0) {
		var manufactureDt = Component.setDatePicker({
			wrapper: 'manufactureDtWrapper',
			element: 'manufactureDt',
			date: new Date()
		});
	}

	// 저장버튼 이벤트 처리
	$saveBtn.on('click', function () {
		//신규 벨리데이션
		if ($("#deviceSeq").val() == 0) {
			if (!Valid.isNumberFormat($("#deviceQty").val(), "장비수량", true)) {
				return;
			}
		} else {
			//수정시 벨리데이션
		}

		if (confirm('저장 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/device/device/api/setDevice", $('#form').serializeObject(), function (result) {
				if (result == "success") {
					alert("저장이 완료 되었습니다.");
				} else {
					alert(result);
				}
				location.href = "/device/device/deviceList?" + queryString;
				Common.hideLoading();
			});
		}

	});

	// 목록버튼 이벤트 처리
	$listBtn.on('click', function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();
			location.href = "/device/device/deviceList?" + queryString;
		}
	});

	// 장비선택 팝업 처리
	$selectBtn.on('click', function () {
		Common.openPopup('/device/device/selectDeviceMasterPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("장비선택", result, {width: 680});
			$('#popupList').append($popup);
			var deviceMasterColumns = [
				{header: '구분', name: 'deviceClassifyText', width: 100, sortable: true, align: 'center'},
				{header: '장비명', name: 'deviceNameText', width: 150, sortable: true, align: 'left'},
				{header: '모델명', name: 'modelName', minWidth: 171, sortable: true, align: 'left'},
				{header: '제조사', name: 'manufactureComText', width: 100, sortable: true, align: 'center'}
			];
			TuiGrid.setLanguage('ko', {display: {noData: '담당자를 추가해주세요.'}});

			var deviceMasterGrid = Grid.createGrid({
				id: 'deviceMasterGrid',
				columns: deviceMasterColumns,
				data: [],
				bodyHeight: 300
			});

			$popup.find('#searchForm').on('submit', function () {
				return false;
			});

			getDeviceMasterList();

			function getDeviceMasterList() {

				var parameter = $popup.find('#searchForm').serializeObject();

				Common.callAjax("/standard/deviceMaster/api/getDeviceMasterList", parameter, function (result) {
					deviceMasterGrid.resetData(result);
					$('#deviceTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			deviceMasterGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];

				if (row == null) {
					return false;
				}

				var record = deviceMasterGrid.getRow(row)
				//사용자 view용 TEXT 세팅
				$.each(record, function (key, value) {
					$('#' + key).text(value);
				});

				//INSERT를 위한 CODE값 세팅
				$('input[name="deviceMstSeq"]').val(record.deviceMstSeq);
				$popup.remove();
			});

			$popup.find('#searchDeviceMasterBtn').on('click', function () {
				getDeviceMasterList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	// 모뎀ID 검색 팝업 버튼 이벤트
	$openModemListPopupBtn.on('click', function () {
		openModemListPopup();
	});

	// 모뎀ID 검색 팝업
	function openModemListPopup() {

		Common.openPopup('/device/device/modemListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("Modem ID", result, {width: 680});
			$('#popupList').append($popup);

			var $searchModemListBtn = $popup.find('#searchModemListBtn');

			var modemColumns = [
				{header: '모뎀ID', name: 'modemId', minWidth: 120, sortable: true, align: 'center'},
				{header: '등록일', name: 'regDtText', minWidth: 100, sortable: true, align: 'center'}
			];

			var modemGrid = Grid.createGrid({
				id: 'modemGrid',
				columns: modemColumns,
				data: []
			});

			$searchModemListBtn.on('submit', function () {
				return false;
			});

			getDeviceMasterList();

			function getDeviceMasterList() {

				var parameter = $popup.find('#searchForm').serializeObject();

				Common.callAjax("/standard/modem/api/getModemList", parameter, function (result) {
					modemGrid.resetData(result);
					$('#modemTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			modemGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = modemGrid.getRow(row);

				$popup.remove();

				//INSERT를 위한 값 세팅
				// 검색창에 modemId와 부모창에 modemId가 겹치는 이름이 있어서 창을 닫은다음에 값 셋팅하도록 순서 변경
				$('input[name="modemId"]').val(record.modemId);
			});

			$searchModemListBtn.on('click', function () {
				getDeviceMasterList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}
});