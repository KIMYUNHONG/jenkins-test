/**
 *  Create By Kim YunHong on 2020-02-27 / 오후 4:58
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $viewAllDeviceFlag = $('#viewAllDeviceFlag');
	var $resetBtn = $('#resetBtn');
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼

	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');
	var $searchForm = $('#searchForm');
	var $statusList = $('input[name="statusList"]');
	var $total_chk = $("#total_chk");
	var $addDeviceBtn = $("#addDeviceBtn");
	var $statusUl = $("#statusUl");

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	var deviceColumns = [
		{header: '장비번호', name: 'deviceId', minWidth: 150, sortable: true, align: 'center'},
		{header: '장비구분', name: 'deviceClassifyText', minWidth: 90, sortable: true, align: 'center'},
		{header: '장비명', name: 'deviceNameText', minWidth: 120, sortable: true, align: 'left'},
		{header: '모델명', name: 'modelName', minWidth: 150, sortable: true, align: 'left'},
		{header: 'ModemId', name: 'modemId', minWidth: 120, sortable: true, align: 'left'},
		{header: '통합 Board Serial 번호', name: 'boardSerial', minWidth: 150, sortable: true, align: 'left'},
		{header: '장비상태', name: 'statusText', minWidth: 110, sortable: true, align: 'center'},
		{header: '제조일', name: 'manufactureDtText', minWidth: 110, sortable: true, align: 'center'},
		{header: '제조사', name: 'manufactureComText', minWidth: 110, sortable: true, align: 'center'},
		{header: '보관장소', name: 'storageText', minWidth: 110, sortable: true, align: 'center'},
		{header: '통신방식', name: 'communicationModeText', minWidth: 90, sortable: true, align: 'center'},
		{header: '등록일', name: 'regDtText', minWidth: 90, sortable: true, align: 'center'},
		{header: '등록자', name: 'regName', minWidth: 90, sortable: true, align: 'center'}
	];

	var deviceGrid = Grid.createGrid({
		id: 'deviceGrid',
		columns: deviceColumns,
		data: []
	});

	getDeviceDataList();
	getDeviceStatusData();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getDeviceDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = deviceColumns;

		var option = {
			fileName: '장비리스트'
		};
		Common.excelDownload("/device/device/excel/getDeviceList", parameter, option);
	});

	// 장비관리 데이터 로드
	function getDeviceDataList() {
		Common.showLoading();

		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "deviceList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/device/device/api/getDeviceList", parameter, function (result) {
			deviceGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

	// 장비관리 데이터 로드(상태별)
	function getDeviceStatusData() {
		Common.callAjax("/device/device/api/getDeviceStatusData", $searchForm.serializeObject(), function (result) {
			$('span[name="sumQty"]').text("0"); //초기화
			$.each(result, function (key, value) {
				$('#' + key.toUpperCase() + '_QTY').text(Common.addComma(value));
			})
		});
	}

	// 장비 미사용 포함 이벤트
	$viewAllDeviceFlag.on('change', function () {
		getDeviceDataList();
	});

	// 등록 버튼 이벤트 처리
	$addDeviceBtn.on('click', function () {
		Common.showLoading();
		location.href = '/device/device/deviceWrite';
	});

	//체크박스 전체 클릭시 이벤트 처리
	$total_chk.on('click', function () {
		$statusList.prop("checked", $total_chk.is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$statusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $statusList.length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=statusList]:checked").length

		$total_chk.prop("checked", chkCnt == totalCnt);
	});

	$statusUl.on('click', 'li', function () {
		var $this = $(this);
		var status = $this.prop('class');
		var isStatusAll = (status == 'ALL');
		$('input[name="statusList"]').prop("checked", isStatusAll);
		$total_chk.prop("checked", isStatusAll);

		if (!isStatusAll) {
			$.each($statusList, function (index, value) {
				var $value = $(value);
				$value.prop("checked", $value.val() == status);
			});
		}
		getDeviceDataList();
	});

	// 그리드 로우 선택시 이벤트 처리
	deviceGrid.on('click', function (result) {
		var row = result['rowKey'];
		if (row == null) {
			return false;
		}
		Common.showLoading();
		var record = deviceGrid.getRow(row);
		location.href = '/device/device/deviceDetail?deviceSeq=' + record["deviceSeq"] + '&' + $searchForm.serializeListParameter();
	});

});