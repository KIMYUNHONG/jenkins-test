/**
 *  Create By Lee DoWon on 2020-03-04 / 오후 12:08
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var $openLocationListPopupBtn = $('#openLocationListPopupBtn');

	var $openAddressMarkerMapPopupBtn = $('#openAddressMarkerMapPopupBtn');
	var $openLatlngMarkerMapPopupBtn = $('#openLatlngMarkerMapPopupBtn');
	var $openDeviceListPopupBtn = $('#openDeviceListPopupBtn');
	var $saveInstallDeviceBtn = $('#saveInstallDeviceBtn');
	var $cancelBtn = $('#cancelBtn');

	var $beforeFile = $('#beforeFile');
	var $beforeFileList = $('#beforeFileList');
	var $afterFile = $('#afterFile');
	var $afterFileList = $('#afterFileList');
	var queryString = $('input[name="queryString"]').val();

	// 설치일 세팅
	Component.setDatePicker({
		wrapper: 'installDtWrapper',
		element: 'installDt',
		date: deviceSeq == 0 ? new Date() : Date(changeDateFormat(installDt, '.'))
	});
	// 가동기간 세팅
	Component.setRangePicker({
		startpicker: {
			date: deviceSeq == 0 ? new Date() : new Date(changeDateFormat(operateStartDt, '.')),
			input: '#operateStartDt',
			container: '#operateStartDtWrapper'
		},
		endpicker: {
			date: deviceSeq == 0 ? new Date() : new Date(changeDateFormat(operateEndDt, '.')),
			input: '#operateEndDt',
			container: '#operateEndDtWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	//수정으로 접근할 때 다음맵 세팅
	if (deviceSeq > 0) {
		$('#map_view').show();
		$('.lat').show();
		$('#selectedAddress').text($('input[name="deviceAddress"]').val());
		$('#selectedLat').text($('input[name="latitude"]').val());
		$('#selectedLng').text($('input[name="longitude"]').val());
		DaumAddress.createStaticMap({
			id: 'map_view',
			lat: $('input[name="latitude"]').val(),
			lng: $('input[name="longitude"]').val()
		});
	}

	// 설치추가부품 추가 버튼
	var $addPartBtn = $(".addPartBtn");

	// 추가부품 추가버튼 이벤트 처리
	$addPartBtn.on('click', function () {
		var $this = $(this);
		openAddPartListPopup($this);
	});

	// 추가부품 X 버튼 클릭 이벤트 처리
	$('.addpart-list').on('click', '.del_part_btn', function () {
		var $this = $(this);
		$this.closest('.addpart-item').remove();
	});

	//추가부품 팝업 OPEN
	function openAddPartListPopup($this) {

		var domainSeq = $('input[name="deviceSeq"]').val(); // 장비 SEQ

		Common.openPopup('/install/installDevice/addPartListPopup?domainName=INSTALL&domainSeq=' + domainSeq + '', {}, function (result) {
			var $popup = Common.getPopupTemplate("부품교체", result, {width: 600});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});

			var $addpartList = $('.addpart-list');

			// 본문에 등록된 추가 부품이 있는경우 팝업 내용 채움
			if ($addpartList.find('.addpart-item').length > 0) {
				$popup.find('input[name="partQty"]').val(0);

				var list = $addpartList.find('.addpart-item');
				$.each(list, function (index, item) {

					var $item = $(item);
					var selectedPartType = $item.find('input[name="partType"]').val();
					var selectedPartQty = $item.find('input[name="partQty"]').val();
					var $partTypeList = $popup.find('input[name="partType"]');

					$.each($partTypeList, function (partTypeIndex, partType) {

						var $partType = $(partType);
						if ($partType.val() == selectedPartType) {
							$partType.siblings('input[name="partQty"]').val(selectedPartQty);
							return false;
						}
					})
				});
			} else {
				$popup.find('input[name="partQty"]').val(0);
			}

			$popup.find('#saveBtn').on('click', function () {
				var $popupAddPartList = $popup.find('.add-part-item');

				$addpartList.empty();

				$.each($popupAddPartList, function (index, item) {
					var $item = $(item);

					if ($item.find('input[name="partQty"]').val() > 0) {
						var html =
							'<span class="addpart-item">' +
							'\t<span>' + $item.find('input[name="partTypeText"]').val() + ' ' + $item.find('input[name="partQty"]').val() + '개' + '</span>' +
							'\t<input type="hidden" name="partType" value="' + $item.find('input[name="partType"]').val() + '" />  ' +
							'\t<input type="hidden" name="partQty" value="' + $item.find('input[name="partQty"]').val() + '" />' +
							'\t<button type="button" class="del_part_btn">X</button>' +
							'</span>';
						$addpartList.append(html);
					}
				});
				$popup.remove();
			});

			$popup.find('.minusBtn').on('click', function () {
				var $this = $(this);
				var qty = $this.siblings('.qty').val() - 1;
				if (qty < 0) {
					return;
				}
				$this.siblings('.qty').val(qty);
			});

			$popup.find('.plusBtn').on('click', function () {
				var $this = $(this);
				var $target = $this.siblings('.qty');
				var qty = parseInt($target.val());
				qty = qty + 1;
				$target.val(qty)
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 설치전 사진 버튼 처리
	$beforeFile.on('change', function () {
		if ($beforeFileList.find('li').length >= 3) {
			alert("사진은 최대 3개까지 등록 가능합니다.");
			return false;
		}
		Common.photoUpload($beforeFile, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$beforeFile.val(null);
			var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
				'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
				'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t\t<input type="hidden" name="beforeUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t</li>\n');
			$beforeFileList.append($html);
		});
	});

	// 설치후 사진 버튼 처리
	$afterFile.on('change', function () {
		if ($afterFileList.find('li').length >= 3) {
			alert("사진은 최대 3개까지 등록 가능합니다.");
			return false;
		}
		Common.photoUpload($afterFile, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$afterFile.val(null);
			var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
				'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
				'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t\t<input type="hidden" name="afterUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t</li>\n');
			$afterFileList.append($html);
		});
	});

	// 파일 삭제 이벤트 처리
	$beforeFileList.on('click', '.btn_img_del', function () {
		$(this).parent('li').remove();
	});

	// 파일 삭제 이벤트 처리
	$afterFileList.on('click', '.btn_img_del', function () {
		$(this).parent('li').remove();
	});

	// 설치장소 팝업 버튼 처리
	$openLocationListPopupBtn.on('click', function () {
		openLocationListPopup();
	});

	// 지도 위치선택 버튼 처리
	$openAddressMarkerMapPopupBtn.on('click', function () {

		var address = $('#mapUseAddress').val();

		if (address != '' && address != null && address.charAt(0) == '(') {
			address = address.substring(address.indexOf(')') + 2, address.length);
		}
		var parameter = {address: address || ''};

		var lat = $('input[name="latitude"]').val();
		var lng = $('input[name="longitude"]').val();

		if (Valid.isNotEmpty(lat) && Valid.isNotEmpty(lng)) {
			parameter.lat = lat;
			parameter.lng = lng;
			parameter.address = '';
		}
		openAddressMarkerMapPopup(parameter);
	});

	// 지도 직접입력 버튼 처리
	$openLatlngMarkerMapPopupBtn.on('click', function () {
		openLatlngMarkerMapPopup();
	});

	// 장비번호 버튼 처리
	$openDeviceListPopupBtn.on('click', function () {
		openDeviceListPopup();
	});

	// 설치장비 저장 처리
	$saveInstallDeviceBtn.on('click', function () {
		if (confirm('저장 하시겠습니까?')) {

			var parameter = $('#installDeviceForm').serializeObject();
			var $addpartList = $('#addpartList');

			if ($addpartList.find('.addpart-item').length > 0) {
				var addpartList = [];

				$.each($addpartList.find('.addpart-item'), function (index, item) {
					var addpart = {};
					var $item = $(item);
					addpart.partType = $item.find('input[name="partType"]').val();
					addpart.partQty = $item.find('input[name="partQty"]').val();
					addpartList.push(addpart);
				});
				parameter.addpartList = addpartList;
			}
			Common.showLoading();
			Common.callAjax("/device/device/api/setInstallDevice", parameter, function (result) {
				if (result.deviceSeq == null || result.deviceSeq == 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					return false;
				}
				alert("저장이 완료 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('deviceSeq') != -1 ? queryString : 'deviceSeq=' + (result.deviceSeq);
				location.href = '/install/installDevice/installDeviceDetail?' + param;
			});
		}
	});

	// 설치장비 취소 처리
	$cancelBtn.on('click', function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();

			if (deviceSeq > 0) {
				location.href = '/install/installDevice/installDeviceDetail?' + queryString;
			} else {
				location.href = '/install/installDevice/installDeviceList?' + queryString;
			}
		}
	});

	// 설치장소 팝업
	function openLocationListPopup() {
		Common.openPopup('/common/locationListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("설치장소", result, {width: 860});
			$('#popupList').append($popup);

			var locationColumns = [
				{header: '지역', name: 'locationAreaText', width: 70, sortable: true, align: 'center'},
				{header: '설치장소', name: 'locationName', width: 100, sortable: true, align: 'left'},
				{header: '거래처구분', name: 'customerTypeText', width: 80, sortable: true, align: 'center'},
				{header: '거래처', name: 'customerName', width: 100, sortable: true, align: 'left'},
				{header: '주소', name: 'addressText', width: 300, sortable: true, align: 'left'},
				{header: '영업담당', name: 'salesUserName', width: 70, sortable: true, align: 'cen1ter'},
				{header: '설치장비수', name: 'deviceCnt', width: 80, sortable: true, align: 'right'},
			];

			var locationGrid = Grid.createGrid({
				id: 'locationGrid',
				columns: locationColumns,
				data: [],
				bodyHeight: 300
			});

			getLocationList();

			function getLocationList() {
				Common.callAjax("/customer/location/api/getLocationList", $popup.find('#searchLocationForm').serializeObject(), function (result) {
					locationGrid.resetData(result);
					$('#locationTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			locationGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = locationGrid.getRow(row);

				$('input[name="locationName"]').val(record.locationName + ' ' + record.address);
				$('input[name="locationSeq"]').val(record.locationSeq);
				$('input[name="locationLatitude"]').val(record.locationLatitude);
				$('input[name="locationLongitude"]').val(record.locationLongitude);
				$('#mapUseAddress').val(record.address);
				$popup.remove();
			});

			$popup.find('#searchLocationBtn').on('click', function () {
				getLocationList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	function openDeviceListPopup() {
		Common.showLoading();
		Common.openPopup('/common/deviceListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("장비번호", result, {width: 860});
			$('#popupList').append($popup);

			var deviceColumns = [
				{header: '장비번호', name: 'deviceId', width: 150, sortable: true, align: 'center'},
				{header: '상태', name: 'statusText', width: 80, sortable: true, align: 'center'},
				{header: '장비구분', name: 'deviceClassifyText', width: 100, sortable: true, align: 'center'},
				{header: '장비명', name: 'deviceNameText', width: 150, sortable: true, align: 'left'},
				{header: 'ModemId', name: 'modemId', width: 120, sortable: true, align: 'left'},
				{header: '통합 Board Serial 번호', name: 'boardSerial', width: 150, sortable: true, align: 'left'},
				{header: '제조사', name: 'manufactureComText', width: 80, sortable: true, align: 'center'},
				{header: '제조일', name: 'manufactureDtText', width: 80, sortable: true, align: 'center'},
				{header: '통신방식', name: 'communicationModeText', width: 80, sortable: true, align: 'center'},
				{header: '등록일', name: 'regDtText', width: 80, sortable: true, align: 'center'},
			];

			var deviceGrid = Grid.createGrid({
				id: 'deviceGrid',
				columns: deviceColumns,
				data: [],
				bodyHeight: 300
			});

			getDeviceList();

			function getDeviceList() {
				var parameter = {
					deviceId: $popup.find('input[name="deviceId"]').val() || '',
					deviceClassify: $popup.find('select[name="deviceClassify"]').val() || '',
					deviceName: $popup.find('input[name="deviceName"]').val() || '',
					manufactureCom: $popup.find('select[name="manufactureCom"]').val() || '',
					statusList: ['DM020001']
				};
				Common.callAjax("/device/device/api/getDeviceList", parameter, function (result) {
					deviceGrid.resetData(result);
					$popup.find('#deviceTotalCount').text(result != null ? Common.addComma(result.length) : 0);
					Common.hideLoading();
				});
			}

			// 로우 선택시 이벤트 처리
			deviceGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = deviceGrid.getRow(row);

				$('input[name="deviceId"]').val(record.deviceId);
				$('input[name="deviceSeq"]').val(record.deviceSeq);

				$("#operateStartDt").val(record.operateStartDt);
				$("#operateEndDt").val(record.operateEndDt);
				$("#operateStartTime").val(record.operateStartTime);
				$("#operateEndTime").val(record.operateEndTime);

				$popup.remove();
			});

			$popup.find('#searchDeviceBtn').on('click', function () {
				getDeviceList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 위치선택 팝업
	function openAddressMarkerMapPopup(parameter) {
		Common.openPopup('/common/addressMarkerMapPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비위치", result, {width: 600, wrapper: $('#popupList')});
			$('#popupList').append($popup);

			$popup.find('#saveLatlngBtn').on('click', function () {
				setLatLng($popup);

				DaumAddress.createStaticMap({
					id: 'map_view',
					lat: $popup.find('#popupLatitude').val(),
					lng: $popup.find('#popupLongitude').val()
				});
				$popup.remove();
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 직접 위경도 입력 선택 팝업
	function openLatlngMarkerMapPopup() {
		var parameter = {
			  lat: $('input[name="locationLatitude"]').val()
			, lng: $('input[name="locationLongitude"]').val()
		};

		var lat = $('input[name="latitude"]').val();
		var lng = $('input[name="longitude"]').val();

		if (Valid.isNotEmpty(lat) && Valid.isNotEmpty(lng)) {
			parameter.lat = lat;
			parameter.lng = lng;
		}

		Common.openPopup('/common/latlngMarkerMapPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("위·경도 입력", result, {width: 600});
			$('#popupList').append($popup);

			$popup.find('#saveLatlngBtn').on('click', function () {
				setLatLng($popup);
				DaumAddress.createStaticMap({
					id: 'map_view',
					lat: $popup.find('#popupLatitude').val(),
					lng: $popup.find('#popupLongitude').val()
				});
				$popup.remove();
			});
			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	function setLatLng($popup) {
		$('#map_view').show();
		$('.lat').show();
		$('#selectedAddress').text($popup.find('#popupAddress').val());
		$('#selectedLat').text($popup.find('#popupLatitude').val());
		$('#selectedLng').text($popup.find('#popupLongitude').val());
		$('input[name="latitude"]').val($popup.find('#popupLatitude').val());
		$('input[name="longitude"]').val($popup.find('#popupLongitude').val());
		$('input[name="deviceAddress"]').val($popup.find('#popupAddress').val());
	}
});