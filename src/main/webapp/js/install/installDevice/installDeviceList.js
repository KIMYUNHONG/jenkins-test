/**
 *  Create By Lee DoWon on 2020-03-04 / 오후 12:08
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $addInstallDeviceBtn = $('#addInstallDeviceBtn');
	var $searchBtn = $('#searchBtn');
	var $resetBtn = $('#resetBtn');
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼

	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');
	var $totalChk = $("#total_chk");
	var $statusList = $("input[name=statusList]");
	var $searchForm = $('#searchForm');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	$('#dateBtn1').on('click', function () {
		picker.setStartDate(new Date());
		picker.setEndDate(new Date());
	});
	$('#dateBtn2').on('click', function () {
		picker.setStartDate(new Date().addDate(0, 0, -7));
		picker.setEndDate(new Date());
	});
	$('#dateBtn3').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -1, 0));
		picker.setEndDate(new Date());
	});
	$('#dateBtn4').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -3, 0));
		picker.setEndDate(new Date());
	});

	//체크박스 전체 클릭시 이벤트 처리
	$totalChk.on('click', function () {
		$statusList.prop("checked", $totalChk.is(':checked'));
	});

	// 체크박스 클릭시 '전체' 항목 체크 이벤트
	$statusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $statusList.length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=statusList]:checked").length;
		$totalChk.prop("checked", chkCnt == totalCnt);
	});

	var installDeviceColumns = [
		{header: '설치일', name: 'installDt', width: 76, sortable: true, align: 'center'},
		{header: '설치담당자', name: 'installUserName', width: 80, sortable: true, align: 'center'},
		{header: '장비번호', name: 'deviceId', width: 160, sortable: true, align: 'center'},
		{header: '장비명', name: 'deviceNameText', width: 115, sortable: true, align: 'left'},
		{header: '상태', name: 'statusText', width: 52, sortable: true, align: 'center'},
		{header: '지역', name: 'locationAreaText', width: 52, sortable: true, align: 'center'},
		{header: '설치장소', name: 'locationName', width: 190, sortable: true, align: 'left'},
		{header: '관리번호', name: 'manageNo', width: 100, sortable: true, align: 'left'},
		{header: '가로등번호', name: 'lampNo', width: 100, sortable: true, align: 'left'},
		{header: '가동기간', name: 'operateDatePeriod', width: 155, sortable: true, align: 'center'},
		{header: '가동시간', name: 'operateTimePeriod', width: 100, sortable: true, align: 'center'},
		{header: '통신방식', name: 'communicationModeText', width: 70, sortable: true, align: 'center'},
		{header: '거래처구분', name: 'customerTypeText', width: 82, sortable: true, align: 'center'},
		{header: '거래처', name: 'customerName', width: 120, sortable: true, align: 'left'},
		{header: '영업담당자', name: 'salesUserName', width: 75, sortable: true, align: 'center'},
		{header: '보수담당자', name: 'asUserName', width: 75, sortable: true, align: 'center'},
		{header: '탈거일', name: 'removeDateText', width: 100, sortable: true, align: 'center'},
		{header: '탈거담당', name: 'removeUserName', width: 100, sortable: true, align: 'center'},
		{header: '폐기일', name: 'discardDateText', width: 100, sortable: true, align: 'center'},
		{header: '폐기담당', name: 'discardUserName', width: 100, sortable: true, align: 'center'},
		{header: '분실일', name: 'lossDateText', width: 75, sortable: true, align: 'center'},
		{header: '분실처리', name: 'lossUserName', width: 70, sortable: true, align: 'center'}
	];

	var installDeviceGrid = Grid.createGrid({
		id: 'installDeviceGrid',
		columns: installDeviceColumns,
		data: []
	});

	// 거래처 로우 선택시 이벤트 처리
	installDeviceGrid.on('click', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];
		if (row == null) {
			return false;
		}
		Common.showLoading();
		var record = installDeviceGrid.getRow(row);
		location.href = '/install/installDevice/installDeviceDetail?deviceSeq=' + record["deviceSeq"] + '&' + $searchForm.serializeListParameter();

	});

	$addInstallDeviceBtn.on('click', function () {
		Common.showLoading();
		location.href = '/install/installDevice/installDeviceWrite';
	});

	$searchBtn.on('click', function () {
		getInstallDeviceList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {

		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = installDeviceColumns;

		var option = {
			fileName: '설치장비'
		};
		Common.excelDownload("/device/device/excel/getInstallDeviceGridList", parameter, option);
	});

	getInstallDeviceList();

	// 설치장비조회 데이터 로드
	function getInstallDeviceList() {
		Common.showLoading();

		var parameter = $searchForm.serializeObject();

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "installDeviceList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/device/device/api/getInstallDeviceGridList", parameter, function (result) {
			installDeviceGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

});