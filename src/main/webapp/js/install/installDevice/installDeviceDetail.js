/**
 *  Create By Lee DoWon on 2020-03-04 / 오후 12:08
 *  Email : vin4195@uniwiz.co.kr
 **/

$(function () {

	var deviceSeq = $("#deviceSeq").val(); // 장비 시퀀스
	var locationSeq = $('input[name="locationSeq"]').val();	// 설치장소 시퀀스
	var $moveMonitorDetail = $("#moveMonitorDetail"); // 모니터링 상세
	var $moveMaintainDetail = $("#moveMaintainDetail"); // 유지보수 상세
	var $openMaintainReportPopupBtn = $("#openMaintainReportPopupBtn"); // 유지보수 리포트
	var $deleteBtn = $("#deleteBtn"); //삭제버튼
	var $openModifyBtn = $("#openModifyBtn");
	var queryString = $('input[name="queryString"]').val();

	// 작동시간 차트
	var chartWrapper = document.getElementById('chart').getContext('2d');

	var $operateStartTime = $('input[name="operateStartTime"]');
	var $operateEndTime = $('input[name="operateEndTime"]');
	var operateStartTimeValue = $operateStartTime.val() == null ? '0000' : $operateStartTime.val();
	var operateEndTimeValue = $operateEndTime.val() == null ? '0000' : $operateEndTime.val();
	var startHour = operateStartTimeValue.substring(0, 2);
	var startMinute = operateStartTimeValue.substring(2, 4);
	var endHour = operateEndTimeValue.substring(0, 2);
	var endMinute = operateEndTimeValue.substring(2, 4);
	var $chart = $('#chart');

	var startTime = ((Number(startHour))) + (startMinute == '00' ? 0 : 0.5) % 24;
	var endTime = ((Number(endHour))) + (endMinute == '00' ? 0 : 0.5) % 24;
	var gap = Math.abs(endTime - startTime);
	var data = [
		24 - parseFloat(gap % 24),
		parseFloat(gap % 24),
	];

	var chart = new Chart(chartWrapper, {
		type: 'pie',
		data: {
			labels: ['작동', '미작동'],
			datasets: [{
				backgroundColor: [
					"rgb(255, 186, 53)",
					"rgb(255,255 , 255)"
				],
				data: data
			}]
		},
		options: {
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			}
		}
	});

	$chart.css('transform', 'rotate(' + ((startHour) * 15 + (startMinute == '00' ? 0 : 7.5)) + 'deg)');
	var agent = navigator.userAgent.toLowerCase();

	// IE에서 차트 깨지는거 보정
	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1)) { // IE 일 경우
		var $hour = $('.hour');
		$hour.find('canvas').css('position', 'absolute');
		$hour.find('canvas').css('left', '50%');
		$hour.find('canvas').css('top', '50%');
		$hour.find('canvas').css('width', '319px');
		$hour.find('canvas').css('height', '159px');
		$chart.css('margin-top', (Number($chart.css('height').replace('px', '')) / -2 + 'px'));
		$chart.css('margin-left', (Number($chart.css('width').replace('px', '')) / -2 + 'px'));
	}

	// 설치장비 삭제버튼 이벤트 처리
	$deleteBtn.on('click', function () {
		if (confirm('유지보수, 민원처리 등 장비에 대한 모든 데이터가 삭제됩니다. \n삭제 하시겠습니까?')) {
			Common.showLoading();

			var parameter = {
				deviceSeq: deviceSeq
			};

			Common.callAjax("/device/device/api/deleteInstallDevice", parameter, function (result) {
				alert("삭제가 완료되었습니다.");
				location.href = "/install/installDevice/installDeviceList?" + queryString;
			});
		}
	});

	// 모니터링 상세버튼 이벤트 처리
	$moveMonitorDetail.on('click', function () {
		location.href = '/monitor/monitor/monitorDetail?deviceSeq=' + deviceSeq;
	});

	// 유지보수 상세버튼 이벤트처리
	$moveMaintainDetail.on('click', function () {
		location.href = '/manage/maintain/maintainDetail?deviceSeq=' + deviceSeq;
	});

	// 유지보수 리포트버튼 이벤트 처리
	$openMaintainReportPopupBtn.on('click', function () {
		var parameter = {
			deviceSeq: deviceSeq,
			searchType: "",
			searchYear: ""
		};

		Common.openPopup('/manage/maintain/maintainReportPopup', {parameter: parameter}, function (result) {

			var $popup = Common.getPopupTemplate("유지보수 보고서", result, {width: 860});
			$('#popupList').append($popup);
			var $printPopup = $popup.find('#printPopup');

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			$printPopup.on('click', function () {
				$printPopup.hide();
				Common.showLoading();
				$('#printPage').printThis({
					afterPrint: function () {
						$printPopup.show();
						Common.hideLoading();
					}
				});
			});

			// 타이밍 이슈가 있어서 그냥 시간 줌
			setTimeout(function () {
				Common.resizeHeightPopup($popup);
			}, 10)
		});
	});

	$("#barcode").barcode($('input[name="deviceId"]').val(), "code128", {barHeight: 28, fontSize: 11});

	DaumAddress.createLocationStaticMap({
		id: 'detailMap',
		locationSeq: locationSeq,
		deviceSeq: deviceSeq,
	});

	// 장비규격 팝업
	$('#openDeviceStandardPopup').on('click', function () {

		var parameter = {deviceMstSeq: $("#deviceMstSeq").val(), deviceSeq: deviceSeq};
		Common.openPopup('/install/installDevice/deviceStandardPopup', {parameter: parameter}, function (result) {

			var $popup = Common.getPopupTemplate("장비정보", result, {width: 750});
			$('#popupList').append($popup);

			var deviceClassify = $popup.find("#deviceClassify").val();

			// 팝업 오픈시 유형에 에 해당하는 input만 보여지게 설정
			if (deviceClassify == "SD020002") {
				$popup.find('.' + 'SD020001').hide();
			} else {
				$popup.find('.' + 'SD020002').hide();
			}

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	// 장비전체보기 팝업
	$('#openDeviceViewAllPopup').on('click', function () {
		var parameter = {
			locationSeq: locationSeq
		};

		// 전체 가로길이에서 절반빼고 팝업 가로 길이만큼 왼쪽으로
		// 전체 세로길이에서 절반빼고 팝업 세로 절반만큼 위로
		Common.openPopup('/install/installDevice/deviceViewAllPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비 전체보기", result, {width: 760});
			$('#popupList').append($popup);

			var markers = DaumAddress.createLocationAlphabetMap({
				id: 'allViewMap',
				locationSeq: $('input[name="locationSeq"]').val(),
				$list: $('.item_li').find('li')
			}, function (type, index) {
				var $list = $('.item_li').find('li');
				var $itemText = $list.eq(index).find('h3');
				if (type == 'over') {
					$itemText.addClass('on');
				} else {
					$itemText.removeClass('on');
				}
				$list.on('click', function () {
					Common.showLoading();
					location.href = '/install/installDevice/installDeviceDetail?deviceSeq=' + $list.eq(index).data('device-seq');
				});
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	// 사진 클릭시 슬라이더 팝업 오픈
	$('.upload-image').on('click', function () {

		var $this = $(this);
		var parameter = {
			domain: $this.closest('ul').prop('id'),
			key: deviceSeq,
			index: $this.closest('li').index()
		};

		if ($this.closest('div').hasClass('device-status')) {
			parameter.key2 = $this.closest('div').find('input[name="deviceStatusSeq"]').val();
		}
		if ($this.closest('div').hasClass('maintain')) {
			parameter.key = $('input[name="deviceSeq"]').val();
			parameter.key2 = $this.closest('div').find('input[name="maintainSeq"]').val();
		}
		if ($this.closest('div').hasClass('complainHandle')) {
			parameter.key = $this.siblings('input[name="complainSeq"]').val();
			parameter.key2 = $this.siblings('input[name="complainHandleSeq"]').val();
		}

		Common.openPopup('/common/imageSliderPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto: true});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	// 수정 버튼 이벤트
	$openModifyBtn.on('click', function () {
		Common.showLoading();
		var param = Valid.isNotEmpty(queryString) && queryString.indexOf('deviceSeq') != -1 ? queryString : 'deviceSeq=' + (deviceSeq);
		location.href = '/install/installDevice/installDeviceWrite?' + param;
	});

	// 변경 버튼 이벤트
	$('#openChangeStatusPopupBtn').on('click', function () {
		var parameter = {deviceSeq: $('input[name="deviceSeq"]').val()};
		Common.openPopup('/install/installDevice/changeStatusPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비상태 변경", result, {width: 630});
			$('#popupList').append($popup);

			var $status = $popup.find('input[name="status"]');
			var $discardType = $popup.find('select[name="discardType"]');
			var $storage = $('.storage');

			var $saveBtn = $popup.find('#saveBtn');

			var $beforeFile = $popup.find('#beforeFile');
			var $beforeFileList = $popup.find('#beforeFileList');

			var $afterFile = $popup.find('#afterFile');
			var $afterFileList = $popup.find('#afterFileList');

			var $photoFile = $popup.find('#photoFile');
			var $photoFileList = $popup.find('#photoFileList');

			var $proofFile = $popup.find('#proofFile');
			var $proofFileList = $popup.find('#proofFileList');

			bindTableComponentByStatus();

			var $operateStartDt = $popup.find('#operateStartDt');
			var $operateEndDt = $popup.find('#operateEndDt');

			var operateRangePicker = Component.setRangePicker({
				startpicker: {
					date: $operateStartDt.val() != '' ? new Date(changeDateFormat($operateStartDt.val(), '/')) : new Date(),
					input: '#operateStartDt',
					container: '#operateStartDtWrapper'
				},
				endpicker: {
					date: $operateEndDt.val() != '' ? new Date(changeDateFormat($operateEndDt.val(), '/')) : new Date(),
					input: '#operateEndDt',
					container: '#operateEndDtWrapper'
				},
				format: "yyyy.MM.dd",
				language: 'ko'
			});

			$status.on('change', function () {
				bindTableComponentByStatus();
				$popup.find('li').remove();
				$popup.find('select').val('');
				$popup.find('textarea').text('');
				$storage.hide();
				if ($(this).val() == 'DM020003') {
					$storage.show();
				}
			});

			$saveBtn.on('click', function () {
				Common.showLoading();
				Common.callAjax("/device/device/api/setDeviceStatus", $('#deviceStatusFrom').serializeObject(), function (result) {
					alert("장비상태가 변경 되었습니다.");
					location.reload();
				});
			});


			$discardType.on('change', function () {
				var value = $(this).val();
				if (value != 'IM070002') {
					$storage.hide();
					$('select[name="storage"]').val('');
				} else {
					$storage.show();
				}
			})

			function bindTableComponentByStatus() {
				var status = $('input[name="status"]:checked').val();
				$popup.find('tr').hide();
				$popup.find('.' + status).show();
			}

			// 사진 버튼 처리
			$photoFile.on('change', function () {
				if ($photoFileList.find('li').length >= 3) {
					alert("사진은 최대 3개까지 등록 가능합니다.");
					return false;
				}
				Common.photoUpload($photoFile, function (result) {
					if (result == null) {
						alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
					}

					$photoFile.val(null);
					var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
						'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
						'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t\t<input type="hidden" name="photoUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t</li>\n');

					$photoFileList.append($html);
				});
			});
			$beforeFile.on('change', function () {
				if ($beforeFileList.find('li').length >= 3) {
					alert("사진은 최대 3개까지 등록 가능합니다.");
					return false;
				}
				Common.photoUpload($beforeFile, function (result) {
					if (result == null) {
						alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
					}

					$beforeFile.val(null);
					var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
						'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
						'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t\t<input type="hidden" name="beforeUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t</li>\n');

					$beforeFileList.append($html);
				});
			});

			// 설치후 사진 버튼 처리
			$afterFile.on('change', function () {
				if ($afterFileList.find('li').length >= 3) {
					alert("사진은 최대 3개까지 등록 가능합니다.");
					return false;
				}
				Common.photoUpload($afterFile, function (result) {
					if (result == null) {
						alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
					}

					$afterFile.val(null);
					var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
						'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
						'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t\t<input type="hidden" name="afterUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
						'\t\t\t\t\t\t\t\t</li>\n');

					$afterFileList.append($html);
				});
			});

			// 파일 추가 버튼 이벤트 처리
			$proofFile.on('change', function () {
				if ($($proofFileList.find('li')).length >= 5) {
					alert("파일은 최대 5개까지 등록 가능합니다.");
					return false;
				}
				Common.fileUpload($proofFile, function (result) {
					if (result == null) {
						alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
					}

					$proofFile.val(null);
					var $html = $('\t\t\t\t\t\t\t<li><a href="/uploadFile/api/download/' + result['uploadFileKey'] + '"' +
						' class="icon file" target="_blank" download="' + result['originalFileName'] + '">' + result['originalFileName'] + '</a>' +
						'<button type="button" class="btn_del"></button>' +
						'<input type="hidden" name="proofUploadFileKey" value="' + result['uploadFileKey'] + '"></li>\n');

					$proofFileList.append($html);
				});
			});
			// 파일 삭제 이벤트 처리
			$photoFileList.on('click', '.btn_img_del', function () {
				$(this).parent('li').remove();
			});
			// 파일 삭제 이벤트 처리
			$beforeFileList.on('click', '.btn_img_del', function () {
				$(this).parent('li').remove();
			});
			// 파일 삭제 이벤트 처리
			$afterFileList.on('click', '.btn_img_del', function () {
				$(this).parent('li').remove();
			});
			// 파일 삭제 이벤트 처리
			$proofFileList.on('click', '.btn_del', function () {
				$(this).parent('li').remove();
			});
			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	});

	// 목록 버튼 이벤트
	$('#moveListBtn').on('click', function () {
		Common.showLoading();
		location.href = "/install/installDevice/installDeviceList?" + queryString;
	});

});

// kakao Map
// 인포윈도우를 표시하는 클로저를 만드는 함수입니다
function makeOverListener(map, marker, infowindow) {
	return function () {
		infowindow.open(map, marker);
	};
}

// kakao Map
// 인포윈도우를 닫는 클로저를 만드는 함수입니다
function makeOutListener(infowindow) {
	return function () {
		infowindow.close();
	};
}