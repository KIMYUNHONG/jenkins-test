/**
 *  Create By Lee DoWon on 2020-04-08 / 오전 11:08
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {
	Common.showLoading();

	var $addInstallRequestBtn = $('#addInstallRequestBtn');
	var $searchBtn = $('#searchBtn');
	var $resetBtn = $('#resetBtn');
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼

	var $searchStartDate = $('#searchStartDate');
	var $searchEndDate = $('#searchEndDate');
	var $searchForm = $('#searchForm');
	var $installRequestTypeList = $('input[name=installRequestTypeList]');
	var $statusList = $('input[name=statusList]');
	var $total_chk1 = $("#total_chk1");
	var $total_chk2 = $("#total_chk2");

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($searchStartDate.val()) ? new Date(changeDateFormat($searchStartDate.val(), '/')) : '',
			input: '#searchStartDate',
			container: '#searchStartDateWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($searchEndDate.val()) ? new Date(changeDateFormat($searchEndDate.val(), '/')) : '',
			input: '#searchEndDate',
			container: '#searchEndDateWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	$('#dateBtn1').on('click', function () {
		picker.setStartDate(new Date());
		picker.setEndDate(new Date());
	});
	$('#dateBtn2').on('click', function () {
		picker.setStartDate(new Date().addDate(0, 0, -7));
		picker.setEndDate(new Date());
	});
	$('#dateBtn3').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -1, 0));
		picker.setEndDate(new Date());
	});
	$('#dateBtn4').on('click', function () {
		picker.setStartDate(new Date().addDate(0, -3, 0));
		picker.setEndDate(new Date());
	});

	//체크박스 전체 클릭시 이벤트 처리
	$total_chk1.on('click', function () {
		$installRequestTypeList.prop("checked", $("#total_chk1").is(':checked'));
	});

	//체크박스 전체 클릭시 이벤트 처리
	$total_chk2.on('click', function () {
		$statusList.prop("checked", $("#total_chk2").is(':checked'));
	});

	//설치구분 체크박스 클릭시 '전체' 항목 체크 이벤트
	$installRequestTypeList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=installRequestTypeList]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=installRequestTypeList]:checked").length;
		$total_chk1.prop("checked", chkCnt == totalCnt);
	});

	// 설치상태 체크박스 클릭시 '전체' 항목 체크 이벤트
	$statusList.on('click', function () {
		// 전체 체크박스 갯수
		var totalCnt = $("input:checkbox[name=statusList]").length;
		// 체크박스 선택된 갯수
		var chkCnt = $("input:checkbox[name=statusList]:checked").length;
		$total_chk2.prop("checked", chkCnt == totalCnt);
	});

	var installRequestColumns = [
		{header: '설치예정일 (설치일)', name: 'installDueFinishDtText', minWidth: 170, sortable: true, align: 'center'},
		{header: 'D-day', name: 'decimalDay', minWidth: 40, sortable: true, align: 'center', numberformat: true},
		{header: '설치 구분', name: 'installRequestTypeText', minWidth: 70, sortable: true, align: 'center'},
		{header: '지역', name: 'locationAreaText', minWidth: 70, sortable: true, align: 'center'},
		{header: '설치장소', name: 'locationName', minWidth: 180, sortable: true, align: 'left'},
		{header: '상태', name: 'installRequestStatusText', minWidth: 60, sortable: true, align: 'center'},
		{header: '설치대수', name: 'installQtyText', minWidth: 70, sortable: true, align: 'center'},
		{header: '장비명', name: 'deviceNameText', minWidth: 150, sortable: true, align: 'left'},
		{header: '거래처 구분', name: 'customerTypeText', minWidth: 70, sortable: true, align: 'center'},
		{header: '거래처', name: 'customerName', minWidth: 90, sortable: true, align: 'center'},
		{header: '영업담당', name: 'salesUserName', minWidth: 70, sortable: true, align: 'center'},
		{header: '설치담당', name: 'installUserName', minWidth: 70, sortable: true, align: 'center'},
		{header: '요청자', name: 'regName', minWidth: 60, sortable: true, align: 'center'},
		{header: '요청일', name: 'regDtText', minWidth: 80, sortable: true, align: 'center'}
	];

	var installRequestGrid = Grid.createGrid({
		id: 'installRequestGrid',
		columns: installRequestColumns,
		data: []
	});

	// 설치요청 로우 선택시 이벤트 처리
	installRequestGrid.on('click', function (result) {
		var row = result['rowKey'];
		var prevRow = result['prevRowKey'];

		if (row == null) {
			return false;
		}

		Common.showLoading();
		var record = installRequestGrid.getRow(row);
		location.href = '/install/installRequest/installRequestDetail?installRequestSeq=' + record["installRequestSeq"] + '&' + $searchForm.serializeListParameter();
	});

	// 설치요청 등록 버튼 클릭시 이벤트 처리
	$addInstallRequestBtn.on('click', function () {
		Common.showLoading();
		location.href = '/install/installRequest/installRequestWrite';
	});

	$searchBtn.on('click', function () {
		getInstallRequestList();
	});

	$resetBtn.on('click', function () {
		$('#searchForm')[0].reset();
		picker.setStartDate(null);
		picker.setEndDate(null);
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $('#searchForm').serializeObject();
		parameter.gridColumnList = installRequestColumns;

		var option = {
			fileName: '장비설치요청'
		};

		Common.excelDownload("/install/installRequest/excel/getInstallRequestList", parameter, option);

	});

	getInstallRequestList();

	// 장비설치요청 데이터 로드
	function getInstallRequestList() {
		Common.showLoading();

		// 검색 Validation
		var parameter = $searchForm.serializeObject();

		history.replaceState(null, "", Valid.isEmpty($searchForm.serializeListParameter()) ? "installRequestList" : "?" + $searchForm.serializeListParameter());

		Common.callAjax("/install/installRequest/api/getInstallRequestList", parameter, function (result) {
			installRequestGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}

});