/**
 *  Create By Kim YunHong on 2020-04-08 / 오후 4:36
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $addInstallRequestBtn = $("#addInstallRequestBtn"); // 등록버튼
	var $modifyBtn = $("#modifyBtn");     // 수정버튼
	var $moveListBtn = $("#moveListBtn"); // 목록버튼
	var $openLocationListPopupBtn = $('#openLocationListPopupBtn');
	var $openDeviceMstListPopupBtn = $("#openDeviceMstListPopupBtn");
	var $openInstallUserListPopupBtn = $("#openInstallUserListPopupBtn");
	var queryString = $('input[name="queryString"]').val();
	var $installRequestForm = $('#installRequestForm');

	var installDueDate = Component.setDatePicker({
		wrapper: 'installDueDateWrapper',
		element: 'installDueDate',
		date: installRequestSeq == 0 ? new Date() : '',
		selectableRanges: [
			[new Date(), new Date(9999, 11, 31)]
		]
	});

	// 설치장소 팝업 버튼 처리
	$openLocationListPopupBtn.on('click', function () {
		openLocationListPopup();
	});

	// 장비마스터 팝업 버튼 처리
	$openDeviceMstListPopupBtn.on('click', function () {
		openDeviceMstListPopup();
	});

	// 설치담당자 팝업 버튼 처리
	$openInstallUserListPopupBtn.on('click', function () {
		openInstallUserListPopup();
	});

	//등록버튼 클릭시 이벤트
	$addInstallRequestBtn.on("click", function () {
		if (confirm('저장 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/setInstallRequest", $installRequestForm.serializeObject(), function (result) {
				if (result.installRequestSeq == null || result.installRequestSeq == 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					return false;
				}
				alert("저장이 완료 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	//수정버튼 클릭시 이벤트
	$modifyBtn.on("click", function () {
		if (confirm('수정 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/setInstallRequest", $installRequestForm.serializeObject(), function (result) {
				if (result.installRequestSeq == null || result.installRequestSeq == 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					return false;
				}
				alert("수정이 완료 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	//목록버튼 클릭시 이벤트
	$moveListBtn.on("click", function () {
		Common.showLoading();
		location.href = "/install/installRequest/installRequestList?" + queryString;
	});

	// 설치장소 팝업
	function openLocationListPopup() {
		Common.openPopup('/common/locationListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("설치장소", result, {width: 860});
			$('#popupList').append($popup);

			var locationColumns = [
				{header: '지역', name: 'locationAreaText', width: 70, sortable: true, align: 'center'},
				{header: '설치장소', name: 'locationName', width: 100, sortable: true, align: 'left'},
				{header: '거래처구분', name: 'customerTypeText', width: 80, sortable: true, align: 'center'},
				{header: '거래처', name: 'customerName', width: 100, sortable: true, align: 'left'},
				{header: '주소', name: 'addressText', width: 300, sortable: true, align: 'left'},
				{header: '영업담당', name: 'salesUserName', width: 70, sortable: true, align: 'cen1ter'},
				{header: '설치장비수', name: 'deviceCnt', width: 80, sortable: true, align: 'right'},
			];

			var locationGrid = Grid.createGrid({
				id: 'locationGrid',
				columns: locationColumns,
				data: [],
				bodyHeight: 300
			});

			getLocationList();

			function getLocationList() {
				Common.callAjax("/customer/location/api/getLocationList", $popup.find('#searchLocationForm').serializeObject(), function (result) {
					locationGrid.resetData(result);
					$('#locationTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			locationGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = locationGrid.getRow(row);

				$('input[name="locationName"]').val(record.locationName + ' ' + record.address);
				$('input[name="locationSeq"]').val(record.locationSeq);

				$('input[name="installUserName"]').val(record.asUserName);
				$('input[name="installUserId"]').val(record.asUserId);
				$('input[name="installCustomerSeq"]').val(record.asCustomerSeq);

				$("#locationAddress").text(record.addressText);
				$("#salesUserName").text(record.salesUserName);
				$("#customerName").text(record.customerTypeText + " / " + record.customerName);

				$popup.remove();
			});

			$popup.find('#searchLocationBtn').on('click', function () {
				getLocationList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 장비MST 조회 팝업
	function openDeviceMstListPopup() {

		Common.openPopup('/device/device/selectDeviceMasterPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("장비선택", result, {width: 680});
			$('#popupList').append($popup);
			var deviceMasterColumns = [
				{header: '구분', name: 'deviceClassifyText', width: 100, sortable: true, align: 'center'},
				{header: '장비명', name: 'deviceNameText', width: 150, sortable: true, align: 'left'},
				{header: '모델명', name: 'modelName', minWidth: 171, sortable: true, align: 'left'},
				{header: '제조사', name: 'manufactureComText', width: 100, sortable: true, align: 'center'}
			];

			var deviceMasterGrid = Grid.createGrid({
				id: 'deviceMasterGrid',
				columns: deviceMasterColumns,
				data: [],
				bodyHeight: 300
			});

			$popup.find('#searchForm').on('submit', function () {
				return false;
			});

			getDeviceMasterList();

			function getDeviceMasterList() {

				var parameter = $popup.find('#searchForm').serializeObject();

				Common.callAjax("/standard/deviceMaster/api/getDeviceMasterList", parameter, function (result) {
					deviceMasterGrid.resetData(result);
					$('#deviceTotalCount').text(result == null ? 0 : Common.addComma(result.length));
				});
			}

			// 로우 선택시 이벤트 처리
			deviceMasterGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = deviceMasterGrid.getRow(row)

				//사용자 view용 TEXT 세팅
				$.each(record, function (key, value) {
					$('#' + key).text(value);
				});

				//INSERT를 위한 CODE값 세팅
				$('input[name="deviceMstSeq"]').val(record.deviceMstSeq);
				$('input[name="deviceNameText"]').val(record.deviceNameText);
				$('input[name="deviceName"]').val(record.deviceName);
				$popup.remove();
			});

			$popup.find('#searchDeviceMasterBtn').on('click', function () {
				getDeviceMasterList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})

	}

	// 설치담당자 조회 팝업
	function openInstallUserListPopup() {

		Common.openPopup('/common/asUserListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("설치담당자", result, {width: 600});
			$('#popupList').append($popup);

			var $searchAsUserForm = $popup.find('#searchAsUserForm');
			var asUserColumns = [
				{header: '성명', name: 'name', width: 100, sortable: true, align: 'center'},
				{header: '직책', name: 'rankText', width: 100, sortable: true, align: 'center'},
				{header: '휴대폰번호', name: 'phone', width: 120, sortable: true, align: 'center'},
				{header: '소속', name: 'customerName', width: 222, sortable: true, align: 'center'},
			];

			var asUserGrid = Grid.createGrid({
				id: 'asUserGrid',
				columns: asUserColumns,
				data: [],
				bodyHeight: 300
			});

			getAsUserList();

			function getAsUserList() {
				Common.callAjax("/standard/user/api/getAsUserList", $searchAsUserForm.serializeObject(), function (result) {
					asUserGrid.resetData(result);
					$popup.find('#asUserTotalCount').text(result != null ? Common.addComma(result.length) : 0);
				});
			}

			// 로우 선택시 이벤트 처리
			asUserGrid.on('click', function (result) {
				var row = result['rowKey'];
				if (row == null) {
					return false;
				}
				var record = asUserGrid.getRow(row);

				$popup.remove();
				$('input[name="installUserName"]').val(record.name);
				$('input[name="installUserId"]').val(record.userId);
				$('input[name="installCustomerSeq"]').val(record.customerSeq);
			});

			$popup.find('#searchAsUserBtn').on('click', function () {
				getAsUserList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});

			Common.resizeHeightPopup($popup);
		}, {})

	}

});