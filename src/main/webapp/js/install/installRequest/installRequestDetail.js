/**
 *  Create By Kim YunHong on 2020-04-08 / 오후 12:08
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var installRequestSeq = $("#installRequestSeq").val();			// INSTALL_REQUEST_SEQ(KEY)

	var $deleteBtn = $("#deleteBtn");     // 삭제버튼
	var $modifyBtn = $("#modifyBtn");     // 수정버튼
	var $moveListBtn = $("#moveListBtn"); // 목록버튼
	var $receiptBtn = $("#receiptBtn"); // 요청접수버튼
	var $addInstallDeviceBtn = $("#addInstallDeviceBtn"); // 설치장비 추가버튼
	var $installComplete = $("#installComplete"); //설치완료 버튼
	var $installConfirm = $("#installConfirm"); //확인완료
	var queryString = $('input[name="queryString"]').val();
	var $installRequestForm = $("#installRequestForm");


	//설치장비 추가버튼 클릭시 이벤트
	$addInstallDeviceBtn.on("click", function () {
		Common.showLoading();
		var gbn = "add"; // 설치대수 비교를 위해 구분자 추가
		var param = queryString;

		// queryString의 특정 파라메터, 값 삭제처리
		param = Common.removeStringParam(param, 'deviceSeq');
		param = Common.removeStringParam(param, 'gbn');
		location.href = "/install/installRequest/installRequestDeviceWrite?" + param + 'deviceSeq=0' + '&gbn=' + gbn;
	});

	//삭제버튼 클릭시 이벤트
	$deleteBtn.on("click", function () {
		if (confirm('삭제 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/deleteInstallRequest", $installRequestForm.serializeObject(), function (result) {
				location.href = "/install/installRequest/installRequestList?" + queryString;
			});
		}
	});

	//설치요청 접수 버튼 클릭시 이벤트
	$receiptBtn.on("click", function () {
		Common.showLoading();
		Common.callAjax("/install/installRequest/api/setReceiptInstallRequest", $installRequestForm.serializeObject(), function (result) {
			alert("설치 요청이 접수 되었습니다.");
			var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
			location.href = '/install/installRequest/installRequestDetail?' + param;
		});
	});

	//수정버튼 클릭시 이벤트
	$modifyBtn.on("click", function () {
		Common.showLoading();
		var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
		location.href = '/install/installRequest/installRequestWrite?' + param;
	});

	//목록버튼 클릭시 이벤트
	$moveListBtn.on("click", function () {
		Common.showLoading();
		location.href = "/install/installRequest/installRequestList?" + queryString;
	});

	//설치완료버튼 클릭시 이벤트
	$installComplete.on("click", function () {
		if (confirm('설치완료시 더 이상 장비정보를 변경 할 수 없습니다. 설치완료 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/setCompleteInstallRequest", $installRequestForm.serializeObject(), function (result) {
				alert("설치완료 처리 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	//확인완료버튼 클릭시 이벤트
	$installConfirm.on("click", function () {
		if (confirm('설치정보를 확인완료 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/setFinishInstallRequest", $installRequestForm.serializeObject(), function (result) {
				alert("확인완료 처리 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	//설치장비 목록 위치버튼 클릭시 이벤트
	$("button[name=locationBtn]").on('click', function () {
		var $this = $(this);
		var locationSeq = $this.siblings('input[name="locationSeqPopup"]').val();

		var parameter = {
			locationSeq: locationSeq
		};
		openLocationMapPopup(parameter);
	});

	//설치장비 목록 수정버튼 클릭시 이벤트
	$("button[name=rowModifyBtn]").on('click', function () {
		var $this = $(this);
		var deviceSeq = $this.siblings('input[name="deviceSeqModify"]').val();
		var installRequestSeq = $this.siblings('input[name="installRequestSeqModify"]').val();
		var gbn = "modify";
		location.href = '/install/installRequest/installRequestDeviceWrite?' + '&deviceSeq=' + deviceSeq + '&gbn=' + gbn + '&' + queryString;
	});

	//설치장비 목록 삭제버튼 클릭시 이벤트
	$("button[name=rowDeleteBtn]").on('click', function () {
		var $this = $(this);
		var deviceSeq = $this.siblings('input[name="deviceSeqDelete"]').val();
		var installRequestSeq = $this.siblings('input[name="installRequestSeqDelete"]').val();

		var parameter = {
			deviceSeq: deviceSeq,
			installRequestSeq: installRequestSeq
		};

		if (confirm('삭제 하시겠습니까?')) {
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/deleteInstallRequestDevice", parameter, function (result) {
				alert("삭제 되었습니다.");
				var param = Valid.isNotEmpty(queryString) && queryString.indexOf('installRequestSeq') != -1 ? queryString : 'installRequestSeq=' + (result.installRequestSeq);
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	// 사진 클릭시 슬라이더 팝업 오픈
	$('.upload-image1').on('click', function () {
		var $this = $(this);
		openPhotoPopup('INSTALL_DEVICE_BEFORE', $this.siblings('input[name="deviceSeqBefore"]').val(), 0, 0);

	});

	// 사진 클릭시 슬라이더 팝업 오픈
	$('.upload-image2').on('click', function () {
		var $this = $(this);
		openPhotoPopup('INSTALL_DEVICE_AFTER', $this.siblings('input[name="deviceSeqAfter"]').val(), 0, 0);
	});

	function openPhotoPopup(domain, key, key2, index) {
		key2 = key2 == null ? 0 : key2;
		var parameter = {
			domain: domain,
			key: key,
			key2: key2,
			index: index
		};

		Common.openPopup('/common/imageSliderPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto: true});
			$('#popupList').append($popup);
			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	}

	//설치장비 위치팝업
	function openLocationMapPopup(parameter) {
		Common.openPopup('/customer/location/locationMapPopup', {parameter: parameter}, function (result) {
			var $detailPopup = Common.getPopupTemplate("설치장소", result, {width: 600});
			$('#popupList').append($detailPopup);
			$detailPopup.find('.popup-close-btn').on('click', function () {
				$detailPopup.remove();
			});
			Common.resizeHeightPopup($detailPopup);
		}, {})
	}

});