/**
 *  Create By Kim YunHong on 2020-04-10 / 오후 1:36
 *  Email : fbsghd123@uniwiz.co.kr
 **/

$(function () {

	var $saveInstallDeviceBtn = $("#saveInstallDeviceBtn"); //저장 버튼
	var $cancelBtn = $("#cancelBtn"); //취소 버튼

	var $openDeviceListPopupBtn = $('#openDeviceListPopupBtn');
	var $openAddressMarkerMapPopupBtn = $('#openAddressMarkerMapPopupBtn');
	var $openLatlngMarkerMapPopupBtn = $('#openLatlngMarkerMapPopupBtn');

	var $beforeFile = $('#beforeFile');
	var $beforeFileList = $('#beforeFileList');
	var $afterFile = $('#afterFile');
	var $afterFileList = $('#afterFileList');

	// 설치추가부품 추가 버튼
	var $addPartBtn = $(".addPartBtn");
	var queryString = $('input[name="queryString"]').val();

	// 추가부품 추가버튼 이벤트 처리
	$addPartBtn.on('click', function () {
		var $this = $(this);
		openAddPartListPopup($this);
	});

	// 추가부품 X 버튼 클릭 이벤트 처리
	$('.addpart-list').on('click', '.del_part_btn', function () {
		var $this = $(this);
		$this.closest('.addpart-item').remove();
	});

	//추가부품 팝업 OPEN
	function openAddPartListPopup($this) {

		var domainSeq = $('input[name="deviceSeq"]').val(); // 장비 SEQ

		Common.openPopup('/install/installRequest/addPartListPopup?domainName=INSTALL&domainSeq=' + domainSeq + '', {}, function (result) {
			var $popup = Common.getPopupTemplate("부품교체", result, {width: 600});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});

			var $addpartList = $('.addpart-list');

			// 본문에 등록된 추가 부품이 있는경우 팝업 내용 채움
			if ($addpartList.find('.addpart-item').length > 0) {
				$popup.find('input[name="partQty"]').val(0);

				var list = $addpartList.find('.addpart-item');
				$.each(list, function (index, item) {

					var $item = $(item);
					var selectedPartType = $item.find('input[name="partType"]').val();
					var selectedPartQty = $item.find('input[name="partQty"]').val();
					var $partTypeList = $popup.find('input[name="partType"]');

					$.each($partTypeList, function (partTypeIndex, partType) {

						var $partType = $(partType);
						if ($partType.val() == selectedPartType) {
							$partType.siblings('input[name="partQty"]').val(selectedPartQty);
							return false;
						}
					})
				});
			} else {
				$popup.find('input[name="partQty"]').val(0);
			}
			$popup.find('#saveBtn').on('click', function () {
				var $addPartList = $popup.find('.add-part-item');

				$('.addpart-list').empty();

				$.each($addPartList, function (index, item) {
					var $item = $(item);

					if ($item.find('input[name="partQty"]').val() > 0) {
						var html =
							'<span class="addpart-item">' +
							'\t<span>' + $item.find('input[name="partTypeText"]').val() + ' ' + $item.find('input[name="partQty"]').val() + '개' + '</span>' +
							'\t<input type="hidden" name="partType" value="' + $item.find('input[name="partType"]').val() + '" />  ' +
							'\t<input type="hidden" name="partQty" value="' + $item.find('input[name="partQty"]').val() + '" />' +
							'\t<button type="button" class="del_part_btn">X</button>' +
							'</span>';
						$('.addpart-list').append(html);
					}
				});
				$popup.remove();
			});

			$popup.find('.minusBtn').on('click', function () {
				var $this = $(this);
				var qty = $this.siblings('.qty').val() - 1;
				if (qty < 0) {
					return;
				}
				$this.siblings('.qty').val(qty);
			});

			$popup.find('.plusBtn').on('click', function () {
				var $this = $(this);
				var $target = $this.siblings('.qty');
				var qty = parseInt($target.val());
				qty = qty + 1;
				$target.val(qty)
			});

			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 장비설치요청 설치장비 저장 처리
	$saveInstallDeviceBtn.on('click', function () {
		if (confirm('저장 하시겠습니까?')) {
			var parameter = $('#installDeviceForm').serializeObject();
			var $addpartList = $('#addpartList');

			if ($addpartList.find('.addpart-item').length > 0) {
				var addpartList = [];

				$.each($addpartList.find('.addpart-item'), function (index, item) {
					var addpart = {};
					var $item = $(item);
					addpart.partType = $item.find('input[name="partType"]').val();
					addpart.partQty = $item.find('input[name="partQty"]').val();
					addpartList.push(addpart);
				});
				parameter.addpartList = addpartList;
			}
			Common.showLoading();
			Common.callAjax("/install/installRequest/api/setInstallRequestDevice", parameter, function (result) {
				if (result.installRequestSeq == null || result.installRequestSeq == 0) {
					alert("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					return false;
				}
				alert("저장이 완료 되었습니다.");
				var param = queryString;
				param = Common.removeStringParam(param,"deviceSeq");
				param = Common.removeStringParam(param,"gbn");
				location.href = '/install/installRequest/installRequestDetail?' + param;
			});
		}
	});

	//취소 버튼 클릭시 이벤트
	$cancelBtn.on("click", function () {
		if (confirm('취소 하시겠습니까?')) {
			Common.showLoading();
			var param = queryString;
			param = Common.removeStringParam(param,"deviceSeq");
			param = Common.removeStringParam(param,"gbn");
			location.href = '/install/installRequest/installRequestDetail?' + param;
		}
	});

	var installDt = Component.setDatePicker({
		wrapper: 'installDtWrapper',
		element: 'installDt',
		date: deviceSeq == 0 ? new Date() : new Date(changeDateFormat(installDt, '/'))
	});

	var operateRangePicker = Component.setRangePicker({
		startpicker: {
			date: deviceSeq == 0 ? new Date() : new Date(changeDateFormat(operateStartDt, '/')),
			input: '#operateStartDt',
			container: '#operateStartDtWrapper'
		},
		endpicker: {
			date: deviceSeq == 0 ? new Date() : new Date(changeDateFormat(operateEndDt, '/')),
			input: '#operateEndDt',
			container: '#operateEndDtWrapper'
		},
		format: "yyyy.MM.dd",
		language: 'ko'
	});

	// 설치전 사진 버튼 처리
	$beforeFile.on('change', function () {
		if ($beforeFileList.find('li').length >= 3) {
			alert("사진은 최대 3개까지 등록 가능합니다.");
			return false;
		}
		Common.photoUpload($beforeFile, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$beforeFile.val(null);
			var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
				'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
				'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t\t<input type="hidden" name="beforeUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t</li>\n');
			$beforeFileList.append($html);
		});
	});

	// 설치후 사진 버튼 처리
	$afterFile.on('change', function () {
		if ($afterFileList.find('li').length >= 3) {
			alert("사진은 최대 3개까지 등록 가능합니다.");
			return false;
		}
		Common.photoUpload($afterFile, function (result) {
			if (result == null) {
				alert("파일업로드중 에러가 발생했습니다. 다시 시도해주세요.");
			}

			$afterFile.val(null);
			var $html = $('\t\t\t\t\t\t\t\t<li>\n' +
				'\t\t\t\t\t\t\t\t\t<button type="button" class="btn_img_del"></button>\n' +
				'\t\t\t\t\t\t\t\t\t<img src="/uploadFile/api/thumbnailDownload/' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t\t<input type="hidden" name="afterUploadFileKey" value="' + result['uploadFileKey'] + '">\n' +
				'\t\t\t\t\t\t\t\t</li>\n');
			$afterFileList.append($html);
		});
	});

	// 사진 클릭시 슬라이더 팝업 오픈(설치전)
	$('.upload-image1').on('click', function () {
		var $this = $(this);
		openPhotoPopup('INSTALL_DEVICE_BEFORE', $this.siblings('input[name="deviceSeq1"]').val(), 0, 0);
	});

	// 사진 클릭시 슬라이더 팝업 오픈(설치후)
	$('.upload-image2').on('click', function () {
		var $this = $(this);
		openPhotoPopup('INSTALL_DEVICE_AFTER', $this.siblings('input[name="deviceSeq2"]').val(), 0, 0);
	});

	function openPhotoPopup(domain, key, key2, index) {
		key2 = key2 == null ? 0 : key2;

		var parameter = {
			domain: domain,
			key: key,
			key2: key2,
			index: index
		};

		Common.openPopup('/common/imageSliderPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("사진", result, {width: 900, isPhoto: true});
			$('#popupList').append($popup);

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		});
	}

	// 파일 삭제 이벤트 처리
	$beforeFileList.on('click', '.btn_img_del', function () {
		$(this).parent('li').remove();
	});

	// 파일 삭제 이벤트 처리
	$afterFileList.on('click', '.btn_img_del', function () {
		$(this).parent('li').remove();
	});

	// 지도 위치선택 버튼 처리
	$openAddressMarkerMapPopupBtn.on('click', function () {

		var address = $('#mapUseAddress').val();

		if (address != '' && address != null && address.charAt(0) == '(') {
			address = address.substring(address.indexOf(')') + 2, address.length);
		}
		var parameter = {address: address || ''};
		var lat = $('input[name="latitude"]').val();
		var lng = $('input[name="longitude"]').val();

		if (Valid.isNotEmpty(lat) && Valid.isNotEmpty(lng)) {
			parameter.lat = lat;
			parameter.lng = lng;
			parameter.address = '';
		}
		openAddressMarkerMapPopup(parameter);
	});

	// 지도 직접입력 버튼 처리
	$openLatlngMarkerMapPopupBtn.on('click', function () {
		openLatlngMarkerMapPopup();
	});

	// 장비번호 버튼 처리
	$openDeviceListPopupBtn.on('click', function () {
		openDeviceListPopup();
	});

	function openDeviceListPopup() {
		Common.showLoading();
		Common.openPopup('/common/deviceListPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("장비번호", result, {width: 860});
			$('#popupList').append($popup);

			var deviceColumns = [
				{header: '장비번호', name: 'deviceId', width: 110, sortable: true, align: 'center'},
				{header: '상태', name: 'statusText', width: 80, sortable: true, align: 'center'},
				{header: '장비구분', name: 'deviceClassifyText', width: 100, sortable: true, align: 'center'},
				{header: '장비명', name: 'deviceNameText', width: 150, sortable: true, align: 'left'},
				{header: '제조사', name: 'manufactureComText', width: 100, sortable: true, align: 'center'},
				{header: '제조일', name: 'manufactureDtText', width: 90, sortable: true, align: 'center'},
				{header: '통신방식', name: 'communicationModeText', width: 80, sortable: true, align: 'center'},
				{header: '등록일', name: 'regDtText', width: 90, sortable: true, align: 'center'}
			];

			var deviceGrid = Grid.createGrid({
				id: 'deviceGrid',
				columns: deviceColumns,
				data: [],
				bodyHeight: 300
			});

			getDeviceList();

			function getDeviceList() {
				var parameter = {
					deviceId: $popup.find('input[name="deviceId"]').val() || '',
					deviceClassify: $popup.find('select[name="deviceClassify"]').val() || '',
					deviceName: $popup.find('input[name="deviceName"]').val() || '',
					manufactureCom: $popup.find('select[name="manufactureCom"]').val() || '',
					statusList: ['DM020001']
				};
				Common.callAjax("/device/device/api/getDeviceList", parameter, function (result) {
					deviceGrid.resetData(result);
					$popup.find('#deviceTotalCount').text(result != null ? Common.addComma(result.length) : 0);
					Common.hideLoading();
				});
			}

			// 로우 선택시 이벤트 처리
			deviceGrid.on('click', function (result) {
				var row = result['rowKey'];
				var prevRow = result['prevRowKey'];
				if (row == null) {
					return false;
				}
				var record = deviceGrid.getRow(row);

				$('input[name="deviceId"]').val(record.deviceId);
				$('input[name="deviceSeq"]').val(record.deviceSeq);

				$("#operateStartDt").val(record.operateStartDt);
				$("#operateEndDt").val(record.operateEndDt);
				$("#operateStartTime").val(record.operateStartTime);
				$("#operateEndTime").val(record.operateEndTime);

				$popup.remove();
			});

			$popup.find('#searchDeviceBtn').on('click', function () {
				getDeviceList();
			});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 위치선택 팝업
	function openAddressMarkerMapPopup(parameter) {

		Common.openPopup('/common/addressMarkerMapPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("장비위치", result, {width: 600, wrapper: $('#popupList')});
			$('#popupList').append($popup);

			$popup.find('#saveLatlngBtn').on('click', function () {
				setLatLng($popup);

				DaumAddress.createStaticMap({
					id: 'map_view',
					lat: $popup.find('#popupLatitude').val(),
					lng: $popup.find('#popupLongitude').val()
				});

				$popup.remove();
			});

			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	// 직접 위경도 입력 선택 팝업
	function openLatlngMarkerMapPopup() {
		var parameter = {
			  lat: $('input[name="locationLatitude"]').val()
			, lng: $('input[name="locationLongitude"]').val()
		};

		var lat = $('input[name="latitude"]').val();
		var lng = $('input[name="longitude"]').val();

		if (Valid.isNotEmpty(lat) && Valid.isNotEmpty(lng)) {
			parameter.lat = lat;
			parameter.lng = lng;
		}
		Common.openPopup('/common/latlngMarkerMapPopup', {parameter: parameter}, function (result) {
			var $popup = Common.getPopupTemplate("위·경도 입력", result, {width: 600});
			$('#popupList').append($popup);

			$popup.find('#saveLatlngBtn').on('click', function () {
				setLatLng($popup);

				DaumAddress.createStaticMap({
					id: 'map_view',
					lat: $popup.find('#popupLatitude').val(),
					lng: $popup.find('#popupLongitude').val()
				});

				$popup.remove();
			});
			$popup.find('.popup-close-btn, #cancelBtn').on('click', function () {
				$popup.remove();
			});
			Common.resizeHeightPopup($popup);
		}, {})
	}

	function setLatLng($popup) {
		$('#map_view').show();
		$('.lat').show();
		$('#selectedAddress').text($popup.find('#popupAddress').val());
		$('#selectedLat').text($popup.find('#popupLatitude').val());
		$('#selectedLng').text($popup.find('#popupLongitude').val());
		$('input[name="latitude"]').val($popup.find('#popupLatitude').val());
		$('input[name="longitude"]').val($popup.find('#popupLongitude').val());
		$('input[name="deviceAddress"]').val($popup.find('#popupAddress').val());
	}

	//수정으로 접근할때 다음맵 세팅
	if (deviceSeq > 0) {
		$('#map_view').show();
		$('.lat').show();
		// $('#selectedAddress').text($popup.find('#popupAddress').val());
		$('#selectedLat').text($('input[name="latitude"]').val());
		$('#selectedLng').text($('input[name="longitude"]').val());
		DaumAddress.createStaticMap({
			id: 'map_view',
			lat: $('input[name="latitude"]').val(),
			lng: $('input[name="longitude"]').val()
		});
	}

});