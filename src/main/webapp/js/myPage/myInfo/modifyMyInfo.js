$(function () {

	var $saveBtn = $('#saveBtn');
	var $cancelBtn = $('#cancelBtn');
	var $myInfoForm = $('#myInfoForm');

	var $selectEmailDomain = $('select[name="selectEmailDomain"]');

	//화면load시 이메일 readonly 처리
	checkEmailDomain($selectEmailDomain);

	$selectEmailDomain.on('change', function () {
		checkEmailDomain($selectEmailDomain);
	});

	//이메일 select 박스 변경 이벤트 처리(POPUP)
	$selectEmailDomain.on('change',function(){
		var $this = $(this).val();
		$selectEmailDomain.siblings('input[name="emailDomain"]').val($this);
		$selectEmailDomain.siblings('input[name="emailDomain"]').prop('readonly', Valid.isNotEmpty($this));
	});

	$saveBtn.on('click', function () {

		Common.showLoading();
		var parameter = $myInfoForm.serializeObject();

		if(Valid.isNotEmpty(parameter.emailId) || Valid.isNotEmpty(parameter.emailDomain)){
			if(!Valid.isLowerCaseAndNumber(parameter.emailId, "이메일")) {
				Common.hideLoading();
				return false;
			}

			if(Valid.isEmpty(parameter.emailDomain)){
				alert("이메일 도메인을 입력해주세요.");
				Common.hideLoading();
				return false;
			}

			if(!Valid.isLowerCaseAndNumber(parameter.emailDomain.replace(/\./gi, ""), "이메일 도메인")){
				Common.hideLoading();
				return false;
			}
		}

		var phone = Common.getPhoneNumber('phone', $myInfoForm);
		var telephone = Common.getTelephoneNumber('telephone', $myInfoForm);

		if(Valid.isEmpty(parameter.name)){
			Common.hideLoading();
			alert("성명을 입력해주세요.");
			return false;
		}
		if(phone != ''){
			if(!Valid.isPhoneFormat(phone, "휴대폰번호")) {
				Common.hideLoading();
				return false;
			}
		}
		if(telephone != ''){
			if(!Valid.isPhoneFormat(telephone, "전화번호")){
				Common.hideLoading();
				return false;
			}
		}
		parameter.phone = phone;
		parameter.telephone = telephone;

		Common.callAjax("/standard/user/api/modifyMyInfo", parameter, function(result){
			alert("저장되었습니다.");
			location.href ='/myPage/myInfo/myInfo';
		});

	});

	$cancelBtn.on('click', function(){
		Common.showLoading();
		location.href ='/myPage/myInfo/myInfo';
	})

	function checkEmailDomain($select) {
		$select.siblings('input[name="emailDomain"]').prop('readonly', !($select.val() == ''));
	}
});