$(function () {

	var $myCustomerForm = $('#myCustomerForm');

	var $saveBtn = $('#saveBtn');
	var $cancelBtn = $('#cancelBtn');

	var $contractStartDt = $('#contractStartDt');
	var $contractEndDt = $('#contractEndDt');

	var picker = Component.setRangePicker({
		startpicker: {
			date: Valid.isNotEmpty($contractStartDt.val()) ? new Date(changeDateFormat($contractStartDt.val(), '/')) : new Date(),
			input: '#contractStartDt',
			container: '#contractStartDtWrapper'
		},
		endpicker: {
			date: Valid.isNotEmpty($contractEndDt.val()) ? new Date(changeDateFormat($contractEndDt.val(), '/')) : new Date(),
			input: '#contractEndDt',
			container: '#contractEndDtWrapper'
		},
		format : "yyyy.MM.dd",
		language : 'ko'
	});

	var $selectEmailDomain = $('select[name="selectEmailDomain"]');

	//화면load시 이메일 readonly 처리
	checkEmailDomain($selectEmailDomain);

	$selectEmailDomain.on('change', function () {
		checkEmailDomain($selectEmailDomain);
	});

	//이메일 select 박스 변경 이벤트 처리(POPUP)
	$selectEmailDomain.on('change',function(){
		var $this = $(this).val();
		$selectEmailDomain.siblings('input[name="emailDomain"]').val($this);
		$selectEmailDomain.siblings('input[name="emailDomain"]').prop('readonly', Valid.isNotEmpty($this));
	});

	// 우편번호 검색 팝업
	$('#openSearchAddressPopup').on('click', function () {
		Common.openPopup('/common/searchAddressPopup', {}, function (result) {
			var $popup = Common.getPopupTemplate("우편번호 찾기", result, {width : 500, bodyClass : 'nospace'});

			$popup.find('.popup-close-btn').on('click', function () {
				$popup.remove();
			});
			$('#popupList').append($popup);

			DaumAddress.createAddressPopup('daumAddress', {
				popup: $popup,
				address: 'address',
				addressDetail: 'addressDetail'
			});
			Common.resizeHeightPopup($popup);
		}, {})
	});

	$saveBtn.on('click', function () {

		Common.showLoading();
		var parameter = $myCustomerForm.serializeObject();

		if(Valid.isEmpty(parameter.representName)){
			alert("담당부서를 입력해주세요.");
			Common.hideLoading();
			return false;
		}

		Common.callAjax("/customer/customer/api/setMyCustomer", parameter, function(result){
			alert("저장되었습니다.");
			location.href ='/myPage/myCustomer/myCustomer';
		});
	});

	$cancelBtn.on('click', function () {
		Common.showLoading();
		location.href = '/myPage/myInfo/myInfo';
	});

	function checkEmailDomain($select) {
		$select.siblings('input[name="emailDomain"]').prop('readonly', !($select.val() == ''));
	}

});