$(function () {


	var $changeBtn = $('#changeBtn');
	var $cancelBtn = $('#cancelBtn');

	$changeBtn.on('click', function () {
		changePassword();
	});

	$cancelBtn.on('click', function () {
		history.back();
	});

	function changePassword(){
		Common.showLoading();
		var parameter = $('#passwordForm').serializeObject();
		Common.callAjax("/standard/user/api/changePassword", parameter, function(result){
			alert("비밀번호가 변경되었습니다.");
			location.reload();
		});
	}
});