$(function () {

	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $("#resetBtn"); //초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $searchForm = $('#searchForm');

	var reportColumns = [
		{header: '담당', width: 200, name: 'authRoleText', sortable: true, align: 'center'},
		{header: '소속', width: 200, name: 'customerName', sortable: true, align: 'left'},
		{header: '성명', width: 200, name: 'name', sortable: true, align: 'center'},
		{header: '거래처', width: 150, name: 'customerCnt', sortable: true, align: 'right'},
		{header: '현장수', width: 150, name: 'locationCnt', sortable: true, align: 'right'},
		{header: '장비수', width: 150, name: 'installCnt', sortable: true, align: 'right'},
		{header: '유지보수', width: 150, name: 'maintainCnt', sortable: true, align: 'right'},
		{header: '민원처리/신청', width: 150, name: 'complainText', sortable: true, align: 'right'}
	];

	var reportGrid = Grid.createGrid({
		id: 'reportGrid',
		columns: reportColumns,
		data: []
	});

	getReportDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getReportDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = reportColumns;

		var option = {
			fileName: '유지보수분석'
		};
		Common.excelDownload("/report/maintain/excel/getMaintainList", parameter, option);
	});

	// 데이터 로드
	function getReportDataList() {
		Common.showLoading();
		Common.callAjax("/report/maintain/api/getMaintainList", $searchForm.serializeObject(), function (result) {
			reportGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}
});