$(function () {
	
	Common.showLoading();

	var startDt = Component.setDatePicker({
		wrapper: 'searchStartDateWrapper',
		element: 'searchStartDate',
		date: new Date(),
		selectableRanges : [[new Date('2020/05/01'), new Date()]]
	});

	startDt.on('change', function () {
		getReportDataList();
		getReportStatusCount();
	});

	var reportColumns = [
		{header: '등록일', minWidth: 100, name: 'regDtLongText', sortable: true, align: 'center'},
		{header: '등록자', minWidth: 80, name: 'regName', sortable: true, align: 'center'},
		{header: '유형', minWidth: 100, name: 'type', sortable: true, align: 'left'},
		{header: '내용', minWidth: 1200, name: 'content', sortable: true, align: 'left'}
	];
	
	var reportGrid = Grid.createGrid({
		id: 'reportGrid',
		columns: reportColumns,
		data: []
	});
	
	getReportDataList();
	getReportStatusCount();
	
	// 데이터 로드
	function getReportDataList() {
		Common.showLoading();
		Common.callAjax("/report/today/api/getTodayList", $("#searchForm").serializeObject(), function (result) {
			reportGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}
	// 데이터 로드
	function getReportStatusCount() {
		Common.callAjax("/report/today/api/getTodayStatusCount", $("#searchForm").serializeObject(), function (result) {

			$('#requestRegCnt').html(Common.addComma(result.requestRegCnt) + '<i>건</i>'); // requestCnt
			$('#requestCnt').html(Common.addComma(result.requestDeviceCnt) + '<i>대</i>' + '(' + Common.addComma(result.requestCnt) + '<i>장소)</i>' ); // requestCnt
			$('#requestCompleteCnt').html(Common.addComma(result.requestCompleteDeviceCnt) + '<i>대</i>'  +  '('+ Common.addComma(result.requestCompleteCnt) + '<i>장소)</i>' ); // requestCompleteCnt
			$('#maintainCnt').html(Common.addComma(result.maintainCnt) + '<i>건</i>'); // maintainCnt
			$('#complainCnt').html(Common.addComma(result.complainCnt) + '<i>건</i>'); // complainCnt
			$('#complainCompleteCnt').html(Common.addComma(result.complainCompleteCnt) + '<i>건</i>'); // complainCompleteCnt
			$('#loginCnt').html(Common.addComma(result.loginCnt) +'<i>명</i>(' + Math.floor((result.loginCnt / result.userCnt) * 100) + '%)'); // loginCnt
		});
	}
});