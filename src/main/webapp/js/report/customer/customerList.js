$(function () {

	Common.showLoading();
	
	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $('#resetBtn');     // 검색조건 초기화 버튼
	var $excelDownloadBtn = $("button.excel"); // 엑셀다운로드 버튼
	var $searchForm = $('#searchForm');

	var $customerTab = $('#customerTab');
	var $locationTab = $('#locationTab');

	Common.tabBind($('.tab'), $('.tab_conts'));

	$locationTab.hide();
	$customerTab.hide();

	getReportDataList(1);
	
	$('#tab1').on('click', function () {
		getReportDataList(1);
	});
	
	$('#tab2').on('click', function () {
		getReportDataList(2);
	});
	
	$('#tab3').on('click', function () {
		getReportDataList(3);
	});
	
	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		var type = $('#tabType').val() != undefined ? $('#tabType').val() : 1;
		getReportDataList(type);
	});
	
	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});
	
	// 데이터 로드
	function getReportDataList(type) {
		Common.showLoading();
		$('#tabType').val(type);
		
		var reportColumns = [];
		reportColumns.push({header: '거래처 구분', width: 100, name: 'customerTypeText', sortable: true, align: 'left'});
		if(type == 2 || type == 3){
			reportColumns.push({header: '거래처', width: 120, name: 'customerName', sortable: true, align: 'left'});
		}
		if(type == 3){
			reportColumns.push({header: '설치장소', width: 120, name: 'locationName', sortable: true, align: 'left'});
		}
		reportColumns.push({header: '설치수', 	width: 60, name: 'td3', sortable: true, align: 'right', groupName: '소계'});
		reportColumns.push({header: '재설치수', 	width: 60, name: 'td4', sortable: true, align: 'right', groupName: '소계'});
		reportColumns.push({header: '탈거수', 	width: 60, name: 'td5', sortable: true, align: 'right', groupName: '소계'});
		reportColumns.push({header: '폐기수', 	width: 60, name: 'td6', sortable: true, align: 'right', groupName: '소계'});
		reportColumns.push({header: '분실수', 	width: 60, name: 'td7', sortable: true, align: 'right', groupName: '소계'});
		
		for(var i=1;i<=12;i++){
			reportColumns.push({header: '설치수', 	width: 60, name: 'm'+i+'d3', sortable: true, align: 'right', groupName: i+'월'});
			reportColumns.push({header: '재설치수', width: 60, name: 'm'+i+'d4', sortable: true, align: 'right', groupName: i+'월'});
			reportColumns.push({header: '탈거수', 	width: 60, name: 'm'+i+'d5', sortable: true, align: 'right', groupName: i+'월'});
			reportColumns.push({header: '폐기수', 	width: 60, name: 'm'+i+'d6', sortable: true, align: 'right', groupName: i+'월'});
			reportColumns.push({header: '분실수', 	width: 60, name: 'm'+i+'d7', sortable: true, align: 'right', groupName: i+'월'});
		}
		
		var complexColumns = [
			{header: '소계', name: 'total', childNames:['td3','td4','td5','td6','td7']}
		];
		for(var i=1;i<=12;i++){
			complexColumns.push({header: i+'월', name: 'month'+i, childNames:['m'+i+'d3','m'+i+'d4','m'+i+'d5','m'+i+'d6','m'+i+'d7']});
		}
		
		var grid = 'typeTabGrid';
		var totalCount = 'typeTabTotalCount';
		var frozenCount = 6;
		var fileName = '';
		if(type == 1){
			grid = 'typeTabGrid';
			totalCount = 'typeTabTotalCount';
			frozenCount = 6;
			fileName = '거래처 구분별 ';
		} else if(type == 2){
			grid = 'customerTabGrid';
			totalCount = 'customerTabTotalCount';
			frozenCount = 7;
			fileName = '거래처별 ';
		} else if(type == 3){
			grid = 'locationTabGrid';
			totalCount = 'locationTabTotalCount';
			frozenCount = 8;
			fileName = '설치장소별 ';
		}
		$('#'+grid).empty();
		
		var reportGrid = Grid.createGrid({
			id: grid,
			columns: reportColumns,
			data: [],
			complexColumns: complexColumns,
			frozenCount : frozenCount
		});
		
		Common.callAjax("/report/customer/api/getCustomerList", $searchForm.serializeObject(), function (result) {
			$.each(result, function (index, value) {
				value.td3 = value.m1d3 + value.m2d3 + value.m3d3 + value.m4d3 + value.m5d3 + value.m6d3 + value.m7d3 + value.m8d3 + value.m9d3 + value.m10d3 + value.m11d3 + value.m12d3;
				value.td4 = value.m1d4 + value.m2d4 + value.m3d4 + value.m4d4 + value.m5d4 + value.m6d4 + value.m7d4 + value.m8d4 + value.m9d4 + value.m10d4 + value.m11d4 + value.m12d4;
				value.td5 = value.m1d5 + value.m2d5 + value.m3d5 + value.m4d5 + value.m5d5 + value.m6d5 + value.m7d5 + value.m8d5 + value.m9d5 + value.m10d5 + value.m11d5 + value.m12d5;
				value.td6 = value.m1d6 + value.m2d6 + value.m3d6 + value.m4d6 + value.m5d6 + value.m6d6 + value.m7d6 + value.m8d6 + value.m9d6 + value.m10d6 + value.m11d6 + value.m12d6;
				value.td7 = value.m1d7 + value.m2d7 + value.m3d7 + value.m4d7 + value.m5d7 + value.m6d7 + value.m7d7 + value.m8d7 + value.m9d7 + value.m10d7 + value.m11d7 + value.m12d7;
			
				value.td3 = Common.addComma(value.td3);
				value.td4 = Common.addComma(value.td4);
				value.td5 = Common.addComma(value.td5);
				value.td6 = Common.addComma(value.td6);
				value.td7 = Common.addComma(value.td7);
				value.m1d3 = Common.addComma(value.m1d3);
				value.m1d4 = Common.addComma(value.m1d4);
				value.m1d5 = Common.addComma(value.m1d5);
				value.m1d6 = Common.addComma(value.m1d6);
				value.m1d7 = Common.addComma(value.m1d7);
				value.m2d3 = Common.addComma(value.m2d3);
				value.m2d4 = Common.addComma(value.m2d4);
				value.m2d5 = Common.addComma(value.m2d5);
				value.m2d6 = Common.addComma(value.m2d6);
				value.m2d7 = Common.addComma(value.m2d7);
				value.m3d3 = Common.addComma(value.m3d3);
				value.m3d4 = Common.addComma(value.m3d4);
				value.m3d5 = Common.addComma(value.m3d5);
				value.m3d6 = Common.addComma(value.m3d6);
				value.m3d7 = Common.addComma(value.m3d7);
				value.m4d3 = Common.addComma(value.m4d3);
				value.m4d4 = Common.addComma(value.m4d4);
				value.m4d5 = Common.addComma(value.m4d5);
				value.m4d6 = Common.addComma(value.m4d6);
				value.m4d7 = Common.addComma(value.m4d7);
				value.m5d3 = Common.addComma(value.m5d3);
				value.m5d4 = Common.addComma(value.m5d4);
				value.m5d5 = Common.addComma(value.m5d5);
				value.m5d6 = Common.addComma(value.m5d6);
				value.m5d7 = Common.addComma(value.m5d7);
				value.m6d3 = Common.addComma(value.m6d3);
				value.m6d4 = Common.addComma(value.m6d4);
				value.m6d5 = Common.addComma(value.m6d5);
				value.m6d6 = Common.addComma(value.m6d6);
				value.m6d7 = Common.addComma(value.m6d7);
				value.m7d3 = Common.addComma(value.m7d3);
				value.m7d4 = Common.addComma(value.m7d4);
				value.m7d5 = Common.addComma(value.m7d5);
				value.m7d6 = Common.addComma(value.m7d6);
				value.m7d7 = Common.addComma(value.m7d7);
				value.m8d3 = Common.addComma(value.m8d3);
				value.m8d4 = Common.addComma(value.m8d4);
				value.m8d5 = Common.addComma(value.m8d5);
				value.m8d6 = Common.addComma(value.m8d6);
				value.m8d7 = Common.addComma(value.m8d7);
				value.m9d3 = Common.addComma(value.m9d3);
				value.m9d4 = Common.addComma(value.m9d4);
				value.m9d5 = Common.addComma(value.m9d5);
				value.m9d6 = Common.addComma(value.m9d6);
				value.m9d7 = Common.addComma(value.m9d7);
				value.m10d3 = Common.addComma(value.m10d3);
				value.m10d4 = Common.addComma(value.m10d4);
				value.m10d5 = Common.addComma(value.m10d5);
				value.m10d6 = Common.addComma(value.m10d6);
				value.m10d7 = Common.addComma(value.m10d7);
				value.m11d3 = Common.addComma(value.m11d3);
				value.m11d4 = Common.addComma(value.m11d4);
				value.m11d5 = Common.addComma(value.m11d5);
				value.m11d6 = Common.addComma(value.m11d6);
				value.m11d7 = Common.addComma(value.m11d7);
				value.m12d3 = Common.addComma(value.m12d3);
				value.m12d4 = Common.addComma(value.m12d4);
				value.m12d5 = Common.addComma(value.m12d5);
				value.m12d6 = Common.addComma(value.m12d6);
				value.m12d7 = Common.addComma(value.m12d7);
			});
			
			reportGrid.resetData(result);
			$('#'+totalCount).text(result == null ? 0 : result.length);
			Common.hideLoading();
		});
		
		//엑셀다운로드 버튼 이벤트 처리
		$excelDownloadBtn.off('click').on('click', function () {
			var parameter = $searchForm.serializeObject();
			parameter.gridColumnList = reportColumns;

			var option = {
				fileName : fileName + '거래처분석'
			};
			Common.excelDownload("/report/customer/excel/getCustomerList", parameter, option);
		});
	}
});