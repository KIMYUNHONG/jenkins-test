$(function () {

	Common.showLoading();

	var $searchBtn = $('#searchBtn');	// 검색 버튼
	var $resetBtn = $("#resetBtn"); //초기화 버튼
	var $excelDownloadBtn = $("#excelDownloadBtn"); // 엑셀다운로드 버튼
	var $searchForm = $('#searchForm');

	var reportColumns = [
		{header: '장비구분', width: 100, name: 'deviceClassifyText', sortable: true, align: 'left'},
		{header: '장비명', width: 120, name: 'deviceNameText', sortable: true, align: 'left'},
		{header: '모델명', width: 120, name: 'modelName', sortable: true, align: 'left'},

		{header: '신규등록수', width: 70, name: 'td1', sortable: true, align: 'right', groupName: '소계'},
		{header: '미설치수', 	width: 60, name: 'td2', sortable: true, align: 'right', groupName: '소계'},
		{header: '설치수', 	width: 60, name: 'td3', sortable: true, align: 'right', groupName: '소계'},
		{header: '재설치수', 	width: 60, name: 'td4', sortable: true, align: 'right', groupName: '소계'},
		{header: '탈거수', 	width: 60, name: 'td5', sortable: true, align: 'right', groupName: '소계'},
		{header: '폐기수', 	width: 60, name: 'td6', sortable: true, align: 'right', groupName: '소계'},
		{header: '분실수', 	width: 60, name: 'td7', sortable: true, align: 'right', groupName: '소계'},
	];
	for(var i=1;i<=12;i++){
		reportColumns.push({header: '신규등록수', 	width: 70, name: 'm'+i+'d1', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '미설치수', 	width: 60, name: 'm'+i+'d2', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '설치수', 	width: 60, name: 'm'+i+'d3', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '재설치수', 	width: 60, name: 'm'+i+'d4', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '탈거수', 	width: 60, name: 'm'+i+'d5', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '폐기수', 	width: 60, name: 'm'+i+'d6', sortable: true, align: 'right', groupName: i+'월'});
		reportColumns.push({header: '분실수', 	width: 60, name: 'm'+i+'d7', sortable: true, align: 'right', groupName: i+'월'});
	}

	var complexColumns = [
		{header: '소계', name: 'total', childNames:['td1','td2','td3','td4','td5','td6','td7']}
	];
	for(var i=1;i<=12;i++){
		complexColumns.push({header: i+'월', name: 'month'+i, childNames:['m'+i+'d1','m'+i+'d2','m'+i+'d3','m'+i+'d4','m'+i+'d5','m'+i+'d6','m'+i+'d7']});
	}

	var reportGrid = Grid.createGrid({
		id: 'reportGrid',
		columns: reportColumns,
		data: [],
		complexColumns: complexColumns,
		frozenCount : 10
	});

	getReportDataList();

	// 검색버튼 이벤트 처리
	$searchBtn.on('click', function () {
		getReportDataList();
	});

	$resetBtn.on('click', function () {
		$searchForm[0].reset();
	});

	//엑셀다운로드 버튼 이벤트 처리
	$excelDownloadBtn.on('click', function () {
		var parameter = $searchForm.serializeObject();
		parameter.gridColumnList = reportColumns;

		var option = {
			fileName : '장비재고분석'
		};

		//parameter.gridColumnList.splice(3, 6);
		//console.log(parameter.gridColumnList);

		Common.excelDownload("/report/deviceStock/excel/getDeviceStockList", parameter, option);
	});

	// 데이터 로드
	function getReportDataList() {
		Common.showLoading();
		Common.callAjax("/report/deviceStock/api/getDeviceStockList", $searchForm.serializeObject(), function (result) {
			$.each(result, function (index, value) {
				value.td1 = value.m1d1 + value.m2d1 + value.m3d1 + value.m4d1 + value.m5d1 + value.m6d1 + value.m7d1 + value.m8d1 + value.m9d1 + value.m10d1 + value.m11d1 + value.m12d1;
				value.td2 = value.m1d2 + value.m2d2 + value.m3d2 + value.m4d2 + value.m5d2 + value.m6d2 + value.m7d2 + value.m8d2 + value.m9d2 + value.m10d2 + value.m11d2 + value.m12d2;
				value.td3 = value.m1d3 + value.m2d3 + value.m3d3 + value.m4d3 + value.m5d3 + value.m6d3 + value.m7d3 + value.m8d3 + value.m9d3 + value.m10d3 + value.m11d3 + value.m12d3;
				value.td4 = value.m1d4 + value.m2d4 + value.m3d4 + value.m4d4 + value.m5d4 + value.m6d4 + value.m7d4 + value.m8d4 + value.m9d4 + value.m10d4 + value.m11d4 + value.m12d4;
				value.td5 = value.m1d5 + value.m2d5 + value.m3d5 + value.m4d5 + value.m5d5 + value.m6d5 + value.m7d5 + value.m8d5 + value.m9d5 + value.m10d5 + value.m11d5 + value.m12d5;
				value.td6 = value.m1d6 + value.m2d6 + value.m3d6 + value.m4d6 + value.m5d6 + value.m6d6 + value.m7d6 + value.m8d6 + value.m9d6 + value.m10d6 + value.m11d6 + value.m12d6;
				value.td7 = value.m1d7 + value.m2d7 + value.m3d7 + value.m4d7 + value.m5d7 + value.m6d7 + value.m7d7 + value.m8d7 + value.m9d7 + value.m10d7 + value.m11d7 + value.m12d7;

				value.td1 = Common.addComma(value.td1);
				value.td2 = Common.addComma(value.td2);
				value.td3 = Common.addComma(value.td3);
				value.td4 = Common.addComma(value.td4);
				value.td5 = Common.addComma(value.td5);
				value.td6 = Common.addComma(value.td6);
				value.td7 = Common.addComma(value.td7);
				value.m1d1 = Common.addComma(value.m1d1);
				value.m1d2 = Common.addComma(value.m1d2);
				value.m1d3 = Common.addComma(value.m1d3);
				value.m1d4 = Common.addComma(value.m1d4);
				value.m1d5 = Common.addComma(value.m1d5);
				value.m1d6 = Common.addComma(value.m1d6);
				value.m1d7 = Common.addComma(value.m1d7);
				value.m2d1 = Common.addComma(value.m2d1);
				value.m2d2 = Common.addComma(value.m2d2);
				value.m2d3 = Common.addComma(value.m2d3);
				value.m2d4 = Common.addComma(value.m2d4);
				value.m2d5 = Common.addComma(value.m2d5);
				value.m2d6 = Common.addComma(value.m2d6);
				value.m2d7 = Common.addComma(value.m2d7);
				value.m3d1 = Common.addComma(value.m3d1);
				value.m3d2 = Common.addComma(value.m3d2);
				value.m3d3 = Common.addComma(value.m3d3);
				value.m3d4 = Common.addComma(value.m3d4);
				value.m3d5 = Common.addComma(value.m3d5);
				value.m3d6 = Common.addComma(value.m3d6);
				value.m3d7 = Common.addComma(value.m3d7);
				value.m4d1 = Common.addComma(value.m4d1);
				value.m4d2 = Common.addComma(value.m4d2);
				value.m4d3 = Common.addComma(value.m4d3);
				value.m4d4 = Common.addComma(value.m4d4);
				value.m4d5 = Common.addComma(value.m4d5);
				value.m4d6 = Common.addComma(value.m4d6);
				value.m4d7 = Common.addComma(value.m4d7);
				value.m5d1 = Common.addComma(value.m5d1);
				value.m5d2 = Common.addComma(value.m5d2);
				value.m5d3 = Common.addComma(value.m5d3);
				value.m5d4 = Common.addComma(value.m5d4);
				value.m5d5 = Common.addComma(value.m5d5);
				value.m5d6 = Common.addComma(value.m5d6);
				value.m5d7 = Common.addComma(value.m5d7);
				value.m6d1 = Common.addComma(value.m6d1);
				value.m6d2 = Common.addComma(value.m6d2);
				value.m6d3 = Common.addComma(value.m6d3);
				value.m6d4 = Common.addComma(value.m6d4);
				value.m6d5 = Common.addComma(value.m6d5);
				value.m6d6 = Common.addComma(value.m6d6);
				value.m6d7 = Common.addComma(value.m6d7);
				value.m7d1 = Common.addComma(value.m7d1);
				value.m7d2 = Common.addComma(value.m7d2);
				value.m7d3 = Common.addComma(value.m7d3);
				value.m7d4 = Common.addComma(value.m7d4);
				value.m7d5 = Common.addComma(value.m7d5);
				value.m7d6 = Common.addComma(value.m7d6);
				value.m7d7 = Common.addComma(value.m7d7);
				value.m8d1 = Common.addComma(value.m8d1);
				value.m8d2 = Common.addComma(value.m8d2);
				value.m8d3 = Common.addComma(value.m8d3);
				value.m8d4 = Common.addComma(value.m8d4);
				value.m8d5 = Common.addComma(value.m8d5);
				value.m8d6 = Common.addComma(value.m8d6);
				value.m8d7 = Common.addComma(value.m8d7);
				value.m9d1 = Common.addComma(value.m9d1);
				value.m9d2 = Common.addComma(value.m9d2);
				value.m9d3 = Common.addComma(value.m9d3);
				value.m9d4 = Common.addComma(value.m9d4);
				value.m9d5 = Common.addComma(value.m9d5);
				value.m9d6 = Common.addComma(value.m9d6);
				value.m9d7 = Common.addComma(value.m9d7);
				value.m10d1 = Common.addComma(value.m10d1);
				value.m10d2 = Common.addComma(value.m10d2);
				value.m10d3 = Common.addComma(value.m10d3);
				value.m10d4 = Common.addComma(value.m10d4);
				value.m10d5 = Common.addComma(value.m10d5);
				value.m10d6 = Common.addComma(value.m10d6);
				value.m10d7 = Common.addComma(value.m10d7);
				value.m11d1 = Common.addComma(value.m11d1);
				value.m11d2 = Common.addComma(value.m11d2);
				value.m11d3 = Common.addComma(value.m11d3);
				value.m11d4 = Common.addComma(value.m11d4);
				value.m11d5 = Common.addComma(value.m11d5);
				value.m11d6 = Common.addComma(value.m11d6);
				value.m11d7 = Common.addComma(value.m11d7);
				value.m12d1 = Common.addComma(value.m12d1);
				value.m12d2 = Common.addComma(value.m12d2);
				value.m12d3 = Common.addComma(value.m12d3);
				value.m12d4 = Common.addComma(value.m12d4);
				value.m12d5 = Common.addComma(value.m12d5);
				value.m12d6 = Common.addComma(value.m12d6);
				value.m12d7 = Common.addComma(value.m12d7);
			});
			reportGrid.resetData(result);
			$('#totalCount').text(result == null ? 0 : Common.addComma(result.length));
			Common.hideLoading();
		});
	}
});