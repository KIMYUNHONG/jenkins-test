/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pharmcle.rms.annotation.PK;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class User extends BaseDomain {

	public static final String AUTH_ADMIN = "SC020001";
	public static final String AUTH_CUSTOMER = "SC020004";

	@PK
	private int userSeq;

	private String userId;
	private String password;
	private String authRole;
	private String name;
	private int customerSeq;
	private String rank;
	private String etc;
	private String useAppYn;
	private LocalDateTime passwordChgDt;
	private String maskId;

	@JsonIgnore
	private int passwordFailCnt;
	private String token;
	private LocalDateTime tokenAccessDt;

	private String phone;
	private String telephone;
	private String representYn;
	private String confirmYn;

	private String emailId;
	private String emailDomain;

	private String emailText;
	public String getEmailText() {
		if(StringUtil.isEmpty(emailId) || StringUtil.isEmpty(emailDomain)){
			return "";
		}
		return emailId + "@" + emailDomain;
	}

	private String useYnText2;
	public String getUseYnText2() {

		// 가입요청 상태 체크후 가입요청 상태면 요청으로 RETURN 필요
		if("N".equals(confirmYn)){
			return "가입요청";
		}

		return "Y".equals(useYn) ? "활동" : "미활동";}

	public User(String userId) {
		this.userId = userId;
	}
	public User(String userId, String password, String authRole) {
		this.userId = userId;
		this.password = password;
		this.authRole = authRole;
	}

	private String representYnText;
	public String getRepresentYnText(){
		return "Y".equals(representYn) ? "O" : "X";
	}

	private String authRoleText;
	private String useAppYnText;
	private String rankText;

	public String getUseAppYnText() { return "Y".equals(useAppYn) ? "O" : "X";}

	public String customerName;
	public String customerType;

	// 최근 로그인 일시
	@JsonIgnore
	private LocalDateTime loginDt;
	public String loginDtText;
	public String getLoginDtText() {
		return DateUtil.localDatetimeToString(loginDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	@JsonIgnore
	public String [] phoneSplitText;
	public String [] getPhoneSplitText() {
		if(phone == null || !(phone.split("-").length == 3)){
			return null;
		}
		return phone.split("-");
	}

	@JsonIgnore
	public String [] telephoneSplitText;
	public String [] getTelephoneSplitText() {
		if(telephone == null || !(telephone.split("-").length == 3)){
			return null;
		}
		return telephone.split("-");
	}

	private String currentPassword;
	private String changePassword;
	private String changePasswordCheck;

	// 암호화용 임시 변수
	private String tempPhone;
	private String tempTelephone;
}

