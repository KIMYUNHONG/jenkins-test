package com.pharmcle.rms.domain.user;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserLoginService {
	
	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 *
	 * @param parameter (userId 아이디, password 패스워드)
	 * @return 확인완료시 user return 그 외 상황에는 Exception 처리
	 */
	public User getUserByUserIdAndPassword(UserParameter parameter) throws Exception {


		if(parameter.getUserId() == null || "".equals(parameter.getUserId())){
			throw new ValidException("ID를 입력해주세요.");
		}

		if(parameter.getPassword() == null || "".equals(parameter.getPassword())){
			throw new ValidException("비밀번호를 입력해주세요.");
		}

		// 1. userId로 계정 조회
		User user = userDao.selectByUserId(parameter);

		// 2.userId 정보가 없을 경우
		if(user == null){
			throw new ValidException("가입하지 않은 아이디이거나, 잘못된 비밀번호입니다.");
		}

		// 3. 패스워드 5회 이상 틀린경우
		if (user.getPasswordFailCnt() > 5) {
			throw new ValidException("비밀번호 5회 초과로 계정이 잠겼습니다. 관리자에게 문의 바랍니다.");
		}
		// 4. 휴면계정인 경우
		String useYn = user.getUseYn();
		if ("N".equals(useYn)) {
			throw new ValidException("휴면회원입니다. 관리자에게 문의 바랍니다.");
		}
		// 4-1. 미승인계정인 경우
		String confirmYn = user.getConfirmYn();
		if ("N".equals(confirmYn)) {
			throw new ValidException("미승인 회원입니다. 관리자에게 문의 바랍니다.");
		}
		// 5. 비밀번호 체크
		boolean result = passwordEncoder.matches(parameter.getPassword(), user.getPassword());

		// 5-1 계정이 없거나 , 비밀번호가 틀린경우
		if(!result){
			// 비밀번호 틀린 횟수 추가
			setPasswordFailCnt(new UserParameter(user.getUserId(), user.getPasswordFailCnt() + 1));
			throw new ValidException("가입하지 않은 아이디이거나, 잘못된 비밀번호입니다.");
		}

		return user;
	}

	public User getToken(UserParameter parameter){
		return userDao.selectToken(parameter);
	}

	/**
	 * 비밀번호 카운트 초기화
	 * @param parameter
	 * @return
	 */
	public boolean setPasswordFailCnt(UserParameter parameter){
		return userDao.updatePasswordFailCnt(parameter) > 0;
	}

	public boolean updateToken(UserParameter parameter){
		return userDao.updateToken(parameter) > 0;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public String encodingPassword(String password){
		return passwordEncoder.encode(password);
	}

	public String isAllowedPattern(String userId){

		// 1. 입력 여부 체크
		if(userId == null || userId.length() == 0 ){
			return "아이디를 입력해주세요";
		}

		// 2. 자리수 체크
		if(userId.length() < 3 || userId.length() > 10){
			return "아이디는 3 ~ 10자내로 입력해주세요.";
		}

		// 특수문자/대문자 포함 여부 체크
		String regExp1 = "^[a-z0-9]*$";
		if(!userId.matches(regExp1)){
			return "아이디는 영문 소문자/숫자만 입력이 가능합니다.";
		}
		return "";
	}


	/**
	 * 비밀번호 변경 서비스
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	public boolean changePassword(User parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		// 현재 입력된 비밀번호 Validaiton 및 체크
		ValidStringUtil.lengthChk(parameter.getCurrentPassword(), "현재 비밀번호", 8,32);
		if(!passwordEncoder.matches(parameter.getCurrentPassword(), user.getPassword())){
			throw new ValidException("현재 비밀번호가 맞지 않습니다.");
		}

		// 새로 입력된 비밀번호 체크 로직 시작
		ValidStringUtil.lengthChk(parameter.getChangePassword(), "신규 비밀번호",8,32);
		ValidStringUtil.lengthChk(parameter.getChangePasswordCheck(), "신규 비밀번호 확인",8,32);

//		checkPasswordPattern(parameter.getCurrentPassword());
		checkPasswordPattern(parameter.getChangePassword());
		checkPasswordPattern(parameter.getChangePasswordCheck());

		if(!parameter.getChangePassword().equals(parameter.getChangePasswordCheck())){
			throw new ValidException("신규 비밀번호가 맞지 않습니다.");
		}
		if(passwordEncoder.matches(parameter.getChangePassword(), user.getPassword())){
			throw new ValidException("현재 비밀번호와 동일합니다. 다시 한번 확인해주시기 바랍니다.");
		}

		// validation 모두 통과시 신규 비밀번호로 변경
		parameter.setUserSeq(user.getUserSeq());
		parameter.setPassword(passwordEncoder.encode(parameter.getChangePassword()));

		return userDao.resetUserPw(parameter) > 0;
	}

	public void checkPasswordPattern(String password) throws Exception{

		final String validMessage = "비밀번호는 8자리 이상 특수문자+영문+숫자 조합만 가능합니다.";

		// 영어+숫자+특숨누자 포함 여부 체크
		Pattern pattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$");
		Matcher matcher = pattern.matcher(password);

		if(!matcher.matches()){
			throw new ValidException(validMessage);
		}

	}

}
