package com.pharmcle.rms.domain.user;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.customer.Customer;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoDao;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	UserLoginService userLoginService;

	@Autowired
	CustomerService customerService;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	//@Autowired
	//CryptoUtil cryptoUtil; // 암호화 유틸

	@Autowired
	UserService userService;

	@Autowired
	TodoDao todoDao;

	public User getUserByUserId(UserParameter parameter) throws Exception {
		return userDao.selectByUserId(parameter);
	}

	public List<User> getUserList(UserParameter userParameter) throws Exception {
		
		List<User> list = userDao.selectList(userParameter);
		List<User> reList = new ArrayList<User>();
		
		// 암호화 - 복호화
		for (User user : list) {
			String phone = CryptoUtil.getAESDecrypt(user.getPhone());
			String telephone = CryptoUtil.getAESDecrypt(user.getTelephone());
			
			user.setPhone(phone);
			user.setTelephone(telephone);
			
			reList.add(user);
		}
		
		return reList;
	}

	public User getUser(int userSeq) throws Exception {
		User user = userDao.select(userSeq);

		if(user != null){
			if(StringUtil.isNotEmpty(user.getPhone())){
				// 암호화 - 복호화
				String phone = CryptoUtil.getAESDecrypt(user.getPhone());
				user.setPhone(phone);
			}
			if(StringUtil.isNotEmpty(user.getTelephone())){
				// 암호화 - 복호화
				String telephone = CryptoUtil.getAESDecrypt(user.getTelephone());
				user.setTelephone(telephone);
			}
		}
		return user;
	}

	public int getUserChk(String userId) throws Exception {
		return userDao.selectUserChk(userId);
	}


	@Transactional(rollbackFor = Exception.class)
	public boolean setUser(User parameter) throws Exception {

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		ValidStringUtil.nullChk(parameter.getUseYn(), "활동여부");

		if (StringUtil.isNotEmpty(parameter.getTelephone())) {
			ValidStringUtil.phoneNumberFormatChk(parameter.getTelephone(), "전화번호");
		}
		if (StringUtil.isNotEmpty(parameter.getPhone())) {
			ValidStringUtil.phoneNumberFormatChk(parameter.getPhone(), "휴대폰번호");
		}
		if (StringUtil.isNotEmpty(parameter.getEmailId()) || StringUtil.isNotEmpty(parameter.getEmailDomain())) {
			ValidStringUtil.emailIdChk(parameter.getEmailId(), "이메일 아이디");
			ValidStringUtil.userIdChk(parameter.getEmailDomain().replaceAll("\\.", ""), "이메일 도메인");
		}
		
		// 암호화  
		String phone = CryptoUtil.getAESEncrypt(parameter.getPhone());
		String telephone = CryptoUtil.getAESEncrypt(parameter.getTelephone());
		
		parameter.setPhone(phone);
		parameter.setTelephone(telephone);
		
		// 5. User insert
		if (parameter.getUserSeq() == 0) {

			ValidStringUtil.nullChk(parameter.getAuthRole(), "사용권한");
			ValidNumberUtil.zeroChk(parameter.getCustomerSeq(), "소속");
			ValidStringUtil.nullChk(parameter.getAuthRole(), "사용권한");
			ValidStringUtil.lengthChk(parameter.getUserId(), "아이디", 3, 10);
			ValidStringUtil.userIdChk(userId, "아이디");
			ValidStringUtil.nullChk(parameter.getName(), "성명");

			//초기 비밀번호 설정
			parameter.setPassword(userLoginService.encodingPassword("phc@12345"));
			parameter.setRegId(userId);

			if (userDao.selectByUserId(new UserParameter(parameter.getUserId())) != null) {
				throw new ValidException("이미 등록된 아이디입니다.");
			}
			parameter.setConfirmYn("Y");
			userDao.insert(parameter);
		} else {
			// 회원가입 사용자인 경우 사용권한이 미입력 상태 -> 사용권한 필수값 체크
			User user = userDao.select(parameter.getUserSeq());
			if (StringUtil.isEmpty(user.getAuthRole())) {
				ValidStringUtil.nullChk(parameter.getAuthRole(), "사용권한");
			}
			// 회원가입 사용자인 경우 미승인 상태 -> 승인여부 필수값 체크
			if ("N".equals(user.getConfirmYn())) {
				ValidStringUtil.nullChk(parameter.getConfirmYn(), "승인여부");
			}

			parameter.setUptId(userId);
			userDao.updateUser(parameter);
		}

		return true;
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean resetUserPw(User parameter) throws Exception {


		int userSeq = parameter.getUserSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		parameter.setRegId(userId);
		parameter.setUptId(userId);
		parameter.setPassword(userLoginService.encodingPassword("phc@12345"));

		String resetUserId = userDao.selectUserId(userSeq);

		UserParameter userParameter = new UserParameter(resetUserId, parameter.getPassword());

		// 1. 필수값 체크
//		if ("".equals(parameter.getDeviceClassify()) ||
//				"".equals(parameter.getDeviceName()) ||
//				"".equals(parameter.getManufactureCom()) ||
//				"".equals(parameter.getManufactureDt()) ||
//				"".equals(parameter.getManufactureComClassify())){
//
//			throw new Exception("~가 입력해주세요");
//		}

		// 2. 비밀번호 초기화
		userDao.resetUserPw(parameter);

		// 패스워드 틀린횟수 0으로 초기화
		parameter.setPasswordFailCnt(0);
		userLoginService.setPasswordFailCnt(userParameter);

		return true;
	}


	@Transactional(rollbackFor = Exception.class)
	public boolean editUserPw(EditUserParameter parameter) throws Exception {

		String userId = parameter.getUserId();
//		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		//현재 비밀번호(DB)
		String userPw = userDao.selectUserPw(userId);

		//새 비밀번호
		String newPw = parameter.getNewPw();
		//새 비밀번호 재입력
		String newPwConf = parameter.getNewPwConf();
		//현재 비밀번호(사용자 입력)
		String password = parameter.getPassword();

		ValidStringUtil.nullChk(password, "현재 비밀번호");
		ValidStringUtil.nullChk(newPw, "새 비밀번호");
		ValidStringUtil.nullChk(newPwConf, "새 비밀번호 재입력");

		if (password.equals(newPwConf)) {
			throw new ValidException("현재 비밀번호와 새 비밀번호가 같습니다.");
		} else if (!newPw.equals(newPwConf)) {
			throw new ValidException("새 비밀번호 재입력이 일치하지 않습니다.");
		} else {
			//DB저장된 비밀번호와 입력한 비밀번호 체크
			boolean result = passwordEncoder.matches(parameter.getPassword(), userPw);
			if (result) {
				//변경하려는 패스워드가 비밀번호 규칙에 적합한지 체크
				String pwPattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";
				Boolean chk = Pattern.matches(pwPattern, newPwConf);

				if (chk) {
					String encodeUserPw = userLoginService.encodingPassword(newPw);
					parameter.setNewPw(encodeUserPw);
					parameter.setUserId(userId);
					userDao.updateUserPw(parameter);
					return true;
				} else {
					//패스워드가 비밀번호 정규식에 적합하지 않음 err 보내야함
					throw new ValidException("8자리 이상 특수문자 + 영문 + 숫자 조합만 가능합니다.");

				}
			} else {
				throw new ValidException("비밀번호가 일치하지 않습니다.");
			}
		}
	}

	public User getUserBySessionOrToken(String token) throws Exception {

		String userId = SessionUtil.getUserId();
		User user;

		if (StringUtil.isEmpty(userId) || ServletUtil.isMobile(ServletUtil.getRequest())) {
			if (token == null) {
				return null;
			}
			user = userDao.selectByToken(token);
		} else {
			user = userDao.selectByUserId(new UserParameter(userId));
		}
		return user;
	}

	public String getAuthroleBySessionOrToken(String token) throws Exception {

		String userId = SessionUtil.getUserId();
		User user;

		if (StringUtil.isEmpty(userId) || ServletUtil.isMobile(ServletUtil.getRequest())) {
			if (token == null) {
				return null;
			}
			user = userDao.selectByToken(token);
		} else {
			user = userDao.selectByUserId(new UserParameter(userId));
		}
		return user == null ? null : user.getAuthRole();
	}

	public String getUserIdBySessionOrToken(String token) throws Exception {

		String userId = SessionUtil.getUserId();

		if (StringUtil.isEmpty(userId) || ServletUtil.isMobile(ServletUtil.getRequest())) {
			if (token == null) {
				return null;
			}
			User user = userDao.selectByToken(token);
			userId = user != null ? user.getUserId() : null;
			log.info("userService userId  : " + userId);
		}
		return userId;
	}

	public User getUserFindId(User parameter) throws Exception {

		User user;

		ValidStringUtil.nullChk(parameter.getName(), "성명");
		ValidStringUtil.nullChk(parameter.getPhone(), "연락처");
		ValidStringUtil.nullChk(parameter.getEmailId(), "이메일 ID");
		ValidStringUtil.nullChk(parameter.getEmailDomain(), "이메일 도메인");


		if(parameter.getPhone().length() != 11){
			throw new ValidException("연락처는 - 를 제외한 11자리를 입력해 주세요.");
		}

		// 전화번호에 - 넣어주기
		String phone = "";
		phone = parameter.getPhone().substring(0,3);
		phone += "-"+ parameter.getPhone().substring(3,7);
		phone += "-"+ parameter.getPhone().substring(7,11);

		// 암호화
		parameter.setPhone(CryptoUtil.getAESEncrypt(phone));

		//userId 뒤에 4자리 **** 마스킹 처리
		user = userDao.selectFindUserId(parameter);

		if (user == null) {throw new ValidException("등록된 정보가 없습니다.");}

		String useYn = user.getUseYn();

		if ("N".equals(useYn)) {
			throw new ValidException("휴면 회원입니다 \n관리자에게 \n문의 바랍니다.");
		}

		int length = user.getUserId().length() - 2;
		String maskId = user.getUserId().substring(0, length);
		maskId = maskId + "**";
		user.setMaskId(maskId);

		return user;
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean regUser(User parameter) throws Exception {

		//String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		ValidStringUtil.nullChk(parameter.getPhone(), "휴대폰번호");
		if (StringUtil.isNotEmpty(parameter.getPhone())) {
			ValidStringUtil.phoneNumberFormatChk(parameter.getPhone(), "휴대폰번호");
		}
		if (StringUtil.isNotEmpty(parameter.getTelephone())) {
			ValidStringUtil.phoneNumberFormatChk(parameter.getTelephone(), "전화번호");
		}
		if (StringUtil.isNotEmpty(parameter.getEmailId()) || StringUtil.isNotEmpty(parameter.getEmailDomain())) {
			ValidStringUtil.nullChk(parameter.getEmailId(), "이메일 아이디");
			ValidStringUtil.nullChk(parameter.getEmailDomain(), "이메일 도메인");
			ValidStringUtil.emailIdChk(parameter.getEmailId(), "이메일 아이디");
			ValidStringUtil.userIdChk(parameter.getEmailDomain().replaceAll("\\.", ""), "이메일 도메인");
		}

		//ValidStringUtil.nullChk(parameter.getAuthRole(), "사용권한");
		ValidNumberUtil.zeroChk(parameter.getCustomerSeq(), "소속");
		ValidStringUtil.lengthChk(parameter.getUserId(), "아이디", 3, 10);
		//ValidStringUtil.userIdChk(userId, "아이디");
		ValidStringUtil.nullChk(parameter.getName(), "성명");

		// 새로 입력된 비밀번호 체크 로직 시작
		ValidStringUtil.lengthChk(parameter.getChangePassword(), "비밀번호", 8, 32);
		ValidStringUtil.lengthChk(parameter.getChangePasswordCheck(), "비밀번호 재입력", 8, 32);
		if (!checkPasswordPattern(parameter.getChangePassword())) {
			throw new ValidException("비밀번호는 8자리 이상 특수문자+영문+숫자 조합만 가능합니다.");
		}
		if (!checkPasswordPattern(parameter.getChangePasswordCheck())) {
			throw new ValidException("비밀번호는 8자리 이상 특수문자+영문+숫자 조합만 가능합니다.");
		}

		if (!parameter.getChangePassword().equals(parameter.getChangePasswordCheck())) {
			throw new ValidException("신규 비밀번호가 맞지 않습니다.");
		}
		parameter.setPassword(userLoginService.encodingPassword(parameter.getChangePassword()));
		//parameter.setRegId(userId);
		parameter.setUseYn("Y");
		parameter.setConfirmYn("N");
		
		// 암호화
		String phone = CryptoUtil.getAESEncrypt(parameter.getPhone());
		String telephone = CryptoUtil.getAESEncrypt(parameter.getTelephone());
		
		parameter.setPhone(phone);
		parameter.setTelephone(telephone);

		if (userDao.selectByUserId(new UserParameter(parameter.getUserId())) != null) {
			throw new ValidException("이미 등록된 아이디입니다.");
		}

		Customer customer = customerService.getCustomer(parameter.getCustomerSeq());

		String customerType = customer.getCustomerType();

		// 팜클, 대리점 회원가입의 경우 대표담당 X
		if(Customer.CUSTOMER_TYPE_PHARMCLE.equals(customerType) || Customer.CUSTOMER_TYPE_AGANCY.equals(customerType)){
			parameter.setRepresentYn("N");
		} else {
			parameter.setAuthRole(User.AUTH_CUSTOMER);
		}

		// 대표담당 중복등록 체크
		if ("Y".equals(parameter.getRepresentYn())) {
			int representCount = userDao.selectRepresentUserCount(parameter.getCustomerSeq());
			if (representCount >= 1) {
				throw new ValidException("대표담당은 2명 이상 등록할 수 없습니다.");
			}
		}
		parameter.setAuthRole("SC020004");
		userDao.insert(parameter);
		todoDao.insert(new Todo(Todo.DOMAIN_NAME_CUSTOMER_JOIN, parameter.getUserSeq(), parameter.getUserId()));

		return true;
	}

	/**
	 * 휴면유저 리스트 조회
	 *
	 * @return
	 */
	public List<User> getRestUserList() {
		return userDao.selectRestUserList();
	}

	public boolean setRestUser(int userSeq) {
		return userDao.updateRestUser(userSeq) > 0;
	}

	public boolean setUserConfirm(int userSeq) throws Exception {
		return userDao.updateConfirm(userSeq) > 0;
	}


	public boolean checkPasswordPattern(String password) throws Exception {
		// 영어+숫자+특숨누자 포함 여부 체크
		Pattern pattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$");
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();

	}
}
