/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UserParameter {

	// userSeq
	private int userSeq;

	// 아이디
	private String userId;

	// 비밀번호
	private String password;

	// 토큰
	private String token;

	// 로그인 실패 count
	private int passwordFailCnt;

	public UserParameter(String userId) {
		this.userId = userId;
	}

	public UserParameter(String userId, String password) {
		this.userId = userId;
		this.password = password;

	}

	public UserParameter(String userId, int passwordFailCnt){
		this.userId = userId;
		this.passwordFailCnt = passwordFailCnt;
	}

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;

	// 사용권한
	private String authRole;

	// 소속
	private int customerSeq;

	// 소속명
	private String customerName;

	// 권한
	private String userAuth;

	// 성명
	private String name;

	// APP 사용여부
	private String useAppYn;

	// 활동상태
	private String useYn;

	//새 비밀번호
	private String newPw;

	//새 비밀번호 재입력
	private String newPwConf;

	// 담당자 조회용 플래그 필드
	private boolean isNotCustomer;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;
}

