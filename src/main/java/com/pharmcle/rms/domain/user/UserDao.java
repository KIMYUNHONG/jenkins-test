/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.user;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserDao {

	User selectByUserId(UserParameter parameter);
	User selectByToken(String token);
	User selectToken(UserParameter parameter);

	int insert(User parameter);
	int update(User parameter);
	int delete(User parameter);

	int updatePasswordFailCnt(UserParameter parameter);
	int updateToken(UserParameter parameter);


	List<User> selectList(UserParameter parameter);
	List<User> selectChargeList(UserParameter parameter);

	User select(int userSeq);

	int updateUser(User parameter);
	int updateCharge(User parameter);
	int updateChargeUserId(User parameter);
	int resetUserPw(User parameter);
	int selectUserChk(String userId);
	int selectRepresentUserCount(int customerSeq);

	// 거래처관리 - 담당자 등록/수정
	int mergeCustomerCharge(String userId);

	String selectUserId(int userSeq);

	//아이디 찾기
	User selectFindUserId(User parameter);

	String selectUserPw(String userId);


	int updateUserPw(EditUserParameter parameter);
	int updateRestUser(int userSeq);
	int updateConfirm(int userSeq);

	List<User> selectRestUserList();
	
	// 임시 암호화 
	int updateCryptPhone(User parameter);
}