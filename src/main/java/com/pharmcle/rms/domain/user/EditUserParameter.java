/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class EditUserParameter {


	private int userSeq;
	// 아이디
	private String userId;

	// 비밀번호
	private String password;

	//새 비밀번호
	private String newPw;

	//새 비밀번호 재입력
	private String newPwConf;


}

