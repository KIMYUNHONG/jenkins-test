/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.app;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AppParameter {
	// App 관리 Seq
	private int appSeq;

	// App 관리 OS 구분
	private String os;

	public AppParameter(String os){
		this.os = os;
	}

}
