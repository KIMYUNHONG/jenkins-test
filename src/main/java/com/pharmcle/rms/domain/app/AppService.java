/**
 * Create By Kim YunHong on 2020-02-18 / 오후 5:49
 * Email : fbsghd123@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.app;

import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidNumberUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AppService {

	@Autowired
	AppDao appDao;

	@Autowired
	UploadFileDao uploadFileDao;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;

	public List<App> getAppList(AppParameter appParameter) throws Exception{
		return  appDao.selectList(appParameter);
	}

	public App getApp(int appSeq) throws Exception{
		return  appDao.select(appSeq);
	}

	/**
	 * App 관리 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setApp(App parameter) throws Exception {

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);



		// 5. App 관리 insert
		if(parameter.getAppSeq() == 0){
			ValidStringUtil.nullChk(parameter.getOs(), "OS");
			ValidStringUtil.lengthChk(parameter.getVersionName(), "버전명", 1, 50);
			ValidNumberUtil.zeroChk(parameter.getVersion(), "버전");
			ValidStringUtil.nullChk(parameter.getUseYn(), "배포");
			ValidStringUtil.nullChk(parameter.getAppFileKey(), "앱파일");

			appDao.insert(parameter);
			uploadFileDao.update(new UploadFileInsertParameter(parameter.getAppFileKey(), UploadFileService.APP, parameter.getAppSeq()));
		} else {
			ValidStringUtil.nullChk(parameter.getUseYn(), "배포");
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain( UploadFileService.APP, parameter.getAppSeq());
			if(uploadFileList != null){
				for(UploadFile item : uploadFileList){
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
			appDao.update(parameter);
			uploadFileDao.update(new UploadFileInsertParameter(parameter.getAppFileKey(), UploadFileService.APP, parameter.getAppSeq()));
		}

		return true;
	}

	public App getAppVersionCheck(AppParameter appParameter) throws Exception{
		return  appDao.selectVersionCheck(appParameter);
	}





}
