/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.app;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AppDao {

	List<App> selectList(AppParameter parameter);
	App select(int appSeq);
	int insert(App parameter);
	int update(App parameter);

	App selectVersionCheck(AppParameter os);
}
