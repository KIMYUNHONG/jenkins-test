/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.app;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@NoArgsConstructor
@Getter
@Setter
public class App extends BaseDomain {

	// App 관리 SEQ
	private int appSeq;
	// App 관리 OS 구분
    private String os;
	// App 관리 버전
	private BigDecimal version;


	// App 관리 버전명
	private String versionName;
	// App 관리 파일
	private String appFileKey;
	// App 관리 비고
	private String etc;

	private String osText;

	private String appFileUri;

	private String originalFileName;

	// 모바일 버전체크용 필드
	private String appFilePath;

	public String getAppFilePath(){
		return "/uploadFile/api/download/" + appFileKey;
	}
	private String appFileName;

	public BigDecimal getVersion(){
		if(version == null ){
			return null;
		}
		version = version.setScale(2, RoundingMode.FLOOR);
		return version;
	}
	private String versionText;
	public String getVersionText(){
		if(version == null ){
			return null;
		}
		version = version.setScale(2, RoundingMode.FLOOR);
		return version.toString();
	}

}
