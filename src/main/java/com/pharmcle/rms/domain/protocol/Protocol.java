/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.protocol;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * 통신기준정보관리
 */
@NoArgsConstructor
@Getter
@Setter
public class Protocol extends BaseDomain {

	//통신기준정보관리 Seq
	private int protocolSeq;
	//장비유형
	private String deviceClassify;
	//기준년도
	private String baseYear;
	//가동시간 시작
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;
	private String operateStartDtText;
	public String getOperateStartDtText() {
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	//가동시간 종료
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;
	private String operateEndDtText;
	public String getOperateEndDtText() {
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	//가동시간 점등
	private String operateStartTime;
	//가동시간 소등
	private String operateEndTime;
	//온도 하한
	private String temperatureLow;
	// 온도 상한
	private String temperatureHigh;
	//습도 하한
	private String humidityLow;
	//습도 상한
	private String humidityHigh;
	//전체전압 하한
	private String voltageLow;
	//전체전압 상한
	private String voltageHigh;
	//전체전류 하한
	private String electricLow;
	//전체전류 상한
	private String electricHigh;
	//속성1 하한
	private String property1Low;
	//속성1 상한
	private String property1High;
	//속성2 하한
	private String property2Low;
	//속성2 상한
	private String property2High;
	//속성3 하한
	private String property3Low;
	//속성3 상한
	private String property3High;
	//속성4 하한
	private String property4Low;
	//속성4 상한
	private String property4High;

	// 장비 키
	private int deviceSeq;

	public Protocol(int deviceSeq){
		this.deviceSeq = deviceSeq;
	}

}
