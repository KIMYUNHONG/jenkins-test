/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.protocol;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 통신기준정보관리
 */
@NoArgsConstructor
@Getter
@Setter
public class ProtocolParameter {


	//기준년도
	private String baseYear;
	//장비유형
	private String deviceClassify;


}
