/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.protocol;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidDateUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProtocolService {

	@Autowired
	ProtocolDao protocolDao;

	@Autowired
	UserService userService;

	/**
	 * 통신기준정보관리 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Protocol> getProtocolList(ProtocolParameter parameter) throws Exception {
		return protocolDao.selectList(parameter);
	}

	/**
	 * 통신기준정보관리 조회
	 *
	 * @param parameter
	 * @return
	 */
	public Protocol getProtocol(ProtocolParameter parameter) throws Exception {
		return protocolDao.select(parameter);
	}

	/**
	 * 통신기준정보관리 등록
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setProtocol(Protocol parameter) throws Exception {

		boolean isNewProtocol = parameter.getProtocolSeq() == 0;
		int protocolSeq = parameter.getProtocolSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);

		ValidStringUtil.lengthChk(parameter.getOperateStartTime(), "점등시간", 4, 4);
		ValidStringUtil.lengthChk(parameter.getOperateEndTime(), "소등시간", 4, 4);
		ValidDateUtil.nullChk(parameter.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(parameter.getOperateEndDt(), "가동기간(종료일)");

		if (isNewProtocol) {

			boolean result = protocolDao.insert(parameter) > 0;
			if (!result) {    // 등록이 안된경우 에러 처리
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
		//수정인경우
		} else {
			boolean result = protocolDao.update(parameter) > 0;
			if (!result) {
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}

		}
		return true;
	}

	/**
	 * 통신기준정보관리 등록(장비등록시 DEVICE_PROTOCOL INSERT)
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setDeviceProtocol(Protocol parameter) throws Exception {

		ValidDateUtil.nullChk(parameter.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(parameter.getOperateEndDt(), "가동기간(종료일)");

		boolean result = protocolDao.insertDeviceProtocol(parameter) > 0;

		if (!result) {    // 등록이 안된경우 에러 처리
			throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
		}

		return true;
	}

	/**
	 * 장비 통신기준 또는 가동기간/시간 변경시 변경 내역 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setDeviceProtocolLog(Protocol parameter) throws Exception {

		return protocolDao.insertDeviceProtocolLog(parameter) > 0;

	}

	/**
	 * 설치장비 삭제시 deviceProtocol,deviceProtocolLog  삭제
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteDeviceProtocol(Protocol parameter) throws Exception {

		// deviceProtocol 삭제
		protocolDao.deleteDeviceProtocol(parameter);
		// deviceProtocolLog 삭제
		protocolDao.deleteDeviceProtocolLog(parameter);

		return true;

	}

}
