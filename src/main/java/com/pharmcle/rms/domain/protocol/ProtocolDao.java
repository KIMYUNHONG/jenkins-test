/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.protocol;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ProtocolDao {

	List<Protocol> selectList(ProtocolParameter parameter);
	Protocol select(ProtocolParameter parameter);
	int insert(Protocol parameter);
	int update(Protocol parameter);
	int insertDeviceProtocol(Protocol parameter);


	int insertDeviceProtocolLog(Protocol parameter);
	int deleteDeviceProtocol(Protocol parameter);
	int deleteDeviceProtocolLog(Protocol parameter);

}
