/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.complain;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ComplainDao {

	List<Complain> selectList(ComplainParameter parameter);
	int selectComplainListCount(ComplainParameter parameter);

	Complain select(ComplainParameter parameter);
	List<ComplainHandle> selectComplainHandle(ComplainParameter parameter);

	int insert(Complain parameter);
	int update(Complain parameter);
	int updateForReceiptComplain(Complain parameter);
	int updateForFinishComplain(Complain parameter);
	int insertComplainHandle(ComplainHandle parameter);
	int updateComplainHandle(ComplainHandle parameter);

	Complain selectComplainStatusCount(ComplainParameter parameter);

	// 민원신청 사용자 권한 체크
	int complainAuthroleChk(ComplainParameter parameter);


	int deleteComplainHandle(ComplainHandle parameter);

	List<ComplainHandle> complainHandleSeqList(ComplainHandle parameter);

}
