/**
 * Create By Kim YunHong on 2020-02-18 / 오후 5:49
 * Email : fbsghd123@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.complain;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartDao;
import com.pharmcle.rms.domain.mail.Mail;
import com.pharmcle.rms.domain.mail.MailService;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoDao;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ComplainService {

	@Autowired
	ComplainDao complainDao;
	@Autowired
	UserService userService;
	@Autowired
	UploadFileDao uploadFileDao;
	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	AddpartDao addpartDao;
	@Autowired
	TodoDao todoDao;
	@Autowired
	MailService mailService;

	@Value("${mail.from.url}")
	String url;

	public List<Complain> getComplainList(ComplainParameter parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		if (ValidAuthUtil.isCustomerAuth(user)) {
			parameter.setCustomerSeq(user.getCustomerSeq());
		}
		return complainDao.selectList(parameter);
	}

	// 민원신청 리스트 카운트
	public int getComplainListCount(ComplainParameter parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			parameter.setCustomerSeq(user.getCustomerSeq());
		}

		return complainDao.selectComplainListCount(parameter);
	}


	public Complain getComplain(ComplainParameter parameter) throws Exception {
		Complain complain = complainDao.select(parameter);

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		if(ValidAuthUtil.isCustomerAuth(user) && complain != null && complain.getCustomerSeq() != user.getCustomerSeq()){
			throw new ValidException("권한이 없습니다.");
		}

		return complain;
	}

	public List<ComplainHandle> getComplainHandle(ComplainParameter parameter) throws Exception {
		List<ComplainHandle> complainHandle = complainDao.selectComplainHandle(parameter);
		return complainHandle;
	}

	/**
	 * 민원 등록
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setComplain(Complain parameter) throws Exception {

		int complainSeq;

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);
		parameter.setStatus(Complain.STATUS_REQUEST);

		ValidNumberUtil.nullChk(parameter.getLocationSeq(), "설치장소");
		ValidStringUtil.lengthChk(parameter.getTitle(), "제목", 1, 100);
		ValidStringUtil.codeChk(parameter.getComplainType(), "민원유형");


//		ValidStringUtil.codeChk(parameter.getSubstitute(), "신청인");

		if ("Y".equals(parameter.getEmailYn())) {
			ValidStringUtil.nullChk(parameter.getEmail(), "이메일");
		}
		if (StringUtil.isNotEmpty(parameter.getEmail())) {
			ValidStringUtil.emailFormatChk(parameter.getEmail(), "이메일");
		}

		// complain insert
		if (parameter.getComplainSeq() == 0) {
			complainDao.insert(parameter);
			complainSeq = parameter.getComplainSeq();
			todoDao.insert(new Todo(Todo.DOMAIN_NAME_COMPLAIN_REQUEST, complainSeq, userId));
		} else {

			// 민원 수정

			// 민원신청 민원수정 사용자 권한 확인 (거래처담당)
			if (!complainAuthroleChk(parameter.getComplainSeq(), userId, ComplainParameter.USER_TYPE_CUSTOMER)) {
				throw new ValidException("권한이 없습니다.");
			}

			// complain update
			complainDao.update(parameter);
			complainSeq = parameter.getComplainSeq();

			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN, complainSeq);
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
		}

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (parameter.getUploadFileKey() != null) {
			for (String key : parameter.getUploadFileKey()) {
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.COMPLAIN, complainSeq);
				uploadFileDao.update(uploadFile);
			}
		}
		return true;
	}

	/**
	 * 민원 접수
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setReceiptComplain(Complain parameter) throws Exception {
		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		String userId = user.getUserId();

		// 민원신청 민원접수 사용자 권한 확인 (보수담당)
		if (!complainAuthroleChk(parameter.getComplainSeq(), userId, ComplainParameter.USER_TYPE_INSTALL)) {
			throw new ValidException("권한이 없습니다.");
		}

		//접수자 거래처 시퀀스
		int receiptCustomerSeq = user.getCustomerSeq();
		parameter.setUptId(userId);
		parameter.setReceiptCustomerSeq(receiptCustomerSeq);

		Complain complain = complainDao.select(new ComplainParameter(parameter.getComplainSeq()));

		if (!Complain.STATUS_REQUEST.equals(complain.getStatus())) {
			throw new ValidException("민원접수는 민원신청 단계에서만 가능합니다.");
		}

		parameter.setStatus(Complain.STATUS_RECEIPT);
		complainDao.updateForReceiptComplain(parameter);

		todoDao.insert(new Todo(Todo.DOMAIN_NAME_COMPLAIN_RECEIPT, parameter.getComplainSeq(), userId));
		return true;
	}

	/**
	 * 민원 처리완료
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setFinishComplain(Complain parameter) throws Exception {
		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		String userId = user.getUserId();

		// 민원신청 처리완료 사용자 권한 확인 (보수담당)
		if (!complainAuthroleChk(parameter.getComplainSeq(), userId, ComplainParameter.USER_TYPE_INSTALL)) {
			throw new ValidException("권한이 없습니다.");
		}

		parameter.setUptId(userId);
		Complain complain = complainDao.select(new ComplainParameter(parameter.getComplainSeq()));
		List <ComplainHandle> ComplainHandle = complainDao.selectComplainHandle(new ComplainParameter(parameter.getComplainSeq()));

		if(ComplainHandle.size() == 0 || ComplainHandle == null){
			throw new ValidException("처리내역을 등록해주세요.");
		}

		if (!Complain.STATUS_RECEIPT.equals(complain.getStatus())) {
			throw new ValidException("처리완료는 민원접수 단계에서만 가능합니다.");
		}
		if (!userId.equals(complain.getAsUserId()) && !User.AUTH_ADMIN.equals(user.getAuthRole())) {
			throw new ValidException("처리완료는 해당 설치장소의 보수담당만 가능합니다.");
		}

		parameter.setStatus(Complain.STATUS_FINISH);
		complainDao.updateForFinishComplain(parameter);

		// 확인 메일 발송
		if (StringUtil.isNotEmpty(complain.getEmailYn()) && "Y".equals(complain.getEmailYn())) {
			// 발송 이메일 확인
			if (StringUtil.isNotEmpty(complain.getEmail())) {

				complain.setStatusText("처리완료");
				String templateFile = "complainMailForm.html";    // 메일 템플릿 파일명
				String subject = "잡스 RMS 민원신청 처리 안내";    // 민원 완료 내용
				String linkUrl = url;    // RMS 접속

				String header = "<table bgcolor='cccccc' border='0'>" +
					"<colgroup> <col width='200px'> <col width='200px'> <col width='200px'> <col width='200px'> </colgroup> " +
					"<tbody>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>민원유형</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getComplainTypeText() +"</p></td>" +
					"<th align='left' bgcolor='EDEDED' height='25'>상태</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getStatusText()  +"</p></td>" +
				    "</tr>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>제목</th>" +
					"<td colspan='3' bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getTitle() +"</p></td>" +
					"</tr>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>내용</th>" +
					"<td colspan='3' bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getContent() +"</p></td>" +
					"</tr>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>신청구분</th>" +
					"<td colspan='3' bgcolor='FFFFFF' align='left' height='25'>" +
					"<p>"+ complain.getSubstituteText() +"</p>" +
					"</td>" +
					"</tr>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>등록자</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getRegName() +"</p>" +
					"</td>" +
					"<th align='left' bgcolor='EDEDED' height='25'>등록일</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getRegDtText() +"</p></td>" +
					"</tr>" +
					"<tr>" +
					"<th align='left' bgcolor='EDEDED' height='25'>접수자</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getReceiptCustomerName() +'|'+ complain.getReceiptName() + "</p>" +
					"</td>" +
					"<th align='left' bgcolor='EDEDED' height='25'>접수일</th>" +
					"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ complain.getReceiptDtText() +"</p></td>" +
					"</tr>" +
					"</tbody>" +
					"</table>" +
					"<div style='margin: 0px 0px 10px 0px'> </div>";

				String body = "";

				if(ComplainHandle != null && ComplainHandle.size() > 0){

					String encodeStr = "";
					String imgTag = "";

					for(ComplainHandle item: ComplainHandle){

						if(item.getFilePath() != null && item.getFilePath().size() > 0){
							// 사진 1개만 전송되도록 제한 (전체 사진을 다 보여주기에 이메일 공간이 제한적임)
							int num = 0;
							for(String temp : item.getFilePath()){
								if(num == 0){
									encodeStr = encodeFiletoString(temp);
									// 줄바꿈 삭제
									encodeStr = "data:image/jpeg;base64,"+encodeStr.replaceAll(System.getProperty("line.separator"),"");
									imgTag += "<img src="+encodeStr+" style='max-width:45%; padding: 5px 5px 5px 5px;'/>";
								}
								num++;
							}
						}

						if(item.getAddpartListText() == null || item.getAddpartListText().length() == 0){
							item.setAddpartListText("");
						}

						if(item.getContent() == null || item.getContent().length() == 0){
							item.setContent("");
						}

						body += "<tr><td bgcolor='FFFFFF' align='center' height='25'><p>" + item.getDeviceId() + "</p></td><td bgcolor='FFFFFF' align='center' height='25'><p>" + item.getHandlingResultText() + "</p> </td> <td bgcolor='FFFFFF' align='left' height='25'> <p>" + item.getAddpartListText() + " " +  item.getContent() + "</p> </td> <td bgcolor='FFFFFF' align='center' height='25'> "+imgTag+"  </td> <td bgcolor='FFFFFF' align='center' height='25'> <p>" + item.getRegName() + "</p> </td> <td bgcolor='FFFFFF' align='center' height='25'> <p>" + item.getHandleDtLongText() + "</p> </td></tr>";

						// 이미지 base64 , imgTag 초기화
						encodeStr = "";
						imgTag = "";
					}
				}

				String content = "<table bgcolor='cccccc' border='0'>" +
					"<colgroup> <col width='150px'> <col width='100px'> <col width='200px'> <col width='250px'> <col width='100px'> <col width='100px'>  </colgroup> " +
					"<thead>" +
					"<tr>" +
					"<th bgcolor='EDEDED' height='25'>장비번호</th>" +
					"<th bgcolor='EDEDED' height='25'>처리내역</th>" +
					"<th bgcolor='EDEDED' height='25'>내용</th>" +
					"<th bgcolor='EDEDED' height='25'>사진</th>" +
					"<th bgcolor='EDEDED' height='25'>등록자</th>" +
					"<th bgcolor='EDEDED' height='25'>처리일</th>" +
					"</tr>" +
					"</thead>" +
					"<tbody>" + body + "</tbody>" +
					"</table>";

				Mail mail = new Mail();
				mail.setToEmail(complain.getEmail());                            // 받는사람 이메일
				mail.setSubject(subject);                                        // 메일 제목
				mail.setTemplateFile(templateFile);                            // 메일 템플릿 파일

				Map<String, String> htmlData = new HashMap<String, String>();    // 템플릿 치환
				htmlData.put("USERNAME", complain.getRegName());                // 신청자명
				htmlData.put("REGDATE", complain.getRegDtText());                // 신청일자
				htmlData.put("LOCATION", complain.getLocationName());            // 설치지역
				htmlData.put("TYPE", complain.getComplainTypeText());            // 민원유형
				htmlData.put("TITLE", complain.getTitle());                      // 민원제목
				htmlData.put("LINKURL", linkUrl);                                // 민원 확인 링크주소
				htmlData.put("HEADER", header);                                  // 민원처리 상세내용
				htmlData.put("MESSAGE", content);                                // 민원처리 내역

				mail.setMailReplaceData(htmlData);

				try {
					boolean mailSend = mailService.sendMail(mail);               // 메일 발송
				} catch (Exception e) {
					log.error("메일발송 오류[setFinishComplain] : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	/**
	 * 민원처리 처리내역 등록(보수담당)
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setComplainHandle(Complain parameter) throws Exception {

		int complainSeq = 0;

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		// 민원신청 처리내역 등록 사용자 권한 확인 (보수담당)
		if (!complainAuthroleChk(parameter.getComplainSeq(), userId, ComplainParameter.USER_TYPE_INSTALL)) {
			throw new ValidException("권한이 없습니다.");
		}

		List<ComplainHandle> complainHandle = parameter.getHandlingList();
		complainSeq = parameter.getComplainSeq();

		Complain complain = complainDao.select(new ComplainParameter(complainSeq));

		if (!Complain.STATUS_RECEIPT.equals(complain.getStatus())) {
			throw new ValidException("민원처리 등록은 접수단계에서만 가능합니다.");
		}

		/**
		 * 1.complainSeq로 complainHandleSeq 조회
		 * 2.화면에서 가져온 complainHandleSeq와 1번에서 조회한 complainHandleSeq 비교하여 포함여부 판단
		 * 3.포함되어 있으면 수정 , 포함되어 있지 않으면 DELETE
		 * 4.HANDLE_SEQ가 0 이면 INSERT
		 * **/

		// 1. 기존에 등록된 처리내역 조회
		List<ComplainHandle> selectComplainHandle = complainDao.selectComplainHandle(new ComplainParameter(complainSeq));

		// 2. 기존리스트중에 수정/등록이 아닌것들 제거
		for(ComplainHandle existHandle : selectComplainHandle){
			boolean deleteFlag = true;

			// 2-1. 파라미터로 올라온 데이터중 기존데이터랑 키가 일치하거나 신규인경우는 삭제할 데이터에서 제외
			for(ComplainHandle item : complainHandle){
				if(item.getComplainHandleSeq() == 0 || existHandle.getComplainHandleSeq() == item.getComplainHandleSeq()){
					deleteFlag = false;
				}
			}

			// 2-2. 파라미터로 온 데이터가 아닌 데이터중 기존에 남아있는 데이터 제거
			if(deleteFlag){

				// 추가부품 삭제
				addpartDao.delete(new Addpart(UploadFileService.COMPLAIN_HANDLE, complainSeq, existHandle.getComplainHandleSeq(), Addpart.MANAGE_ADDPART_MR07));

				// 기존에 등록된 파일들 사용 X 처리
				List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN_HANDLE, complainSeq, existHandle.getComplainHandleSeq());
				if (uploadFileList != null) {
					for (UploadFile item : uploadFileList) {
						uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
					}
				}

				// 민원처리내역 삭제
				complainDao.deleteComplainHandle(existHandle);

			}
		}

		if (complainHandle != null && complainHandle.size() > 0) {
			for (ComplainHandle param : complainHandle) {
				param.setRegId(userId);

				ValidStringUtil.lengthChk(param.getHandlingResult(), "처리결과", 1, 8);

				if (param.getComplainHandleSeq() == 0) {
					complainDao.insertComplainHandle(param);
				} else {
					complainDao.updateComplainHandle(param);
					addpartDao.delete(new Addpart(UploadFileService.COMPLAIN_HANDLE, complainSeq, param.getComplainHandleSeq(), Addpart.MANAGE_ADDPART_MR07));

					// 기존에 등록된 파일들 사용 X 처리
					List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN_HANDLE, complainSeq, param.getComplainHandleSeq());
					if (uploadFileList != null) {
						for (UploadFile item : uploadFileList) {
							uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
						}
					}
				}

				// 남은 파일 또는 신규등록된 파일들 매핑
				if (param.getUploadFileKey() != null) {
					for (String key : param.getUploadFileKey()) {
						UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.COMPLAIN_HANDLE, complainSeq, param.getComplainHandleSeq());
						uploadFileDao.update(uploadFile);
					}
				}
				if (param.getAddpartList() != null && param.getAddpartList().size() > 0) {
					for (Addpart item : param.getAddpartList()) {
						if (StringUtil.isEmpty(item.getPartType()) || item.getPartQty() == 0) {
							throw new Exception();
						}
						item.setDomainName(UploadFileService.COMPLAIN_HANDLE);
						item.setDomainSeq(complainSeq);
						item.setDomainSeq2(param.getComplainHandleSeq());
						addpartDao.insert(item);
					}
				}
			}
		}

		return true;
	}

	public Complain getComplainStatusCount(ComplainParameter complainParameter) throws Exception {

		return complainDao.selectComplainStatusCount(complainParameter);
	}

	/**
	 * 민원신청 사용자 권한 체크
	 **/
	public boolean complainAuthroleChk(int complainSeq, String userId, String userType) throws Exception {

		if (StringUtil.isEmpty(userId) || StringUtil.isEmpty(userType) || complainSeq <= 0) {
			throw new ValidException("권한 확인중 문제가 발생하였습니다. 관리자에게 문의해 주세요.");
		}

		ComplainParameter parameter = new ComplainParameter();

		parameter.setComplainSeq(complainSeq);
		parameter.setUserType(userType);
		parameter.setUserId(userId);

		return complainDao.complainAuthroleChk(parameter) > 0;

	}

	/**
	 * Base64로 변환
	 **/
	private static String encodeFiletoString(String inputFileName) throws IOException {

		BASE64Encoder base64Encoder = new BASE64Encoder();

		InputStream in = new FileInputStream(new File(inputFileName));

		ByteArrayOutputStream byteOutStream=new ByteArrayOutputStream();

		int len=0;

		byte[] buf = new byte[1024];

		while((len=in.read(buf)) != -1){

			byteOutStream.write(buf, 0, len);

		}

		byte fileArray[]=byteOutStream.toByteArray();

		String encodeString=base64Encoder.encodeBuffer(fileArray);

		System.out.println(encodeString);

		return encodeString;

	}

	/**
	 * 설치장비 삭제시 민원데이터 삭제
	 *
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteComplain(int deviceSeq) throws Exception {

		ComplainHandle complainHandle = new ComplainHandle();
		complainHandle.setDeviceSeq(deviceSeq);
		List<ComplainHandle> complainHandleSeqList = complainDao.complainHandleSeqList(complainHandle);

		/**
		 * 1. 민원처리 삭제
		 * 2. 관련 data 삭제
		 * **/
		if(complainHandleSeqList != null && complainHandleSeqList.size() > 0){
			for(ComplainHandle item : complainHandleSeqList){

				//민원처리 삭제
				boolean result = complainDao.deleteComplainHandle(item) > 0;

				// 기존에 등록된 파일들 사용 X 처리
				List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN_HANDLE, item.getComplainSeq(), item.getComplainHandleSeq());
				if (uploadFileList != null) {
					for (UploadFile item2 : uploadFileList) {
						uploadFileDao.update(new UploadFileInsertParameter(item2, "N"));
					}
				}

				//추가부품 삭제
				addpartDao.delete(new Addpart(UploadFileService.COMPLAIN_HANDLE, item.getDeviceSeq(), 0, Addpart.MANAGE_ADDPART_MR07));

				if(!result){
					throw new ValidException("민원처리 내용삭제에 실패했습니다. 관리자에게 문의해 주세요.");
				}

			}
		}
		return true;
	}


}
