/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.complain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Complain extends BaseDomain {

	public static final String STATUS_REQUEST = "MR030001";
	public static final String STATUS_RECEIPT = "MR030002";
	public static final String STATUS_FINISH = "MR030003";

	// 민원신청 시퀀스
	private int complainSeq;
	// 민원유형
	private String complainType;
	// 민원유형(TEXT)
	private String complainTypeText;
	// 민원상태
	private String status;
	// 민원상태(Text)
	private String statusText;
	private List<String> statusList;
	// 민원 접수자 거래처 시퀀스
	private int receiptCustomerSeq;
	// 민원 접수자 ID
	private String receiptId;
	// 민원 접수자 명
	private String receiptName;
	// 민원 접수자 거래처명
	private String receiptCustomerName;
	// 민원접수일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate receiptDt;
	// 민원접수일 TEXT
	private String receiptDtText;
	public String getReceiptDtText(){
		return DateUtil.localDateToString(receiptDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 민원제목
	private String title;
	// 민원내용
	private String content;
	private String contentShortText;
	public String getContentShortText(){
		if(StringUtil.isEmpty(content)){
			return null;
		}else if(content.length() < 20){
			return content;
		}
		return content.substring(0, 20) + "...";
	};
	// 설치장소 시퀀스
	private int locationSeq;
	// 설치장소명
	private String locationName;
	// 지역코드(시/도)
	private String locationArea;
	// 지역(시/도) TEXT
	private String locationAreaText;
	//대리신청 여부
	private String substituteYn;
	//대리신청
	private String substitute;
	//대리신청(Text)
	private String substituteText;
	public String getSubstituteText() {
		return StringUtil.isEmpty(substitute) ? "본인신청" : substituteText;
	}

	// 이메일 답변알림 여부
	private String emailYn;
	// 답변 이메일
	private String email;
	// 거래처 이메일 ID
	private String customerMailId;
	// 거래처 이메일 DOMAIN
	private String customerMailDomain;
	private String customerEmail;
	public String getCustomerEmail() {
		if(StringUtil.isEmpty(customerMailId) || StringUtil.isEmpty(customerMailDomain)){
			return "";
		}
		return customerMailId + "@" + customerMailDomain;
	}

	public String getEmailYnText() { return "Y".equals(emailYn) ? "Y" : "N";}

	//보수담당자 명
	private String asUserName;
	//보수담당자 ID
	private String asUserId;
	//거래처 키
	private int customerKey;
	//거래처 명
	private String customerName;
	//거래처 유형
	private String customerType;
	//거래처 유형TEXT
	private String customerTypeText;
	//등록자 거래처명
	private String 	regCustomerName;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<ComplainHandle> handlingList;

	// 설치장소 주소
	private String address;
	private String addressDetail;

	// 대시보드용
	private String mr030001;
	private String mr030002;
	private String mr030003;
	private String userId;

	private int customerSeq;
}
