/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.complain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class ComplainHandle extends BaseDomain {
	private int complainSeq;
	private int complainHandleSeq;
	private int deviceSeq;
	private String deviceId;
	private String deviceNameText;
	private String handlingResult;
	private String handlingResultText;
	private String content;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime handleDt;
	// 처리일 TEXT
	private String handleDtText;
	public String getHandleDtText(){
		return DateUtil.localDatetimeToString(handleDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	private String handleDtLongText;
	public String getHandleDtLongText(){
		return DateUtil.localDatetimeToString(handleDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> filePath;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Addpart> addpartList;

	private String addpartListText;
	public String getAddpartListText(){
		if(addpartList == null || addpartList.size() == 0){
			return "";
		}
		StringBuilder result = new StringBuilder();
		for(Addpart item : addpartList){
			result.append(item.getPartTypeText()).append(" ").append(item.getPartQty()).append("개, ");
		}
		return result.substring(0, result.length() - 2);
	}

	private String lampNo;
	private String manageNo;

	public ComplainHandle(int complainHandleSeq) {
		this.complainHandleSeq = complainHandleSeq;
	}

}
