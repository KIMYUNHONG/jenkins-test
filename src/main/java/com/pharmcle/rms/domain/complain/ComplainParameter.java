/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.complain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class ComplainParameter extends BaseParameter {

	// 사용자 권한
	public static final String USER_TYPE_INSTALL  = "SC020003"; // 보수담당
	public static final String USER_TYPE_CUSTOMER = "SC020004"; // 거래처담당


	// 민원신청 Seq
	private int complainSeq;

	// 민원제목
	private String title;

	// 지역(시/군)
	private String locationArea;

	// 설치장소 명
	private String locationName;

	// 거래처구분
	private String customerType;

	// 거래처 명
	private String customerName;

	// 등록자(대리신청)
	private String substitute;

	// 보수 담당자
	private String asUserName;
	private String asUserId;

	// 보수 담당자 시퀀스
	private int asCustomerSeq;

	// 민원상태
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;

	// 민원유형
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> complainTypeList;

	// 시작기간
	private String searchStartDate;
	// 종료기간
	private String searchEndDate;

	public ComplainParameter(int complainSeq) {
		this.complainSeq = complainSeq;
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	// 민원 등록자 아이디
	private String regId;
	private String userId;
	private String regName;
	private int customerSeq;

	// 담당자 유형
	private String userType;


	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키
}
