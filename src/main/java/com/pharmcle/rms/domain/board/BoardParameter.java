/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.board;

import com.pharmcle.rms.base.BaseParameter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 공지사항
 */
@NoArgsConstructor
@Getter
@Setter
public class BoardParameter extends BaseParameter {


	//공지사항 Seq
	private int boardSeq;
	//게시판 유형
	private String boardType;
	//카테고리
	private String boardCategory;
	//제목
	private String title;
	//내용
	private String content;

	private String target;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;

	public BoardParameter(int boardSeq) {
		this.boardSeq = boardSeq;
	}


}
