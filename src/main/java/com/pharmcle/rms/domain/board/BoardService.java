/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.board;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.StringJoiner;

@Service
public class BoardService {

	@Autowired
	BoardDao boardDao;

	@Autowired
	UserService userService;

	@Autowired
	UploadFileDao uploadFileDao;

	@Autowired
	UploadFileService uploadFileService;

	/**
	 * 공지사항 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Board> getNoticeList(BoardParameter parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if("SC020004".equals(user.getAuthRole())){
			parameter.setTarget(user.getCustomerType());
		}
		return boardDao.selectNoticeList(parameter);
	}

	/**
	 * 공지사항 상세조회
	 *
	 * @param parameter
	 * @return
	 */
	public Board getNotice(BoardParameter parameter) throws Exception {

		Board board = boardDao.selectNotice(parameter);

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(board != null && ValidAuthUtil.isCustomerAuth(user) && !board.getTarget().contains(user.getCustomerType())){
			throw new ValidException("권한이 없습니다.");
		}
		return board;
	}

	/**
	 * 공지사항 조회수 증가
	 *
	 * @param parameter
	 * @return
	 */
	public boolean updateCnt(BoardParameter parameter) {

		boolean result = boardDao.updateCnt(parameter) > 0;

		return result;
	}

	/**
	 * 공지사항 등록
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setNotice(Board parameter) throws Exception {

		boolean isNewBoard = parameter.getBoardSeq() == 0;
		int boardSeq = parameter.getBoardSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);
		parameter.setBoardType("HO010001");

		List<String> list = parameter.getTargetList();
		StringJoiner sj = new StringJoiner(",");

		if (list != null) {
			for (String s : list) {
				sj.add(s);
			}
			parameter.setTarget(sj.toString());
		}

		ValidStringUtil.lengthChk(parameter.getTitle(), "제목", 1, 100);
		ValidStringUtil.lengthChk(parameter.getContent(), "내용", 1, 3000);
		if (isNewBoard) {

			boolean result = boardDao.insertNotice(parameter) > 0;
			if (!result) {    // 등록이 안된경우 에러 처리
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			boardSeq = parameter.getBoardSeq();

			// 파일업로드
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.NOTICE, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
			//수정인경우
		} else {
			boolean result = boardDao.updateNotice(parameter) > 0;
			if (!result) {
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.NOTICE, boardSeq);
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
			// 남은 파일 또는 신규등록된 파일들 매핑
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.NOTICE, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
		}
		return true;
	}

	/**
	 * 자료실 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Board> getArchiveList(BoardParameter parameter) throws Exception {
		return boardDao.selectArchiveList(parameter);
	}

	/**
	 * 자료실 상세조회
	 *
	 * @param parameter
	 * @return
	 */
	public Board getArchive(BoardParameter parameter) throws Exception {
		return boardDao.selectArchive(parameter);
	}


	/**
	 * 자료실 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setArchive(Board parameter) throws Exception {

		boolean isNewBoard = parameter.getBoardSeq() == 0;
		int boardSeq = parameter.getBoardSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);
		parameter.setBoardType("HO010002");

		List<String> list = parameter.getTargetList();
		StringJoiner sj = new StringJoiner(",");

		if (list != null) {
			for (String s : list) {
				sj.add(s);
			}
			parameter.setTarget(sj.toString());
		}

		ValidStringUtil.lengthChk(parameter.getTitle(), "제목", 1, 100);
		ValidStringUtil.lengthChk(parameter.getContent(), "내용", 1, 3000);
		ValidStringUtil.nullChk(parameter.getBoardCategory(), "구분");

		if (isNewBoard) {

			boolean result = boardDao.insertArchive(parameter) > 0;
			if (!result) {    // 등록이 안된경우 에러 처리
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			boardSeq = parameter.getBoardSeq();

			// 파일업로드
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.ARCHIVE, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
			//수정인경우
		} else {
			boolean result = boardDao.updateArchive(parameter) > 0;
			if (!result) {
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.ARCHIVE, boardSeq);
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
			// 남은 파일 또는 신규등록된 파일들 매핑
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.ARCHIVE, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
		}
		return true;
	}

	/**
	 * FAQ 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Board> getFaqList(BoardParameter parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		parameter.setTarget(user.getAuthRole());

		return boardDao.selectFaqList(parameter);
	}
	// 등록된 FAQ COUNT 조회
	public int getFaqListCount(BoardParameter parameter) throws Exception{
		return  boardDao.selectFaqListCount(parameter);
	}


	/**
	 * FAQ 상세조회
	 *
	 * @param parameter
	 * @return
	 */
	public Board getFaq(BoardParameter parameter) throws Exception {

		return boardDao.selectFaq(parameter);
	}


	/**
	 * FAQ 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setFaq(Board parameter) throws Exception {

		boolean isNewBoard = parameter.getBoardSeq() == 0;
		int boardSeq = parameter.getBoardSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);
		parameter.setBoardType("HO010003");

		List<String> list = parameter.getTargetList();
		StringJoiner sj = new StringJoiner(",");

		if (list != null) {
			for (String s : list) {
				sj.add(s);
			}
			parameter.setTarget(sj.toString());
		}

		ValidStringUtil.lengthChk(parameter.getTitle(), "제목", 1, 100);
		ValidStringUtil.lengthChk(parameter.getContent(), "내용", 1, 3000);
		ValidStringUtil.nullChk(parameter.getBoardCategory(), "구분");

		if (isNewBoard) {

			boolean result = boardDao.insertFaq(parameter) > 0;
			if (!result) {    // 등록이 안된경우 에러 처리
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			boardSeq = parameter.getBoardSeq();

			// 파일업로드
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.FAQ, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
			//수정인경우
		} else {
			boolean result = boardDao.updateFaq(parameter) > 0;
			if (!result) {
				throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
			}
			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.FAQ, boardSeq);
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
			// 남은 파일 또는 신규등록된 파일들 매핑
			if (parameter.getUploadFileKey() != null) {
				for (String key : parameter.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.FAQ, boardSeq);
					uploadFileDao.update(uploadFile);
				}
			}
		}
		return true;
	}

	//	Board 삭제(단건)
	public boolean deleteBoard(Board board) throws Exception{

		board.setUptId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));

		boardDao.delete(board);

		return  true;
	}
}
