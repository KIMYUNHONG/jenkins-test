/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.board;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BoardDao {

	List<Board> selectNoticeList(BoardParameter parameter);
	Board selectNotice(BoardParameter parameter);
	int insertNotice(Board parameter);
	int updateNotice(Board parameter);
	int updateCnt(BoardParameter parameter);


	List<Board> selectArchiveList(BoardParameter parameter);
	Board selectArchive(BoardParameter parameter);
	int insertArchive(Board parameter);
	int updateArchive(Board parameter);

	List<Board> selectFaqList(BoardParameter parameter);
	Board selectFaq(BoardParameter parameter);
	int insertFaq(Board parameter);
	int updateFaq(Board parameter);

	boolean delete(Board parameter);


	int selectFaqListCount(BoardParameter parameter);



}
