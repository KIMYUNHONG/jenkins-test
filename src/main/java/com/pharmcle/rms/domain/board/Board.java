/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.board;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 공지사항
 */
@NoArgsConstructor
@Getter
@Setter
public class Board extends BaseDomain {

	//공지사항 Seq
	private int boardSeq;
	//게시판 유형
	private String boardType;
	//카테고리
	private String boardCategory;
	//카테고리 TEXT
	private String boardCategoryText;
    //제목
	private String title;
	private String titleShortText;
	public String getTitleShortText(){
		if(StringUtil.isEmpty(title)){
			return null;
		}else if (title.length() < 20){
			return title;
		}
		return title.substring(0,20) + "...";
	};
	//내용
	private String content;
	//공개대상
	private String target;
	//조회수
	private int count;

	// 데이터 저장용
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> targetList;

	// 화면 데이터 바인드용
	private Map<String, String> splitTarget;
	public Map<String, String> getSplitTarget(){
		Map<String, String> result = new HashMap<>();
		if(StringUtil.isEmpty(target)){
			return result;
		}
		for(String item : target.split(",")){
			result.put(item, item);
		}
		return result;
	}

	//파일업로드
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;

	private String existFileYn;

}
