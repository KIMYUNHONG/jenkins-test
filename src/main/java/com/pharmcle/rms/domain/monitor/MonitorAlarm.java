/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MonitorAlarm {
	private int seq;				// 장비일령번호
	private String node;			// 모뎀 아이디
	private String part;			// 구분자
	private LocalDateTime rDate;	// 알람일자
	private int rDateHour;			// 알람시간
	private String relay;			// 알람구분
	
	private LocalDateTime sDate;	// 알람시작 일자
	private LocalDateTime eDate;	// 알람완료 일자
	
	private int useTime;				// 진행 시간	
	private String viewPart;			// 구분자
	private String viewDate;			// 일자	
	private String viewSTime;			// 시작시간	
	private String viewETime;			// 종료시간	
}
