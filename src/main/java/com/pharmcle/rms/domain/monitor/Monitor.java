/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.base.GridColumn;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Monitor extends BaseDomain {
	private int deviceSeq;				// 장비일령번호
	private int relay;				// 장비 ON OFF 여부
	private String locationAreaText;	// 지역 TEXT
	private String locationArea;		// 지역 코드
	private String locationName;		// 설치장소
	private String customerTypeText;	// 거래처구분 TEXT
	private String customerType;		// 거래처구분 코드
	private String customerName;		// 거래처
	private int customerSeq;			// 거래처 키
	private String deviceId;			// 장비코드
	private String deviceClassify;	    // 장비구분
	private String power;				// 전원  (수신 일자 기준 - 하루전데이터면 OFF 설정)	//TODO : 설정값 확인필요
	private String stateText;			// 상태 TEXT
	private String state;				// 상태 코드 								//TODO : 전압으로 계산, 전류값?? - 설정값 확인필요
	private String connection; 			// 접속 (0 : 단선, 1 : 접속) 				//TODO : 모뎀유무로 판단 - 설정값 확인필요
	private String deviceNameText;		// 장비명 TEXT
	private String deviceName;			// 장비명 코드 
	private String protocolText;		// 통신방식 TEXT
	private String protocol;			// 통신방식
	
	private String devEUI;				// 모뎀 시리얼 코드			
	private String nodeName;			// 모뎀 아이디
	private String applicationID;		// 채널 번호
	private String applicationName;		// 설치지역
	private String modemId;				// 모뎀아이디

	private String temperature;		// 온도
	private String humidity;		// 습도
	private Double currents;		// 전류1 (단위 mA)
	private Double currents2;		// 전류2
	private Double currents3;		// 전류3
	private Double voltage;			// 전압
	// 관리번호
	private String manageNo;
	// 가로등번호
	private String lampNo;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime receiveDate;			// 마지막데이터 수신 일자
	private String receiveDateText;				// 마지막데이터 수신 일자

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;		// 가동시작일 일자
	private String operateStartDtText;
	public String getOperateStartDtText() {
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;		// 가동종요일 일자
	private String operateEndDtText;
	public String getOperateEndDtText() {
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	
	private String operateStartTime;	// 가동시작 시간
	private String operateEndTime;		// 가동종료 시간
	private String moterTopDate;		// 상부모터 가동시간 (설치/교체일 부터 현재까지 일자)	//TODO : 설정값 확인필요
	private String moterBottomDate;		// 하부모터 가동시간 (설치/교체일 부터 현재까지 일자)	//TODO : 설정값 확인필요
	private String lampDate;			// 램프 가동시간 (설치/교체일 부터 현재까지 일자)		//TODO : 설정값 확인필요
	private String modemDate;			// 모뎀 가동시간  	
	private String totalUseDate;		// 총 가동시간

	// 설치일자
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;
		
	public String installDtText;
	public String getInstallDtText(){
		return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	
	// 운영기간
	private String operateDateText;
	public String getOperateDateText() {
		if (operateStartDt == null) {
			return "";
		}

		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT) + "~" + DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 운영시간
	private String operateTimeText;
	public String getOperateTimeText() {
		if (StringUtil.isEmpty(operateStartTime) || StringUtil.isEmpty(operateEndTime)) {
			return "";
		}

		return operateStartTime.substring(0,2) + ":" + operateStartTime.substring(2,4) + " ~ "  + operateEndTime.substring(0,2) + ":" + operateEndTime.substring(2,4);
	}
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList; // 엑셀 저장용 컬럼 리스트
	
	//######################################
	//DashBoard 통계
	private int stateGood; 		// 정상
	private int stateBad;		// 알람
	private int stateStop;		// 꺼짐
	private int connectionOn;	// 정상
	private int connectionOff;	// 단절	
	private int nomal;			// 일반장비	
	private int total;			// 전체장비	
	
	
	//######################################
	//검색 파라메터
	private String searchStartDate; 		// 검색 시작일
	private String searchEndDate;			// 검색 종료일
	private String communicationMode;		// 통신방식
	private String nomalFlag;				// 일반장비 박스 검색(Y: 일반장비만 검색
	private String useNomalFlag;			// 일반장비 제외(N : 제외 처리)
	
	private List<Integer> deviceSeqList;		// 검색 장비  일련번호 리스트
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> powerList;			// 전원
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;		// 상태
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> connectionList;	// 통신
	
	//######################################
	//페이징 파라메터
	private Integer startIndex;
	private Integer indexCount;

	//######################################
	//기준정보 설정 파라메터
	public Monitor(int deviceSeq) {
		this.deviceSeq = deviceSeq;
	}

	// 서버기준시간
    private String endDate;
	//통합보드 시리얼
	private String boardSerial;
	// 통합보드 시리얼 List
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> boardSerialList;
	//기준년도
	private String baseYear;
	//온도 하한
	private String temperatureLow;
	// 온도 상한
	private String temperatureHigh;
	//습도 하한
	private String humidityLow;
	//습도 상한
	private String humidityHigh;
	//전체전압 하한
	private String voltageLow;
	//전체전압 상한
	private String voltageHigh;
	//전체전류 하한
	private String electricLow;
	//전체전류 상한
	private String electricHigh;
	//속성1 하한
	private String property1Low;
	//속성1 상한
	private String property1High;
	//속성2 하한
	private String property2Low;
	//속성2 상한
	private String property2High;
	//속성3 하한
	private String property3Low;
	//속성3 상한
	private String property3High;
	//속성4 하한
	private String property4Low;
	//속성4 상한
	private String property4High;
	
	private String highMoter;
	private String highMoterLow;
	private String highMoterHigh;
	private String lowMoter;
	private String lowMoterLow;
	private String lowMoterHigh;
	private String led;
	private String ledLow;
	private String ledHigh;
	
	// 오류 상태 변수
	private String temperatureStat;
	private String humidityStat;
	private String voltageStat;
	private String electricStat;
	
	private String highMoterStat;
	private String lowMoterStat;
	private String ledStat;


	//######################################
	//리포트 파라메터
	//리포트 타입(당일,1주일,1개월)
	private String reportType;
	private int cnt;
	private String reportReceiveDate;

	public String getReportReceiveDateText() {
		if (StringUtil.isEmpty(reportReceiveDate) ) {
			return "";
		}
		// 일단위 조회일경우 시간만 리턴
		if("toDay".equals(reportType)){
			return reportReceiveDate.substring(0,2);
		}
		return reportReceiveDate;
	}

	//#######################################
	// 대시보드에서 사용
	private int locationSeq;
	private String searchAllText;
	private String userId;	// 권한체크용


	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키

	// 지도(홈 대시보드, 모바일 모니터링)에서 사용
	private int deviceMstSeq;				// 장비일령번호
	private int depth;
	private BigDecimal neLat;
	private BigDecimal neLng;
	private BigDecimal swLat;
	private BigDecimal swLng;
}
