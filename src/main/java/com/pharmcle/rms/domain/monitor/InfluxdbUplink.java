/**
 * Create By jwh on 2020-04-23 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import java.time.Instant;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Measurement(name = "device_uplink") // 기타정보(웹어서 사용안함)
public class InfluxdbUplink {
	
	private String dataType = "uplink";			// 구분자 (uplink)
	
	@Column(name = "time")
	private Instant time;				// 시간
	
	@Column(name = "application_name")
	private String applicationName;		// 설치지역
	
	@Column(name = "dev_eui")
	private String devEUI; 				// 모뎀 시리얼 번호
	
	@Column(name = "device_name")
	private String deviceName;			// 장비명

	@Column(name = "dr")
	private String dr;					// 장비명
	
	@Column(name = "f_CNT")
	private String fCnt;				// 데이터 프레임 카운터
	
	@Column(name = "frequency")
	private String frequency;			// int 주파수 값
	
	@Column(name = "rssi")
	private String rssi;				// int 로라 통신 세기
	
	@Column(name = "snr")
	private String loraSnr ;			// float rssi와 동일

	@Column(name = "value")
	private String value;				// 값
	
	@Override
	public String toString() {
		return "InfluxdbAnalogInput0 {" +
			"time=" + time +
			", applicationName=" + applicationName +
			", devEUI=" + devEUI +
			", deviceName='" + deviceName + '\'' +
			", dr='" + dr + '\'' +
			", fCnt='" + fCnt + '\'' +
			", frequency='" + frequency + '\'' +
			", rssi='" + rssi + '\'' +
			", snr='" + loraSnr + '\'' +
			", value='" + value + '\'' +
        '}';
	}
}
