/**
 * Create By jwh on 2020-04-23 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import java.time.Instant;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Measurement(name = "device_frmpayload_data_analog_input_0") // 전압
public class InfluxdbAnalogInput0 {
	
	private String dataType = "voltage";			// 구분자 (voltage)
	
	@Column(name = "time")
	private Instant time;				// 시간
	
	@Column(name = "application_name")
	private String applicationName;		// 설치지역
	
	@Column(name = "dev_eui")
	private String devEUI; 				// 모뎀 시리얼 번호
	
	@Column(name = "device_name")
	private String deviceName;			// 장비명
	
	@Column(name = "f_port")
	private String fPort;					// 포트, (업링크:19, 다운링크:99)
	
	@Column(name = "value")
	private String value;				// 값
	
	@Override
	public String toString() {
		return "InfluxdbAnalogInput0 {" +
			"time=" + time +
			", applicationName=" + applicationName +
			", devEUI=" + devEUI +
			", deviceName='" + deviceName + '\'' +
			", fPort='" + fPort + '\'' +
			", value='" + value + '\'' +
        '}';
	}
}
