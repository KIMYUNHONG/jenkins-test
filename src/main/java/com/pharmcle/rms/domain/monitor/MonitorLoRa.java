/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class MonitorLoRa extends BaseDomain {
	private int deviceSeq;			// 장비일령번호
	private String devEUI;			// 모뎀 시리얼 코드			
	private String nodeName;		// 모뎀 아이디
	private String applicationID;	// 
	private String applicationName;	// 설치지역 
	private String gwName;			// 게이트웨이 이름
	private String rssi;			// 로라 통신 강도
	private String loraSnr;			// rssi와 동일
	private String frequency;		// 로라 사설 주파수
	private String fCnt;			// 프레임 카운터
	private String fPort;			// 정의된 포트(예: 업링크는 19포트, 다운링크는 99포트)
	private String temperature;		// 온도
	private String humidity;		// 습도
	private Double currents;		// 전류1 (단위 mA)
	private Double currents2;		// 전류2
	private Double currents3;		// 전류3
	private Double voltage;		// 전압

	private String relay;			// 스위치(장비를 켜고 끔)
	private String sentDate;		// 전송시간
	
	// temp_influx_data 데이터 추가 입력용
	private Instant time;			// 로라서버 타임스템프
	private String dataType;		// 데이터 타입(influx 데이터 구분 용)
	private String value;			// 설정값
	private String deviceName;		// 모뎀 이름
	
	// 장비 컨트롤 추가 변수 
	private String operateStartDt;		// 시작일자 (0000-00-00)
	private String operateEndDt;		// 종료일자 (0000-00-00)
	private String operateStartTime;	// 시작시간 (00:00)
	private String operateEndTime;		// 종료시간 (00:00)
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> devEUIList;	// 모뎀 시리얼 코드 리스트
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> protocolList;	// 모뎀 프로토콜 리스트
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Integer> seqList;	// 모뎀 장비 시퀀스
}
