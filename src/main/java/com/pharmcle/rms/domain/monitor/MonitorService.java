/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceDao;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.protocol.Protocol;
import com.pharmcle.rms.domain.protocol.ProtocolParameter;
import com.pharmcle.rms.domain.protocol.ProtocolService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class MonitorService {

	@Autowired
	MonitorDao monitorDao;
	
	@Autowired
	UserService userService;
	
	@Autowired
	InfluxdbService influxdbService;
	
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	DeviceDao deviceDao;
	
	@Autowired
	ProtocolService protocolService;
	
	// API 접속 정보
	@Value("${api.lora.auth.username}") 
	String apiInternalId; // LoRa API 인증 아이디
	
	@Value("${api.lora.auth.password}") 
	String apiInternalPassword; // LoRa API 인증 비밀번호
	
	@Value("${api.lora.domain}") 
	String apiLoraDomain; // LoRa API 도메인
	
	@Value("${api.lora.auth.url}") 
	String apiLoraAuth; // LoRa API 인증 URL
	
	@Value("${api.lora.queue.url}") 
	String apiLoraQueue; // LoRa API Queue URL (전원/스케줄 설정 주소)
	
	// 오류 CSS 
	private String errCssStr = "height";
	
	// 상부모터 
	private Double highMoterLow = 0.314;
	private Double highMoterHigh = 0.400;
			
	// 하부모터
	private Double lowMoterLow = 0.401;
	private Double lowMoterHigh = 0.521;
			
	// LED 
	private Double ledLow = 0.177;
	private Double ledHigh = 0.240;
			
	// UV LED
	private Double uvLedLow = 0.151;
	private Double uvLedHigh = 0.204;
	
	/**
	 * 모니터 리스트 조회
	 * @param Monitor
	 * @return
	 */
	public List<Monitor> getMonitorList(Monitor monitorParameter) throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			monitorParameter.setCustomerSeq(user.getCustomerSeq());
		}
		return  monitorDao.selectList(monitorParameter);
	}

	/**
	 * 모니터 상세 조회
	 * @param Monitor
	 * @return
	 */
	public Monitor getMonitorDetail(Monitor monitorParameter) throws Exception {
		
		Monitor monitor = monitorDao.select(monitorParameter);

		if(monitor == null){
			throw new ValidException("등록된 모니터링 데이타가 없습니다.");
		}
		// 모니터링 장비 장비 오류 판단

		// 온도 
		if(monitor.getTemperature() != null) {
			Double temperatureLow = Double.parseDouble(monitor.getTemperatureLow());
			Double temperatureHigh = Double.parseDouble(monitor.getTemperatureHigh());
			Double temperature = Double.parseDouble(monitor.getTemperature());

			String setTemperatureStat = compareInterval(temperatureHigh, temperatureLow, temperature);
			monitor.setTemperatureStat(setTemperatureStat);
		} else {
			monitor.setTemperatureStat("");
		}
		
		// 습도 Low
		if(monitor.getHumidity() != null) {
			Double humidityLow = Double.parseDouble(monitor.getHumidityLow());
			Double humidityHigh = Double.parseDouble(monitor.getHumidityHigh());
			Double humidity = Double.parseDouble(monitor.getHumidity());
			
			String humidityStat = compareInterval(humidityHigh, humidityLow, humidity);
			monitor.setHumidityStat(humidityStat);
		} else {
			monitor.setHumidityStat("");
		}
		
		// 전압 
		if(monitor.getVoltage() != null) {
			Double voltageLow = Double.parseDouble(monitor.getVoltageLow());
			Double voltageHigh = Double.parseDouble(monitor.getVoltageHigh());
			Double voltage = monitor.getVoltage();
	
			String voltageStat = compareInterval(voltageHigh, voltageLow, voltage);
			monitor.setVoltageStat(voltageStat);
		} else {
			monitor.setVoltageStat("");
		}
		
		// ## 전류 #################################
		if(monitor.getCurrents() != null) {
			Double electricLow = Double.parseDouble(monitor.getElectricLow());
			Double electricHigh = Double.parseDouble(monitor.getElectricHigh());
			Double electric = monitor.getCurrents();
			
			String electricStat = compareInterval(electricHigh, electricLow, electric);
			monitor.setElectricStat(electricStat);
			
			// 모터, LED 상태 확인
			Double defaultElectric = 0.0;
			if (electricLow > electric) { // 전류가 하한값 이하일 경우 기준값의 평균값으로 계산.
				defaultElectric = (electricLow + electricHigh) / 2;
			} else { // 전류값이 정상일경우 기타 전류의 기준값을 로그의 전류로 해서 범위안에 들어 오게 한다.
				defaultElectric = electric;
			}
			
			// UV 제품 체크
			if((monitor.getDeviceNameText()).toUpperCase().indexOf("UV LED") > 0) { 
				ledLow = uvLedLow;
				ledHigh = uvLedHigh;
			}

			// 상부모터
			Double tempHMoterLow = defaultElectric * highMoterLow;
			Double tempHMoterHigh = defaultElectric * highMoterHigh;
			Double tempHMoter = electric * ((highMoterLow + highMoterHigh) / 2);

			monitor.setHighMoterLow(String.format("%.2f",tempHMoterLow));
			monitor.setHighMoterHigh(String.format("%.2f",tempHMoterHigh));
			monitor.setHighMoter(String.format("%.2f",tempHMoter));
			
			// 하부모터
			Double tempLMoterLow = defaultElectric * lowMoterLow;
			Double tempLMoterHigh = defaultElectric * lowMoterHigh;
			Double tempLMoter = electric * ((lowMoterLow + lowMoterHigh) / 2);

			monitor.setLowMoterLow(String.format("%.2f",tempLMoterLow));
			monitor.setLowMoterHigh(String.format("%.2f",tempLMoterHigh));
			monitor.setLowMoter(String.format("%.2f",tempLMoter));
			
			// LED
			Double tempLedLow = defaultElectric * ledLow;
			Double tempLedHigh = defaultElectric * ledHigh;
			Double tempLed = electric * ((ledLow + ledHigh) / 2);

			monitor.setLedLow(String.format("%.2f",tempLedLow));
			monitor.setLedHigh(String.format("%.2f",tempLedHigh));
			monitor.setLed(String.format("%.2f",tempLed));
			
			// 전류가 오류일 경우 나머지 오류 체크 
			if (electricLow > electric) { 
				if(tempLMoterLow <= electric && tempLMoterHigh >= electric) {
					// 1. 상부모터 단독
					monitor.setHighMoterStat("");
					monitor.setLowMoterStat(errCssStr);
					monitor.setLedStat(errCssStr);
					
					// 상부모터
					tempHMoterLow = electric * highMoterLow;
					tempHMoterHigh = electric * highMoterHigh;
					monitor.setHighMoterLow(String.format("%.2f",tempHMoterLow));
					monitor.setHighMoterHigh(String.format("%.2f",tempHMoterHigh));
				} else if (tempLMoterLow <= electric && tempLMoterHigh >= electric) {
					// 2. 하부모터 단독
					monitor.setHighMoterStat(errCssStr);
					monitor.setLowMoterStat("");
					monitor.setLedStat(errCssStr);
					
					// 하부모터
					tempLMoterLow = defaultElectric * lowMoterLow;
					tempLMoterHigh = defaultElectric * lowMoterHigh;
					monitor.setLowMoterLow(String.format("%.2f",tempLMoterLow));
					monitor.setLowMoterHigh(String.format("%.2f",tempLMoterHigh));
				} else if (tempLedLow <= electric && tempLedHigh >= electric) {
					// 3. LED 단독
					monitor.setHighMoterStat(errCssStr);
					monitor.setLowMoterStat(errCssStr);
					monitor.setLedStat("");
					
					// LED
					tempLedLow = defaultElectric * ledLow;
					tempLedHigh = defaultElectric * ledHigh;
					monitor.setLedLow(String.format("%.2f",tempLedLow));
					monitor.setLedHigh(String.format("%.2f",tempLedHigh));
				} else if ((tempHMoterLow + tempLedLow) <= electric && (tempHMoterHigh + tempLedHigh)  >= electric) {
					// 4. 상부모터 + LED
					monitor.setHighMoterStat("");
					monitor.setLowMoterStat(errCssStr);
					monitor.setLedStat("");
					
					// 상부모터
					tempHMoterLow = electric * highMoterLow;
					tempHMoterHigh = electric * highMoterHigh;
					monitor.setHighMoterLow(String.format("%.2f",tempHMoterLow));
					monitor.setHighMoterHigh(String.format("%.2f",tempHMoterHigh));
					
					// LED
					tempLedLow = electric * ledLow;
					tempLedHigh = electric * ledHigh;
					monitor.setLedLow(String.format("%.2f",tempLedLow));
					monitor.setLedHigh(String.format("%.2f",tempLedHigh));
				} else if ((tempLMoterLow + tempLedLow) <= electric && (tempLMoterHigh + tempLedLow) >= electric) {
					// 5. 하부모터 + LED
					monitor.setHighMoterStat(errCssStr);
					monitor.setLowMoterStat("");
					monitor.setLedStat("");
					
					// 하부모터
					tempLMoterLow = electric * lowMoterLow;
					tempLMoterHigh = electric * lowMoterHigh;
					monitor.setLowMoterLow(String.format("%.2f",tempLMoterLow));
					monitor.setLowMoterHigh(String.format("%.2f",tempLMoterHigh));
					
					// LED
					tempLedLow = electric * ledLow;
					tempLedHigh = electric * ledHigh;
					monitor.setLedLow(String.format("%.2f",tempLedLow));
					monitor.setLedHigh(String.format("%.2f",tempLedHigh));
				} else if ((tempHMoterLow + tempLMoterLow) <= electric && (tempHMoterHigh + tempLMoterHigh) >= electric) {
					// 6. 상부모터 + 하부모터
					monitor.setHighMoterStat("");
					monitor.setLowMoterStat("");
					monitor.setLedStat(errCssStr);
					
					// 상부모터
					tempHMoterLow = electric * highMoterLow;
					tempHMoterHigh = electric * highMoterHigh;
					monitor.setHighMoterLow(String.format("%.2f",tempHMoterLow));
					monitor.setHighMoterHigh(String.format("%.2f",tempHMoterHigh));
					
					// 하부모터
					tempLMoterLow = electric * lowMoterLow;
					tempLMoterHigh = electric * lowMoterHigh;
					monitor.setLowMoterLow(String.format("%.2f",tempLMoterLow));
					monitor.setLowMoterHigh(String.format("%.2f",tempLMoterHigh));
				} else if (tempLedLow > electric) {
					// 7. 전체 오류
					monitor.setHighMoterStat(errCssStr);
					monitor.setLowMoterStat(errCssStr);
					monitor.setLedStat(errCssStr);
				} 
			}
		} else {
			monitor.setElectricStat("");
			monitor.setHighMoterStat("");
			monitor.setLowMoterStat("");
			monitor.setLedStat("");
		}

		return monitor;
	}
	
	/**
	 * 장비 상태 체크
	 * @param Double
	 * @param Double
	 * @param Double
	 * @return String
	 */
	private String compareInterval(Double big, Double small, Double val) {
		if (big >= val && small <= val) {
			return "";
		} else {
			return errCssStr;
		}
	}
	
	/**
	 * 모니터 DashBoard
	 * @param Monitor
	 * @return
	 */
	public Monitor getMonitorStatCount(Monitor monitorParameter) throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			monitorParameter.setCustomerSeq(user.getCustomerSeq());
		}
		return  monitorDao.selectStatCount(monitorParameter);
	}
	
	/**
	 * 모니터 DashBoard
	 * @param Monitor
	 * @return
	 */
	public String getLastReceiveDate(Monitor monitorParameter) throws Exception{
		return  monitorDao.selectLastReceiveDate(monitorParameter);
	}
	
	/**
	 * 로라 API 서버 인증 키 받아오기
	 * @return
	 */
	private String getLoRaAuthKey() {
		
		String apiInternalLogUrl = apiLoraDomain + apiLoraAuth; // LoRa API 인증키 전체 주소
		
		String jwt = ""; // 인증키
		
		// BODY에 전달할 Json설정
		JSONObject ob = new JSONObject();
		ob.put("username", apiInternalId);
		ob.put("password", apiInternalPassword);
		
		MappingJacksonValue value = new MappingJacksonValue(ob);
		HttpEntity<MappingJacksonValue> entry = new HttpEntity<MappingJacksonValue>(value); 
		RestTemplate rest = new RestTemplate();
		
		String respones = "";
		 
		try {
			// 인증키 받아 오기 
			respones = rest.postForObject(apiInternalLogUrl, value, String.class);
			 
			JSONParser jsonParser = new JSONParser();
			JSONObject obj = (JSONObject) jsonParser.parse(respones);
			 
			jwt =  (String) obj.get("jwt");
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			log.info("##### respones : " + e.toString());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("##### respones : " + e.toString());
		}
		
		return jwt;
	}
	
	/**
	 * 로라 API 서버 Queue 전송
	 * @param String jwt
	 * @param String devEui
	 * @param String data
	 * @param int port
	 * @return
	 */
	private int setLoRaQueue(String jwt, String devEui, String data, int port) {
		
		int stat = 0;
		
		if(StringUtil.isEmpty(devEui)) {
			return stat;
		} else {
			// Queue 주소 생성
			String apiQueuePathUrl = apiLoraDomain + apiLoraQueue.replace("{devEui}", devEui);
			
			//헤더설정-> 기본인증처리
			HttpHeaders headers = new HttpHeaders();
			headers.add("Grpc-Metadata-Authorization", jwt);
			
			// BODY에 전달할 Json설정 값 세팅
			JSONObject ob = new JSONObject();
			ob.put("confirmed", true);
			ob.put("devEUI", devEui);
			ob.put("data", data);
			ob.put("fPort", port);
			
			// BODY에 전달할 Json 
			JSONObject deviceQueue = new JSONObject();
			deviceQueue.put("deviceQueueItem", ob);
			
			MappingJacksonValue value = new MappingJacksonValue(deviceQueue);
			HttpEntity<MappingJacksonValue> entry = new HttpEntity<MappingJacksonValue>(value, headers); 
			RestTemplate rest = new RestTemplate();
			
			String respones = "";
			
			try {
				// 전원 작동 처리 하기 
				respones = rest.postForObject(apiQueuePathUrl, entry, String.class);   // {"fCnt":6}
				
				log.info("##### respones : " + respones.toString());
				
				stat++; // 성공 횟수 저장
			} catch (HttpClientErrorException e) {
				e.printStackTrace();
				log.error("##### error[HttpClientErrorException] : " + e.toString());
			} catch (Exception e) {
				e.printStackTrace();
				log.error("##### error[Exception] : " + e.toString());
			}
		}
		
		return stat;
	}
	
	/**
	 * 모니터링 LoRa 전원  컨트롤
	 * @param MonitorLoRa
	 * @return
	 */
	public int setLoraPowerStat(MonitorLoRa parameter) throws Exception {
		
		int stat = 0;
		
		ValidStringUtil.nullChk(parameter.getRelay(), "켜짐/꺼짐");
		
		// 인증키 받아 오기 
		String jwt = getLoRaAuthKey();
		
		// 인증 키 오류  
		if(StringUtil.isEmpty(jwt)) {
			throw new ValidException(ResponseUtil.FILE_AUTH_FAIL);
		} else {			
			// 장비 모뎀 시리얼 리스트
			List<String> devEUIList =  parameter.getDevEUIList();

			// 전원 작동 설정
			// base64 : AQ== : 1, AA== : 0 (0: Relay off / 1: Relay on)
			String relay = parameter.getRelay();  
			String data = (relay.equals("0")) ? "AA==" : "AQ==" ;
			int port = 96; // 전원 작동 포트 

			MonitorLoRa loRaData = new MonitorLoRa();
			loRaData.setRelay(relay);
			
			List<String> eduUpList = new ArrayList<String>(); // 업데이트 리스트 
			
			for (String item : devEUIList) {
				//item = "373838396a37790b"; // TODO : 테스트 시리얼 번호 디비 처리 후 주석요!!!

				int i = setLoRaQueue(jwt, item, data, port);
				
				if(i > 0) { // 실행 결과 세팅
					// 디비 relay 컬럼 업데이트
					eduUpList.add(item);
					stat ++;
				}
			}
			
			// 전원 업데이트 내용이 있을경우 디비 업데이트
			if(stat > 0) {
				loRaData.setDevEUIList(eduUpList);
				monitorDao.updateRelay(loRaData); // 디비 업데이트
			}
		}
		
		return stat;
	}
	
	/**
	 * 모니터링 LoRa 작동 시간 컨트롤
	 * @param MonitorLoRa
	 * @return
	 */
	public int setLoraSchedule(MonitorLoRa parameter) throws Exception {
		
		int stat = 0;

		ValidStringUtil.nullChk(parameter.getOperateStartDt(), "가동시작 일자");
		ValidStringUtil.lengthChk(parameter.getOperateStartTime(), "가동시작 시간", 1, 4);
		ValidStringUtil.nullChk(parameter.getOperateEndDt(), "가동종료 일자");
		ValidStringUtil.lengthChk(parameter.getOperateEndTime(), "가동종료 시간", 1,4);
		
		// 인증키 받아 오기 
		String jwt = getLoRaAuthKey();
		
		// 인증 키 오류  
		if(StringUtil.isEmpty(jwt)) {
			throw new ValidException(ResponseUtil.FILE_AUTH_FAIL);
		} else {
			// 장비 모뎀 시리얼 리스트
			List<String> devEUIList =  parameter.getDevEUIList();

			// 가동 일자 설정
			String operateStartDt = parameter.getOperateStartDt();
			String operateStartTime = parameter.getOperateStartTime();
			
			String operateEndDt = parameter.getOperateEndDt();
			String operateEndTime = parameter.getOperateEndTime();

			String operateStartDate = operateStartDt + " " + operateStartTime.substring(0,2) + ":" + operateStartTime.substring(2,4) + ":00";
			String operateEndDate = operateEndDt + " " + operateEndTime.substring(0,2) + ":" + operateEndTime.substring(2,4) + ":00";
			
			long sratTime = DateUtil.getUnixDate(operateStartDate);
			long endTime = DateUtil.getUnixDate(operateEndDate);

			String settingDate = sratTime + "" + endTime;
			//log.info("settingDate : " + settingDate);
			
	        byte[] strs;
	        strs = settingDate.getBytes();
	        //String data = Base64Coder.binaryToHex(strs);
	        
	        String data = new String(Base64Coder.encodeString(Base64Coder.binaryToHex(strs)));
			
			//log.info("getUnixDate2 : " + data);
			
			int port = 95; // 스케줄 세팅 포트 
			
			for (String item : devEUIList) {
				
				int i = setLoRaQueue(jwt, item, data, port);
				
				if(i > 0) { // 실행 결과 세팅
					stat ++;
				}
			}
		}

		return stat;
	}

	/**
	 * 모니터 장비 데이터 저장
	 * @return void
	 */
	@Transactional(rollbackFor = Exception.class)
	public void setLoRaData(String type) throws Exception{
		
		SimpleDateFormat format2 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:00");
		SimpleDateFormat format1;
		if (type.equals("T")) {
			format1 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:00");
		} else {
			format1 = new SimpleDateFormat ("yyyy-MM-dd HH:00:00");
		}
		
		
		// 데이터 기준시간 받아 오기 
		String strStartDate = monitorDao.selectInfluxEndDate();
		Date startDate = format2.parse(strStartDate);
		
		// 기준시간 -9시간 처리
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(startDate);
		cal.add(Calendar.HOUR, -9);
		
		String strStartDateTime = format2.format(cal.getTime());  
		
		// 현재 시간 받아 오기 시간은 yyyy-mm-dd HH:00:00 형식
		Date nowDate = new Date();
		
		// 현재 시간에서 -9시간
		String strEndDate = format1.format(nowDate);
		cal.setTime(nowDate);
		cal.add(Calendar.HOUR, -9);
		
		String strEndDateTime = format1.format(cal.getTime());
		
		// 타임스템프값
		long startTimeStamp = DateUtil.getUnixDate(strStartDateTime);
		long endTimeStamp = DateUtil.getUnixDate(strEndDateTime);
		
		log.info("setLoRaData[strStartDate] : " + strStartDate);
		log.info("setLoRaData[strEndDate] : " + strEndDate);
		log.info("setLoRaData[startTimeStamp] : " + startTimeStamp);
		log.info("setLoRaData[endTimeStamp] : " + endTimeStamp);
		
		// 디비 저장 시작
		int result = influxdbService.influxdbInsert(startTimeStamp, endTimeStamp);
		
		// LoRa 데이터 입력
		if(result >= 6) {
			monitorDao.insertNodeReceivelog(); // log 입력
			monitorDao.updateNodeReceivelogDeviceRelay(strStartDate); // device 업데이트
			monitorDao.updateTempInfluxDate(strEndDate); // 시간 업데이트
		}
	}

	/**
	 * 모니터 장비 데이터 저장
	 * @param Monitor
	 * @return void
	 */
	public Monitor getDeviceProtocol(Monitor parameter)throws Exception{
		return monitorDao.selectDeviceProtocol(parameter);
	}


	/**
	 * 통신기준정보관리 관리
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setDeviceProtocol(Monitor parameter) throws Exception {

		/**
		 * 1. LORA 장비의 가동기간, 가동시간 UPDATE
		 * 2. DEVICE_PROTOCOL TABLE UPDATE
		 * 3. DEVICE TABLE 가동기간 , 가동시간 UPDATE
		 * 4. DEVICE_PROTOCOL_LOG DATA INSERT
		 * **/

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);

		ValidDateUtil.nullChk(parameter.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(parameter.getOperateEndDt(), "가동기간(종료일)");

		// LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
		MonitorLoRa loraParameter = new MonitorLoRa();
		Device device = new Device();

		// DEVICE_PROTOCOL_LOG 저장을 위한 객체생성
		Protocol protocol;
		ProtocolParameter protocolParameter = new ProtocolParameter();

		// DEVICE DATA SELECT
		Device deviceInfo = deviceDao.select(new DeviceParameter(parameter.getDeviceSeq()));

		// LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
		loraParameter.setOperateStartDt(DateUtil.localDateToString(parameter.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
		loraParameter.setOperateStartTime(parameter.getOperateStartTime());
		loraParameter.setOperateEndDt(DateUtil.localDateToString(parameter.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
		loraParameter.setOperateEndTime(parameter.getOperateEndTime());
		loraParameter.setDevEUIList(parameter.getBoardSerialList());

		/**
		 * RMS LOG DATA 등 저장 후 실제 장비와 통신
		 * 1. DEVICE_PROTOCOL UPDATE
		 * 2. DEVICE UPDATE
		 * 3. DEVICE_PROTOCOL_LOG INSERT
		 * **/

		// DEVICE_PROTOCOL TABLE UPDATE
		boolean result = monitorDao.updateDeviceProtocol(parameter) > 0;

		if (!result) {
			throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
		}

		// DEVICE TABLE의 가동기간,가동시간 UPDATE를 위한 DATA SET
		device.setDeviceSeq(parameter.getDeviceSeq());
		device.setOperateEndDt(parameter.getOperateEndDt());
		device.setOperateStartDt(parameter.getOperateStartDt());
		device.setOperateEndTime(parameter.getOperateEndTime());
		device.setOperateStartTime(parameter.getOperateStartTime());

		// DEVICE TABLE 장비 가동기간 , 가동시간 수정
		deviceService.updateDeviceOperateInfo(device);

		// 기준정보 SELECT를 위한 protocolParameter DATA SET (baseYear , DeviceClassify)
		LocalDate localDate = LocalDate.now();
		protocolParameter.setDeviceClassify(deviceInfo.getDeviceClassify());
		protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

		// 기준정보 select
		protocol = protocolService.getProtocol(protocolParameter);

		if (protocol == null) {
			throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
		}

		// DEVICE_PROTOCOL_LOG 등록을 위한 DATA SET
		protocol.setDeviceSeq(deviceInfo.getDeviceSeq());
		protocol.setOperateStartDt(parameter.getOperateStartDt());
		protocol.setOperateEndDt(parameter.getOperateEndDt());
		protocol.setOperateStartTime(parameter.getOperateStartTime());
		protocol.setOperateEndTime(parameter.getOperateEndTime());

		// DEVICE_PROTOCOL_LOG 등록
		protocolService.setDeviceProtocolLog(protocol);

		// LORA장비 가동기간 , 가동시간 UPDATE
		int stat = setLoraSchedule(loraParameter);

		if(stat <= 0 ){
			//  TODO : [김윤홍] 장비와의 통신 실패시 처리를 어떻게 할지 결정해야함
//			throw new Exception();
		}

		return true;
	}

	// 모니터 상세 Report Data
	public List<Monitor> getMonitorReportData(Monitor monitorParameter) throws Exception{

		List<Monitor> list = new ArrayList<>();

		ValidStringUtil.nullChk(monitorParameter.getSearchStartDate(), "차트 시작일");
		ValidStringUtil.nullChk(monitorParameter.getSearchEndDate(), "차트 종료일");


		if(monitorParameter.getSearchStartDate().equals(monitorParameter.getSearchEndDate())){

			// 일단위 조회일때
			// 일단위 조회시 00시부터  23시까지 조회되도록 Parameter Set
			monitorParameter.setSearchStartDate(monitorParameter.getSearchStartDate() + " 00");
			monitorParameter.setSearchEndDate(monitorParameter.getSearchEndDate() + " 23");
			list = monitorDao.selectTodayMonitorReportData(monitorParameter);

		}else{
			// 주,월단위 조회일때
			list = monitorDao.selectMonitorReportData(monitorParameter);
		}

		return  list;
	}

	/**
	 * 최신 모니터링 데이터 수신일시 조회
	 * @return
	 */
	public String getLatestMontiorDate(){
		return monitorDao.selectInfluxEndDate();
	}

	/**
	 * 모니터링 알람 로그 저장
	 * @param MonitorAlarm
	 * @return int
	 */
	private int insertAlarm(MonitorAlarm parameter) {
		return monitorDao.insertAlarmLog(parameter);
	}
	
	/**
	 * 모니터링 알람 로그 저장
	 * @param MonitorAlarm
	 * @return int
	 */
	private int insertOperateTime(MonitorAlarm parameter) {
		return monitorDao.insertOperateTime(parameter);
	}
	
	/**
	 * 모니터링 알람 로그 저장
	 * @param MonitorAlarm
	 * @return int
	 */
	private void insertAlarmLogPrint(String type, String typeNum, MonitorAlarm parameter) {
		if("".equals(type)) {
			log.info(type + "[" + typeNum + "] : log ################");
			log.info(type + "[seq] : " + parameter.getSeq() + " = " + parameter.getUseTime());
			log.info(type + "[sDate] : " + parameter.getSDate());
			log.info(type + "[eDate] : " + parameter.getEDate());
			log.info(type + "[part] : " + parameter.getPart());
		}
	}

	/**
	 * 모니터링 알람 로그 생성 
	 * @return void
	 */
	@Transactional(rollbackFor = Exception.class)
	public void setMonitorAlarmUpdate(){
		
		final int timeInterval = 2;
		
		List<MonitorAlarm> list = monitorDao.selectMonitorAlarmLog();
		MonitorAlarm parameterAlarm = new MonitorAlarm();
		MonitorAlarm parameterOperate = new MonitorAlarm();
		MonitorAlarm parameterOperateTime = new MonitorAlarm();
		
		int tempSeq = 0;
		String tempNode = "";
		String tempPart = "";
		
		String tempRelay = "";
		int tempHour = 0;
		int totalOperateTime = 0;
		LocalDateTime tempDate = null;
		
		for (MonitorAlarm monitorAlarm : list) {
			int seq = monitorAlarm.getSeq();
			String node = monitorAlarm.getNode();
			LocalDateTime rDate = monitorAlarm.getRDate();
			int rDateHour = monitorAlarm.getRDateHour();
			String part = monitorAlarm.getPart();
			String relay = monitorAlarm.getRelay();
			
			//log.info("############# for Start ################################");
			//log.info("[seq]=" + seq + "[node]=" + node + "[rDateHour]=" + rDateHour + "[part]=" + part + "[relay]=" + relay  + "[rDate]=" + rDate);
			
			// [1] 장비 alarm 로그 저장 
			// [1.1] 장비가 바뀔 경우 저장
			if(tempSeq != 0 && tempSeq != seq && "MT040002".equals(tempPart)) {
				
				insertAlarmLogPrint("parameterAlarm", "1.1", parameterAlarm);				
				//log.info("[1.1] tempSeq : " + tempSeq + "[seq] : " + seq + "[tempPart] : " + tempPart);
				int result = insertAlarm(parameterAlarm);
			}

			// 장비 변경 or 장비 알람 상태 변경
			if( tempSeq == 0 || tempSeq != seq || (tempSeq == seq && !(tempPart.equals(part))) ) {
				if("MT040002".equals(part)) {
					parameterAlarm.setSeq(seq);
					parameterAlarm.setNode(node);
					parameterAlarm.setPart(part);
					parameterAlarm.setSDate(rDate);
				}
			}
			
			// [1.2] 같은 장비 알람 저장
			if(tempSeq == seq && (rDateHour - tempHour) >= timeInterval) {
				// 2시간 이상일 경우 처리 
				if ("MT040002".equals(tempPart)) {
					parameterAlarm.setEDate(tempDate);
					insertAlarmLogPrint("parameterAlarm", "1.4", parameterAlarm);		
					int result = insertAlarm(parameterAlarm);
					
					parameterAlarm.setSDate(rDate);
				}
			} else if(tempSeq == seq && "MT040002".equals(tempPart) && !(tempPart.equals(part)) ) {
				//log.info("[1.2] tempSeq : " + tempSeq + "[seq] : " + seq + "[tempPart] : " + tempPart);
				insertAlarmLogPrint("parameterAlarm", "1.2", parameterAlarm);		
				int result = insertAlarm(parameterAlarm);
			}
			
			parameterAlarm.setEDate(rDate);
			
			
			// [2] 장비 on/off 로그 저장 			
			// [2.2] 장비 데이터 변경
			if(tempSeq != seq && "MT040001".equals(relay) && rDateHour > 1 && rDateHour < 23) {
				
				// [2.1] 이전 장비 off처리
				if(tempSeq > 0) {
					parameterOperate.setSeq(tempSeq);
					parameterOperate.setNode(tempNode);
					parameterOperate.setSDate(tempDate);
					parameterOperate.setEDate(tempDate);
					parameterOperate.setPart("MT040003"); // off
					
					insertAlarmLogPrint("parameterOperate", "2.1", parameterOperate);
					int result = insertAlarm(parameterOperate);
				}
				
				parameterOperate.setSeq(seq);
				parameterOperate.setNode(node);
				parameterOperate.setSDate(rDate);
				parameterOperate.setEDate(rDate);
				parameterOperate.setPart(relay);
				
				insertAlarmLogPrint("parameterOperate", "2.2", parameterOperate);
				int result = insertAlarm(parameterOperate);
			}
			
			// 같은 장비 on/off 설정
			if(tempSeq == seq && (rDateHour - tempHour) >= timeInterval) {
				// [2.4] 로그 데이터 간격이 2시간 이상일경우 on/off 처리
				// 이전데이터 off
				if("MT040001".equals(tempRelay)) {
					// TODO 장비 off시간을 이전 시간에서 1시간 후로 설정해야 한다.
					parameterOperate.setSeq(seq);
					parameterOperate.setNode(node);
					parameterOperate.setSDate(tempDate);
					parameterOperate.setEDate(tempDate);
					parameterOperate.setPart("MT040003"); //off

					insertAlarmLogPrint("parameterOperate", "2.4.1", parameterOperate);
					int result = insertAlarm(parameterOperate);
				}
				
				// 현재 on일경우 
				if("MT040001".equals(relay)) {
					parameterOperate.setSeq(seq);
					parameterOperate.setNode(node);
					parameterOperate.setSDate(rDate);
					parameterOperate.setEDate(rDate);
					parameterOperate.setPart(relay);
					
					insertAlarmLogPrint("parameterOperate", "2.4.2", parameterOperate);
					int result = insertAlarm(parameterOperate);
				}
			} else if (tempSeq == seq) {
				// [2.3] 같은 장비 on/off 처리
				if(tempSeq == seq && !(tempRelay.equals(relay))) {
					parameterOperate.setSeq(seq);
					parameterOperate.setNode(node);
					parameterOperate.setSDate(rDate);
					parameterOperate.setEDate(rDate);
					parameterOperate.setPart(relay);
					
					insertAlarmLogPrint("parameterOperate", "2.3", parameterOperate);
					int result = insertAlarm(parameterOperate);
				}
			}
			
			
			// [3] 장비 운영 시간 설정 totalOperateTime
			if(tempSeq > 0 && tempSeq != seq) {
				parameterOperateTime.setSeq(tempSeq);
				parameterOperateTime.setNode(tempNode);
				parameterOperateTime.setRDate(rDate);
				parameterOperateTime.setUseTime(totalOperateTime);
				
				insertAlarmLogPrint("parameterOperateTime", "3.1", parameterOperateTime);
				int result = insertOperateTime(parameterOperateTime);
				
				// 운영 시간 초기화
				tempHour = 0;
				totalOperateTime = 0;
			}
			
			// 장비 운영 시간 설정 totalOperateTime
			if("MT040001".equals(relay)) {
				if(tempHour == 0) {
					totalOperateTime = 1;
				} else if(tempHour != rDateHour) {
					totalOperateTime = totalOperateTime + 1;
				}
			}
			
			// 이전 데이터 저장
			tempSeq = seq;
			tempNode = node;
			tempRelay = relay;
			tempPart = part;
			tempHour = rDateHour;
			tempDate = rDate;
		}
		
		// 마지막 데이터가 있을때 실행
		if (list.size() > 0) {
			// [1.3] 마지막 데이터 알람 처리
			if("MT040002".equals(tempPart)) {
				
				//log.info("[1.3] === [tempPart]=" + tempPart + "[seq]=" + parameterAlarm.getSeq() + "[part]=" + parameterAlarm.getPart());
				
				insertAlarmLogPrint("parameterAlarm", "1.3", parameterAlarm);
				if(parameterAlarm.getSeq() > 0) {
					int result = insertAlarm(parameterAlarm);
				}
			}
		
			// [2.5] 마지막 데이터 off처리 
			if("MT040001".equals(tempRelay) &&  tempHour < 23) {
				parameterOperate.setSeq(tempSeq);
				parameterOperate.setNode(tempNode);
				parameterOperate.setSDate(tempDate);
				parameterOperate.setEDate(tempDate);
				parameterOperate.setPart("MT040003");  // off
				
				insertAlarmLogPrint("parameterOperate", "2.5", parameterOperate);
				int result = insertAlarm(parameterOperate);
			}
			
			// [3.1] 마지막 데이터 장비 운영 시간 처리
			parameterOperateTime.setSeq(tempSeq);
			parameterOperateTime.setNode(tempNode);
			parameterOperateTime.setRDate(tempDate);
			parameterOperateTime.setUseTime(totalOperateTime);
			
			insertAlarmLogPrint("parameterOperateTime", "3.2", parameterOperateTime);
			int result = insertOperateTime(parameterOperateTime);
		}
	}
	
	/**
	 * 모니터 상세보기 알람 리스트 조회
	 * @param MonitorAlarm
	 * @return MonitorAlarm
	 */
	public List<MonitorAlarm> getMonitorConditionList(Monitor parameter) throws Exception {
		
		ValidStringUtil.nullChk(parameter.getSearchStartDate(), "시작일");
		ValidStringUtil.nullChk(parameter.getSearchEndDate(), "종료일");
		
		log.info("searchStartDate : " + parameter.getSearchStartDate());
		log.info("searchStartDate : " + parameter.getSearchEndDate());
		
		String searchStartDate = parameter.getSearchStartDate();
		String searchEndDate = parameter.getSearchEndDate();
		
		//String searchStartDate = parameter.getSearchStartDate().replaceAll("\\.", "-");
		//String searchEndDate = parameter.getSearchEndDate().replaceAll("\\.", "-");
		
		log.info("searchStartDate : " + searchStartDate);
		log.info("searchEndDate : " + searchEndDate);
		
		parameter.setSearchStartDate(searchStartDate);
		parameter.setSearchEndDate(searchEndDate);
		
		return monitorDao.selectMonitorCondition(parameter);
	}
}


