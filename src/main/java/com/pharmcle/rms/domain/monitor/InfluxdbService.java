/**
 * Create By jwh on 2020-04-23 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import java.util.List;

import org.influxdb.dto.BoundParameterQuery.QueryBuilder;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class InfluxdbService {
	
	@Autowired
	MonitorDao monitorDao;
	
	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;
	
	/**
	 * influx 데이터 저장 하기
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	public int influxdbInsert(long startDate, long endDate) throws Exception {
		int result = 0;
		
		int del = monitorDao.deleteTempInfluxData();

		result += influxdbAnalogInput0List(startDate, endDate);
		result += influxdbAnalogInput1List(startDate, endDate);
		result += influxdbDigitalOutput0List(startDate, endDate);
		result += influxdbHumiditySensor0List(startDate, endDate);
		result += influxdbTemperatureSensor0List(startDate, endDate);
		result += influxdbUplinkList(startDate, endDate);		
		
		//log.info("influxdbInsert [result] : " + result);
		
		return result;
	}
	
	/**
	 * influxdb 전압 Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbAnalogInput0List(long startDate, long endDate) throws Exception {
		int result = 0;
		
		String table = "device_frmpayload_data_analog_input_0";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);
		
		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbAnalogInput0> list = resultMapper.toPOJO(queryResult, InfluxdbAnalogInput0.class);

		for (InfluxdbAnalogInput0 item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFPort(item.getFPort());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * influxdb 전류 Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbAnalogInput1List(long startDate, long endDate) {

		int result = 0;
		
		String table = "device_frmpayload_data_analog_input_1";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);

		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbAnalogInput1> list = resultMapper.toPOJO(queryResult, InfluxdbAnalogInput1.class);

		for (InfluxdbAnalogInput1 item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFPort(item.getFPort());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * influxdb relay Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbDigitalOutput0List(long startDate, long endDate) throws Exception {
		int result = 0;
		
		String table = "device_frmpayload_data_digital_output_0";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);
		
		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbDigitalOutput0> list = resultMapper.toPOJO(queryResult, InfluxdbDigitalOutput0.class);

		for (InfluxdbDigitalOutput0 item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFPort(item.getFPort());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * influxdb 습도 Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbHumiditySensor0List(long startDate, long endDate) throws Exception {
		int result = 0;
		
		String table = "device_frmpayload_data_humidity_sensor_0";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);
		
		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbHumiditySensor0> list = resultMapper.toPOJO(queryResult, InfluxdbHumiditySensor0.class);

		for (InfluxdbHumiditySensor0 item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFPort(item.getFPort());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * influxdb 온도 Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbTemperatureSensor0List(long startDate, long endDate) throws Exception {
		int result = 0;
		
		String table = "device_frmpayload_data_temperature_sensor_0";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);
		
		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbTemperatureSensor0> list = resultMapper.toPOJO(queryResult, InfluxdbTemperatureSensor0.class);

		for (InfluxdbTemperatureSensor0 item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFPort(item.getFPort());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * influxdb 기타정보 Temp 저장 
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int influxdbUplinkList(long startDate, long endDate) throws Exception {
		int result = 0;
		
		String table = "device_uplink";
		QueryResult queryResult = selecInfluxdbtList(table, startDate, endDate);
		
		InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
		List<InfluxdbUplink> list = resultMapper.toPOJO(queryResult, InfluxdbUplink.class);

		for (InfluxdbUplink item : list) {
			// Temp디비 저장
			MonitorLoRa parameter = new MonitorLoRa();
			parameter.setTime(item.getTime());
			parameter.setDataType(item.getDataType());
			parameter.setDevEUI(item.getDevEUI());
			parameter.setApplicationName(item.getApplicationName());
			parameter.setDeviceName(item.getDeviceName());
			parameter.setFCnt(item.getFCnt());
			parameter.setFrequency(item.getFrequency());
			parameter.setRssi(item.getRssi());
			parameter.setLoraSnr(item.getLoraSnr());
			parameter.setValue(item.getValue());
			
			result = insertInfluxdb(parameter);
		}
		
		return result;
	}
	
	/**
	 * (공통) InfluxDB select
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private QueryResult selecInfluxdbtList(String table, long startDate, long endDate) {
		String startDateTime = startDate + "000000";
		String endDateTime = endDate + "000000";
		
		//log.info("influxdbAnalogInput0List[startDate] : " + startDateTime);
		//log.info("influxdbAnalogInput0List[endDate] : " + endDateTime);
		
		String sql = "select * from " + table + " where time >= " + startDateTime + " and time < " + endDateTime + " order by time asc";
		//String sql = "select * from " + table + " where time >= " + startDateTime + "";
		//log.info("influxdbAnalogInput0List[sql] : " + sql);
		
		Query query = QueryBuilder.newQuery(sql)
				.forDatabase("pharmcle")
				.create();

		return influxDBTemplate.query(query);
	}
	
	/**
	 * (공통)temp_influx_data 디비 저장
	 * @param long startDate
	 * @param long endDate
	 * @return 
	 */
	private int insertInfluxdb(MonitorLoRa parameter) {
		return monitorDao.insertTempInfluxData(parameter);
	}
}
