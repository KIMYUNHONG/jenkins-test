/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitor;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MonitorDao {
	//모니터 리스트 조회
	List<Monitor> selectList(Monitor parameter);
	//모니터 상세 조회
	Monitor select(Monitor parameter);

	//모니터 DashBoard 조회
	Monitor selectStatCount(Monitor parameter);
	
	//모니터링 마지막데이터 수신 조회
	String selectLastReceiveDate(Monitor parameter);

	//모니터 상세 Report Data (주,월단위 조회)
	List<Monitor> selectMonitorReportData(Monitor parameter);

	//모니터 상세 Report Data (일단위 조회)
	List<Monitor> selectTodayMonitorReportData(Monitor parameter);

	//모니터 로그 데이터 저장
	int insertNodeReceivelog();

	//모니터링 로그 최종 저장 시간(unix타임스템프)
	int updateNodeReceivelogDeviceRelay(String startDate);
	
	//모니터링 로그 최종 저장 시간(unix타임스템프)
	int updateTempInfluxDate(String endDate);

	// Influx Temp select 시작일자
	String selectInfluxEndDate();

	//Influx Temp 데이터 저장
	int insertTempInfluxData(MonitorLoRa parameter);

	//Influx Temp 데이터 삭제
	int deleteTempInfluxData();
	
	//모니터 장비 전원 컨트롤 저장
	int updateRelay(MonitorLoRa parameter);

	//모니터 장비 통신기준정보 조회
	Monitor selectDeviceProtocol(Monitor parameter);

	//모니터 장비 통신기준정보 저장
	int updateDeviceProtocol(Monitor parameter);
	
	// 모니터링 장비 알람 정보 조회
	List<MonitorAlarm> selectMonitorAlarmLog();
	
	// 모니터링 장비 알람 정보 저장
	int insertAlarmLog(MonitorAlarm parameter);
	
	// 모니터링 장비 가동 시간 저장
	int insertOperateTime(MonitorAlarm parameter);
	
	// 모니터링 장비 알람 정보 조회
	List<MonitorAlarm> selectMonitorCondition(Monitor parameter); 
	
}
