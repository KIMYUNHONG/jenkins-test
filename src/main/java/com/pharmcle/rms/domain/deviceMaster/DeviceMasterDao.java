/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceMaster;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DeviceMasterDao {

	List<DeviceMaster> selectList(DeviceMasterParameter parameter);
	DeviceMaster select(int deviceMstSeq);
	int insert(DeviceMaster parameter);
	int update(DeviceMaster parameter);
}
