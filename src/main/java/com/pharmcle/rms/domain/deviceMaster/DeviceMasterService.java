/**
 * Create By Kim YunHong on 2020-02-18 / 오후 5:49
 * Email : fbsghd123@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceMaster;

import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DeviceMasterService {

	@Autowired
	DeviceMasterDao deviceMasterDao;
	@Autowired
	UserService userService;

	public List<DeviceMaster> getDeviceList(DeviceMasterParameter deviceMasterParameter) throws Exception{
		return  deviceMasterDao.selectList(deviceMasterParameter);
	}
	public DeviceMaster getDevice(int deviceMstSeq) throws Exception{
		return  deviceMasterDao.select(deviceMstSeq);
	}

	/**
	 * 장비마스터 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setDeviceMaster(DeviceMaster parameter) throws Exception {

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		parameter.setRegId(userId);
		parameter.setUptId(userId);

		ValidStringUtil.nullChk(parameter.getUseYn(), "사용여부");

		// 5. deviceMaster insert
		if(parameter.getDeviceMstSeq() == 0){
			ValidStringUtil.codeChk(parameter.getDeviceClassify(), "장비구분");
			ValidStringUtil.codeChk(parameter.getDeviceName(), "장비명");
			ValidStringUtil.lengthChk(parameter.getModelName(), "모델명", 1, 25);
			ValidStringUtil.codeChk(parameter.getManufactureComClassify(), "제조사구분");
			ValidStringUtil.codeChk(parameter.getManufactureCom(), "제조사");
			deviceMasterDao.insert(parameter);
		} else {
			deviceMasterDao.update(parameter);
		}

		return true;
	}


}
