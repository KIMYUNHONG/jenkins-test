/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceMaster;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DeviceMasterParameter {

	// 장비마스터 Seq
	private int deviceMstSeq;

	// 제조사_구분
	private String deviceClassify;

	// 장비명
	private String deviceName;

	// 기간 구분
	private String dateType;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;

	// 미사용포함 플래그 (Y/N)
	private String viewAllDeviceMasterFlag;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;
}
