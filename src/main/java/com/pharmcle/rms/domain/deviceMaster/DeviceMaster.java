/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceMaster;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class DeviceMaster extends BaseDomain {

	// 장비마스터 SEQ
	private int deviceMstSeq;
	// 장비구분
	private String deviceClassify;
	// 장비명
	private String deviceName;
	// 모델명
	private String modelName;
	// 제조사_구분
	private String manufactureComClassify;
	// 제조사
	private String manufactureCom;
	// 제조일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate manufactureDt;
	// 장비_타입
	private String deviceType;
	// 전원(정격)
	private String power;
	//모니터(상)
	private String monitorTop;
	//모니터(하)
	private String monitorBottom;
	// 규격
	private String standard;
	// 파장
	private String wavelength;
	// 램프수명
	private String lampLife;
	// 외관
	private String external;
	// 태양광 패널
	private String solarPanel;
	// 배터리
	private String battery;
	// 펌프
	private String pump;
	// 약재 량
	private String medicinalResidue;

	// ------------------------------------

	// 장비구분

	private String manufactureDtText;
	public String getManufactureDtText(){
		return DateUtil.localDateToString(manufactureDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private String deviceClassifyText;
	private String deviceNameText;
	private String manufactureComClassifyText;
	private String manufactureComText;

}
