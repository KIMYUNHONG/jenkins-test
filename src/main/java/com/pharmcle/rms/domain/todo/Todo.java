/**
 * Create By Lee DoWon on 2020-04-22 / 오후 4:47
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.todo;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Todo extends BaseDomain {

	private String domainStatus;
	private int domainSeq;
	private String viewUserId;



	public static final String DOMAIN_NAME_INSTALL_REQUEST_REQUEST = "IM050001";	// 설치요청 - 설치요청
	public static final String DOMAIN_NAME_INSTALL_REQUEST_RECEIPT = "IM050002";	// 설치요청 - 요청접수
	public static final String DOMAIN_NAME_INSTALL_REQUEST_INSTALL = "IM050004";	// 설치요청 - 설치완료
	public static final String DOMAIN_NAME_INSTALL_REQUEST_FINISH = "IM050005";	// 설치요청 - 확인완료


	public static final String DOMAIN_NAME_COMPLAIN_REQUEST = "MR030001";	// 민원 - 민원신청
	public static final String DOMAIN_NAME_COMPLAIN_RECEIPT = "MR030002";	// 민원 - 민원접수

	public static final String DOMAIN_NAME_CUSTOMER_JOIN = "SC080001";		// 거래처 가입

	// ---------------------------------------

	private String domainStatusText;        // 업무구분
	private String title;

	private String requestUserName;        // 요청자
	private String receiveUserId;        // 수신자 아이디
	private String receiveUserName;        // 수신자

	private String checkedYn;        // 처리여부 (Y/N)
	private String checkedYnText;    // 처리여부 텍스트
	public String getCheckedYnText() {
		return "Y".equals(checkedYn) ? "확인" : "미확인";
	}

	private String handleYn;        // 처리여부 (Y/N)
	private String handleYnText;    // 처리여부 텍스트

	public String getHandleYnText() {
		return "Y".equals(handleYn) ? "처리" : "미처리";
	}

	public Todo(String domainStatus, int domainSeq, String regId) {
		this.domainStatus = domainStatus;
		this.domainSeq = domainSeq;
		this.setRegId(regId);
	}
	public Todo(String receiveUserId) {
		this.receiveUserId = receiveUserId;
	}

	private String searchStartDate;
	private String searchEndDate;

	private int domainSeq2;	// (상세 페이지가기위해 키가 두개이상 필요한경우 사용)

}
