/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.todo;

import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

	@Autowired
	TodoDao todoDao;

	@Autowired
	UserService userService;


	/**
	 * TO-DO 리스트 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Todo> getTodoList(Todo parameter) throws Exception {
		parameter.setReceiveUserId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));
		return todoDao.selectList(parameter);
	}


	/**
	 * TO-DO 카운트 조회
	 *
	 * @param parameter
	 * @return
	 */
	public int getTodoCount(Todo parameter) throws Exception {
		return todoDao.selectListCount(parameter);
	}

	/**
	 * TO-DO 등록
	 *
	 * @return
	 */
	public boolean addTodo(String domainName, int domainSeq) throws Exception {
		return todoDao.insert(new Todo(domainName, domainSeq, userService.getUserIdBySessionOrToken(SessionUtil.getToken()))) > 0;
	}
	/**
	 * TO-DO 확인 유저 업데이트
	 *
	 * @return
	 */
	public boolean setViewUserId(String domainName, int domainSeq) throws Exception {
		return todoDao.updateViewUserId(new Todo(domainName, domainSeq, userService.getUserIdBySessionOrToken(SessionUtil.getToken()))) > 0;
	}

}
