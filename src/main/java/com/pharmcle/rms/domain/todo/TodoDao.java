/**
 * Create By Lee DoWon on 2020-04-22 / 오후 4:53
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.todo;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TodoDao {

	List<Todo> selectList(Todo todo);
	int selectListCount(Todo todo);

	int insert(Todo todo);
	int updateViewUserId(Todo todo);
}
