/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.installRequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class InstallRequestListMobile extends BaseDomain {

	//장비요청수량
	private int deviceQty;
	//장비 설치완료 건수
	private int installRequestCount;
	// 설치장소 키
	private int locationSeq;
	// 설치시간(시)
	private String installHour;
	// 설치시간(분)
	private String installMinute;
	// 설치장소 지역
	private String locationArea;
	// 설치장소 지역 TEXT
	private String locationAreaText;
	// 설치장소 명
	private String locationName;
	// 거래처 명
	private String customerName;
	// 거래처타입
	private String customerType;
	// 거래처타입 Text
	private String customerTypeText;
	// 설치자 이름
	private String installUserId;
	// 설치자 이름
	private String installUserName;
	// searchAllText(모바일)
	private String searchAllText;
	// 장비설치요청 시퀀스
	private int installRequestSeq;
	// 설치구분
	private String installRequestType;
	// 설치구분 TEXT
	private String installRequestTypeText;
	// 설치상태
	private String installRequestStatus;
	// 설치상태 TEXT
	private String installRequestStatusText;
	//D-DAY
	private int decimalDay;
	// 설치예정일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDueDate;
	private String installDueDateText;
	public String getInstallDueDateText(){
		return DateUtil.localDateToString(installDueDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 확인일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime finishDt;
	private String finishDtText;
	public String getFinishDtText(){
		return DateUtil.localDatetimeToString(finishDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	// 설치완료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installFinishDt;
	private String installFinishDtText;
	public String getInstallFinishDtText(){
		return DateUtil.localDateToString(installFinishDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

}
