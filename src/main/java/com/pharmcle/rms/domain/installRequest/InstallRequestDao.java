/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.installRequest;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface InstallRequestDao {

	//장비설치요청 리스트 조회
	List<InstallRequest> selectInstallRequestList(InstallRequestParameter parameter);
	//장비설치요청 리스트 조회(모바일)
	List<InstallRequestListMobile> selectMobileInstallRequestList(InstallRequestParameter parameter);

	//장비설치요청 리스트 조회 카운트
	int selectInstallRequestListCount(InstallRequestParameter parameter);
	//장비설치요청 상세조회
	InstallRequest selectInstallRequest(InstallRequestParameter parameter);
	//장비설치요청 상세조회(모바일)
	InstallRequestDetailMobile selectMobileInstallRequest(InstallRequestParameter parameter);

	//설치장비요청 등록
	int installRequestInsert(InstallRequest parameter);
	//설치장비요청 상세 수정
	int installRequestUpdate(InstallRequest parameter);
	//장비설치요정 요청접수 처리
	int updateReceiptInstallRequest(InstallRequest parameter);
	//장비설치요정 확인완료 처리
	int updateFinishInstallRequest(InstallRequest parameter);
	//장비설치요청 상태변경 처리(설치중,설치완료)
	int updateInstallRequestStatus(InstallRequest parameter);
	//장비설치요청 삭제처리
	int deleteInstallRequest(InstallRequest parameter);
	//장비설치요청 사용자 권한 체크
	int installRequestAuthroleChk(InstallRequestParameter parameter);

}
