/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.installRequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class InstallRequestParameter extends BaseParameter {

	// 사용자 권한
	public static final String USER_TYPE_INSTALL  = "SC020003"; // 보수담당
	public static final String USER_TYPE_SALES    = "SC020005"; // 영업담당
	public static final String USER_TYPE_CUSTOMER = "SC020004"; // 거래처담당

	// 설치요청 시퀀스
	private int installRequestSeq;
	// 장비 시퀀스
	private int deviceSeq;
	// 시작기간
	private String searchStartDate;
	// 종료기간
	private String searchEndDate;
	// 상태
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;
	// 설치구분
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> installRequestTypeList;
	//지역
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> locationAreaList;
	private String locationArea;
	public String getLocationArea(){
		if(locationAreaList == null || locationAreaList.size() == 0){
			return "";
		}
		return locationAreaList.get(0);
	}



	// 장비명
	private String deviceName;
	private String deviceNameText;
	// 기간유형
	private String dateType;

	// 설치장소명
	private String locationName;
	// 거래처 유형
	private String customerType;
	// 거래처 명
	private String customerName;
	// 권한 체크용 계정 아이디
	private String userId;
	// 담당자 유형
	private String userType;
	// 담당자 명
	private String userName;
	// 추가 , 수정 구분
	private String gbn;

	public InstallRequestParameter(int installRequestSeq) {
		this.installRequestSeq = installRequestSeq;
	}
	public InstallRequestParameter(int installRequestSeq,String gbn) {
		this.installRequestSeq = installRequestSeq;
		this.gbn = gbn;
	}
	public InstallRequestParameter(int installRequestSeq, int deviceSeq) {
		this.installRequestSeq = installRequestSeq;
		this.deviceSeq = deviceSeq;
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;


	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private String authUserId;	// 유저아이디
	private int authCustomerSeq;		// 거래처_키

}
