/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.installRequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class InstallRequest extends BaseDomain {

	public static final String STATUS_REQUEST = "IM050001"; // 설치요청
	public static final String STATUS_RECEIPT = "IM050002"; // 요청접수
	public static final String STATUS_INSTALLING = "IM050003"; // 설치중
	public static final String STATUS_COMPLETE = "IM050004"; // 설치완료
	public static final String STATUS_FINISH = "IM050005"; // 확인완료
	public static final String STATUS_INSTALL = "DM020002"; // 설치(장비상태 : DM02)
	public static final String STATUS_NOT_INSTALL = "DM020001"; // 미설치(장비상태 : DM02)

	// 장비 마스터 SEQ
	private int deviceMstSeq;
	// 장비 SEQ
	private int deviceSeq;
	// 장비명
	private String deviceName;
    private String deviceNameText;
	// 장비구분
	private String deviceClassify;
	private String deviceClassifyText;
	//장비 모델명
	private String modelName;
	private String modelNameText;
	//장비 제조사
	private String manufactureCom;
	private String manufactureComText;
	//장비 제조일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate manufactureDt;
	private String manufactureDtText;
	public String getManufactureDtText(){
		return DateUtil.localDateToString(manufactureDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	//장비상태
	private String status;
    private String statusText;
	//장비요청수량
	private int deviceQty;
	//장비 설치완료 건수
	private int installRequestCount;
	// 장비 설치료건수 / 장비요청수량 (3대 / 총 3대)

	//엑셀 다운로드용 설치대수 (0대/총2대)
	public String installQty;
	public String installQtyText;

	public String getInstallQtyText(){
		if(deviceQty == installRequestCount){
			return installRequestCount+"대";
		}
		return installRequestCount + "대 / 총" + deviceQty + "대";
	}

	// 설치장소 키
	private int locationSeq;
	// 위도
	private BigDecimal locationLatitude;
	// 경도
	private BigDecimal locationLongitude;
	// 설치일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;
    public String installDtText;
    public String getInstallDtText(){
        return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT);
    }
    public String installDtTimeText;
    public String getInstallDtTimeText() {
        if (StringUtil.isEmpty(getInstallDtText()) || StringUtil.isEmpty(installHour)) {
            return "";
        }
        return getInstallDtText() + " " + installHour + ":" + installMinute;
    }

	// 설치시간(시)
	private String installHour;
	// 설치시간(분)
	private String installMinute;
	// 기기주소
	private String deviceAddress;
	// 설치예정일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDueDate;
	private String installDueDateText;
	public String getInstallDueDateText(){
		return DateUtil.localDateToString(installDueDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 설치완료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installFinishDt;
	private String installFinishDtText;
	public String getInstallFinishDtText(){
		return DateUtil.localDateToString(installFinishDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	//설치예정일(설치완료일)
	private String installDueFinishDtText;
	public String getInstallDueFinishDtText() {
		String finishDt = installFinishDt != null ? " (" + DateUtil.localDateToString(installFinishDt, DateUtil.DEFAULT_DATE_FORMAT) + ")" : "";
		return DateUtil.localDateToString(installDueDate, DateUtil.DEFAULT_DATE_FORMAT) + finishDt;
	}

	// 설치장소 지역
	private String locationArea;
	// 설치장소 지역 TEXT
	private String locationAreaText;
	private int distance;
	// 설치장소 명
	private String locationName;
	// 거래처명
	private String customerName;
	// 거래처타입
	private String customerType;
	// 거래처타입 Text
	private String customerTypeText;
	// 영업담당자 아이디
	private String salesUserId;
	// 영업담당 이름 / 연락처
	private String salesUserName;
	// 영업담당 거래처 명
	private String salesCustomerName;
	// 설치자 아이디
	private String installUserId;
	// 설치자 소속 키
	private int installCustomerSeq;
	// 설치자 이름 / 연락처
	private String installUserName;
	// 설치자 거래처 명
	private String installCustomerName;

	// 설치일 한국어 포맷 - nnnn년 mm월 ll일
	public String installDtKoreanText;
	public String getInstallDtKoreanText() {
		return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT_KOREAN);
	};
	private String searchAllText;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> beforeUploadFileKey;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> afterUploadFileKey;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;
	private String thumbUploadFileKey;
	// 장비설치요청 시퀀스
	private int installRequestSeq;
	// 설치구분
	private String installRequestType;
	// 설치구분 TEXT
	private String installRequestTypeText;
	// 설치상태
	private String installRequestStatus;
	// 설치상태 TEXT
	private String installRequestStatusText;
	// 설치장소 주소
	private String locationAddress;
	// 비고
	private String etc;
	// 접수자 ID
	private String receiptUserId;
	// 접수자 명
	private String receiptUserName;
	// 접수자 거래처 시퀀스
	private int receiptCustomerSeq;
	// 접수자 거래처 명
	private String receiptCustomerName;
	// 접수일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime receiptDt;
	private String receiptDtText;
	public String getReceiptDtText(){
		return DateUtil.localDatetimeToString(receiptDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	// 요청자 거래처 시퀀스
	private int requestCustomerSeq;
	// 요청자 거래처명
	private String requestCustomerName;
	// 요청자 ID
	private String requestUserId;
	// 요청자 명
	private String requestUserName;
	// 요청자 거래처 유형
	private String requestCustomerType;
	// 요청자 거래처 유형 TEXT
	private String requestCustomerTypeText;
	// 요청일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime requestDt;
	private String requestDtText;
	public String getRequestDtText(){
		return DateUtil.localDatetimeToString(requestDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	//확인자 ID
	private String finishUserId;
	//확인자 명
	private String finishUserName;
	//확인자 거래처 시퀀스
	private int finishCustomerSeq;
	//확인자 거래처 명
	private String finishCustomerName;
	// 확인일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime finishDt;
	private String finishDtText;
	public String getFinishDtText(){
		return DateUtil.localDatetimeToString(finishDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}

	//D-DAY
	private int decimalDay;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Device> deviceList;

}
