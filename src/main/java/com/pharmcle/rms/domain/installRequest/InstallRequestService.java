/**
 * Create By Kim YunHong on 2020-02-18 / 오후 5:49
 * Email : fbsghd123@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.installRequest;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.DeviceDao;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InstallRequestService {

    @Autowired
    DeviceDao deviceDao;

    @Autowired
    InstallRequestDao installRequestDao;

    @Autowired
    UploadFileDao uploadFileDao;

    @Autowired
    UploadFileService uploadFileService;

    @Autowired
    UserService userService;

    @Autowired
    TodoDao todoDao;

    /**
     * 장비설치요청 리스트 조회
     */
    public List<InstallRequest> getInstallRequestList(InstallRequestParameter parameter) throws Exception {
        return installRequestDao.selectInstallRequestList(parameter);
    }

    /**
     * 장비설치요청 리스트 조회(모바일)
     */
    public List<InstallRequestListMobile> getMobileInstallRequestList(InstallRequestParameter parameter) throws Exception {
        return installRequestDao.selectMobileInstallRequestList(parameter);
    }

    // 장비설치요청 리스트 카운트
    public int getInstallRequestListCount(InstallRequestParameter parameter) throws Exception {
        return installRequestDao.selectInstallRequestListCount(parameter);
    }

    /**
     * 장비설치요청 상세조회
     */
    public InstallRequest getInstallRequestDetail(InstallRequestParameter parameter) throws Exception {
        return installRequestDao.selectInstallRequest(parameter);
    }

    /**
     * 장비설치요청 상세조회(모바일)
     */
    public InstallRequestDetailMobile getMobileInstallRequestDetail(InstallRequestParameter parameter) throws Exception {
        return installRequestDao.selectMobileInstallRequest(parameter);
    }

    /**
     * 장비설치요청 상세조회
     * 수정이 아니라 신규로 설치장비 추가일경우 설치대수 체크 및 설치요청 DATA SELECT RETURN
     */
    public InstallRequest getInstallRequest(InstallRequestParameter parameter) throws Exception {

        InstallRequest installRequest = installRequestDao.selectInstallRequest(parameter);

        String gbn = parameter.getGbn();

        if (!installRequest.getInstallRequestStatus().equals(InstallRequest.STATUS_RECEIPT) &&
                !installRequest.getInstallRequestStatus().equals(InstallRequest.STATUS_INSTALLING)) {
            throw new ValidException("설치장비 등록은 요청접수 , 설치중 상태일때만 등록 가능합니다.");
        }

        //설치요청한 장비 수량
        int deviceQty = installRequest.getDeviceQty();
        //설치한 장비 수량
        int installDeviceQty = installRequest.getInstallRequestCount();

        // 추가버튼으로 접근할때만 설치대수 비교 벨리데이션
        if ("add".equals(gbn)) {
            if (deviceQty == installDeviceQty) {
                throw new ValidException("설치대수를 모두 등록하여 더이상 추가 할 수 없습니다.");
            }
        }

        return installRequest;
    }

    /**
     * 장비설치요청 등록
     */
    @Transactional(rollbackFor = Exception.class)
    public int setInstallRequest(InstallRequest parameter) throws Exception {

        boolean isNewInstallRequest = parameter.getInstallRequestSeq() == 0;

        int installRequestSeq = parameter.getInstallRequestSeq();

        User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

        // 신규 등록인경우
        if (isNewInstallRequest) {

            ValidStringUtil.lengthChk(parameter.getInstallRequestType(), "설치구분", 1, 8);
            ValidNumberUtil.zeroChk(parameter.getLocationSeq(), "설치장소");
            ValidDateUtil.nullChk(parameter.getInstallDueDate(), "설치예정일");
            ValidNumberUtil.zeroChk(parameter.getDeviceQty(), "설치대수");
            ValidStringUtil.lengthChk(parameter.getDeviceName(), "장비명", 1, 8);
            ValidStringUtil.nullChk(parameter.getInstallUserId(), "설치담당");

            parameter.setRegId(user.getUserId());
            parameter.setRequestUserId(user.getUserId());
            parameter.setRequestCustomerSeq(user.getCustomerSeq());

            //신규 등록시 설치요청 상태로 insert
            parameter.setInstallRequestStatus("IM050001");

            // 신규등록시에만 체크 하는 부분
            boolean result = installRequestDao.installRequestInsert(parameter) > 0;
            if (!result) {    // 등록이 안된경우 에러 처리
                throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
            }
            installRequestSeq = parameter.getInstallRequestSeq();

            result = todoDao.insert(new Todo(Todo.DOMAIN_NAME_INSTALL_REQUEST_REQUEST, installRequestSeq, user.getUserId())) > 0;
            if (!result) {    // 등록이 안된경우 에러 처리
                throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
            }
            // 설치요청 수정인경우
        } else {

            // 장비설치요청 수정 사용자 권한 확인 (거래처담당)
            if (installRequestAuthroleChk(parameter.getInstallRequestSeq(), user.getUserId(), InstallRequestParameter.USER_TYPE_CUSTOMER)) {
                throw new ValidException("권한이 없습니다.");
            }

            ValidStringUtil.lengthChk(parameter.getInstallRequestType(), "설치구분", 1, 8);
            ValidDateUtil.nullChk(parameter.getInstallDueDate(), "설치예정일");
            ValidNumberUtil.zeroChk(parameter.getDeviceQty(), "설치대수");
            ValidStringUtil.lengthChk(parameter.getDeviceName(), "장비명", 1, 8);
            ValidStringUtil.nullChk(parameter.getInstallUserId(), "설치담당");

            parameter.setUptId(user.getUserId());
            boolean result = installRequestDao.installRequestUpdate(parameter) > 0;
            if (!result) {
                throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
            }

        }
        return installRequestSeq;
    }

    /**
     * 장비설치요청 요청접수
     *
     * @param parameter
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean setReceiptInstallRequest(InstallRequest parameter) throws Exception {
        User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
        String userId = user.getUserId();

        // 요청접수 사용자 권한 확인 (보수담당)
        if (installRequestAuthroleChk(parameter.getInstallRequestSeq(), userId, InstallRequestParameter.USER_TYPE_INSTALL)) {
            throw new ValidException("권한이 없습니다.");
        }

        //접수자 거래처 시퀀스
        int receiptCustomerSeq = user.getCustomerSeq();

        parameter.setUptId(userId);
        parameter.setReceiptCustomerSeq(receiptCustomerSeq);
        InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter((parameter.getInstallRequestSeq())));

        if (!InstallRequest.STATUS_REQUEST.equals(installRequest.getInstallRequestStatus())) {
            throw new ValidException("설치요청 접수는 설치요청 단계에서만 가능합니다.");
        }

        parameter.setInstallRequestStatus(InstallRequest.STATUS_RECEIPT);
        installRequestDao.updateReceiptInstallRequest(parameter);
        todoDao.insert(new Todo(Todo.DOMAIN_NAME_INSTALL_REQUEST_RECEIPT, parameter.getInstallRequestSeq(), userId));

        return true;
    }

    /**
     * 장비설치요청 설치완료 처리(설치담당)
     *
     * @param parameter
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean setCompleteInstallRequest(InstallRequest parameter) throws Exception {
        User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

        String userId = user.getUserId();

        // 설치완료 사용자 권한 확인 (보수담당)
        if (installRequestAuthroleChk(parameter.getInstallRequestSeq(), userId, InstallRequestParameter.USER_TYPE_INSTALL)) {
            throw new ValidException("권한이 없습니다.");
        }

        parameter.setUptId(userId);
        InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter((parameter.getInstallRequestSeq())));

        if (!InstallRequest.STATUS_INSTALLING.equals(installRequest.getInstallRequestStatus())) {
            throw new ValidException("설치완료 처리는 설치중 단계에서만 가능합니다.");
        }

        //설치요청한 장비 수량
        int deviceQty = installRequest.getDeviceQty();
        //설치한 장비 수량
        int installDeviceQty = installRequest.getInstallRequestCount();
        if (deviceQty != installDeviceQty) {
            throw new ValidException("설치대수가 부족합니다. 장비를 모두 설치해 주세요.");
        }

        parameter.setInstallRequestStatus(InstallRequest.STATUS_COMPLETE);
        //장비설치요청 상태 '설치완료'로 변경
        installRequestDao.updateInstallRequestStatus(parameter);
        todoDao.insert(new Todo(Todo.DOMAIN_NAME_INSTALL_REQUEST_INSTALL, parameter.getInstallRequestSeq(), userId));
        return true;
    }

    /**
     * 장비설치요청 확인완료 처리(영업담당)
     *
     * @param parameter
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean setFinishInstallRequest(InstallRequest parameter) throws Exception {
        User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

        // 확인완료 사용자 권한 확인 (거래처 담당)
        if (installRequestAuthroleChk(parameter.getInstallRequestSeq(), user.getUserId(), InstallRequestParameter.USER_TYPE_CUSTOMER)) {
            throw new ValidException("권한이 없습니다.");
        }

        //영업담당 거래처 시퀀스
        int finishCustomerSeq = user.getCustomerSeq();

        parameter.setFinishUserId(user.getUserId());
        parameter.setFinishCustomerSeq(finishCustomerSeq);
        parameter.setUptId(user.getUserId());
        parameter.setStatus(InstallRequest.STATUS_INSTALL);

        InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter((parameter.getInstallRequestSeq())));

        if (!InstallRequest.STATUS_COMPLETE.equals(installRequest.getInstallRequestStatus())) {
            throw new ValidException("확인완료 처리는 설치완료 단계에서만 가능합니다.");
        }

        parameter.setInstallRequestStatus(InstallRequest.STATUS_FINISH);
        //장비설치요청 상태 '확인완료'로 변경
        installRequestDao.updateFinishInstallRequest(parameter);
        //DEVICE 테이블 상태 '설치' 로 변경
        deviceDao.updateDeviceStatusFinish(parameter);
        todoDao.insert(new Todo(Todo.DOMAIN_NAME_INSTALL_REQUEST_FINISH, parameter.getInstallRequestSeq(), user.getUserId()));

        return true;
    }

    /**
     * 장비설치요청 - 장비설치요청 삭제처리
     *
     * @param parameter
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteInstallRequest(InstallRequest parameter) throws Exception {
        User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
        String userId = user.getUserId();

        // 장비설치요청 삭제 사용자 권한 확인 (거래처담당)
        if (installRequestAuthroleChk(parameter.getInstallRequestSeq(), userId, InstallRequestParameter.USER_TYPE_CUSTOMER)) {
            throw new ValidException("권한이 없습니다.");
        }

        parameter.setUptId(userId);
        InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter((parameter.getInstallRequestSeq())));

        if (!InstallRequest.STATUS_REQUEST.equals(installRequest.getInstallRequestStatus())) {
            throw new ValidException("삭제는 설치요청 단계에서만 가능합니다.");
        }
        installRequestDao.deleteInstallRequest(parameter);
        return true;
    }

    /**
     * 장비설치요청 사용자 권한 체크
     **/
    public boolean installRequestAuthroleChk(int installRequestSeq, String userId, String userType) throws Exception {

        if(StringUtil.isEmpty(userId) || StringUtil.isEmpty(userType) || installRequestSeq <= 0){
            throw new ValidException("권한 확인중 문제가 발생하였습니다. 관리자에게 문의해 주세요.");
        }

        InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter(installRequestSeq));

        if("SC020003".equals(userType)){
            //보수담당
            return installRequest.getInstallUserId() == userId;
        }else if("SC020004".equals(userType)){
            //거래처 담당
            return installRequest.getRegId() == userId;
        }else if("SC020005".equals(userType)){
            //영업담당
            return installRequest.getSalesUserId() == userId;
        }else{
            return false;
        }

    }


}
