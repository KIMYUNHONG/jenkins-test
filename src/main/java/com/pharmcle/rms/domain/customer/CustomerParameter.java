/**
 * Create By Lee DoWon on 2020-02-25 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 거래처
 */
@NoArgsConstructor
@Getter
@Setter
public class CustomerParameter extends BaseParameter {

	private int customerSeq;

	// 시작기간
	private String customerType;

	// 시작기간
	private String customerName;

	// 영업담당 이름
	private String salesUserName;

	// 담당부서명
	private String representName;

	// 담당자명
	private String chargeName;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;

	// 검색기간구분
	private String dateType;

	// 계약상태
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> contractStatusList;

	private String searchContractStatus;

	public String getSearchContractStatus(){
		if(contractStatusList == null ){
			return "ALL";
		} else if(contractStatusList.size() > 1){
			return "ALL";
		}else {
			return contractStatusList.get(0);
		}
	}

	private String viewAllCustomerFlag;

	// 일반거래처와 대러짐을 구분하기위해 사용
	// 만약 해당 필드에 값이 null이 아니면 대리점만 나옴, 반대로 null인경우 대리점을 제외
	private String isAgencyMenu;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Integer> excludeCustomerSeq;

}
