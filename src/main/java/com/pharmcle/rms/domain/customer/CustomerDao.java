/**
 * Create By Lee DoWon on 2020-02-25 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.customer;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CustomerDao {

	List<Customer> selectList(CustomerParameter parameter);
	Customer select(int customerSeq);
	int update(Customer parameter);
	int insert(Customer parameter);
	int updateMyCustomer(Customer parameter);

}
