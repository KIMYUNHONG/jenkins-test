/**
 * Create By Lee DoWon on 2020-02-25 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

/**
 * 거래처
 */
@NoArgsConstructor
@Getter
@Setter
public class Customer extends BaseDomain {

	public static final String CUSTOMER_TYPE_AGANCY = "CM010001";
	public static final String CUSTOMER_TYPE_PHARMCLE = "CM010005";

	private int customerSeq;
	private String customerType;
	private String customerName;
	private String customerShortName;
	private String businessNumber;
	private String representName;
	private String customerPhone;
	private String emailId;
	private String emailDomain;
	private String address;
	private String addressDetail;
	private int salesCustomerSeq;
	private String salesUserId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate contractStartDt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate contractEndDt;

	//--------------------------
	// 아래서부터는 조회용 필드

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;

	private String customerTypeText;

	// 담당자 이름, 직책, 연락처 그리드 표기용
	private String chargeName;
	private String chargeRankText;
	private String chargePhone;
	private String contractStatusText;
	public String getContractStatusText(){
		return DateUtil.isLocalDateBetween(LocalDate.now(), contractStartDt, contractEndDt) ? "유지" : "종료";
	}

	// 영업담당 이름, 소속명
	private String salesUserName;
	private String salesCustomerName;

	// 설치장소, 장비 카운트
	private int locationCnt;
	private int deviceCnt;

	private User[] chargeList;

	private String contractStartDtText;
	public String getContractStartDtText() {
		return DateUtil.localDateToString(contractStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private String contractPeriodText;
	public String getContractPeriodText() {
		return getContractStartDtText() + " ~ " + getContractEndDtText();
	}

	private String contractEndDtText;
	public String getContractEndDtText() {
		return DateUtil.localDateToString(contractEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private String emailText;
	public String getEmailText() {
		if(StringUtil.isEmpty(emailId) || StringUtil.isEmpty(emailDomain)){
			return "";
		}
		return emailId + "@" + emailDomain;
	}

	@JsonIgnore
	private String addressText;
	public String getAddressText() {
		if(StringUtil.isEmpty(address) && StringUtil.isEmpty(addressDetail)){
			return "";
		}
		return address + " " + addressDetail;
	}

	@JsonIgnore
	private String phoneSplitText;
	public String[] getPhoneSplitText() {
		if (customerPhone == null || !(customerPhone.split("-").length == 3)) {
			return null;
		}
		return customerPhone.split("-");
	}

	private String phone1;
	private String phone2;
	private String phone3;
	
	@JsonIgnore
	private String concatPhoneText;
	public String getConcatPhoneText(){
		if(StringUtil.isEmpty(phone1) && StringUtil.isEmpty(phone2) && StringUtil.isEmpty(phone3)){
			return "";
		}
		return phone1 + "-" + phone2 + "-" + phone3;
	};

	public String number1;
	public String number2;
	public String number3;

	@JsonIgnore
	private String concatBusinessNumberText;
	public String getConcatBusinessNumberText(){
		if(StringUtil.isEmpty(number1) || StringUtil.isEmpty(number2) || StringUtil.isEmpty(number3)){
			return "";
		}
		return number1 + "-" + number2 + "-" + number3;
	};

	@JsonIgnore
	private String businessNumberSplitText;
	public String[] getBusinessNumberSplitText() {
		if (businessNumber == null || !(businessNumber.split("-").length == 3)) {
			return null;
		}
		return businessNumber.split("-");
	}

	private String existFileYn;

	// 영업담당자 연락처
	private String salesUserPhone;

	private int confirmNoCnt;
}
