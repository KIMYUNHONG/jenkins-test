/**
 * Create By Lee DoWon on 2020-02-25 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.customer;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.*;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CustomerService {

	@Autowired
	CustomerDao customerDao;

	@Autowired
	UserDao userDao;

	@Autowired
	UploadFileDao uploadFileDao;

	@Autowired
	UserLoginService userLoginService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;

	public List<Customer> getCustomerList(CustomerParameter parameter) throws Exception {
		List<Customer> list = customerDao.selectList(parameter);
		List<Customer> reList = new ArrayList<Customer>();

		for (Customer customer : list) {
			String chargePhone = CryptoUtil.getAESDecrypt(customer.getChargePhone());
			customer.setChargePhone(chargePhone);
			
			reList.add(customer);
		}

		return reList;
	}

	public Customer getCustomer(int customerSeq) throws Exception {

		Customer customer = customerDao.select(customerSeq);

		if(customer != null){
			if(StringUtil.isNotEmpty(customer.getChargePhone())){
				String chargePhone = CryptoUtil.getAESDecrypt(customer.getChargePhone());
				customer.setChargePhone(chargePhone);
			}
			if(StringUtil.isNotEmpty(customer.getSalesUserPhone())){
				String salesUserPhone = CryptoUtil.getAESDecrypt(customer.getSalesUserPhone());
				customer.setSalesUserPhone(salesUserPhone);
			}
		}

		return customer;
	}

	public List<User> getChargeList(int customerSeq) throws Exception {
		if (customerSeq <= 0) {
			throw new Exception("");
		}
		UserParameter userParameter = new UserParameter();
		userParameter.setCustomerSeq(customerSeq);
		
		List<User> list = userDao.selectChargeList(userParameter);
		List<User> reList = new ArrayList<User>();
		
		// 암호화 - 복호화
		for (User user : list) {
			String phone = CryptoUtil.getAESDecrypt(user.getPhone());
			String telephone = CryptoUtil.getAESDecrypt(user.getTelephone());
			
			user.setPhone(phone);
			user.setTelephone(telephone);
			
			reList.add(user);
		}
		
		return reList;
	}

	@Transactional(rollbackFor = Exception.class)
	public int setCustomer(Customer customer) throws Exception {

		boolean isNewCustomer = customer.getCustomerSeq() == 0;int customerSeq = customer.getCustomerSeq();

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		if("CM010001".equals(customer.getCustomerType())){
			ValidStringUtil.lengthChk(customer.getCustomerShortName(), "대리점(약어)", 1, 25);
		}else{
			ValidStringUtil.lengthChk(customer.getCustomerShortName(), "거래처(약어)", 1, 25);
		}

		ValidDateUtil.nullChk(customer.getContractStartDt(), "계약기간(시작일)");
		ValidDateUtil.nullChk(customer.getContractEndDt(), "계약기간(종료일)");

		if(StringUtil.isNotEmpty(customer.getConcatBusinessNumberText())) {
			ValidStringUtil.bisNumberFormatChk(customer.getConcatBusinessNumberText(), "사업자번호");
		}

		if (StringUtil.isNotEmpty(customer.getConcatPhoneText())) {
			ValidStringUtil.phoneNumberFormatChk(customer.getConcatPhoneText(), "대표 전화번호");
		}
		if (StringUtil.isNotEmpty(customer.getEmailId()) || StringUtil.isNotEmpty(customer.getEmailDomain())) {
			ValidStringUtil.emailIdChk(customer.getEmailId(), "이메일 아이디");
			ValidStringUtil.userIdChk(customer.getEmailDomain().replaceAll("\\.", ""), "이메일 도메인");
		}
		if (StringUtil.isNotEmpty(customer.getAddress()) || StringUtil.isNotEmpty(customer.getAddressDetail())) {
			ValidStringUtil.nullChk(customer.getAddress(), "주소");
			ValidStringUtil.nullChk(customer.getAddressDetail(), "주소");
		}
		ValidStringUtil.nullChk(customer.getSalesUserId(), "영업담당");
		ValidNumberUtil.zeroChk(customer.getSalesCustomerSeq(), "영업담당");

		// 거래처 신규 등록인경우 - 거래처, 담당자 전부 신규등록
		if (isNewCustomer) {
			customer.setRegId(userId);

			// 신규등록시에만 체크 하는 부분
			ValidStringUtil.codeChk(customer.getCustomerType(), "거래처구분");

			if("CM010001".equals(customer.getCustomerType())){
				ValidStringUtil.lengthChk(customer.getCustomerName(), "대리점(정식명)", 1, 25);
			}else{
				ValidStringUtil.lengthChk(customer.getCustomerName(), "거래처(정식명)", 1, 25);
			}

			ValidStringUtil.lengthChk(customer.getRepresentName(), "담당부서", 1, 10);

			boolean result = customerDao.insert(customer) > 0;
			if (!result) {	// 등록이 안된경우 에러 처리
				throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
			}
			customerSeq = customer.getCustomerSeq();

			// 파일업로드
			if (customer.getUploadFileKey() != null) {
				for (String key : customer.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.CUSTOMER, customerSeq);
					uploadFileDao.update(uploadFile);
				}
			}

			// 거래처 담당자 등록
			if (customer.getChargeList() != null) {
				for (User charge : customer.getChargeList()) {
					charge.setCustomerSeq(customerSeq);
					charge.setRegId(userId);
					charge.setConfirmYn("Y");

					// 아이디 저장하는경우에만 패스워드 설정

					validationCharge(charge);
					setChargeUserId(charge);

					// 암호화 
					String phone = CryptoUtil.getAESEncrypt(charge.getPhone());
					String telephone = CryptoUtil.getAESEncrypt(charge.getTelephone());
					
					charge.setPhone(phone);
					charge.setTelephone(telephone);
					
					result = userDao.insert(charge) > 0;
					if (!result) {
						throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					}
				}
			}

			// 거래처 수정인경우
		} else {
			customer.setUptId(userId);
			boolean result = customerDao.update(customer) > 0;
			if (!result) {
				throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
			}

			// 거래처 담당자 등록
			if (customer.getChargeList() != null) {
				for (User charge : customer.getChargeList()) {
					charge.setCustomerSeq(customerSeq);
					charge.setUptId(userId);
					charge.setConfirmYn("Y");
					validationCharge(charge);

					// 암호화 
					String phone = CryptoUtil.getAESEncrypt(charge.getPhone());
					String telephone = CryptoUtil.getAESEncrypt(charge.getTelephone());
					
					charge.setPhone(phone);
					charge.setTelephone(telephone);
					
					if (charge.getUserSeq() == 0) {
						setChargeUserId(charge);
						result = userDao.insert(charge) > 0;
					} else {
						if(StringUtil.isNotEmpty(charge.getUserId()) && StringUtil.isEmpty(userDao.select(charge.getUserSeq()).getUserId())){
							setChargeUserId(charge);
						}
						result = userDao.updateCharge(charge) > 0;
					}
					if (!result) {
						throw new ValidException("데이터를 불러오는중 문제가 발생했습니다.\n 관리자에게 문의해주세요.");
					}
				}
			}

			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.CUSTOMER, customerSeq);
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}

			// 남은 파일 또는 신규등록된 파일들 매핑
			if (customer.getUploadFileKey() != null) {
				for (String key : customer.getUploadFileKey()) {
					UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.CUSTOMER, customerSeq);
					uploadFileDao.update(uploadFile);
				}
			}
		}

		int representCount = userDao.selectRepresentUserCount(customerSeq);
		if(representCount > 1){
			throw new ValidException("대표담당은 2명 이상 등록할 수 없습니다.");
		}

		return customerSeq;
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean setMyCustomer(Customer customer) throws Exception {

		ValidStringUtil.nullChk(customer.getRepresentName(), "담당부서");

		ValidDateUtil.nullChk(customer.getContractStartDt(), "계약기간(시작일)");
		ValidDateUtil.nullChk(customer.getContractEndDt(), "계약기간(종료일)");

		if(StringUtil.isNotEmpty(customer.getConcatBusinessNumberText())) {
			ValidStringUtil.bisNumberFormatChk(customer.getConcatBusinessNumberText(), "사업자번호");
		}
		if (StringUtil.isNotEmpty(customer.getConcatPhoneText())) {
			ValidStringUtil.phoneNumberFormatChk(customer.getConcatPhoneText(), "대표 전화번호");
		}
		if (StringUtil.isNotEmpty(customer.getEmailId()) || StringUtil.isNotEmpty(customer.getEmailDomain())) {
			ValidStringUtil.emailIdChk(customer.getEmailId(), "이메일 아이디");
			ValidStringUtil.nullChk(customer.getEmailDomain(),"이메일 도메인");
			ValidStringUtil.userIdChk(customer.getEmailDomain().replaceAll("\\.", ""), "이메일 도메인");
		}
		if (StringUtil.isNotEmpty(customer.getAddress()) || StringUtil.isNotEmpty(customer.getAddressDetail())) {
			ValidStringUtil.nullChk(customer.getAddress(), "주소");
			ValidStringUtil.nullChk(customer.getAddressDetail(), "주소");
		}

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		customer.setUptId(userId);

		return customerDao.updateMyCustomer(customer) > 0;
	}

	private void setChargeUserId(User charge) throws Exception{

		if(StringUtil.isNotEmpty(charge.getUserId()) && userDao.selectByUserId(new UserParameter(charge.getUserId())) != null ){
			throw new ValidException("이미 등록된 아이디입니다.");
		}

		if(StringUtil.isNotEmpty(charge.getUserId())){
			ValidStringUtil.userIdChk(charge.getUserId(), "담당자 아이디");
			charge.setPassword(userLoginService.encodingPassword("phc@12345"));
			userDao.updateChargeUserId(new User(charge.getUserId(), charge.getPassword(), ""));
		}
	}

	private void validationCharge(User charge) throws Exception{

		charge.setAuthRole("SC020004");
		ValidStringUtil.nullChk(charge.getUseYn(), "담당자 활동여부");
		ValidStringUtil.nullChk(charge.getRepresentYnText(), "담당자 대표여부");
		ValidStringUtil.lengthChk(charge.getName(), "담당자 성명", 2, 10);
		if (StringUtil.isNotEmpty(charge.getPhone())) {
			ValidStringUtil.phoneNumberFormatChk(charge.getPhone(), "담당자 휴대폰번호");
		}
		if (StringUtil.isNotEmpty(charge.getTelephone())) {
			ValidStringUtil.phoneNumberFormatChk(charge.getTelephone(), "담당자 전화번호");
		}
		if(StringUtil.isNotEmpty(charge.getEmailId()) && StringUtil.isNotEmpty(charge.getEmailDomain())){
			ValidStringUtil.userIdChk(charge.getEmailId(), "담당자 이메일 아이디");
			ValidStringUtil.userIdChk(charge.getEmailDomain().replaceAll("\\.", ""), "담당자 이메일 도메인");
		}
	}

}
