/**
 * Create By Lee DoWon on 2020-04-13 / 오후 5:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.dashboard;

import com.pharmcle.rms.domain.device.DashboardDeviceModel;
import com.pharmcle.rms.domain.device.DeviceDao;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DashboardService {

	@Autowired
	DashboardDao dashboardDao;

	@Autowired
	DeviceDao deviceDao;

	@Autowired
	UserService userService;

	public List<Dashboard> getAreaCnt(Dashboard parameter) throws Exception {
		return dashboardDao.selectAreaCnt(parameter);
	}
	public List<Dashboard> getDeviceStatusCnt(Dashboard parameter) throws Exception {
		return dashboardDao.selectDeviceStatusCnt(parameter);
	}
	public List<Dashboard> getUserList(Dashboard parameter) throws Exception {
		return dashboardDao.selectUserList(parameter);
	}
	public List<Dashboard> getCustomerCnt(Dashboard parameter) throws Exception {
		return dashboardDao.selectCustomerCnt(parameter);
	}
	public Dashboard getTotalCnt(Dashboard parameter) throws Exception {
		return dashboardDao.selectTotalCnt(parameter);
	}
	public List<DashboardDeviceModel> getDashboardDeviceList(SelectInstallDeviceParameter parameter) throws Exception {
		return deviceDao.selectDashboardDeviceList(parameter);
	}
	public int getDashboardDeviceListCnt(SelectInstallDeviceParameter parameter) throws Exception {
		return deviceDao.selectDashboardDeviceListCnt(parameter);
	}

	public List<Area> getDeviceCntByArea(AreaParameter parameter) throws Exception {
		return dashboardDao.selectDeviceCntByArea(parameter);
	}

}
