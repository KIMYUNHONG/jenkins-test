/**
 * Create By Lee DoWon on 2020-04-13 / 오후 5:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.dashboard;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class Area {

	private String area;		// 지역코드
	private String areaText;	// 지역명
	private int cnt;			// 카운트
	private BigDecimal lat;		// 위도
	private BigDecimal lng;		// 경도
}
