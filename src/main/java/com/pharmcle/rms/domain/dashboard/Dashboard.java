/**
 * Create By Lee DoWon on 2020-04-13 / 오후 5:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.dashboard;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Dashboard {

	private String column;
	private String month;
	private int year;
	private int cnt;

	// 장비 설치상태별 카운트
	private int dm020001;	// 미설치
	private int dm020002;	// 설치
	private int dm020003;	// 탈거
	private int dm020004;	// 재설치
	private int dm020005;	// 폐기
	private int dm020006;	// 분실

	// 지역별 관리현황
	private String area;		// 구분 코드(지역코드)
	private String areaText;	// 구분 코드명(지역명)
	private int customerCnt;	// 거래처수
	private int maintainCnt;	// 유지보수
	private int complainTotalCnt;	// 민원신청
	private int complainHandleCnt;	// 민원처리

	// 거래처별 현황
	private int customerSeq;			// 거래처 키
	private String customerType;		// 거래처타입
	private String customerTypeText;	// 거래처타입명
	private String customerName;	// 거래처명

	private String name;	// 담당자 명
	private String authRoleText;	// 담당
	private int locationCnt;	// 설치장소수
	private int requestCnt;		// 설치계획수
	private int installCnt;		// 설치수
	private int mr060001Cnt;	// 정기점검
	private int mr060002Cnt;	// 야간점검
	private int complainCnt;	// 민원처리

	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키
}
