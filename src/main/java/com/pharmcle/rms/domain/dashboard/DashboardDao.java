/**
 * Create By Lee DoWon on 2020-04-13 / 오후 5:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.dashboard;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DashboardDao {

	List<Dashboard> selectAreaCnt(Dashboard parameter);
	List<Dashboard> selectDeviceStatusCnt(Dashboard parameter);
	List<Dashboard> selectUserList(Dashboard parameter);
	List<Dashboard> selectCustomerCnt(Dashboard parameter);
	Dashboard selectTotalCnt(Dashboard parameter);

	List<Area> selectDeviceCntByArea(AreaParameter parameter);
}
