/**
 * Create By Lee DoWon on 2020-04-13 / 오후 5:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.dashboard;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class AreaParameter {

	private int depth;
	private int zoomLevel;

	private BigDecimal neLat;
	private BigDecimal neLng;

	private BigDecimal swLat;
	private BigDecimal swLng;

	private String authCustomerType;
	private int authCustomerSeq;

	private int deviceMstSeq;
}
