/**
 * Create By Lee DoWon on 2020-04-09 / 오전 11:33
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DeviceStatus extends BaseDomain {

	public static final String DM020001_NON_INSTALL = "DM020001";	// 미설치
	public static final String DM020002_INSTALL = "DM020002"; 	// 설치
	public static final String DM020003_REMOVE = "DM020003";	// 탈거
	public static final String DM020004_RE_INSTALL = "DM020004";	// 재설치
	public static final String DM020005_DISCARD = "DM020005";	// 폐기
	public static final String DM020006_LOSS = "DM020006";	// 분실

	public static final String IM070001_DIRECT = "IM070001";	// 즉시폐기
	public static final String IM070002_STORAGE = "IM070002";	// 보관 후 폐기
	public static final String IM070003_CUSTOMER = "IM070003";	// 거래처 전달 후 폐기

	private int deviceSeq;			// 장비 시퀀스
	private String status;			// 장비 상태
	private int deviceStatusSeq;	// 장비 상태 시퀀스
	private String storage;			// 보관장소
	private String discardType;		// 폐기분류
	private String etc;				// 비고


	// -------------------------------------------------
	// 등록 및 조회용 필드

	private String statusText;
	private String discardTypeText;
	private String storageText;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> beforeUploadFileKey;		// 설치전 사진
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> afterUploadFileKey;		// 설치후 사진
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> photoUploadFileKey;		// 사진

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> proofUploadFileKey;		// 증빙자료
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<UploadFile> proofUploadFileList;	// 증빙자료
	public DeviceStatus(int deviceSeq){
		this.deviceSeq = deviceSeq;
	}

	// 가동 시작일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;
	private String operateStartDtText;
	public String getOperateStartDtText(){
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 가동 종료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;
	private String operateEndDtText;
	public String getOperateEndDtText(){
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 가동 시작 시간 (hhmm)
	private String operateStartTime;
	// 가동 종료 시간 (hhmm)
	private String operateEndTime;

	private String operateTimeText;
	public String getOperateTimeText() {
		if (StringUtil.isEmpty(operateStartTime) || StringUtil.isEmpty(operateEndTime)) {
			return "";
		}

		return operateStartTime.substring(0,2) + ":" + operateStartTime.substring(2,4) + " ~ "  + operateEndTime.substring(0,2) + ":" + operateEndTime.substring(2,4);
	}



}
