/**
 * Create By Lee DoWon on 2020-04-09 / 오후 1:31
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceStatus;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceDao;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.monitor.MonitorLoRa;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.protocol.Protocol;
import com.pharmcle.rms.domain.protocol.ProtocolParameter;
import com.pharmcle.rms.domain.protocol.ProtocolService;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DeviceStatusService {

    @Autowired
    DeviceStatusDao deviceStatusDao;

    @Autowired
    DeviceDao deviceDao;

    @Autowired
    UploadFileDao uploadFileDao;

    @Autowired
    DeviceService deviceService;

    @Autowired
    UserService userService;

    @Autowired
    ProtocolService protocolService;

    @Autowired
    MonitorService monitorService;

    public List<DeviceStatus> getDeviceStatusList(DeviceStatus parameter) throws Exception {
        return deviceStatusDao.selectList(parameter);
    }

    public DeviceStatus getDeviceStatus(DeviceStatus parameter) {
        return deviceStatusDao.select(parameter);
    }

    public boolean setDeviceStatus(DeviceStatus parameter) throws Exception {

        // DEVICE_PROTOCOL_LOG 저장을 위한 객체생성
        Protocol protocol;
        ProtocolParameter protocolParameter = new ProtocolParameter();
        // LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
        MonitorLoRa loraParameter = new MonitorLoRa();
        // boardSerial LIST
        List<String> devEUIList = new ArrayList<>();

        ValidNumberUtil.zeroChk(parameter.getDeviceSeq(), "변경할 장비");
        ValidStringUtil.nullChk(parameter.getStatus(), "장비상태");

        // 폐기시 보관후 폐기인경우에만 보관장소 체크
        if (DeviceStatus.DM020005_DISCARD.equals(parameter.getStatus()) && DeviceStatus.IM070002_STORAGE.equals(parameter.getDiscardType())) {
            ValidStringUtil.nullChk(parameter.getStorage(), "보관장소");
        }

        Device device = deviceService.getDevice(new DeviceParameter(parameter.getDeviceSeq()));
        String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

        // 미설치, 폐기, 분실 상태일때는 상태변경 불가능
        if (DeviceStatus.DM020001_NON_INSTALL.equals(device.getStatus()) || DeviceStatus.DM020005_DISCARD.equals(device.getStatus()) || DeviceStatus.DM020006_LOSS.equals(device.getStatus())) {
            throw new ValidException(device.getStatusText() + "상태의 장비는 상태 변경이 불가능합니다.");
        }

        // 탈거, 폐기, 분실인경우 사진키 필수!
        if (DeviceStatus.DM020003_REMOVE.equals(parameter.getStatus()) || DeviceStatus.DM020005_DISCARD.equals(parameter.getStatus()) || DeviceStatus.DM020006_LOSS.equals(parameter.getStatus())) {
            if (!(parameter.getPhotoUploadFileKey() != null && parameter.getPhotoUploadFileKey().size() > 0)) {
                throw new ValidException("사진을 등록해주세요.");
            }
        }
        // 재설치인경우 설치전, 설치후 사진 키 필수!
        if (DeviceStatus.DM020004_RE_INSTALL.equals(parameter.getStatus())) {
            if (!(parameter.getBeforeUploadFileKey() != null && parameter.getBeforeUploadFileKey().size() > 0)) {
                throw new ValidException("설치전 사진을 등록해주세요.");
            }
            if (!(parameter.getAfterUploadFileKey() != null && parameter.getAfterUploadFileKey().size() > 0)) {
                throw new ValidException("설치후 사진을 등록해주세요.");
            }
        }

        parameter.setRegId(userId);
        boolean result = deviceStatusDao.insert(parameter) > 0;

        // 설치전 사진
        if (parameter.getBeforeUploadFileKey() != null && parameter.getBeforeUploadFileKey().size() > 0) {

            if (parameter.getBeforeUploadFileKey().size() > 3) {
                throw new Exception("설치전 사진은 최대 3개까지 등록 가능합니다.");
            }
            // 남은 파일 또는 신규등록된 파일들 매핑
            for (String key : parameter.getBeforeUploadFileKey()) {
                UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.DEVICE_STATUS_BEFORE, parameter.getDeviceSeq(), parameter.getDeviceStatusSeq());
                uploadFileDao.update(uploadFile);
            }
        }

        // 설치후 사진
        if (parameter.getAfterUploadFileKey() != null && parameter.getAfterUploadFileKey().size() > 0) {

            if (parameter.getAfterUploadFileKey().size() > 3) {
                throw new Exception("설치후 사진은 최대 3개까지 등록 가능합니다.");
            }
            // 남은 파일 또는 신규등록된 파일들 매핑
            for (String key : parameter.getAfterUploadFileKey()) {
                UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.DEVICE_STATUS_AFTER, parameter.getDeviceSeq(), parameter.getDeviceStatusSeq());
                uploadFileDao.update(uploadFile);
            }
        }

        // 사진
        if (parameter.getPhotoUploadFileKey() != null && parameter.getPhotoUploadFileKey().size() > 0) {

            // 남은 파일 또는 신규등록된 파일들 매핑
            for (String key : parameter.getPhotoUploadFileKey()) {
                UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.DEVICE_STATUS_PHOTO, parameter.getDeviceSeq(), parameter.getDeviceStatusSeq());
                uploadFileDao.update(uploadFile);
            }
        }

        // 증빙 자료
        if (parameter.getProofUploadFileKey() != null && parameter.getProofUploadFileKey().size() > 0) {

            // 남은 파일 또는 신규등록된 파일들 매핑
            for (String key : parameter.getProofUploadFileKey()) {
                UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.DEVICE_STATUS_PROOF, parameter.getDeviceSeq(), parameter.getDeviceStatusSeq());
                uploadFileDao.update(uploadFile);
            }
        }

        // 장비 (DEVICE) 테이블 상태 UPDATE
        if (result) {
            device.setStatus(parameter.getStatus());
            device.setUptId(userId);
            result = deviceDao.updateDeviceStatus(device) == 1;
            log.info("update result :  " + result);

            if (!result) {
                throw new Exception();
            }
            // 재설치인경우 DEVICE 가동기간, 가동시간 변경.
            if (DeviceStatus.DM020004_RE_INSTALL.equals(parameter.getStatus())) {


                String test = parameter.getOperateStartTime();

                ValidStringUtil.lengthChk(parameter.getOperateStartTime(), "점등시간", 4, 4);
                ValidStringUtil.lengthChk(parameter.getOperateEndTime(), "소등시간", 4, 4);
                ValidDateUtil.nullChk(parameter.getOperateStartDt(), "가동기간(시작일)");
                ValidDateUtil.nullChk(parameter.getOperateEndDt(), "가동기간(종료일)");

                // DEVICE TABLE의 가동기간,가동시간 UPDATE를 위한 DATA SET
                device.setDeviceSeq(parameter.getDeviceSeq());
                device.setOperateEndDt(parameter.getOperateEndDt());
                device.setOperateStartDt(parameter.getOperateStartDt());
                device.setOperateEndTime(parameter.getOperateEndTime());
                device.setOperateStartTime(parameter.getOperateStartTime());

                // DEVICE TABLE 장비 가동기간 , 가동시간 수정
                result = deviceService.updateDeviceOperateInfo(device);

                if (!result) {
                    throw new Exception();
                }

                // 장비정보 조회
                Device deviceInfo = deviceDao.select(new DeviceParameter(device.getDeviceSeq()));

                // 기준정보 select를 위한 protocol parameter set (baseYear , DeviceClassify)
                LocalDate localDate = LocalDate.now();
                protocolParameter.setDeviceClassify(deviceInfo.getDeviceClassify());
                protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

                // 기준정보 select
                protocol = protocolService.getProtocol(protocolParameter);

                if (protocol == null) {
                    throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
                }

                // DEVICE_PROTOCOL_LOG 등록을 위한 DATA SET
                protocol.setDeviceSeq(deviceInfo.getDeviceSeq());
                protocol.setOperateStartDt(device.getOperateStartDt());
                protocol.setOperateEndDt(device.getOperateEndDt());
                protocol.setOperateStartTime(device.getOperateStartTime());
                protocol.setOperateEndTime(device.getOperateEndTime());

                // DEVICE_PROTOCOL_LOG 등록
                protocolService.setDeviceProtocolLog(protocol);

                // DEVICE에 SERIAL_NO가 있으면 LORA장비에 가동기간/가동시간 셋팅
                String boardSerial = deviceInfo.getBoardSerial();
                if (StringUtil.isNotEmpty(boardSerial)) {

                    // LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
                    devEUIList.add(boardSerial);
                    loraParameter.setOperateStartDt(DateUtil.localDateToString(deviceInfo.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
                    loraParameter.setOperateStartTime(deviceInfo.getOperateStartTime());
                    loraParameter.setOperateEndDt(DateUtil.localDateToString(deviceInfo.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
                    loraParameter.setOperateEndTime(deviceInfo.getOperateEndTime());
                    loraParameter.setDevEUIList(devEUIList);

                    // LORA장비 가동기간 , 가동시간 UPDATE
                    int stat = monitorService.setLoraSchedule(loraParameter);

                    if (stat <= 0) {
                        throw new Exception();
                    }

                }

            }

        } else {
            throw new Exception();
        }

        return result;
    }

    public boolean deleteDeviceStatus(DeviceStatus parameter) throws Exception {

        /**
         * 1.DeviceStatus 삭제
         * **/
        List<DeviceStatus> deviceStatusList = deviceStatusDao.selectList(new DeviceStatus(parameter.getDeviceSeq()));

        if(deviceStatusList != null && deviceStatusList.size() > 0){
	        for(DeviceStatus item : deviceStatusList){
		        boolean result = deviceStatusDao.deleteDeviceStatus(item) > 0;

		        if(!result){
			        throw new ValidException("장비상태 삭제에 실패했습니다. 관리자에게 문의해 주세요.");
		        }
	        }
        }

        return true;
    }
}
