/**
 * Create By Lee DoWon on 2020-04-09 / 오전 11:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.deviceStatus;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DeviceStatusDao {

	List<DeviceStatus> selectList(DeviceStatus parameter);
	DeviceStatus select(DeviceStatus parameter);

	int insert(DeviceStatus parameter);
	int deleteDeviceStatus(DeviceStatus parameter);

}
