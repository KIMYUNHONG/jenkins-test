/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class SelectInstallDeviceParameter extends BaseParameter {

	private int locationSeq;

	private String deviceId;

	private String deviceName;

	@DateTimeFormat(pattern = "yyyy.MM.dd")
	private LocalDate installDt;

	private String asUserId;

	private String asCustomerSeq;

	// 검색조건
	private String dateType;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;


	// 상태List
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;

	private String manageNo;

	private String lampNo;

	private String communicationMode;


	// 지역
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> locationAreaList;
	private String locationArea;
	public String getLocationArea(){
		if(locationArea != null){
			return locationArea;
		}
		if(locationAreaList == null || locationAreaList.size() == 0){
			return "";
		}
		return locationAreaList.get(0);
	}

	// 설치장소
	private String locationName;

	// 담당자 유형
	private String userType;

	// 담당자 명
	private String userName;

	// 거래처 명
	private String customerName;

	public SelectInstallDeviceParameter(int locationSeq){
		this.locationSeq = locationSeq;
	}

	// 거래처 명
	private String customerType;


	private String deviceNameText;
	private String searchAllText;
	private BigDecimal latitude;
	private BigDecimal longitude;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> deviceSeqList;
	private int deviceMstSeq;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	private int customerSeq;

	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키


	// 대시보드 지도 조회용 필드
	private int depth;
	private BigDecimal neLat;
	private BigDecimal neLng;
	private BigDecimal swLat;
	private BigDecimal swLng;

	private String connectYn;

	// 메일발송용 필드
	private int deviceSeq;



}


