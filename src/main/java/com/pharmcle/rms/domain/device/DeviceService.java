/**
 * Create By Kim YunHong on 2020-02-18 / 오후 5:49
 * Email : fbsghd123@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartDao;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterDao;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatus;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatusService;
import com.pharmcle.rms.domain.installRequest.InstallRequest;
import com.pharmcle.rms.domain.installRequest.InstallRequestDao;
import com.pharmcle.rms.domain.installRequest.InstallRequestParameter;
import com.pharmcle.rms.domain.installRequest.InstallRequestService;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.modem.Modem;
import com.pharmcle.rms.domain.modem.ModemDao;
import com.pharmcle.rms.domain.monitor.MonitorLoRa;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.protocol.Protocol;
import com.pharmcle.rms.domain.protocol.ProtocolDao;
import com.pharmcle.rms.domain.protocol.ProtocolParameter;
import com.pharmcle.rms.domain.protocol.ProtocolService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DeviceService {

	@Autowired
	DeviceDao deviceDao;
	@Autowired
	DeviceMasterDao deviceMasterDao;
	@Autowired
	UploadFileDao uploadFileDao;
	@Autowired
	ModemDao modemDao;
	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	UserService userService;
	@Autowired
	InstallRequestDao installRequestDao;
	@Autowired
	InstallRequestService installRequestService;
	@Autowired
	ProtocolService protocolService;
	@Autowired
	ProtocolDao protocolDao;
	@Autowired
	MonitorService monitorService;
	@Autowired
	AddpartDao addpartDao;
	@Autowired
	ComplainService complainService;
	@Autowired
	MaintainService maintainService;
	@Autowired
	DeviceStatusService deviceStatusService;

	//	장비 리스트 조회
	public List<DeviceListModel> getDeviceList(DeviceParameter deviceParameter) throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			deviceParameter.setCustomerSeq(user.getCustomerSeq());
		}
		return  deviceDao.selectList(deviceParameter);
	}
	//	장비 리스트 조회(단건)
	public Device getDevice(DeviceParameter parameter) throws Exception{

		Device device = deviceDao.select(parameter);

		if(device != null){
			// 암호화 - 복호화
			String chargePhone = device.getChargePhone();
			String salesUserPhone = device.getSalesUserPhone();
			String asUserPhone = device.getAsUserPhone();
			String asUserTelephone = device.getAsUserTelephone();
			String installUserPhone = device.getInstallUserPhone();

			device.setChargePhone(CryptoUtil.getAESDecrypt(chargePhone));
			device.setSalesUserPhone(CryptoUtil.getAESDecrypt(salesUserPhone));
			device.setAsUserPhone(CryptoUtil.getAESDecrypt(asUserPhone));
			device.setAsUserTelephone(CryptoUtil.getAESDecrypt(asUserTelephone));
			device.setInstallUserPhone(CryptoUtil.getAESDecrypt(installUserPhone));
		}

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user) && device.getCustomerSeq() != user.getCustomerSeq()){
			throw new ValidException("권한이 없습니다.");
		}
		// 설치가 되었던 장비면 설치 사진 조회
		if(device != null && StringUtil.isNotEmpty(device.getInstallDtText())) {
			device.setBeforeUploadFileKey(uploadFileService.getUploadFileKeyListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq()));
			device.setAfterUploadFileKey(uploadFileService.getUploadFileKeyListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq()));
		}
		return device;
	}
	//	장비 리스트 조회(상태별)
	public Map<String, Object> getDeviceStatusData(DeviceParameter deviceParameter) throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			deviceParameter.setCustomerSeq(user.getCustomerSeq());
		}
		return  deviceDao.selectDeviceStatusData(deviceParameter);
	}

	// 설치된 장비 리스트 조회
	public List<Device> getInstallDeviceList(SelectInstallDeviceParameter deviceParameter) throws Exception{
		List<Device> list = deviceDao.selectInstallList(deviceParameter);
		List<Device> reList = new ArrayList<Device>();
		
		// 암호화 - 복호화
		for (Device device : list) {
			String chargePhone = CryptoUtil.getAESDecrypt(device.getChargePhone());
			String asUserPhone = CryptoUtil.getAESDecrypt(device.getAsUserPhone());
			String asUserTelephone = CryptoUtil.getAESDecrypt(device.getAsUserTelephone());

			device.setChargePhone(chargePhone);
			device.setAsUserPhone(asUserPhone);
			device.setAsUserTelephone(asUserTelephone);
			
			reList.add(device);
		}
		
		return  reList;
	}
	// 설치된 장비 리스트 카운트 조회
	public int getInstallDeviceListCount(SelectInstallDeviceParameter deviceParameter) throws Exception{

		return  deviceDao.selectInstallListCount(deviceParameter);
	}

	/**
	 * 장비 등록
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public String setDevice(Device parameter) throws Exception {

		/**
		 * 신규장비 INSERT시
		 * PROTOCOL의 최신DATA 조회하여 DEVICE의 가동기간, 가동시간 SET하고 (SELECT 기준 : DeviceClassify , BaseYear(현재년도) , REG_DT 가장 최근)
		 * DEVICE_PROTOCOL , DEVICE_PROTOCOL_LOG 에 INSERT한다.
		 *
		 * 신규장비 INSERT 후 boardSerial 입력 및 수정시 LoRa 장비 통신(가동기간 , 가동시간 SET)
		 * */

        String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
        Protocol protocol;
        ProtocolParameter protocolParameter = new ProtocolParameter();

        parameter.setRegId(userId);
        parameter.setUptId(userId);

        if (parameter.getDeviceQty() > 1000) {
            throw new ValidException("장비 수량은 1000개 이상 등록할 수 없습니다.");
        }

        // 5. device insert
        if (parameter.getDeviceSeq() == 0) {

            ValidNumberUtil.zeroChk(parameter.getDeviceMstSeq(), "장비");
            ValidNumberUtil.rangeChk(parameter.getDeviceQty(), "장비수량", 1, 1000);

            long deviceIdNum = 0;

            // 기준정보 select를 위한 protocol parameter set (baseYear , DevicClassify)
			LocalDate localDate = LocalDate.now();
            protocolParameter.setDeviceClassify(parameter.getDeviceClassify());
            protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

            // PROTOCOL SELECT
            protocol = protocolService.getProtocol(protocolParameter);

            if (protocol == null) {
                throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
            }

            // DEVICE INSERT 가동기간,가동시간 SET
            parameter.setOperateStartTime(protocol.getOperateStartTime());
            parameter.setOperateEndTime(protocol.getOperateEndTime());
            parameter.setOperateStartDt(protocol.getOperateStartDt());
            parameter.setOperateEndDt(protocol.getOperateEndDt());

            // DEVICE_PROTOCOL INSERT DATA SET
            protocol.setRegId(userId);

            // 최초 deviceID 채번
            String deviceId = deviceDao.selectDeviceId(parameter);
            deviceIdNum = Long.parseLong(deviceId.substring(7, 17));
            String deviceIdHeader = deviceId.substring(0, 7);
            int deviceQty = parameter.getDeviceQty();

            // deviceQty 만큼 for돌리면서 deviceID +1
            for (int i = 0; i < deviceQty; i++) {
                parameter.setDeviceId(deviceIdHeader + deviceIdNum);
                deviceDao.insert(parameter);                     //장비등록
                protocol.setDeviceSeq(parameter.getDeviceSeq()); // 장비등록후 deviceSeq PROTOCOL에 SET
                parameter.setDeviceSeq(0);                       // deviceSeq 초기화
                protocolService.setDeviceProtocol(protocol);     // DEVICE_PROTOCOL 등록

                // DEVICE_PROTOCOL_LOG 등록
				protocolService.setDeviceProtocolLog(protocol);

                deviceIdNum++;
            }

        } else {

        	// 수정시

			// LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
			MonitorLoRa loraParameter = new MonitorLoRa();
			// boardSerial LIST
			List<String> devEUIList = new ArrayList<>();

            Device existDevice = deviceDao.select(new DeviceParameter(parameter.getDeviceSeq()));

            // 가끔 null이 아닌 ""가 들어가서 처리
//			parameter.setModemId(StringUtil.nvl(parameter.getModemId(), null));
            String modemId = parameter.getModemId();
            String existModemId = existDevice.getModemId();
            // 기존에 모뎀ID가 등록된경우
            if (StringUtil.isNotEmpty(existModemId)) {
                // 기존에 등록된 모뎀 ID가 바뀐경우 매핑 해제
                if (!existModemId.equals(modemId)) {
                    modemDao.update(new Modem(existModemId, null));
                }
            }
            if (StringUtil.isNotEmpty(parameter.getModemId())) {
                modemDao.update(new Modem(parameter.getModemId(), existDevice.getDeviceId()));
            }

			boolean result = deviceDao.update(parameter) > 0;

            if(!result){
            	throw new Exception();
			}

            String boardSerial =  parameter.getBoardSerial();
            String existBoardSerial = existDevice.getBoardSerial();

            if(StringUtil.isNotEmpty(boardSerial)){

				// BoardSerial 수정시 LoRa장비 통신(가동기간 , 가동시간 세팅)
            	if(!boardSerial.equals(existBoardSerial)){

					// 기준정보 select를 위한 protocol parameter set (baseYear , DeviceClassify)
					LocalDate localDate = LocalDate.now();
					protocolParameter.setDeviceClassify(existDevice.getDeviceClassify());
					protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

					// 기준정보 select
					protocol = protocolService.getProtocol(protocolParameter);

					if (protocol == null) {
						throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
					}

					// DEVICE TABLE의 가동기간,가동시간 UPDATE를 위한 DATA SET
					parameter.setOperateEndDt(protocol.getOperateEndDt());
					parameter.setOperateStartDt(protocol.getOperateStartDt());
					parameter.setOperateEndTime(protocol.getOperateEndTime());
					parameter.setOperateStartTime(protocol.getOperateStartTime());

					// DEVICE TABLE 장비 가동기간 , 가동시간 수정
					result =  deviceDao.updateDeviceOperateInfo(parameter) > 0;

					if(!result){
						throw new Exception();
					}

					// LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
					devEUIList.add(boardSerial);
					loraParameter.setOperateStartDt(DateUtil.localDateToString(protocol.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
					loraParameter.setOperateStartTime(protocol.getOperateStartTime());
					loraParameter.setOperateEndDt(DateUtil.localDateToString(protocol.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
					loraParameter.setOperateEndTime(protocol.getOperateEndTime());
					loraParameter.setDevEUIList(devEUIList);

					// LORA장비 가동기간 , 가동시간 UPDATE
					int stat = monitorService.setLoraSchedule(loraParameter);

					if(stat <= 0 ){
						return "    저장되었습니다.\r ※ 장비와의 통신이 실패했습니다. \r    장비전원 확인 후 다시 가동기간을 설정해주세요.";
					}

				}

			}

        }

        return "success";

}

	//	장비 삭제(단건)
	public boolean deleteDevice(Device device) throws Exception{

		// 모뎀 ID 있는경우 체크하여 연동 취소
		Device existDevice = deviceDao.select(new DeviceParameter(device.getDeviceSeq()));
		String existModemId = existDevice.getModemId();
		if(StringUtil.isNotEmpty(existModemId)){
			modemDao.update(new Modem(existModemId, null));
		}

		device.setUptId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));

		deviceDao.delete(device);
		return  true;
	}

	/**
	 * 설치장비 등록
	 * @param device
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean installDevice(InstallDeviceParameter device) throws Exception{

		// DEVICE_PROTOCOL_LOG 저장을 위한 객체생성
		Protocol protocol;
		ProtocolParameter protocolParameter = new ProtocolParameter();
		// LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
		MonitorLoRa loraParameter = new MonitorLoRa();
		// boardSerial LIST
		List<String> devEUIList = new ArrayList<>();

		ValidStringUtil.nullChk(device.getDeviceId(), "장비번호");

		ValidDateUtil.nullChk(device.getInstallDt(), "설치일");
		ValidStringUtil.lengthChk(device.getInstallHour(), "설치시간", 2, 2);
		ValidStringUtil.lengthChk(device.getInstallMinute(), "설치시간", 2, 2);
		ValidNumberUtil.zeroChk(device.getLocationSeq(), "설치장소");

		ValidNumberUtil.nullChk(device.getLatitude(), "장비위치(위도)");
		ValidNumberUtil.zeroChk(device.getLongitude(), "장비위치(경도)");
		ValidStringUtil.lengthChk(device.getDeviceAddress(), "장비위치", 1, 100);
		ValidNumberUtil.zeroChk(device.getDeviceSeq(), "장비시퀀스");

		ValidStringUtil.lengthChk(device.getOperateStartTime(), "점등시간", 4, 4);
		ValidStringUtil.lengthChk(device.getOperateEndTime(), "소등시간", 4, 4);
		ValidDateUtil.nullChk(device.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(device.getOperateEndDt(), "가동기간(종료일)");

		if (StringUtil.isNotEmpty(device.getInstallEtc())) {
			ValidStringUtil.lengthChk(device.getInstallEtc(), "비고", 0, 1000);
		}
		if (StringUtil.isNotEmpty(device.getManageNo())) {
			ValidStringUtil.lengthChk(device.getManageNo(), "관리번호", 0, 20);
		}
		if (StringUtil.isNotEmpty(device.getLampNo())) {
			ValidStringUtil.lengthChk(device.getLampNo(), "가로등번호", 0, 20);
		}
		if (StringUtil.isNotEmpty(device.getAddPart())) {
			ValidStringUtil.codeChk(device.getAddPart(), "추가부품");
		}

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq());
		if (uploadFileList != null) {
			for (UploadFile item : uploadFileList) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList2 = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq());
		if (uploadFileList2 != null) {
			for (UploadFile item : uploadFileList2) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (device.getBeforeUploadFileKey() != null && device.getBeforeUploadFileKey().size() > 0) {
			for(String key : device.getBeforeUploadFileKey()){
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq());
				uploadFileDao.update(uploadFile);
			}
		}

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (device.getAfterUploadFileKey() != null && device.getAfterUploadFileKey().size() > 0) {
			for(String key : device.getAfterUploadFileKey()){
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq());
				uploadFileDao.update(uploadFile);
			}
		}

		//추가부품 삭제
		addpartDao.delete(new Addpart(UploadFileService.INSTALL, device.getDeviceSeq(), 0, Addpart.INSTALL_ADDPART_IM03));

		// 추가부품 등록
		if(device.getAddpartList() != null && device.getAddpartList().size() > 0) {
			for (Addpart item : device.getAddpartList()) {
				if (StringUtil.isEmpty(item.getPartType()) || item.getPartQty() == 0) {
					throw new Exception();
				}
				item.setDomainName(UploadFileService.INSTALL);
				item.setDomainSeq(device.getDeviceSeq());
				addpartDao.insert(item);
			}
		}

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		device.setInstallUserId(user.getUserId());
		device.setInstallCustomerSeq(user.getCustomerSeq());

		boolean result = false;

		result = deviceDao.installDevice(device) > 0;

		if(!result){
			throw new Exception();
		}

		Device deviceInfo = deviceDao.select(new DeviceParameter(device.getDeviceSeq()));

		// 기준정보 select를 위한 protocol parameter set (baseYear , DeviceClassify)
		LocalDate localDate = LocalDate.now();
		protocolParameter.setDeviceClassify(deviceInfo.getDeviceClassify());
		protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

		// 기준정보 select
		protocol = protocolService.getProtocol(protocolParameter);

		if (protocol == null) {
			throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
		}

		// DEVICE_PROTOCOL_LOG 등록을 위한 DATA SET
		protocol.setDeviceSeq(deviceInfo.getDeviceSeq());
		protocol.setOperateStartDt(device.getOperateStartDt());
		protocol.setOperateEndDt(device.getOperateEndDt());
		protocol.setOperateStartTime(device.getOperateStartTime());
		protocol.setOperateEndTime(device.getOperateEndTime());

		// DEVICE_PROTOCOL_LOG 등록
		protocolService.setDeviceProtocolLog(protocol);

		// DEVICE에 SERIAL_NO가 있으면 LORA장비에 가동기간/가동시간 셋팅
		String boardSerial =  deviceInfo.getBoardSerial();
		if(StringUtil.isNotEmpty(boardSerial)){

			// LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
			devEUIList.add(boardSerial);
			loraParameter.setOperateStartDt(DateUtil.localDateToString(device.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
			loraParameter.setOperateStartTime(device.getOperateStartTime());
			loraParameter.setOperateEndDt(DateUtil.localDateToString(device.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
			loraParameter.setOperateEndTime(device.getOperateEndTime());
			loraParameter.setDevEUIList(devEUIList);

			// LORA장비 가동기간 , 가동시간 UPDATE
			int stat = monitorService.setLoraSchedule(loraParameter);

			if(stat <= 0 ){
				//  TODO : [김윤홍] 장비와의 통신 실패시 처리를 어떻게 할지 결정해야함
//				throw new Exception();
			}

		}

		return true;

	}
	/**
	 * 장비 설치정보 수정(가동기간 , 가동시간 등)
	 * @param device
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean modifyInstallDevice(InstallDeviceParameter device) throws Exception{

        // DEVICE_PROTOCOL_LOG 저장을 위한 객체생성
        Protocol protocol;
        ProtocolParameter protocolParameter = new ProtocolParameter();
        // LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
        MonitorLoRa loraParameter = new MonitorLoRa();
        // boardSerial LIST
        List<String> devEUIList = new ArrayList<>();

	    ValidNumberUtil.zeroChk(device.getDeviceSeq(), "장비번호");
		ValidDateUtil.nullChk(device.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(device.getOperateEndDt(), "가동기간(종료일)");
		ValidStringUtil.lengthChk(device.getOperateStartTime(), "점등시간", 4, 4);
		ValidStringUtil.lengthChk(device.getOperateEndTime(), "소등시간", 4, 4);

		if (StringUtil.isNotEmpty(device.getManageNo())) {
			ValidStringUtil.lengthChk(device.getManageNo(), "관리번호", 0, 20);
		}
		if (StringUtil.isNotEmpty(device.getLampNo())) {
			ValidStringUtil.lengthChk(device.getLampNo(), "가로등번호", 0, 20);
		}

		device.setInstallUserId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));

		// 장비정보 조회
        Device deviceInfo = deviceDao.select(new DeviceParameter(device.getDeviceSeq()));

        // 기준정보 select를 위한 protocol parameter set (baseYear , DeviceClassify)
		LocalDate localDate = LocalDate.now();
		protocolParameter.setDeviceClassify(deviceInfo.getDeviceClassify());
        protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

        // 기준정보 select
        protocol = protocolService.getProtocol(protocolParameter);

        if (protocol == null) {
            throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
        }

        // DEVICE_PROTOCOL_LOG 등록을 위한 DATA SET
        protocol.setDeviceSeq(deviceInfo.getDeviceSeq());
        protocol.setOperateStartDt(device.getOperateStartDt());
        protocol.setOperateEndDt(device.getOperateEndDt());
        protocol.setOperateStartTime(device.getOperateStartTime());
        protocol.setOperateEndTime(device.getOperateEndTime());

        // DEVICE_PROTOCOL_LOG 등록
        protocolService.setDeviceProtocolLog(protocol);

        // DEVICE에 SERIAL_NO가 있으면 LORA장비에 가동기간/가동시간 셋팅
        String boardSerial =  deviceInfo.getBoardSerial();
        if(StringUtil.isNotEmpty(boardSerial)){

            // LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
            devEUIList.add(boardSerial);
            loraParameter.setOperateStartDt(DateUtil.localDateToString(deviceInfo.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
            loraParameter.setOperateStartTime(deviceInfo.getOperateStartTime());
            loraParameter.setOperateEndDt(DateUtil.localDateToString(deviceInfo.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
            loraParameter.setOperateEndTime(deviceInfo.getOperateEndTime());
            loraParameter.setDevEUIList(devEUIList);

            // LORA장비 가동기간 , 가동시간 UPDATE
            int stat = monitorService.setLoraSchedule(loraParameter);

            if(stat <= 0 ){
				//  TODO : [김윤홍] 장비와의 통신 실패시 처리를 어떻게 할지 결정해야함
//                throw new Exception();
            }

        }

		return deviceDao.modifyInstallDevice(device) > 0;

	}

	/**
	 * 장비설치요청 상세조회(설치장비 리스트)
	 */
	public List<Device> getInstallRequestDeviceList(InstallRequestParameter parameter) throws Exception{
		List<Device> deviceList = deviceDao.selectInstallRequestDeviceList(parameter);
		return  deviceList;
	}

	/**
	 * 장비설치요청 상세조회(설치장비 상세)
	 */
	public Device getInstallRequestDevice(InstallRequestParameter parameter) throws Exception{
		Device device = deviceDao.getInstallRequestDevice(parameter);
		return  device;
	}

	/**
	 * 장비설치요청 설치장비 등록
	 * @param device
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean installRequestDevice(Device device) throws Exception{

		User user  = userService.getUserBySessionOrToken(SessionUtil.getToken());

		// DEVICE_PROTOCOL_LOG 저장을 위한 객체생성
		Protocol protocol;
		ProtocolParameter protocolParameter = new ProtocolParameter();
		// LORA 장비의 가동기간,가동시간 SET을 위한 객체생성
		MonitorLoRa loraParameter = new MonitorLoRa();
		// boardSerial LIST
		List<String> devEUIList = new ArrayList<>();
		// 추가장비 LIST
		List<Addpart> addpartList= device.getAddpartList();

		//설치장비 공통 벨리데이션
		ValidNumberUtil.zeroChk(device.getDeviceSeq(), "장비번호");
		ValidStringUtil.lengthChk(device.getDeviceAddress(), "장비위치", 1, 100);
		ValidNumberUtil.nullChk(device.getLatitude(), "장비위치(위도)");
		ValidNumberUtil.nullChk(device.getLongitude(), "장비위치(경도)");
		ValidStringUtil.lengthChk(device.getOperateStartTime(), "점등시간", 4, 4);
		ValidStringUtil.lengthChk(device.getOperateEndTime(), "소등시간", 4, 4);
		ValidDateUtil.nullChk(device.getOperateStartDt(), "가동기간(시작일)");
		ValidDateUtil.nullChk(device.getOperateEndDt(), "가동기간(종료일)");
		if (StringUtil.isNotEmpty(device.getInstallEtc())) {
			ValidStringUtil.lengthChk(device.getInstallEtc(), "비고", 0, 1000);
		}
		if (StringUtil.isNotEmpty(device.getManageNo())) {
			ValidStringUtil.lengthChk(device.getManageNo(), "관리번호", 0, 20);
		}
		if (StringUtil.isNotEmpty(device.getLampNo())) {
			ValidStringUtil.lengthChk(device.getLampNo(), "가로등번호", 0, 20);
		}
		if (StringUtil.isNotEmpty(device.getAddPart())) {
			ValidStringUtil.codeChk(device.getAddPart(), "추가부품");
		}

		// 장비정보
		Device deviceInfo = deviceDao.select(new DeviceParameter(device.getDeviceSeq()));

		int installRequestSeq = deviceInfo.getInstallRequestSeq();

		//설치장비 등록일 경우 벨리데이션
		if(installRequestSeq == 0 ){
			ValidDateUtil.nullChk(device.getInstallDt(), "설치일");
			ValidStringUtil.lengthChk(device.getInstallHour(), "설치시간", 2, 2);
			ValidStringUtil.lengthChk(device.getInstallMinute(), "설치시간", 2, 2);
			ValidNumberUtil.zeroChk(device.getLocationSeq(), "설치장소");
		}

		// 설치장비등록 사용자 권한 확인 (보수담당)
		if (installRequestService.installRequestAuthroleChk(device.getInstallRequestSeq(), user.getUserId(), InstallRequestParameter.USER_TYPE_INSTALL)) {
			throw new ValidException("권한이 없습니다.");
		}

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq());
		if (uploadFileList != null) {
			for (UploadFile item : uploadFileList) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList2 = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq());
		if (uploadFileList2 != null) {
			for (UploadFile item : uploadFileList2) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		//추가부품 삭제
		addpartDao.delete(new Addpart(UploadFileService.INSTALL, device.getDeviceSeq(), 0, Addpart.INSTALL_ADDPART_IM03));

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (device.getBeforeUploadFileKey() != null && device.getBeforeUploadFileKey().size() > 0) {
			for(String key : device.getBeforeUploadFileKey()){
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq());
				uploadFileDao.update(uploadFile);
			}
		}

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (device.getAfterUploadFileKey() != null && device.getAfterUploadFileKey().size() > 0) {
			for(String key : device.getAfterUploadFileKey()){
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq());
				uploadFileDao.update(uploadFile);
			}
		}

		device.setInstallUserId(user.getUserId());
		device.setInstallCustomerSeq(user.getCustomerSeq());
		device.setUptId(user.getUserId());

		// 장비설치요청 설치장비 등록(UPDATE)
		boolean result = deviceDao.installRequestDevice(device) > 0;

		// 추가부품 등록
		if(device.getAddpartList() != null && device.getAddpartList().size() > 0) {
			for (Addpart item : device.getAddpartList()) {
				if (StringUtil.isEmpty(item.getPartType()) || item.getPartQty() == 0) {
					throw new Exception();
				}
				item.setDomainName(UploadFileService.INSTALL);
				item.setDomainSeq(device.getDeviceSeq());
				addpartDao.insert(item);
			}
		}

		if(!result){
			throw new Exception();
		}

		InstallRequest installRequest = new InstallRequest();
		installRequest.setInstallRequestSeq(device.getInstallRequestSeq());
		installRequest.setInstallRequestStatus(device.getInstallRequestStatus());

		// 장비성치요청 상태변경
		installRequestDao.updateInstallRequestStatus(installRequest);

		// PROTOCOL_LOG TABLE INSERT
		// 기준정보 select를 위한 protocol parameter set (baseYear , DeviceClassify)
		LocalDate localDate = LocalDate.now();
		protocolParameter.setDeviceClassify(deviceInfo.getDeviceClassify());
		protocolParameter.setBaseYear(Integer.toString(localDate.getYear()));

		// 기준정보 select
		protocol = protocolService.getProtocol(protocolParameter);

		if (protocol == null) {
			throw new ValidException("현재 해당 장비로 저장된 통신기준정보가 없습니다. 해당 장비의 통신기준정보를 등록해주세요.");
		}

		// DEVICE_PROTOCOL_LOG 등록을 위한 DATA SET
		protocol.setDeviceSeq(deviceInfo.getDeviceSeq());
		protocol.setOperateStartDt(device.getOperateStartDt());
		protocol.setOperateEndDt(device.getOperateEndDt());
		protocol.setOperateStartTime(device.getOperateStartTime());
		protocol.setOperateEndTime(device.getOperateEndTime());

		// DEVICE_PROTOCOL_LOG 등록
		protocolService.setDeviceProtocolLog(protocol);

		// DEVICE에 SERIAL_NO가 있으면 LORA장비에 가동기간/가동시간 셋팅
		String boardSerial =  deviceInfo.getBoardSerial();
		if(StringUtil.isNotEmpty(boardSerial)){

			// LORA장비 가동기간 , 가동시간 UPDATE 위한 DATA SET
			devEUIList.add(boardSerial);
			loraParameter.setOperateStartDt(DateUtil.localDateToString(device.getOperateStartDt(), DateUtil.DASH_DATE_FORMAT));
			loraParameter.setOperateStartTime(device.getOperateStartTime());
			loraParameter.setOperateEndDt(DateUtil.localDateToString(device.getOperateEndDt(), DateUtil.DASH_DATE_FORMAT));
			loraParameter.setOperateEndTime(device.getOperateEndTime());
			loraParameter.setDevEUIList(devEUIList);

			// LORA장비 가동기간 , 가동시간 UPDATE
			int stat = monitorService.setLoraSchedule(loraParameter);

			if(stat <= 0 ){
				//  TODO : [김윤홍] 장비와의 통신 실패시 처리를 어떻게 할지 결정해야함
				// 				throw new Exception();
			}

		}

		return result;
	}

	/**
	 * 장비설치요청 - 설치장비 삭제처리
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteInstallRequestDevice(Device parameter) throws Exception {

		/**
		 * 1.device Table에 installRequest 관련된 정보 리셋
		 * 2.이미지 업로드 한것 삭제처리
		 * 3.추가부품 삭제처리
		 * 4.상태는 그대로 '설치중' 으로 유지
		 *
		 * **/

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());;
		parameter.setUptId(user.getUserId());

		// 장비설치요청 장비삭제 사용자 권한 확인 (보수담당)
		if (installRequestService.installRequestAuthroleChk(parameter.getInstallRequestSeq(), user.getUserId(), InstallRequestParameter.USER_TYPE_INSTALL)) {
			throw new ValidException("권한이 없습니다.");
		}

		InstallRequest installRequest = installRequestDao.selectInstallRequest(new InstallRequestParameter((parameter.getInstallRequestSeq())));

		if (!InstallRequest.STATUS_INSTALLING.equals(installRequest.getInstallRequestStatus())) {
			throw new ValidException("삭제는 설치중 단계에서만 가능합니다.");
		}

		parameter.setStatus(InstallRequest.STATUS_NOT_INSTALL);
		deviceDao.deleteInstallRequestDevice(parameter);

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, installRequest.getDeviceSeq());
		if (uploadFileList != null) {
			for (UploadFile item : uploadFileList) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 기존에 등록된 파일들 사용 X 처리
		List<UploadFile> uploadFileList2 = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, installRequest.getDeviceSeq());
		if (uploadFileList2 != null) {
			for (UploadFile item : uploadFileList2) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		//추가부품 삭제
		addpartDao.delete(new Addpart(UploadFileService.INSTALL, parameter.getDeviceSeq(), 0, Addpart.INSTALL_ADDPART_IM03));

		return true;
	}

	/**
	 * 통신상태 기준정보 장비 가동기간 / 가동시간 수정
	 *
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean updateDeviceOperateInfo(Device parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());;
		parameter.setUptId(user.getUserId());

		return deviceDao.updateDeviceOperateInfo(parameter) > 0;
	}

	/**
	 * 설치장비 삭제
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteInstallDevice(DeviceParameter parameter) throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		Device device = deviceDao.select(parameter);

		if(!DeviceStatus.DM020002_INSTALL.equals(device.getStatus()) && !DeviceStatus.DM020004_RE_INSTALL.equals(device.getStatus())){
			throw new ValidException("장비삭제는 설치 또는 재설치 상태일때만 가능합니다.");
		}

		// 민원처리삭제
		complainService.deleteComplain(device.getDeviceSeq());

		// 유지보수 삭제
		maintainService.deleteMaintain(new Maintain(device.getDeviceSeq()));

		// 장비상태 삭제
		deviceStatusService.deleteDeviceStatus(new DeviceStatus(device.getDeviceSeq()));

		// deviceProtocol,device_protocolLog 삭제
		protocolService.deleteDeviceProtocol(new Protocol(device.getDeviceSeq()));


		device.setUptId(user.getUserId());
		device.setStatus(DeviceStatus.DM020001_NON_INSTALL); // 장비상태 미설치로 세팅

		// 기존에 등록된 파일들 사용 X 처리(설치장비)
		List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, device.getDeviceSeq());
		if (uploadFileList != null) {
			for (UploadFile item : uploadFileList) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 기존에 등록된 파일들 사용 X 처리(설치장비)
		List<UploadFile> uploadFileList2 = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, device.getDeviceSeq());
		if (uploadFileList2 != null) {
			for (UploadFile item : uploadFileList2) {
				uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
			}
		}

		// 추가부품 삭제
		addpartDao.delete(new Addpart(UploadFileService.INSTALL, device.getDeviceSeq(), 0, Addpart.INSTALL_ADDPART_IM03));

		boolean result = false;

		// 설치장비 삭제(DEVICE TABLE UPDATE)
		result = deviceDao.deleteInstallRequestDevice(device) > 0;

		if(!result){
			throw new Exception();
		}

		return true;
	}

	//	장비 리스트 조회
	public List<InstallDeviceListModel> getInstallDeviceGridList(SelectInstallDeviceParameter deviceParameter) throws Exception {
		return deviceDao.selectInstallDeviceGridList(deviceParameter);
	}

}
