/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * 웹 설치장비조회 리스트 모델
 */
@NoArgsConstructor
@Getter
@Setter
public class InstallDeviceListModel extends BaseDomain {

	// 장비 SEQ
	private int deviceSeq;
	// 설치일
	@JsonIgnore
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;
	// 설치담당자
	private String installUserName;
	// 장비번호
	private String deviceId;
	// 장비명
	private String deviceNameText;
	// 상태
	private String statusText;
	// 지역
	private String locationAreaText;
	// 설치장소
	private String locationName;
	// 관리번호
	private String manageNo;
	// 가로등번호
	private String lampNo;
	// 가동기간
	public String operateDatePeriod;
	// 가동시간
	public String operateTimePeriod;
	// 통신방식
	public String communicationModeText;
	// 거래처구분
	public String customerTypeText;
	// 거래처
	public String customerName;
	// 영업담당자
	public String salesUserName;
	// 보수담당자
	public String asUserName;
	// 탈거일
	public String lossUserName;
	// 탈거담당자
	public String removeUserName;
	// 폐기일
	private LocalDate discardDate;
	public String discardDateText;
	public String getDiscardDateText(){
		return DateUtil.localDateToString(discardDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 폐기담당자
	public String discardUserName;
	// 분실일
	private LocalDate lossDate;
	public String lossDateText;
	public String getLossDateText(){
		return DateUtil.localDateToString(lossDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 분실일
	private LocalDate removeDate;
	public String removeDateText;
	public String getRemoveDateText(){
		return DateUtil.localDateToString(removeDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 장비주소
	private String deviceAddress;
}
