/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.domain.addpart.Addpart;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class InstallDeviceParameter {

	private String status;

	private int locationSeq;

	private int deviceSeq;

	private String deviceId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;

	private String installHour;

	private String installMinute;

	private BigDecimal latitude;

	private BigDecimal longitude;

	private String manageNo;

	private String lampNo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;

	private String operateStartTime;

	private String operateEndTime;

	private String asUserId;

	private String asCustomerSeq;

	private String installEtc;

	private String installUserId;

	private int installCustomerSeq;

	private String addPart;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> beforeUploadFileKey;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> afterUploadFileKey;

	private String deviceAddress;

	public InstallDeviceParameter(int locationSeq){
		this.locationSeq = locationSeq;
	}

	private List<Addpart> addpartList;

}
