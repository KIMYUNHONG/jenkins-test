/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatus;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Device extends BaseDomain {

	public static final String STATUS_INSTALLING = "DM020007"; // 설치중(설치상태)
	public static final String STATUS_INSTALL = "DM020002"; // 설치(장비상태 : DM02)

	// 장비 마스터 SEQ
	private int deviceMstSeq;

	// 장비 SEQ
	private int deviceSeq;

	// 장비번호
	private String deviceId;

	// 장비구분
	private String deviceClassify;

	// 장비명
	private String deviceName;

	// 모델명
	private String modelName;

	// 제조사
	private String manufactureCom;

	// 제조일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate manufactureDt;

	// MODEM ID
	private String modemId;

	// BoardSerial
	private String boardSerial;

	//ModemFwVersion
	private String modemFwVersion;

	//BoardHwVersion
	private String boardHwVersion;

	//BoardFwVersion
	private String boardFwVersion;

	//장비상태
	private String status;

	//통신방식
	private String communicationMode;

	//보관장소
	private String storage;

	//장비수량
	private int deviceQty;

	//제조사 구분
	private String manufactureComClassify;

	// 설치장소 키
	private int locationSeq;

	// 위도
	private BigDecimal latitude;

	// 경도
	private BigDecimal longitude;
	// 설치일자
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;
	// 설치시간(시)
	private String installHour;
	// 설치시간(분)
	private String installMinute;

	private BigDecimal locationLongitude;
	private BigDecimal locationLatitude;


	// 관리번호
	private String manageNo;
	// 가로등번호
	private String lampNo;
	// AS 담당자 아이디
	private String asUserId;
	// AS 담당자 소속 키
	private int asCustomerSeq;
	// 설치 비고
	private String installEtc;
	// 기기주소
	private String deviceAddress;
	// 가동 시작일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;
	private String operateStartDtText;
	public String getOperateStartDtText(){
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 가동 종료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;
	private String operateEndDtText;
	public String getOperateEndDtText(){
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 가동 시작 시간 (hhmm)
	private String operateStartTime;
	private String operateTimeText;
	public String getOperateTimeText() {
		if (StringUtil.isEmpty(operateStartTime) || StringUtil.isEmpty(operateEndTime)) {
			return "";
		}
		return operateStartTime.substring(0,2) + ":" + operateStartTime.substring(2,4) + " ~ "  + operateEndTime.substring(0,2) + ":" + operateEndTime.substring(2,4);
	}
	// 가동 종료 시간 (hhmm)
	private String operateEndTime;
	// 설치자 아이디
	private String installUserId;
	// 설치자 소속 키
	private int installCustomerSeq;
	// 추가 부품
	private String addPart;

    //제조일 TEXT
	public String getManufactureDtText(){
		return DateUtil.localDateToString(manufactureDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	public String operateStartDtKoreanText;
	public String getOperateStartDtKoreanText(){
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT_KOREAN);
	}
	public String operateEndDtKoreanText;
	public String getOperateEndDtKoreanText(){
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT_KOREAN);
	}
	public String operatePeriodText;
	public String getOperatePeriodText(){
		if(operateStartDt == null || operateEndDt == null){
			return "";
		}
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT) +
			" ~ " + DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private int relay;				// 장비 ON OFF 여부
	public String relayText;

	//장비구분 TEXT
	private String deviceClassifyText;

	//장비명 TEXT
	private String deviceNameText;

	//제조사 TEXT
	private String manufactureComText;

	//제조일 TEXT
	private String manufactureDtText;

	//통신방식 TEXT
	private String communicationModeText;

	//장비상태 TEXT
	private String statusText;

	//보관장소 TEXT
	private String storageText;

	//미설치 건수
	private int dm020001;
	//설치 건수
	private int dm020002;
	//재설치 건수
	private int dm020003;
	//탈거 건수
	private int dm020004;
	//폐기 건수
	private int dm020005;
	//분실 건수
	private int dm020006;
	//전체 건수
	private int total;

	// ----------------------------

	// 설치장소 지역
	private String locationArea;
	// 설치장소 지역 TEXT
	private String locationAreaText;
	private int distance;
	// 설치장소 명
	private String locationName;
	// 거래처명
	private String customerName;
	// 거래처타입
	private String customerType;
	// 거래처타입 Text
	private String customerTypeText;
	// 영업담당자 아이디
	private String salesUserId;
	// 영업담당 이름 / 연락처
	private String salesUserName;
	private String salesUserPhone;
	// 영업담당 거래처 명
	private String salesCustomerName;
	// 설치자 이름 / 연락처
	private String installUserName;
	private String installUserPhone;
	// 설치자 거래처 명
	private String installCustomerName;
	// AS담당자 이름
	private String asUserName;
	// AS 담당자 거래처 명
	private String asCustomerName;
	// 거래처 담당자 이름
	private String chargeName;
	// 거래처 담당자 연락처
	private String chargePhone;

	public String installDtText;
	public String getInstallDtText(){
		return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	public String installDtTimeText;
	public String getInstallDtTimeText() {
		if (StringUtil.isEmpty(getInstallDtText()) || StringUtil.isEmpty(installHour)) {
			return "";
		}
		return getInstallDtText() + " " + installHour + ":" + installMinute;
	}

	// 설치일 한국어 포맷 - nnnn년 mm월 ll일
	public String installDtKoreanText;
	public String getInstallDtKoreanText() {
		return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT_KOREAN);
	};

	// 카카오맵 표기용 필드
	String alphabet;

	private String searchAllText;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> beforeUploadFileKey;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> afterUploadFileKey;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;
	private String thumbUploadFileKey;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<DeviceStatus> deviceStatusList;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Maintain> maintainList;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> maintainRegYearList;

	// 주소, 주소상세
	private String address;
	private String addressDetail;

	private String power;
	private String monitorTop;
	private String monitorBottom;
	private String standard;
	private String wavelength;
	private String lampLife;
	private String external;

	// 태양광 패널
	private String solarPanel;
	// 배터리
	private String battery;
	// 펌프
	private String pump;
	// 약재 량
	private String medicinalResidue;

	private String handlingResult;

	// 민원신청에서 사용(실제로 데이터를 넣는용도는 아니고 화면 바인딩용
	private String content;
	private List<Addpart> addpartList;

	private String addpartListText;
	public String getAddpartListText(){
		if(addpartList == null || addpartList.size() == 0){
			return "";
		}
		StringBuilder result = new StringBuilder();
		for(Addpart item : addpartList){
			result.append(item.getPartTypeText()).append(" ").append(item.getPartQty()).append("개, ");
		}
		return result.substring(0, result.length() - 2);
	}

	private int complainHandleSeq;

	// 해당 장비 유지보수자 전화번호
	private String asUserPhone;
	// 해당 장비 유지보수자 휴대폰번호
	private String asUserTelephone;

	// ------------------------- 장비설치요청

	// 장비설치요청 시퀀스
	private int installRequestSeq;
	// 설치구분
	private String installRequestType;
	// 설치구분 TEXT
	private String installRequestTypeText;
	// 설치상태
	private String installRequestStatus;
	// 설치상태 TEXT
	private String installRequestStatusText;
	// 설치장소 주소
	private String locationAddress;
	// 비고
	private String etc;
	// 설치예정일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDueDate;
	private String installDueDateText;
	public String getInstallDueDateText(){
		return DateUtil.localDateToString(installDueDate, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 접수자 ID
	private String receiptUserId;
	// 접수자 명
	private String receiptUserName;
	// 접수자 거래처 시퀀스
	private int receiptCustomerSeq;
	// 접수자 거래처 명
	private String receiptCustomerName;
	// 접수일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime receiptDt;
	private String receiptDtText;
	public String getReceiptDtText(){
		return DateUtil.localDatetimeToString(receiptDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	// 요청자 거래처 시퀀스
	private int requestCustomerSeq;
	// 요청자 거래처명
	private String requestCustomerName;
	// 요청자 ID
	private String requestUserId;
	// 요청자 명
	private String requestUserName;
	// 요청일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime requestDt;
	private String requestDtText;
	public String getRequestDtText(){
		return DateUtil.localDatetimeToString(requestDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	//확인자 ID
	private String finishUserId;
	//확인자 명
	private String finishUserName;
	//확인자 거래처 시퀀스
	private int finishCustomerSeq;
	//확인자 거래처 명
	private String finishCustomerName;
	// 확인일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime finishDt;
	private String finishDtText;
	public String getFinishDtText(){
		return DateUtil.localDatetimeToString(finishDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}
	//D-DAY
	private int decimalDay;
	//총 건수
	private int installRequestCount;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Device> deviceList;

	// 탈거담당자
	private String removeUserName;
	// 탈거담당자
	private LocalDate removeDate;
	private String removeDateText;
	public String getRemoveDateText(){
		return DateUtil.localDateToString(removeDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 폐기담당자
	private String discardUserName;
	// 폐기일
	private LocalDate discardDate;
	private String discardDateText;
	public String getDiscardDateText(){
		return DateUtil.localDateToString(discardDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 분실처리자
	private String lossUserName;
	// 분실일
	private LocalDate lossDate;
	private String lossDateText;
	public String getLossDateText(){
		return DateUtil.localDateToString(lossDate, DateUtil.DEFAULT_DATE_FORMAT);
	}
	private String addpartText;

	private LocalDateTime lastMaintainDate;
	private String lastMaintainDateText;
	public String getLastMaintainDateText(){
		return DateUtil.localDatetimeToString(lastMaintainDate, DateUtil.DEFAULT_DATE_FORMAT);
	}

	// 전원 여부 (Y/N)
	private String powerYn;
	private String powerYnText;
	public String getPowerYnText(){
		return "Y".equals(powerYn) ? "ON" : "OFF";
	}

	private String connectionYn;
	private String connectionYnText;
	public String getConnectionYnText(){
		return "Y".equals(powerYn) ? "정상" : "단절";
	}

	// 모니터링 상태
	private String monitorState;
	private String monitorStateText;

	private int customerSeq;

	// 마이그레이션용 임시
	private String excelAddress;
	private int no;

	public Device(int deviceSeq){
		this.deviceSeq = deviceSeq;
	}

	// 유지보수 보고서 메일발송용
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> beforeFilePath;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> afterFilePath;

}
