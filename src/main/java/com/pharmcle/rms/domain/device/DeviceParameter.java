/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DeviceParameter extends BaseParameter {

	private int deviceSeq;

	// 제조사_구분
	private String deviceClassify;

	// 장비번호
	private String deviceId;

	// 장비명
	private String deviceName;

	// 제조사
	private String manufactureCom;

	// 장비상태
	private String status;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;

	// 미사용포함 플래그 (Y/N)
	private String viewAllDeviceFlag;

	// 상태
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;

	private String deviceNameText;
	private String searchAllText;

	public DeviceParameter(int deviceSeq) {
		this.deviceSeq = deviceSeq;
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	private String searchYear;
	private String searchType;

	private int customerSeq;
}
