/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class DashboardDeviceModel {

	// 장비 SEQ
	private int deviceSeq;
	// 장비번호
	private String deviceId;
	// 모뎀ID
	private String modemId;
	// 모니터링 상태 코드
	private String monitorState;
	// 설치일자
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate installDt;
	public String installDtText;
	public String getInstallDtText(){
		return DateUtil.localDateToString(installDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 모니터링 상태 코드명
	private String monitorStateText;
	// 전원 상태
	private String powerYn;
	// 연결여부(Y/N)
	private String connectionYn;
	// 장비명
	private String deviceNameText;
	// 관리번호
	private String manageNo;
	// 가로등번호
	private String lampNo;
	// 장비주소
	private String deviceAddress;
	private String boardSerial;
	// 위도
	private BigDecimal latitude;
	// 경도
	private BigDecimal longitude;
	private int distance;
}
