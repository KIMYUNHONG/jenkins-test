/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.pharmcle.rms.domain.installRequest.InstallRequest;
import com.pharmcle.rms.domain.installRequest.InstallRequestParameter;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface DeviceDao {

	List<DeviceListModel> selectList(DeviceParameter parameter);
	Map<String, Object> selectDeviceStatusData(DeviceParameter parameter);
	Device select(DeviceParameter parameter);
	int insert(Device parameter);
	int update(Device parameter);
	boolean delete(Device parameter);
	String selectDeviceId(Device parameter);
	List<Device> selectInstallList(SelectInstallDeviceParameter parameter);
	int selectInstallListCount(SelectInstallDeviceParameter parameter);

	int installDevice(InstallDeviceParameter device);
	// 설치장비 가동기간, 가동시간 등 수정
	int modifyInstallDevice(InstallDeviceParameter device);
	// 장비설치요청 관련
	//설치장비 상태변경(deviceStatus)
	int updateDeviceStatus(Device parameter);
	//설치장비 상태변경(InstallRequestFinish)
	int updateDeviceStatusFinish(InstallRequest parameter);
	//장비설치요청 설치장비 리스트 조회
	List<Device> selectInstallRequestDeviceList(InstallRequestParameter parameter);
	//장비설치요청 설치장비 상세조회
	Device getInstallRequestDevice(InstallRequestParameter parameter);
    //장비설치요청 설치장비 등록
	int installRequestDevice(Device device);
	//장비설치요청 삭제처리
	int deleteInstallRequestDevice(Device parameter);

	// 통신상태 기준정보 장비 가동기간 / 가동시간 수정
	int updateDeviceOperateInfo(Device parameter);

	List<Device> selectTempDevice(Device paramaeter);
	int insertTempDevice(Device parameter);

	// 대시보드, 모바일모니터링 장비 조회용
	List<DashboardDeviceModel> selectDashboardDeviceList(SelectInstallDeviceParameter parameter);
	int selectDashboardDeviceListCnt(SelectInstallDeviceParameter parameter);

	// 설치장비조회 그리드 조회용
	List<InstallDeviceListModel> selectInstallDeviceGridList(SelectInstallDeviceParameter parameter);
}
