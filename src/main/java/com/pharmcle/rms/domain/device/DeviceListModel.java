/**
 * Create By Lee DoWon on 2020-02-25 / 오전 10:21
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DeviceListModel extends BaseDomain {

	// 장비 마스터 SEQ
	private int deviceMstSeq;
	// 장비 SEQ
	private int deviceSeq;
	// 장비번호
	private String deviceId;
	// 장비구분
	private String deviceClassify;
	// 장비명
	private String deviceName;
	// 모델명
	private String modelName;
	// 제조사
	private String manufactureCom;
	// 제조일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate manufactureDt;
	//제조일 TEXT
	public String getManufactureDtText(){
		return DateUtil.localDateToString(manufactureDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	//장비상태
	private String status;
	//제조사 구분
	private String manufactureComClassify;
	//장비구분 TEXT
	private String deviceClassifyText;
	//장비명 TEXT
	private String deviceNameText;
	//제조사 TEXT
	private String manufactureComText;
	//제조일 TEXT
	private String manufactureDtText;
	// MODEM ID
	private String modemId;
	// BoardSerial
	private String boardSerial;
	//ModemFwVersion
	private String modemFwVersion;
	//BoardHwVersion
	private String boardHwVersion;
	//BoardFwVersion
	private String boardFwVersion;
	//통신방식 TEXT
	private String communicationModeText;
	//장비상태 TEXT
	private String statusText;
	//보관장소
	private String storage;
	//보관장소 TEXT
	private String storageText;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<DeviceListModel> deviceList;
	private String searchAllText;
	// 가동 시작일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateStartDt;
	private String operateStartDtText;
	public String getOperateStartDtText(){
		return DateUtil.localDateToString(operateStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 가동 종료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate operateEndDt;
	private String operateEndDtText;
	public String getOperateEndDtText(){
		return DateUtil.localDateToString(operateEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	// 가동 시작 시간 (hhmm)
	private String operateStartTime;
	private String operateTimeText;
	public String getOperateTimeText() {
		if (StringUtil.isEmpty(operateStartTime) || StringUtil.isEmpty(operateEndTime)) {
			return "";
		}
		return operateStartTime.substring(0,2) + ":" + operateStartTime.substring(2,4) + " ~ "  + operateEndTime.substring(0,2) + ":" + operateEndTime.substring(2,4);
	}
	// 가동 종료 시간 (hhmm)
	private String operateEndTime;
}
