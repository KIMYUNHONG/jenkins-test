/**
 * Create By Lee DoWon on 2020-04-03 / 오전 10:12
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.addpart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddpartService {

	@Autowired
	AddpartDao addpartDao;

	public List<Addpart> getAddpartList(Addpart parameter) throws Exception{
		return addpartDao.selectList(parameter);
	}

	public boolean setAddpartList(List<Addpart> parameter) throws Exception{

		if(parameter == null || parameter.size() ==0){
			return false;
		}

		addpartDao.delete(parameter.get(0));

		for(Addpart addpart : parameter){
			addpartDao.insert(addpart);
		}

		return true;
	}
}
