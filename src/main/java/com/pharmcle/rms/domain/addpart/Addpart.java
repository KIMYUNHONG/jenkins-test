/**
 * Create By Lee DoWon on 2020-04-03 / 오전 9:59
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.addpart;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Addpart extends BaseDomain {

	// 유지보수, 민원신청때 쓰는 추가부품
	public static final String MANAGE_ADDPART_MR07 = "MR07";

	// 신규등록, 설치요청 설치장비 등록시 쓰는 추가부품
	public static final String INSTALL_ADDPART_IM03 = "IM03";



	// 부품 사용하는 도메인명
	private String domainName;

	// 부품 사용하는 도메인의 시퀀스 키
	private int domainSeq;

	// 부품 사용하는 도메인의 하위 시퀀스 키(있는경우만 사용)
	private int domainSeq2;

	// 부품 종류 - MR07
	private String partType;
	private String partTypeText;

	// 부품 수량
	private int partQty;

	public Addpart(String domainName, int domainSeq, int domainSeq2, String codeGroup){
		this.domainName = domainName;
		this.domainSeq = domainSeq;
		this.domainSeq2 = domainSeq2;
		this.codeGroup = codeGroup;
	}

	private String codeGroup;
}
