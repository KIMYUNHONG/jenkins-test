/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 리포트
 */
@NoArgsConstructor
@Getter
@Setter
public class ReportParameter extends BaseParameter {

	private int deviceMstSeq;

	// 년도
	private String baseYear;

	// 담당자 구분
	private String authRole;

	// 제조사_구분
	private String deviceClassify;
	
	// 장비명
	private String deviceName;
	
	// 거래처구분
	private String customerType;

	// 거래처명
	private String customerName;

	// 거래처SEQ
	private int customerSeq;
		
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;
	
	// 거래처분석 리스트 타입
	private String tabType;

	// 일별업무내역 기준일
	private String searchStartDate;

	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키
}
