/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {

	@Autowired
	ReportDao reportDao;

	/**
	 * 장비재고분석 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Report> getDeviceStockList(ReportParameter parameter) throws Exception {
		return reportDao.selectDeviceStockList(parameter);
	}
	
	/**
	 * 거래처분석 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Report> getCustomerList(ReportParameter parameter) throws Exception {
		List<Report> reportList = null;
		String tabType = parameter.getTabType() != null ? parameter.getTabType() : "1";
		if("1".equals(tabType)) {
			reportList = reportDao.selectCustomerListForType(parameter);
		} else if("2".equals(tabType)) {
			reportList = reportDao.selectCustomerListForCustomer(parameter);
		} else if("3".equals(tabType)) {
			reportList = reportDao.selectCustomerListForLocation(parameter);
		}
		return reportList;
	}
	
	/**
	 * 유지보수분석 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Report> getMaintainList(ReportParameter parameter) throws Exception {
		return reportDao.selectMaintainList(parameter);
	}
	/**
	 * 일별 업무내역 카운트 조회
	 *
	 * @param parameter
	 * @return
	 */
	public Report getTodayStatusCount(ReportParameter parameter) throws Exception {
		return reportDao.selectTodayStatusCount(parameter);
	}
	/**
	 * 일별 업무내역 카운트 조회
	 *
	 * @param parameter
	 * @return
	 */
	public List<Report> selectTodayList(ReportParameter parameter) throws Exception {
		return reportDao.selectTodayList(parameter);
	}

}
