/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.report;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ReportDao {

	List<Report> selectDeviceStockList(ReportParameter parameter);
	List<Report> selectCustomerListForType(ReportParameter parameter);
	List<Report> selectCustomerListForCustomer(ReportParameter parameter);
	List<Report> selectCustomerListForLocation(ReportParameter parameter);
	List<Report> selectMaintainList(ReportParameter parameter);

	// 일별 업무내역 카운트
	Report selectTodayStatusCount(ReportParameter parameter);
	List<Report> selectTodayList(ReportParameter parameter);
}
