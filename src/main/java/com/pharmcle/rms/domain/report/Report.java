/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * 공지사항
 */
@NoArgsConstructor
@Getter
@Setter
public class Report extends BaseDomain {

	private int deviceMstSeq;
	private int locationSeq;
	private int customerSeq;
	private String customerName;
	private String locationName;
	private String customerType;
	private String customerTypeText;
	private String authRole;
	private String authRoleText;
	private String name;
	private String userId;
	private int customerCnt;	// 거래처수
	private int locationCnt;	// 설치장소수
	private int installCnt;		// 설치수
	private int maintainCnt;	// 유지보수
	private int complainTotalCnt;	// 민원신청
	private int complainHandleCnt;	// 민원처리
	private String complainText;
	private String deviceClassify;
	private String deviceName;
	private String modelName;
	private String manufactureCom;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate manufactureDt;

	private String manufactureDtText;
	public String getManufactureDtText(){
		return DateUtil.localDateToString(manufactureDt, DateUtil.DEFAULT_DATE_FORMAT);
	}
	
	private String deviceClassifyText;
	private String deviceNameText;
	private String manufactureComText;
	
	private int td1;
	private int td2;
	private int td3;
	private int td4;
	private int td5;
	private int td6;
	private int td7;
	
	private int m1d1;
	private int m1d2;
	private int m1d3;
	private int m1d4;
	private int m1d5;
	private int m1d6;
	private int m1d7;
	
	private int m2d1;
	private int m2d2;
	private int m2d3;
	private int m2d4;
	private int m2d5;
	private int m2d6;
	private int m2d7;
	
	private int m3d1;
	private int m3d2;
	private int m3d3;
	private int m3d4;
	private int m3d5;
	private int m3d6;
	private int m3d7;
	
	private int m4d1;
	private int m4d2;
	private int m4d3;
	private int m4d4;
	private int m4d5;
	private int m4d6;
	private int m4d7;
	
	private int m5d1;
	private int m5d2;
	private int m5d3;
	private int m5d4;
	private int m5d5;
	private int m5d6;
	private int m5d7;
	
	private int m6d1;
	private int m6d2;
	private int m6d3;
	private int m6d4;
	private int m6d5;
	private int m6d6;
	private int m6d7;
	
	private int m7d1;
	private int m7d2;
	private int m7d3;
	private int m7d4;
	private int m7d5;
	private int m7d6;
	private int m7d7;
	
	private int m8d1;
	private int m8d2;
	private int m8d3;
	private int m8d4;
	private int m8d5;
	private int m8d6;
	private int m8d7;
	
	private int m9d1;
	private int m9d2;
	private int m9d3;
	private int m9d4;
	private int m9d5;
	private int m9d6;
	private int m9d7;
	
	private int m10d1;
	private int m10d2;
	private int m10d3;
	private int m10d4;
	private int m10d5;
	private int m10d6;
	private int m10d7;
	
	private int m11d1;
	private int m11d2;
	private int m11d3;
	private int m11d4;
	private int m11d5;
	private int m11d6;
	private int m11d7;
	
	private int m12d1;
	private int m12d2;
	private int m12d3;
	private int m12d4;
	private int m12d5;
	private int m12d6;
	private int m12d7;


	// -----------------------------------------------------------------------------------------------------------------
	// 일별 업무내역 필드

	// 장비설치요청
	private int requestRegCnt;
	// 설치예정 장소/장비
	private int requestCnt;
	private int requestDeviceCnt;
	// 설치완료 장소/장비
	private int requestCompleteCnt;
	private int requestCompleteDeviceCnt;

	// 민원신청
	private int complainCnt;
	// 민원처리
	private int complainCompleteCnt;
	// ZAPS RMS 로그인 수
	private int loginCnt;
	private int userCnt;

	// 유형
	private String type;
	// 내용
	private String content;

	// -----------------------------------------------------------------------------------------------------------------
}
