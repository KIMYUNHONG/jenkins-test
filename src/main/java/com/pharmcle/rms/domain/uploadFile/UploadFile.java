/**
 * Create By Lee DoWon on 2020-03-07 / 오전 11:13
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.uploadFile;

import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.FileUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UploadFile extends BaseDomain {

	public String uploadFileKey;
	public String originalFileName;
	public String filePath;
	public String serverFileName;
	public String savePath;
	public long fileSize;
	public String uploadDomain;
	public int uploadDomainSeq;
	public int uploadDomainSeq2;
	public String fileType;
	public String thumbnailFilePath;

	public UploadFile(MultipartFile file, String path){

		this.uploadFileKey = UUID.randomUUID().toString();
		this.originalFileName = file.getOriginalFilename();
		this.serverFileName = UUID.randomUUID().toString() + FileUtil.getFileFormat(originalFileName);
		this.fileSize = file.getSize();
		this.savePath = path;
		this.filePath = path + '/' + serverFileName;
	}

	public UploadFile(String uploadFileKey){
		this.uploadFileKey = uploadFileKey;
	}

	public UploadFile(String uploadDomain, int uploadDomainSeq ){
		this.uploadDomain = uploadDomain;
		this.uploadDomainSeq = uploadDomainSeq;
	}
	public UploadFile(String uploadDomain, int uploadDomainSeq, int uploadDomainSeq2 ){
		this.uploadDomain = uploadDomain;
		this.uploadDomainSeq = uploadDomainSeq;
		this.uploadDomainSeq2 = uploadDomainSeq2;
	}
}
