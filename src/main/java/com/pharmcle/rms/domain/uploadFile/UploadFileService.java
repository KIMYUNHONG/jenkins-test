/**
 * Create By Lee DoWon on 2020-03-07 / 오전 11:17
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.uploadFile;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.List;

@Service
public class UploadFileService {

	@Value("${file.upload-dir}")
	private String uploadDir;

	@Autowired
	UploadFileDao uploadFileDao;
	@Autowired
	UserService userService;

	public static final String CUSTOMER = "CUSTOMER";    // 거래처
	public static final String INSTALL_DEVICE_BEFORE = "INSTALL_DEVICE_BEFORE";    // 장비설치 전 사진
	public static final String INSTALL_DEVICE_AFTER = "INSTALL_DEVICE_AFTER";    // 장비설치 후 사진

	public static final String DEVICE_STATUS_BEFORE = "DEVICE_STATUS_BEFORE";    // 장비상태 설치전 사진
	public static final String DEVICE_STATUS_AFTER = "DEVICE_STATUS_AFTER";    // 장비상태 설치 후 사진
	public static final String DEVICE_STATUS_PHOTO = "DEVICE_STATUS_PHOTO";    // 장비상태 사진
	public static final String DEVICE_STATUS_PROOF = "DEVICE_STATUS_PROOF";    // 장비설치 증빙자료

	public static final String APP = "APP";    // 장비설치 후 사진
	public static final String NOTICE = "NOTICE";    // 공지사항
	public static final String ARCHIVE = "ARCHIVE";    // 자료실
	public static final String FAQ = "FAQ";    // 자료실
	public static final String COMPLAIN = "COMPLAIN";    // 민원
	public static final String COMPLAIN_HANDLE = "COMPLAIN_HANDLE";    // 민원처리
	public static final String MAINTAIN = "MAINTAIN";    // 민원처리
	public static final String INSTALL = "INSTALL";    // 장비설치



	public UploadFile getUploadFile(String uploadFileKey) throws Exception {
		return uploadFileDao.select(new UploadFile(uploadFileKey));
	}

	public boolean setUploadDomain(UploadFileInsertParameter parameter) throws Exception {
		return uploadFileDao.update(parameter) > 0;
	}

	public List<UploadFile> getUploadFileListByDomain(String domain, int seq, int seq2) throws Exception {
		return uploadFileDao.selectByDomain(new UploadFile(domain, seq, seq2));
	}

	public List<UploadFile> getUploadFileListByDomain(String domain, int seq) throws Exception {
		return uploadFileDao.selectByDomain(new UploadFile(domain, seq, 0));
	}

	public List<String> getUploadFileKeyListByDomain(String domain, int seq) throws Exception {
		return uploadFileDao.selectKeyListByDomain(new UploadFile(domain, seq));
	}

	/**
	 * 단건 업로드
	 *
	 * @return
	 */
	public UploadFile upload(UploadFileInsertParameter parameter, boolean isPhoto) throws Exception {

		MultipartFile file = parameter.getFile();

		String regId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		// 저장경로 획득 및 폴더 생성
		String filePath = uploadDir + DateUtil.localDateToString(LocalDate.now(), DateUtil.DATE_NO_FORMAT);
		File uploadFileFolder = new File(uploadDir);
		uploadFileFolder.mkdir();
		File folder = new File(filePath);
		folder.mkdir();

		UploadFile uploadFile = new UploadFile(file, filePath);
		uploadFile.setRegId(regId);
		uploadFile.setThumbnailFilePath(filePath + "/t_" + uploadFile.getServerFileName());
		try {
			Path path = Paths.get(uploadFile.getFilePath());
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
			uploadFileDao.insert(uploadFile);
		} catch (Exception e) {
			throw new ValidException("서버 오류 메시지");
		}

		return uploadFile;
	}
}
