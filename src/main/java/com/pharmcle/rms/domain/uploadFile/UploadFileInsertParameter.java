/**
 * Create By Lee DoWon on 2020-03-07 / 오전 11:13
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.uploadFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UploadFileInsertParameter extends BaseDomain {

	private MultipartFile file;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<MultipartFile> fileList;

	public String uploadFileKey;
	public String uploadDomain;
	public int uploadDomainSeq;
	public int uploadDomainSeq2;

	public UploadFileInsertParameter(MultipartFile multipartFile){
		this.file = multipartFile;
	}

	public UploadFileInsertParameter(UploadFile uploadFile, String useYn){
		this.uploadFileKey = uploadFile.getUploadFileKey();
		this.useYn = useYn;
	}

	public UploadFileInsertParameter(String uploadFileKey, String uploadDomain, int uploadDomainSeq){
		this.uploadFileKey = uploadFileKey;
		this.uploadDomain = uploadDomain;
		this.uploadDomainSeq = uploadDomainSeq;
		this.useYn = "Y";
	}
	public UploadFileInsertParameter(String uploadFileKey, String uploadDomain, int uploadDomainSeq, int uploadDomainSeq2){
		this.uploadFileKey = uploadFileKey;
		this.uploadDomain = uploadDomain;
		this.uploadDomainSeq = uploadDomainSeq;
		this.uploadDomainSeq2 = uploadDomainSeq2;
		this.useYn = "Y";
	}
}
