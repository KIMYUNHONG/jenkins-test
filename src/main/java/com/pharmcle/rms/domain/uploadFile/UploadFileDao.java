/**
 * Create By Lee DoWon on 2020-03-07 / 오전 11:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.uploadFile;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UploadFileDao {

	List<UploadFile> selectList(UploadFile uploadFile);
	UploadFile select(UploadFile uploadFile);
	List<UploadFile> selectByDomain(UploadFile uploadFile);
	List<String> selectKeyListByDomain(UploadFile uploadFile);
	int insert(UploadFile uploadFile);
	int update(UploadFileInsertParameter uploadFile);
}
