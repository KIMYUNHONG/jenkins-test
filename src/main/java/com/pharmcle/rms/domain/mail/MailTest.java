package com.pharmcle.rms.domain.mail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserDao;
import com.pharmcle.rms.domain.user.UserParameter;
import com.pharmcle.rms.util.CryptoUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MailTest {
	@Autowired 
	private MailService mailservice;
	
	@Autowired
	private UserDao userDao;
	
	@GetMapping("/mail_test")
	public String testMail(Model model) {
		return "/mail_test";
	} 
	
	@PostMapping("/mail_test")
	public void testMailSend(@RequestBody Mail mail) throws Exception {		
		boolean mailSend = mailservice.sendMail(mail);
	}
	
	// 임시 데이터 넣기
	@GetMapping("/criptProc")
	public String criptProc() throws Exception {		
		// 암호화 처리 
		UserParameter userParameter = new UserParameter();
		List<User> list = userDao.selectList(userParameter);
		
//		for (User user : list) {
//			String phone = user.getPhone();
//			String telephone = user.getTelephone();
//			
//			log.info("[" + user.getUserSeq() + "] criptProc[phone] : " + phone + "[telephone] : " + telephone);
//
//			String tempPhone = "";
//			if(phone == null || "".equals(phone) ) {
//				tempPhone = "";
//			} else {
//				tempPhone = CryptoUtil.getAESEncrypt(phone);
//				log.info("getAESDecrypt[phone] : " + CryptoUtil.getAESDecrypt(tempPhone));
//			}
//			
//			String tempTelephone = "";
//			if(telephone == null || "".equals(telephone) ) {
//				tempTelephone = "";
//			} else {
//				tempTelephone = CryptoUtil.getAESEncrypt(telephone);
//				log.info("getAESDecrypt[telephone] : " + CryptoUtil.getAESDecrypt(tempTelephone));
//			}
//			
//			log.info("[" + user.getUserSeq() + "] criptProc[tempPhone] : " + tempPhone + " [tempTelephone] : " + tempTelephone);
//			
//			user.setTempPhone(tempPhone);
//			user.setTempTelephone(tempTelephone);
//			
//			int ret = userDao.updateCryptPhone(user);
//			
//			log.info("[" + user.getUserSeq() + "] criptProc : " + ret);
//		}
		
		return "/login";
	}
	
	
	// 임시 데이터 넣기
		@GetMapping("/test123")
		public String test123() throws Exception {		
			// 암호화 처리 
			UserParameter userParameter = new UserParameter();
			List<User> list = userDao.selectList(userParameter);
			
//			for (User user : list) {
//				String phone = user.getPhone();
//				String telephone = user.getTelephone();
//				
//				log.info("[" + user.getUserSeq() + "] criptProc[phone] : " + phone + "[telephone] : " + telephone);
//
//				String tempPhone = "";
//				if(phone == null || "".equals(phone) ) {
//					tempPhone = "";
//				} else {
//					tempPhone = CryptoUtil.getAESDecrypt(phone);
//					log.info("getAESDecrypt[phone] : " + CryptoUtil.getAESDecrypt(tempPhone));
//				}
//				
//				String tempTelephone = "";
//				if(telephone == null || "".equals(telephone) ) {
//					tempTelephone = "";
//				} else {
//					tempTelephone = CryptoUtil.getAESDecrypt(telephone);
//					log.info("getAESDecrypt[telephone] : " + CryptoUtil.getAESDecrypt(tempTelephone));
//				}
//				
//				log.info("[" + user.getUserSeq() + "] criptProc[tempPhone] : " + tempPhone + " [tempTelephone] : " + tempTelephone);
//			}
			
			return "/login";
		}
}
