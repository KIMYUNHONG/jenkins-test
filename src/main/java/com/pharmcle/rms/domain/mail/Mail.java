package com.pharmcle.rms.domain.mail;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * 메일 발송
 */
@Getter
@Setter
@NoArgsConstructor
public class Mail {
	private String fromName;		// 보내는사람 이름
	private String fromEmail;		// 보내는사람 이메일
	
	private String toName;			// 받는사람 이름
	private String toEmail;			// 받는사람 이메일
	private String ccEmail;			// 받는사람 이메일
	
    private String subject;			// 제목
    private String message;			// 내용
    
    private String templateFile; 					// 메일 템플릿 치환 내용
    private Map<String, String> mailReplaceData; 	// 메일 템플릿 치환 내용  
}
