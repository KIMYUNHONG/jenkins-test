package com.pharmcle.rms.domain.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 메일 발송 서비스 
 */
@Slf4j
@Component
@PropertySource("classpath:application.properties")
public class MailService {
	@Autowired	
	private JavaMailSender javaMailSender;
	
	@Value("${spring.mail.username}") String fromEmail;
	@Value("${mail.from.name}") String fromName;
	@Value("${mail.from.url}") String url;
	@Value("${spring.mail.properties.mail.mime.charset}") String charSet;

	/**
	 * 메일 발송 템플릿 파일 받아 오기  
	 * @param Mail
	 * @return boolean
	 */
	public boolean sendMail(Mail mail) throws MessagingException, IOException {
		
		MimeMessage message = javaMailSender.createMimeMessage();
		message.setContent("<h1>Hello world</h1>", "text/html");
        MimeMessageHelper helper = new MimeMessageHelper(message, true, charSet);
        
        //수신자 설정
        helper.setTo(mail.getToEmail());
        
        //메일 제목 설정
        helper.setSubject(mail.getSubject());
        
        //테스트 데이터
//        Map<String, String> htmlData = new HashMap<String, String>(); // 템플릿 치환
//		htmlData.put("URL", "http://opentest.kr");
//		htmlData.put("USERNAME", "테스트");
//		htmlData.put("REGDATE", "202.05.01");
//		htmlData.put("LOCATION", "유니위즈");
//		htmlData.put("TYPE", "포충기");
//		htmlData.put("TITLE", "포충기 메일 테스트");
//		htmlData.put("LINKURL", "http://opentest.kr/manage/complain/complainDetail?complainSeq=" + 45);
//		mail.setMailReplaceData(htmlData);
        
        
        //메일 내용 세팅
        Map<String, String> mailReplaceDate = mail.getMailReplaceData();
        String html = mailTemplate(mail.getTemplateFile());

        if(mailReplaceDate != null && mailReplaceDate.size() > 0) {
        	mailReplaceDate.put("URL", url); // 도메인 세팅
        	for (Map.Entry<String, String> entry : mailReplaceDate.entrySet()) {
        		// value 값이 없어도 메일발송 가능하도록 처리
        		if(entry.getValue() == null || entry.getValue().length() == 0){
			        entry.setValue("");
		        }
	        	html = html.replaceAll("@@<" + entry.getKey() + ">@@", entry.getValue());
	        }
        }

        helper.setText(html, true);
        
        javaMailSender.send(message);
        
        //메일 보내기
        try {
        	return true;
		} catch (Exception e) {
			log.error("메일발송 오류[sendMail] : " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 메일 발송 템플릿 파일 받아 오기
	 * @param String
	 * @return String
	 */
	private String mailTemplate(String file) throws IOException {		
		ClassPathResource classPathResource = new ClassPathResource("template/" + file);
		byte[] bdata = FileCopyUtils.copyToByteArray(classPathResource.getInputStream());
		String html = new String(bdata, StandardCharsets.UTF_8);
		
		return html; 
	}
}
