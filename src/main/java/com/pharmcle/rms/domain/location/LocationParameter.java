/**
 * Create By Lee DoWon on 2020-03-04 / 오전 11:26
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.location;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseParameter;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 설치장소
 */
@NoArgsConstructor
@Getter
@Setter
public class LocationParameter extends BaseParameter {

	// 거래처 키
	private int customerSeq;

	// 거래처 키
	private int locationSeq;

	// 지역
	private String locationArea;

	// 기간유형
	private String dateType;

	// 시작기간
	private String searchStartDate;

	// 종료기간
	private String searchEndDate;


	// 설치장소
	private String locationName;

	// 거래처구분
	private String customerType;

	// 영업담당자
	private String salesUserName;

	// 보수담당자
	private String asUserName;

	// 거래처명
	private String customerName;
	// 위도
	private String locationLatitude;
	// 경도
	private String locationLongitude;

	// 계약상태
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> contractStatusList;

	private String searchContractStatus;

	public String getSearchContractStatus(){
		if(contractStatusList == null ){
			return "ALL";
		} else if(contractStatusList.size() > 1){
			return "ALL";
		}else {
			return contractStatusList.get(0);
		}
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	private String authRole;
	private String userId;

	// 권한 체크용 필드
	private String authCustomerType;
	private int authCustomerSeq;
}
