/**
 * Create By Lee DoWon on 2020-03-04 / 오전 11:26
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.location;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 설치장소
 */
@NoArgsConstructor
@Getter
@Setter
public class Location extends BaseDomain {

	// 설치장소 키
	private int locationSeq;

	// 거래처 키
	private int customerSeq;

	// 거래처명
	private String locationName;

	// 주소
	private String address;

	// 주소 상세
	private String addressDetail;

	// 계약기간_시작일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate contractStartDt;

	// 계약기간_종료일
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDate contractEndDt;

	// 영업담당_아이디
	private String salesUserId;

	// 영업담당_소속 키
	private int salesCustomerSeq;

	// 보수담당_아이디
	private String asUserId;

	// 보수담당_소속 키
	private int asCustomerSeq;

	// 지역 코드(시/도)
	private String locationArea;

	// 지역 코드(시/군/구)
	private String locationAreaDetail;

	// 설치장소_위도
	private BigDecimal locationLatitude;

	// 설치장소_경도
	private BigDecimal locationLongitude;

	// 거래처 대표 이메일 ID
	private String customerEmailId;

	// 거래처 대표 이메일 DOMAIN
	private String customerEmailDomain;
	// 거래처 대표 이메일
	public String customerEmail;
	public String getCustomerEmail() {
		if(StringUtil.isEmpty(customerEmailId) || StringUtil.isEmpty(customerEmailDomain)){
			return "";
		}
		return customerEmailId + "@" + customerEmailDomain;
	}
	//--------------------------
	// 아래서부터는 조회용 필드

	// 거래처 담당자 이메일
	private String chargeEmail;
	// 대리점 영업담당자 이메일
	private String salesUserEmail;

	// 영업담당자 연락처
	private String salesUserPhone;

	private String locationAreaText;
	private String locationAreaDetailText;
	private int locationDistance;
	public String salesUserName;
	public String asUserName;
	public String asCustomerName;
	public String customerName;
	public String customerTypeText;
	private int deviceCnt;	// 등록장비수
	private int installCnt;	// 설치된 장비수

	private String contractStatusText;
	public String getContractStatusText(){
		return DateUtil.isLocalDateBetween(LocalDate.now(), contractStartDt, contractEndDt) ? "유지" : "종료";
	}

	private String contractStartDtText;
	public String getContractStartDtText() {
		return DateUtil.localDateToString(contractStartDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private String contractPeriodText;
	public String getContractPeriodText() {
		return getContractStartDtText() + " ~ " + getContractEndDtText();
	}

	private String contractEndDtText;
	public String getContractEndDtText() {
		return DateUtil.localDateToString(contractEndDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	private String addressText;
	public String getAddressText(){
		return address + ' ' + addressDetail;
	}

	// 설치장소 포집량 평균 계산용 필드
	private int cntVolume;
	private int sumVolume;

	private BigDecimal avgVolume;
	public BigDecimal getAvgVolume(){
		if(cntVolume == 0){
			return BigDecimal.ZERO;
		}
		BigDecimal cnt = new BigDecimal(cntVolume);
		BigDecimal sum = new BigDecimal(sumVolume);

		return sum.divide(cnt, 0, BigDecimal.ROUND_CEILING);
	}
	private String volumeState;
	public String getVolumeState(){
		BigDecimal volume = getAvgVolume();

		BigDecimal goodCondition = new BigDecimal(60);
		BigDecimal badCondition = new BigDecimal(20);

		// 포집율이 60 이상인경우
		if(goodCondition.compareTo(volume) <= 0){
			return "nice";
		} else if(badCondition.compareTo(volume) <= 0){
			return "good";
		} else {
			return "bad";
		}
	}
	private String volumeStateText;
	public String getVolumeStateText(){
		BigDecimal volume = getAvgVolume();

		BigDecimal goodCondition = new BigDecimal(60);
		BigDecimal badCondition = new BigDecimal(20);

		// 포집율이 60 이상인경우
		if(goodCondition.compareTo(volume) <= 0){
			return "좋음";
		} else if(badCondition.compareTo(volume) <= 0){
			return "보통";
		} else {
			return "나쁨";
		}
	}
}
