/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.location;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface LocationDao {

	List<Location> selectList(LocationParameter parameter);
	Location select(int locationSeq);
	int insert(Location parameter);
	int update(Location parameter);

	int selectDefaultLocationSeq(LocationParameter parameter);

}
