/**
 * Create By Lee DoWon on 2020-03-04 / 오전 11:26
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.location;

import com.pharmcle.rms.domain.code.CodeDao;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 설치장소
 */
@Service
public class LocationService {

	@Autowired
	LocationDao locationDao;

	@Autowired
	CodeDao codeDao;

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	public List<Location> getLocationList(LocationParameter locationParameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
			locationParameter.setAuthCustomerSeq(user.getCustomerSeq());
			locationParameter.setAuthCustomerType(user.getCustomerType());
		}

		List<Location> list = locationDao.selectList(locationParameter);
		List<Location> reList = new ArrayList<Location>();
				
		// 암호화 - 복호화
		for (Location location : list) {
			String salesUserPhone = CryptoUtil.getAESDecrypt(location.getSalesUserPhone());
			location.setSalesUserPhone(salesUserPhone);
			
			reList.add(location);
		}
		
		return reList;
	}

	public List<Location> getLocationAllList(LocationParameter locationParameter) throws Exception {
		List<Location> list = locationDao.selectList(locationParameter);
		List<Location> reList = new ArrayList<Location>();
				
		// 암호화 - 복호화
		for (Location location : list) {
			String salesUserPhone = CryptoUtil.getAESDecrypt(location.getSalesUserPhone());
			location.setSalesUserPhone(salesUserPhone);
			
			reList.add(location);
		}
		
		return reList;
	}

	public Location getLocation(int locationSeq) throws Exception {
		return locationDao.select(locationSeq);
	}

	public boolean setLocation(Location location) throws Exception {

		boolean result = true;

		ValidStringUtil.nullChk(location.getAddress(), "주소");
		ValidStringUtil.nullChk(location.getAddressDetail(), "상세 주소");
		ValidDateUtil.nullChk(location.getContractStartDt(), "계약기간(시작일)");
		ValidDateUtil.nullChk(location.getContractEndDt(), "계약기간(종료일)");
		ValidStringUtil.nullChk(location.getAsUserId(), "보수담당자");

		location.setLocationArea(codeService.getCodeByCodeName(location.getLocationAreaText(), "SC03", ""));
		location.setLocationAreaDetail(codeService.getCodeByCodeName(location.getLocationAreaDetailText(), "SC05", location.getLocationArea()));

		// 신규
		if (location.getLocationSeq() <= 0) {
			ValidNumberUtil.zeroChk(location.getCustomerSeq(), "거래처");
			ValidStringUtil.nullChk(location.getLocationName(), "설치장소");
			location.setRegId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));
			result = locationDao.insert(location) > 0;
			// 수정
		} else {
			location.setUptId(userService.getUserIdBySessionOrToken(SessionUtil.getToken()));
			result = locationDao.update(location) > 0;
		}

		return result;
	}

	public int getDefaultLocationSeq() throws Exception{

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

		LocationParameter locationParameter = new LocationParameter();
		locationParameter.setAuthCustomerType(user.getCustomerType());
		locationParameter.setAuthCustomerSeq(user.getCustomerSeq());
		return locationDao.selectDefaultLocationSeq(locationParameter);
	}

}
