/**
 * Create By Lee DoWon on 2020-02-20 / 오후 4:20
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.loginLog;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface LoginLogDao {

	int insert(LoginLog parameter);
}