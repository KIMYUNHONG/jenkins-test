/**
 * Create By Lee DoWon on 2020-02-20 / 오후 4:20
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.loginLog;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 로그인 기록
 */

@NoArgsConstructor
@Getter
@Setter
public class LoginLog {
	private String userId;
	private String clientIp;
	private String agent;
	private String token;
	private LocalDateTime loginDt;
}
