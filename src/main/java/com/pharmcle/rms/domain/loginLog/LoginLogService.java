/**
 * Create By Lee DoWon on 2020-02-20 / 오후 4:38
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.loginLog;

import com.pharmcle.rms.domain.user.UserParameter;
import com.pharmcle.rms.util.ServletUtil;
import com.pharmcle.rms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginLogService {

	@Autowired
	LoginLogDao loginLogDao;

	/**
	 *  로그인 로그 insert
	 * @param user 로그인하는 유저 객체
	 * @return 인서트 여부
	 */
	public boolean addLoginLog(UserParameter user)  throws Exception{

		LoginLog parameter = new LoginLog();
		parameter.setUserId(user.getUserId());
		parameter.setClientIp(SessionUtil.getClientIp());
		parameter.setAgent(ServletUtil.getUserAgent());
		parameter.setToken(user.getToken());

		return loginLogDao.insert(parameter) > 0;
	}
}
