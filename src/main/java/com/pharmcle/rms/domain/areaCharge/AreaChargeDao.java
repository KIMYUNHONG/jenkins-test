/**
 * Create By Lee DoWon on 2020-03-31 / 오전 10:03
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.areaCharge;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AreaChargeDao {

	List<AreaCharge> selectList(AreaCharge parameter);
	AreaCharge select(AreaCharge parameter);
	int merge(AreaCharge parameter);
}
