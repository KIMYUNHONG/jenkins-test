/**
 * Create By Lee DoWon on 2020-03-31 / 오전 10:04
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.areaCharge;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.StringUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AreaChargeService {

	@Autowired
	AreaChargeDao areaChargeDao;

	@Autowired
	CodeService codeService;

	@Autowired
	UserService userService;

	// 지역별 담당자 리스트 조회
	public List<AreaCharge> getAreaChargeList(AreaCharge parameter) throws Exception {
		return areaChargeDao.selectList(parameter);
	}

	// 지역별 담당자 조회
	public AreaCharge getAreaCharge(AreaCharge parameter) throws Exception {
		return areaChargeDao.select(parameter);
	}

	// 지역명을 통한 지역별 담당자 조회
	public AreaCharge getAreaChargeByAreaName(AreaCharge parameter) throws Exception {

		String area = codeService.getCodeByCodeName(parameter.getAreaText(), "SC03", "");
		String areaDetail = codeService.getCodeByCodeName(parameter.getAreaDetailText(), "SC05", area);

		if(StringUtil.isEmpty(area) || StringUtil.isEmpty(areaDetail)){
			return null;
		}
		return areaChargeDao.select(new AreaCharge(area, areaDetail));
	}

	// 지역별 담당자 수정
	@Transactional(rollbackFor = Exception.class)
	public int setAreaChargeList(AreaCharge parameter) throws Exception {
		if (parameter.getUpdatedRows() == null || parameter.getUpdatedRows().size() == 0) {
			throw new ValidException("변경할 데이터가 없습니다.");
		}

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());


		int affectedRows = 0;
		for (AreaCharge item : parameter.getUpdatedRows()) {

			ValidStringUtil.nullChk(item.getAsUserId1(), item.getAreaText() + " / " + item.getAreaDetailText() + " 보수1담당자");

			item.setRegId(user.getUserId());
			item.setUptId(user.getUserId());
			int result = areaChargeDao.merge(item);
			affectedRows += (result > 0) ? 1 : 0;
		}

		return affectedRows;
	}

}
