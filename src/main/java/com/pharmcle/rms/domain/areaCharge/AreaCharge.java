/**
 * Create By Lee DoWon on 2020-03-31 / 오전 10:01
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.areaCharge;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.base.GridColumn;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AreaCharge extends BaseDomain {

	private String area;
	private String areaDetail;
	private String installUserId;
	private String installUserName;
	private String asUserId1;
	private String asUserName1;
	private String asUserId2;
	private String asUserName2;

	///////////////////////////////////
	// 조회용 필드

	private String areaText;
	private String areaDetailText;

	private String dateType;

	private String searchStartDate;
	private String searchEndDate;

	private String installCustomerSeq;
	private String asCustomerSeq1;
	private String asCustomerSeq2;

	// 검색용 담당자 이름
	private String searchUserName;

	// TUI 그리드 등록/수정용 리스트
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<AreaCharge> updatedRows;

	public AreaCharge (String area, String areaDetail){
		this.area = area;
		this.areaDetail = areaDetail;
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;
}
