/**
 * Create By Lee DoWon on 2020-04-02 / 오후 2:45
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.maintain;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartDao;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.mail.Mail;
import com.pharmcle.rms.domain.mail.MailService;
import com.pharmcle.rms.domain.maintainType.MaintainType;
import com.pharmcle.rms.domain.maintainType.MaintainTypeDao;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileDao;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Encoder;

import javax.xml.bind.ValidationException;
import java.io.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MaintainService {

	@Autowired
	MaintainDao maintainDao;

	@Autowired
	MaintainTypeDao maintainTypeDao;

	@Autowired
	AddpartDao addpartDao;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UploadFileDao uploadFileDao;

	@Autowired
	UserService userService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	MaintainService maintainService;

	@Autowired
	MailService mailService;

	@Autowired
	ComplainService complainService;

	@Value("${mail.from.url}")
	String url;

	public List<Maintain> getMaintainList(Maintain parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			parameter.setCustomerSeq(user.getCustomerSeq());
		}

		return maintainDao.selectList(parameter);
	}

	public List<Maintain> getMaintainAndComplainList(Maintain parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			parameter.setCustomerSeq(user.getCustomerSeq());
		}

		return maintainDao.selectMaintainAndComplainList(parameter);
	}

	public int getMaintainListCount(Maintain parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			parameter.setCustomerSeq(user.getCustomerSeq());
		}
		return maintainDao.selectListCount(parameter);
	}

	public Maintain getMaintain(Maintain parameter) throws Exception {
		return maintainDao.select(parameter);
	}
	public List<String> getMaintainRegYear(Maintain parameter) throws Exception {

		User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
		if(ValidAuthUtil.isCustomerAuth(user)){
			parameter.setCustomerSeq(user.getCustomerSeq());
		}

		return maintainDao.selectMaintainRegYear(parameter);
	}

	public List<String> getMaintainAndComplainRegYear(Maintain parameter) throws Exception {

		return maintainDao.selectMaintainAndComplainRegYear(parameter);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean setMaintain(Maintain parameter) throws Exception {

		boolean result;
		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		// 유형 리스트
		// maintainTypeList 에서 statusList 으로 변경 (모바일 요청)  2020-04-21 김윤홍
		// StatusList로 받아서 실제 소스에서는 maintainTypeList으로 진행
		List<String> maintainTypeList = parameter.getStatusList();

		// 유지보수 유형은 장비이전을 제외하고 2개까지 선택이 가능하며 필수로 1개 이상 선택해야함.
		if (maintainTypeList == null || maintainTypeList.size() == 0) {
			throw new ValidationException("유지보수 유형을 선택해주세요.");
		}
		if (maintainTypeList.size() > 2) {
			throw new ValidationException("유지보수 유형은 최대 2개까지 선택가능합니다.");
		}

		if (parameter.getAddpartList() != null && parameter.getAddpartList().size() > 10) {
			throw new ValidationException("부품교체는 최대 10개까지 가능합니다.");
		}

		// 정기점검, 야간점검인경우 - 점검결과 resultType [필수]
		if (maintainTypeList.contains("MR060001") || maintainTypeList.contains("MR060002")) {
			ValidStringUtil.nullChk(parameter.getMaintainResult(), "점검결과");
		}
		// 장비이전의 경우 - 이전정보( lattitude, longitude, deviceAddress ) [필수]
		if (maintainTypeList.contains("MR060006")) {
			ValidStringUtil.nullChk(parameter.getLastDeviceAddress(), "장비위치");
			ValidNumberUtil.nullChk(parameter.getLastLatitude(), "위도");
			ValidNumberUtil.nullChk(parameter.getLastLongitude(), "경도");
		}

		ValidNumberUtil.nullChk(parameter.getVolume(), "포집량");

		if (parameter.getMaintainSeq() == 0) {

			// 유지보수 등록 사용자 권한 확인 (보수담당)
			if (!maintainAuthroleChk(parameter.getDeviceSeq(), userId, Maintain.USER_TYPE_INSTALL,"insert")) {
				throw new ValidException("권한이 없습니다.");
			}

			parameter.setRegId(userId);
			result = maintainDao.insert(parameter) > 0;
		} else {

			// 유지보수 수정 사용자 권한 확인 (보수담당)
			if (!maintainAuthroleChk(parameter.getMaintainSeq(), userId, Maintain.USER_TYPE_INSTALL,"modify")) {
				throw new ValidException("권한이 없습니다.");
			}

			// 수정은 등록으로부터 하루까지만 가능..
			Maintain existMaintain = maintainDao.select(parameter);
			LocalDateTime regDt = existMaintain.getRegDt().plusDays(1);
			if(LocalDateTime.now().isAfter(regDt)){
				throw new ValidationException("유지보수내역 수정은 등록으로부터 24시간까지 가능합니다.");
			}

			parameter.setUptId(userId);
			result = maintainDao.update(parameter) > 0;

			// 기존에 등록된 추가장비 제거
			addpartDao.delete(new Addpart("MAINTAIN", parameter.getDeviceSeq(), parameter.getMaintainSeq(), Addpart.MANAGE_ADDPART_MR07));
			maintainTypeDao.delete(new MaintainType(parameter.getDeviceSeq(), parameter.getMaintainSeq()));

			// 기존에 등록된 파일들 사용 X 처리
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.MAINTAIN, parameter.getDeviceSeq(), parameter.getMaintainSeq());
			if (uploadFileList != null) {
				for (UploadFile item : uploadFileList) {
					uploadFileDao.update(new UploadFileInsertParameter(item, "N"));
				}
			}
		}

		// 남은 파일 또는 신규등록된 파일들 매핑
		if (parameter.getUploadFileKey() != null) {
			for (String key : parameter.getUploadFileKey()) {
				UploadFileInsertParameter uploadFile = new UploadFileInsertParameter(key, UploadFileService.MAINTAIN,parameter.getDeviceSeq(), parameter.getMaintainSeq());
				uploadFileDao.update(uploadFile);
			}
		}

		for (String type : maintainTypeList) {
			maintainTypeDao.insert(new MaintainType(parameter.getDeviceSeq(), parameter.getMaintainSeq(), type, userId));
		}

		// 추가장비 등록
		if (parameter.getAddpartList() != null && parameter.getAddpartList().size() > 0) {
			for (Addpart item : parameter.getAddpartList()) {
				if (StringUtil.isEmpty(item.getPartType()) || item.getPartQty() == 0) {
					throw new Exception();
				}
				item.setDomainName(UploadFileService.MAINTAIN);
				item.setDomainSeq(parameter.getDeviceSeq());
				item.setDomainSeq2(parameter.getMaintainSeq());
				addpartDao.insert(item);
			}
		}
		return result;
	}

	/**
	 * 유지보수 사용자 권한 체크
	 **/
	public boolean maintainAuthroleChk(int seq , String userId, String userType, String authroleType) throws Exception {

		if(StringUtil.isEmpty(userId) || StringUtil.isEmpty(userType) || StringUtil.isEmpty(authroleType) || seq <= 0 ){
			throw new ValidException("권한 확인중 문제가 발생하였습니다. 관리자에게 문의해 주세요.");
		}

		// 등록일 경우 LOCATION의 AS_USER_ID 로 권한체크
		// 수정일 경우 MAINTAIN의 REG_ID 로 권한체크
		Maintain parameter = new Maintain();

		if("insert".equals(authroleType)){
			parameter.setDeviceSeq(seq);
			parameter.setUserType(userType);
			parameter.setUserId(userId);
			return maintainDao.maintainInsertAuthroleChk(parameter) > 0;
		}else{
			parameter.setMaintainSeq(seq);
			parameter.setUserType(userType);
			parameter.setUserId(userId);
			return maintainDao.maintainModifyAuthroleChk(parameter) > 0;
		}

	}

	@Transactional(rollbackFor = Exception.class)
	public boolean deleteMaintain	(Maintain parameter) throws Exception {

		/**
		 * 1.유지보수 삭제
		 * 2.maintainType 데이터 삭제
		 * 3.연관데이터 삭제
		 * **/

		List<Maintain> maintainList = maintainDao.selectList(parameter);
		List<Maintain> maintainTypeList = maintainDao.selectMaintainTypeList(parameter);

		// Maintain 삭제
		if(maintainList != null && maintainList.size() > 0){
			for(Maintain item : maintainList){
				boolean result = maintainDao.delete(item) > 0;

				// 기존에 등록된 파일들 사용 X 처리
				List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.MAINTAIN, item.getDeviceSeq(), item.getMaintainSeq());
				if (uploadFileList != null) {
					for (UploadFile item2 : uploadFileList) {
						uploadFileDao.update(new UploadFileInsertParameter(item2, "N"));
					}
				}

				// 기존에 등록된 추가장비 제거
				addpartDao.delete(new Addpart("MAINTAIN", item.getDeviceSeq(), item.getMaintainSeq(), Addpart.MANAGE_ADDPART_MR07));

				if(!result){
					throw new ValidException("유지보수 내용삭제에 실패했습니다. 관리자에게 문의해 주세요.");
				}

			}
		}

		// MaintainType 삭제
		if(maintainTypeList != null && maintainTypeList.size() > 0){
			for(Maintain item : maintainTypeList){
				boolean result = maintainTypeDao.delete(new MaintainType(item.getDeviceSeq(), item.getMaintainSeq())) > 0;

				if(!result){
					throw new ValidException("유지보수유형 삭제에 실패했습니다. 관리자에게 문의해 주세요.");
				}

			}
		}

		return true;

	}

	/**
	 * 유지보수 보고서 메일발송
	 **/
	public boolean maintainReportSendMail(Maintain parameter) throws Exception {

		/**
		 *
		 * 유지보수 메일을 거래처 대시보드에서는 location에 설치된 전체 장비에 대한 유지보수 data를 전송하고
		 * 유지보수 화면에서는 각 디바이스의 유지보수 data를 전송하기 위해 gbn으로 대시보드인지 유지보수인지 구분하여 data를 조회한다.
		 *
		 * 1.받는사람 Email 스플릿 , 벨리데이션
		 * 2.deviceList 조회
		 * 3.메일 템플릿 , 제목 , url 설정
		 * 4.이메일 내용 생성 및 발송처리
		 * **/

		ValidStringUtil.nullChk(parameter.getMailTo(), "받는사람");
		ValidStringUtil.nullChk(parameter.getMailTitle(), "제목");
		ValidNumberUtil.nullChk(parameter.getLocationSeq(), "설치장소");

		// 받는사람 스플릿
		String[] arrayMailTo = parameter.getMailTo().split(",");

		// 최대 받는사람 수 제한
		if(arrayMailTo.length > 10){
			throw new ValidException("받는사람은 최대 10명까지 설정 할 수 있습니다.");
		}

		// 이메일 형식확인
		for(String str : arrayMailTo){
			ValidStringUtil.emailFormatChk(str, "받는사람");
		}

		List<Device> deviceList;

		// 거래처 대시보드에서는 deviceList , 유지보수 화면에서는 단일 device로 보여지기 떄문에 gbn 으로 구분하여 select한다.
		if("manage".equals(parameter.getGbn())){
			SelectInstallDeviceParameter param = new SelectInstallDeviceParameter();
			param.setDeviceSeq(parameter.getDeviceSeq());
			deviceList = deviceService.getInstallDeviceList(param);
		}else if("dashBoard".equals(parameter.getGbn())){
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(parameter.getLocationSeq()));
		}else{
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(parameter.getLocationSeq()));
		}



		for (Device item : deviceList) {
			item.setMaintainList(maintainService.getMaintainAndComplainList(new Maintain(item.getDeviceSeq())));
		}

		String templateFile = "maintainMailForm.html";    // 메일 템플릿 파일명
		String subject = parameter.getMailTitle();    // 민원 제목
		String linkUrl = url;    // RMS 접속

		String header = ""; // 장비정보
		String body = "";   // 유지보수 내역 ROW

		String customerName = ""; // 거래처명
		String chargeName = "";   // 거래처 담당자명
		String locationName = ""; // 설치장소명

		// base64 이미지 생성위한 변수
		String encodeStr = ""; // 설치전
		String encodeStr2 = ""; // 설치후
		String imgTag = ""; // 이미지 TAG

		// 메일내용 생성 및 발송처리
		for(Device item2 : deviceList){

			int index = 0;

			if(index == 0){
				customerName = item2.getCustomerName();
				chargeName = item2.getChargeName();
				locationName = item2.getLocationName();
			}

			// base64 이미지 생성 (설치전 사진)
			if(item2.getBeforeFilePath() != null && item2.getBeforeFilePath().size() > 0){
				// 사진 1개만 전송되도록 제한 (전체 사진을 다 보여주기에 이메일 공간이 제한적임)
				int num = 0;
				for(String temp : item2.getBeforeFilePath()){
					if(num == 0){
						encodeStr = encodeFiletoString(temp);
						encodeStr = "data:image/jpeg;base64,"+encodeStr.replaceAll(System.getProperty("line.separator"),"");
						imgTag += "<img src="+encodeStr+" style='max-width:45%; padding: 5px 5px 5px 5px;'/>";
					}
					num++;
				}
			}

			// base64 이미지 생성 (설치후 사진)
			if(item2.getAfterFilePath() != null && item2.getAfterFilePath().size() > 0){
				int num2 = 0;
				for(String temp : item2.getAfterFilePath()){
					if(num2 == 0){
						encodeStr2 = encodeFiletoString(temp);
						encodeStr2 = "data:image/jpeg;base64,"+encodeStr2.replaceAll(System.getProperty("line.separator"),"");
						imgTag += "<img src="+encodeStr2+" style='max-width:45%; padding: 5px 5px 5px 5px;'/>";
					}
					num2++;
				}
			}

			// data null일경우 ""로 치환
			if(item2.getDeviceId() == null || item2.getDeviceId().length() == 0){
				item2.setDeviceId("");
			}
			if(item2.getDeviceNameText() == null || item2.getDeviceNameText().length() == 0){
				item2.setDeviceNameText("");
			}
			if(item2.getInstallDtKoreanText() == null || item2.getInstallDtKoreanText().length() == 0){
				item2.setInstallDtKoreanText("");
			}
			if(item2.getLocationName() == null || item2.getLocationName().length() == 0){
				item2.setLocationName("");
			}
			if(item2.getManageNo() == null || item2.getManageNo().length() == 0){
				item2.setManageNo("");
			}
			if(item2.getLampNo() == null || item2.getLampNo().length() == 0){
				item2.setLampNo("");
			}
			if(item2.getAsUserName() == null || item2.getAsUserName().length() == 0){
				item2.setAsUserName("");
			}
			if(item2.getAsUserPhone() == null || item2.getAsUserPhone().length() == 0){
				item2.setAsUserPhone("");
			}
			if(item2.getAsUserTelephone() == null || item2.getAsUserTelephone().length() == 0){
				item2.setAsUserTelephone("");
			}
			if(item2.getManufactureComText() == null || item2.getManufactureComText().length() == 0){
				item2.setManufactureComText("");
			}

			// 장비정보 생성
			header += "<table bgcolor='cccccc' border='0'>" +
				"<colgroup> <col width='200px'> <col width='200px'> <col width='200px'> <col width='200px'> </colgroup> " +
				"<tbody>" +
				"<tr>" +
				"<th bgcolor='EDEDED' align='left' height='25'>장비번호</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getDeviceId() +"</p></td>" +
				"<th bgcolor='EDEDED' align='left'>장비명</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getDeviceNameText()  +"</p></td>" +
				"</tr>" +
				"<tr>" +
				"<th bgcolor='EDEDED' align='left'>설치일</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getInstallDtKoreanText() +"</p></td>" +
				"<th bgcolor='EDEDED' align='left'>관리번호</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getManageNo() +"</p></td>" +
				"</tr>" +
				"<tr>" +
				"<th bgcolor='EDEDED' align='left'>설치장소</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getLocationName() +"</p></td>" +
				"<th bgcolor='EDEDED' align='left'>가로등번호</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getLampNo() +"</p></td>" +
				"</tr>" +
				"<tr>" +
				"<th bgcolor='EDEDED' align='left'>판매사</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'>" +"<p>"+ item2.getManufactureComText() +"</p></td>" +
				"<th bgcolor='EDEDED' align='left'>담당자</th>" +
				"<td bgcolor='FFFFFF' align='left' height='25'><p>"+ item2.getAsUserName() + "(전화번호 "+ item2.getAsUserPhone()+ ", 휴대폰 " + item2.getAsUserTelephone() +")</p>" +
				"</td>" +
				"</tr>" +
				"<tr>" +
				"<th bgcolor='EDEDED' align='left'>설치사진</th>" +
				"<td colspan='3' bgcolor='FFFFFF' align='center' height='25'>"+imgTag+"</td>" +
				"</tr>";

			//유지보수 내역 ROW 초기화
			body = "";

			// 유지보수 내역 ROW 생성
			for(Maintain item3 : item2.getMaintainList()){

				// data null일경우 ""로 치환
				if(item3.getRegDtText() == null || item3.getRegDtText().length() == 0){
					item3.setRegDtText("");
				}
				if(item3.getMaintainNoString() == null || item3.getMaintainNoString().length() == 0){
					item3.setMaintainNoString("");
				}
				if(item3.getMaintainTypeTextConcat() == null || item3.getMaintainTypeTextConcat().length() == 0){
					item3.setMaintainTypeTextConcat("");
				}
				if(item3.getContent() == null || item3.getContent().length() == 0){
					item3.setContent("");
				}

				body += "<tr><td bgcolor='FFFFFF' align='center' height='25'><p>" + item3.getRegDtText() + "</p></td><td colspan= '2' bgcolor='FFFFFF' align='center' height='25'><p>" + item3.getMaintainNoString() + " " + item3.getMaintainTypeTextConcat()+"</p> </br> <p>" + item3.getContent() + "</p> </td> <td bgcolor='FFFFFF' align='center' height='25'></td> </tr>";

			}

			if(item2.getMaintainList() == null || item2.getMaintainList().size() == 0){
				body = "<tr><td colspan='4' bgcolor='FFFFFF' align='center' height='25'><p> 등록된 유지보수내역이 없습니다. </p></td></tr>";
			}

			// 유지보수 내역 LIST 생성
			header +=   "<tr>" +
						"<th colspan='4' bgcolor='EDEDED'>관리내역</th>" +
						"</tr>"+
						"<tr>" +
						"<th bgcolor='EDEDED'>설치일</th>" +
						"<th colspan='2' bgcolor='EDEDED'>점검내용</th>" +
						"<th bgcolor='EDEDED'>비고</th>" +
						"</tr>" + body +
						"</table>" +
				        "<div style='margin: 0px 0px 10px 0px'> </div>";

			index++;

		}

		// 받는사람 수 만큼 메일발송 반복
		for(String mailTo : arrayMailTo){
			Mail mail = new Mail();
			mail.setToEmail(mailTo);                                         // 받는사람 이메일
			mail.setSubject(subject);                                        // 메일 제목
			mail.setTemplateFile(templateFile);                              // 메일 템플릿 파일

			Map<String, String> htmlData = new HashMap<String, String>();                 // 템플릿 치환
			htmlData.put("CUSTOMERNAME", customerName);                      // 거래처명
			htmlData.put("CHARGENAME", chargeName);                          // 거래처 담당자명
			htmlData.put("LOCATIONNAME", locationName);                      // 설치장소명
			htmlData.put("LINKURL", linkUrl);                                // RMS 접속주소
			htmlData.put("MESSAGE", header);                                 // 관리내역

			mail.setMailReplaceData(htmlData);

			try {
				boolean mailSend = mailService.sendMail(mail);               // 메일 발송
			} catch (Exception e) {
				log.error("메일발송 오류[maintainReport] : " + e.getMessage());
				e.printStackTrace();
			}
		}

		return true;
	}

	/**
	 * Base64로 변환
	 **/
	private static String encodeFiletoString(String inputFileName) throws IOException {

		BASE64Encoder base64Encoder = new BASE64Encoder();

		InputStream in = new FileInputStream(new File(inputFileName));

		ByteArrayOutputStream byteOutStream=new ByteArrayOutputStream();

		int len=0;

		byte[] buf = new byte[1024];

		while((len=in.read(buf)) != -1){

			byteOutStream.write(buf, 0, len);

		}

		byte fileArray[]=byteOutStream.toByteArray();

		String encodeString=base64Encoder.encodeBuffer(fileArray);

		return encodeString;

	}

}
