/**
 * Create By Lee DoWon on 2020-04-02 / 오후 2:25
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.maintain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.base.GridColumn;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Maintain extends BaseDomain {

	// 사용자 권한
	public static final String USER_TYPE_INSTALL  = "SC020003"; // 보수담당

	// 장비 키
	private int deviceSeq;

	// 유지보수 키
	private int maintainSeq;

	// 유지보수 차수
	private int maintainNo;

	// 유지보수 타입
	private String maintainType;

	// 유지보수 결과
	private String maintainResult;

	// 유지보수 내용
	private String content;

	// 변경전 위도
	private BigDecimal lastLatitude;
	private BigDecimal latitude;

	// 변경전 경도
	private BigDecimal lastLongitude;
	private BigDecimal longitude;

	// 변경전 장비주소
	private String lastDeviceAddress;
	private String deviceAddress;

	// 변경전 관리번호
	private String lastManageNo;
	private String manageNo;

	// 변경전 가로등번호
	private String lastLampNo;
	private String lampNo;

	// 포집량
	private int volume;


	////////////////////////////////////////////////
	// 조회 또는 등록용 필드

	// 페이징 조회 필드
	private Integer startIndex;
	private Integer indexCount;

	// 검색 기간 조회
	private String searchStartDate;
	private String searchEndDate;

	// 지역
	private String locationArea;
	private String locationAreaText;
	private List<String> locationAreaList;

	// 설치장소명
	private String locationName;

	// 장비번호
	private String deviceId;

	// 장비명
	private String deviceNameText;

	// 거래처구분명
	private int customerSeq;
	private String customerType;
	private String customerTypeText;

	// 거래처명
	private String customerName;

	// AS 담당자명
	private String asUserId;
	private String asUserName;

	// 유지보수 종류
	// maintainTypeList 에서 statusList 으로 변경 (모바일 요청)  2020-04-21 김윤홍
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> statusList;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> maintainTypeTextList;
	private String maintainTypeTextConcat;
	public String getMaintainTypeTextConcat(){
		if(maintainTypeTextList == null || statusList.size() == 0){
			return "";
		}
		return StringUtil.ListToString(maintainTypeTextList.toString());
	}

	// 업로드 사진 키
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> uploadFileKey;

	// 추가 부품
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Addpart> addpartList;


	public Maintain(int deviceSeq) {
		this.deviceSeq = deviceSeq;
	}

	public Maintain(int deviceSeq, String searchYear, String searchType) {
		this.deviceSeq = deviceSeq;
		this.searchYear = searchYear;
		this.searchType = searchType;
	}

	// 유지보수 차수 n차
	private String maintainNoString;

	// 유지보수 차수 + '차'
	private String maintainNoText;
	public String getMaintainNoText(){
		return maintainNo + "차";
	}
	private String maintainResultText;

	private String addpartListText;
	public String getAddpartListText(){
		if(addpartList == null || addpartList.size() == 0){
			return "";
		}
		StringBuilder result = new StringBuilder();
		for(Addpart item : addpartList){
			result.append(item.getPartTypeText()).append(" ").append(item.getPartQty()).append("개, ");
		}
		return result.substring(0, result.length() - 2);
	}

	// 년도 검색 필드
	private String searchYear;

	public static final String ORDER_BY_DESC = "MR090001";
	public static final String ORDER_BY_ASC = "MR090002";

	// 정렬 필드
	private String searchType;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	// 권한 체크용 계정 아이디
	private String userId;
	// 담당자 유형
	private String userType;
	// 유지보수 권한체크 등록,수정 구분
	private String authroleType;
	// 설치장소 시퀀스
	private int locationSeq;
	// 등록구분(M/C) : 민원신청 / 유지보수 같이 보여주기 위한 구분자
	private String regType;
	// 민원처리 시퀀스
	private int complainHandleSeq;


	// 권한처리용 필드
	private String authCustomerType;	// 거래처_타입
	private int authCustomerSeq;		// 거래처_키

	// 메일발송용 필드
	private String mailTo; // 메일 수신자
	private String mailTitle; // 메일 제목
	private String gbn; // dashBoard , manage 구분
}
