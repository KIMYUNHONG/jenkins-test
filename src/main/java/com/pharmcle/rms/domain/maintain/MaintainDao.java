/**
 * Create By Lee DoWon on 2020-04-02 / 오후 2:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.maintain;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MaintainDao {

	List<Maintain> selectList(Maintain parameter);
	List<Maintain> selectMaintainAndComplainList(Maintain parameter);
	List<Maintain> selectMaintainTypeList(Maintain parameter);

	int selectListCount(Maintain parameter);
	Maintain select(Maintain parameter);

	List<String> selectMaintainRegYear(Maintain parameter);
	List<String> selectMaintainAndComplainRegYear(Maintain parameter);

	int insert(Maintain parameter);
	int update(Maintain parameter);
	int delete(Maintain parameter);
	int maintainModifyAuthroleChk(Maintain parameter);
	int maintainInsertAuthroleChk(Maintain parameter);



}
