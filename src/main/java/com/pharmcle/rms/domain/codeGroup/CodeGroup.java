/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.codeGroup;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 코드 그룹
 */
@NoArgsConstructor
@Getter
@Setter
public class CodeGroup extends BaseDomain {

	// 그룹 코드
	private String codeGroup;

	// 그룹 코드명
	private String codeGroupName;

	// 그룹코드 분류
	private String codeType;

	// 그룹코드 순서
	private BigDecimal orderSeq;

	// 비고
	private String etc;

	// 수정가능 여부 Y/N
	private String modifyYn;

	//--------------------------
	// 아래서부터는 조회용 필드

	// 소수점이 .00 인경우 자동으로 자꾸 절삭되는 문제가 생겨 조회용으로 text추가
	private String orderSeqText;
	public String getOrderSeqText(){
		if(orderSeqText != null) return orderSeqText;
		if(orderSeq == null) return "";

		orderSeq = orderSeq.setScale(2, RoundingMode.FLOOR);
		return orderSeq.toString();
	}

	// 수정가능 여부 Y/N
	private String modifyYnText;

	public String getModifyYnText(){
		return "N".equals(modifyYn) ? "X" : "O";
	}
}
