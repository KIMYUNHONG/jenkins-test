/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.codeGroup;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 코드 그룹
 */
@NoArgsConstructor
@Getter
@Setter
public class CodeGroupParameter {

	// 그룹 코드
	private String codeGroup;

	// 그룹 코드명
	private String codeGroupName;

	// 그룹코드 분류
	private String codeType;

	// 전체조회 플래그 (값이 있으면 조회)
	private String viewAllCodeGroupFlag;

	// TUI 그리드 등록/수정용 리스트
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<CodeGroup> createdRows;
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<CodeGroup> updatedRows;
}
