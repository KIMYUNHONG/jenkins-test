/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.codeGroup;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.Code;
import com.pharmcle.rms.domain.code.CodeDao;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.NumberUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CodeGroupService {

	@Autowired
	CodeGroupDao codeGroupDao;
	@Autowired
	CodeDao codeDao;
	@Autowired
	UserService userService;

	public List<CodeGroup> getCodeGroupList(CodeGroupParameter parameter) {
		return codeGroupDao.selectList(parameter);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean setCodeGroup(CodeGroupParameter parameter) throws Exception{

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		if (parameter.getCreatedRows() != null) {

			for (CodeGroup codeGroup : parameter.getCreatedRows()) {

				chkCodeGroup(codeGroup);

				// 3. 코드유형을 통해 코드그룹 조회 (pk 생성을 위해)
				CodeParameter codeParameter = new CodeParameter();
				codeParameter.setCode(codeGroup.getCodeType());
				Code code = codeDao.select(codeParameter);

				// 4. 해당 코드유형으로 코드조회시 코드가 없는경우 체크
				if (code == null) {
					throw new ValidException("분류를 입력해주세요.");
				}

				// 5. GROUP_CODE 생성
				codeGroup.setCodeGroup(codeGroupDao.selectNewGroupCode(code.getGrpCd()));
				codeGroup.setOrderSeq(NumberUtil.stringToBigDecimal(codeGroup.getOrderSeqText(), 2));
				codeGroup.setRegId(userId);

				// 6. GROUP_CODE insert
				codeGroupDao.insert(codeGroup);
			}
		}
		if (parameter.getUpdatedRows() != null) {
			for (CodeGroup codeGroup : parameter.getUpdatedRows()) {

				chkCodeGroup(codeGroup);
				codeGroup.setUptId(userId);
				codeGroup.setOrderSeq(NumberUtil.stringToBigDecimal(codeGroup.getOrderSeqText(), 2));

				// 2. GROUP_CODE update
				codeGroupDao.update(codeGroup);
			}
		}
		return true;
	}

	private void chkCodeGroup(CodeGroup codeGroup) throws Exception{
		ValidStringUtil.codeChk(codeGroup.getCodeType(), "분류");
		ValidStringUtil.nullChk(codeGroup.getCodeGroupName(), "코드그룹명");
		ValidStringUtil.nullChk(codeGroup.getOrderSeqText(), "순서");
	}
}
