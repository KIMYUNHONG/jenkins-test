/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.codeGroup;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CodeGroupDao {

	List<CodeGroup> selectList(CodeGroupParameter parameter);
	int update(CodeGroup parameter);
	int insert(CodeGroup parameter);
	String selectNewGroupCode(String grpCd);
}
