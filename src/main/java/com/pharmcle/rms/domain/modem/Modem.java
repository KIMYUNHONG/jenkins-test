/**
 * Create By Lee DoWon on 2020-03-27 / 오후 4:28
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.modem;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.base.GridColumn;
import com.pharmcle.rms.util.StringUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Modem extends BaseDomain {

	// 모뎀 ID
	private String modemId;

	//////////////////////////////////////
	// 조회용 필드

	// 연결 여부 Y/N
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> connectYn;

	private String connectYnText;
	public String getConnectYnText(){
		return StringUtil.isNotEmpty(deviceId) ? "O" : "X";
	}

	// 장비 아이디
	private String deviceId;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<String> modemIdList;

	public Modem (String modemId){
		this.modemId = modemId;
	}
	public Modem (String modemId, String deviceId){
		this.modemId = modemId;
		this.deviceId = deviceId;
	}

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GridColumn> gridColumnList;

	private String uploadFileKey;
}
