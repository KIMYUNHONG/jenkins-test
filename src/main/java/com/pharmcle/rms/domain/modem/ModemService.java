/**
 * Create By Lee DoWon on 2020-03-27 / 오후 4:33
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.modem;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ModemService {

	@Autowired
	ModemDao modemDao;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;


	public List<Modem> getModemList(Modem parameter) throws Exception {
		return modemDao.selectList(parameter);
	}


	@Transactional(rollbackFor = Exception.class)
	public int deleteModem(Modem parameter) throws Exception {
		if (parameter.getModemIdList() == null || parameter.getModemIdList().size() <= 0) {
			throw new ValidException("삭제할 모뎀을 선택해주세요.");
		}
		return modemDao.delete(parameter);
	}


	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> setModemListByExcel(String uploadFileKey) throws Exception {

		Map<String, Object> result = new HashMap<>();
		UploadFile uploadFile = uploadFileService.getUploadFile(uploadFileKey);

		// 엑셀 파일이 있는지 체크
		if (uploadFile == null) {
			throw new Exception();
		}

		Sheet sheet = ExcelUtil.getExcelSheetByExcelFile(uploadFile.getFilePath(), 0);

		int totalCnt = 0;
		int insertCnt = 0;
		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());


		//행의 수
		int rows = sheet.getPhysicalNumberOfRows();

		// 데이터 체크
		if(rows < 2){
			throw new ValidException("등록된 내용이 없습니다. 업로드 파일을 확인해주세요.");
		}

		// 엑셀 양식 체크
		Row headerRow = sheet.getRow(0);

		String title = headerRow.getCell(0).getStringCellValue();
		if(headerRow.getCell(0) == null){
			throw new ValidException("정상적인 엑셀 양식이 아닙니다. 다시 시도해주세요.");
		} if(!"모뎀ID".equals(headerRow.getCell(0).getStringCellValue())){
			throw new ValidException("정상적인 엑셀 양식이 아닙니다. 다시 시도해주세요.");
		}

		for (int rowIndex = 1; rowIndex < rows; rowIndex++) {
			//행을읽는다
			Row row = sheet.getRow(rowIndex);
			if (row != null) {
				//셀값을 읽는다
				Cell cell = row.getCell(0);
				String value = "";
				//셀이 빈값일경우를 위한 널체크
				if (cell == null) {
					continue;
				} else{
					//타입별로 내용 읽기
					switch (cell.getCellType()){
						case NUMERIC:
							BigDecimal bigDecimal = new BigDecimal(cell.getNumericCellValue());
							value = bigDecimal.setScale(0, BigDecimal.ROUND_DOWN).toString();
							break;
						case STRING:
							value=cell.getStringCellValue();
							break;
					}

					//타입별로 내용 읽기
					if(StringUtil.isNotEmpty(value)){
						totalCnt++;
						try {
							Modem item = new Modem(value);
							item.setRegId(userId);
							int affectedRows = modemDao.insert(item);
							insertCnt += affectedRows;
						} catch (Exception e){
						}
					}
				}
			}
		}

		result.put("totalRow", totalCnt);
		result.put("insertRow", insertCnt);
		return result;
	}


}
