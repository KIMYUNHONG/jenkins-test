/**
 * Create By Lee DoWon on 2020-03-27 / 오후 4:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.modem;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ModemDao {

	List<Modem> selectList(Modem parameter);
	Modem select(Modem parameter);

	int insert(Modem parameter);
	int update(Modem parameter);
	int delete(Modem parameter);
}
