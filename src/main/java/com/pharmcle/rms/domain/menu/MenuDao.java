/**
 * Create By Lee DoWon on 2020-02-17 / 오후 1:58
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menu;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MenuDao {

	List<Menu> selectMyMenuList(MenuParameter parameter);
	List<Menu> selectList(MenuParameter parameter);

}
