/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menu;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MenuParameter {

	private String userAuth;
	private String requestUri;

	private int menuDepth;	// 메뉴 계층

	public MenuParameter(String userAuth, String requestUri){
		this.userAuth = userAuth;
		this.requestUri = requestUri;
	}
	public MenuParameter(int menuDepth ){
		this.menuDepth = menuDepth;
	}
}

