/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:36
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menu;

import com.pharmcle.rms.annotation.AutoIncrement;
import com.pharmcle.rms.annotation.PK;
import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Menu extends BaseDomain {

	// 메뉴 SEQ
	@PK
	@AutoIncrement
	private int menuSeq;

	// 메뉴명
	private String menuName;

	// 메뉴 URI 주소
	private String menuUri;

	// 메뉴 계층(0이 최상위)
	private int menuDepth;

	// 메뉴 상위 계층 SEQ (최상위인경우 0)
	private int parentMenuSeq;

	// 메뉴 순서
	private int orderSeq;

	// 메뉴리스트에 나올지 여부
	private String displayYn;

	// 모델용 하위 메뉴
	private List<Menu> childMenuList;

	// ----------------------------------------------------------------------------

	// 현재 화면 메뉴인지 여부
	boolean currentMenu;
	String menuAuth;
}

