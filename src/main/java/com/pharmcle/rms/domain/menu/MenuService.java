/**
 * Create By Lee DoWon on 2020-02-17 / 오후 1:57
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

	@Autowired
	MenuDao menuDao;

	public List<Menu> getMyMenuList(MenuParameter menuParameter) {
		return menuDao.selectMyMenuList(menuParameter);
	}

	public List<Menu> getMenuList(MenuParameter menuParameter) {
		return menuDao.selectList(menuParameter);
	}
}
