package com.pharmcle.rms.domain.monitorLte;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface MonitorLteDao {
	//모니터 로그 데이터 저장
	int insertNodeReceivelogLte(MonitorLte parameter);
	
	//모니터 Device relay 데이터 저장
	int updateNodeReceivelogLteDeviceRelay(MonitorLte parameter);
	
	//모니터 Device 정보
	MonitorLte selectLteDeviceInfo(int seq);
}
