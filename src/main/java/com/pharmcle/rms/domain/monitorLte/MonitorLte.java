/**
 * Create By jwh on 2020-04-08 / 오후 5:49
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.monitorLte;

import java.time.LocalDateTime;
import java.util.List;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pharmcle.rms.base.BaseDomain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MonitorLte extends BaseDomain {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	private LocalDateTime receiveDate;	// 데이터 수신 시간
	private String devEUI;				// 모뎀 시리얼 코드
	private String nodeName;			// 모뎀 아이디
	private int deviceSeq;				// 장비일련번호
	private int rssi;					// Modem RSSI 

	private String temperature;			// 온도
	private String humidity;			// 습도
	private Double currents;			// 전체 전류 (단위 mA)
	private Double currents1;			// 전류2
	private Double currents2;			// 전류2
	private Double currents3;			// 전류3
	private Double currents4;			// 전류4
	private int relay1;					// 스위치
	private int relay2;					// 스위치
	private int relay3;					// 스위치
	private int relay4;					// 스위치
		
	private String ip;					// Modem 아이피				
	private String lteNum;				// Modem 폰번호
	private String imei;				// Modem IMEI 정보
	private String sw;					// Modem SW 정보
	private String fw;					// FW Version
	private String hw;					// HW Version
	
	private String command;				// (READ, WRITE, RESET, FWUP)
	private String cmdData;				// SYSTEM, RELAY, LTE
	
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Integer> devSeqList;	// 모뎀 시리얼 코드 리스트
}
