package com.pharmcle.rms.domain.monitorLte;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.monitor.MonitorLoRa;
import com.pharmcle.rms.util.ResponseUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MonitorLteService {
	@Autowired
	MonitorLteDao monitorLteDao;
	
	/**
	 * 모니터 LTE 장비 데이터 저장
	 * @return boolean
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setLteData(Map<String, Object> parameter) throws Exception{	
		if (parameter != null) { // json 데이터 체크
			Map<String, Object> cmdData =  (Map<String, Object>) parameter.get("CMD");
			Map<String, Object> systemData = (Map<String, Object>) parameter.get("SYSTEM");
			Map<String, Object> relayData = (Map<String, Object>) parameter.get("RELAY");
			Map<String, Object> sensorData = (Map<String, Object>) parameter.get("SENSOR");
			Map<String, Object> loadData = (Map<String, Object>) parameter.get("LOAD");
			Map<String, Object> lteData = (Map<String, Object>) parameter.get("LTE");

			// 서브 데이터 체크
			if (cmdData == null) {
				log.error("setLteData[cmdData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			if (systemData == null) {
				log.error("setLteData[systemData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			if (relayData == null) {
				log.error("setLteData[relayData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			if (sensorData == null) {
				log.error("setLteData[sensorData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			if (loadData == null) {
				log.error("setLteData[loadData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			if (lteData == null) {
				log.error("setLteData[lteData] : NULL");
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
			
			log.info("setLteData[READ] :" + cmdData.get("COMMAND").toString());
			
			// CMD : COMMAND 가 READ가 아닐 경우 체크 
			if("READ".equalsIgnoreCase(cmdData.get("COMMAND").toString())) {
				
				// 데이터 자리수 체크
				/*
				ValidStringUtil.lengthChk(systemData.get("SERIAL").toString(), "SERIAL", 1, 15);
				ValidStringUtil.lengthChk(sensorData.get("TEMPERATURE").toString(), "TEMPERATURE", 1, 10);
				ValidStringUtil.lengthChk(sensorData.get("HUMIDITY").toString(), "HUMIDITY", 1, 10);
				ValidStringUtil.lengthChk(lteData.get("IP").toString(), "IP", 1, 15);
				ValidStringUtil.lengthChk(lteData.get("NUM").toString(), "NUM", 1, 15);
				ValidStringUtil.lengthChk(lteData.get("IMEI").toString(), "IMEI", 1, 20);
				ValidStringUtil.lengthChk(lteData.get("SW").toString(), "NUM", 1, 50);
				*/
				
				if(systemData.get("SERIAL") == null || systemData.get("SERIAL").toString().length() > 15) {
					log.error("setLteData[SERIAL] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(sensorData.get("TEMPERATURE") == null || sensorData.get("TEMPERATURE").toString().length() > 10) {
					log.error("setLteData[TEMPERATURE] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(sensorData.get("HUMIDITY") == null || sensorData.get("HUMIDITY").toString().length() > 10) {
					log.error("setLteData[HUMIDITY] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(lteData.get("IP") == null || lteData.get("IP").toString().length() > 15) {
					log.error("setLteData[IP] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(lteData.get("NUM") == null || lteData.get("NUM").toString().length() > 15) {
					log.error("setLteData[NUM] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(lteData.get("IMEI") == null || lteData.get("IMEI").toString().length() > 20) {
					log.error("setLteData[IMEI] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(lteData.get("SW") == null || lteData.get("SW").toString().length() > 50) {
					log.error("setLteData[SW] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(systemData.get("HW") == null || systemData.get("HW").toString().length() > 50) {
					log.error("systemData[HW] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				if(systemData.get("FW") == null || systemData.get("FW").toString().length() > 50) {
					log.error("systemData[FW] : error");
					throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
				}
				
				// 데이터 저장
				MonitorLte monitorLte = new MonitorLte();
				
				String strReceiveDate = lteData.get("TIME").toString().replace(",", " ");
				//String strReceiveDate = "2020-06-22 02:39:24";
				log.info("setLteData[strReceiveDate] :" + strReceiveDate);
				
				//LocalDateTime receiveDate = LocalDateTime.parse((CharSequence) strReceiveDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				LocalDateTime receiveDate = LocalDateTime.parse(strReceiveDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				//LocalDateTime receiveDate = (LocalDateTime) strReceiveDate;
				
				monitorLte.setReceiveDate(receiveDate);
				monitorLte.setDevEUI(lteData.get("NUM").toString());
				monitorLte.setNodeName(systemData.get("SERIAL").toString());
				monitorLte.setRssi(Integer.parseInt(lteData.get("RSSI").toString()));
				monitorLte.setTemperature(sensorData.get("TEMPERATURE").toString());
				monitorLte.setHumidity(sensorData.get("HUMIDITY").toString());
				monitorLte.setCurrents(Double.parseDouble(loadData.get("CURRENT_ALL").toString()));
				monitorLte.setCurrents1(Double.parseDouble(loadData.get("CURRENT1").toString()));
				monitorLte.setCurrents2(Double.parseDouble(loadData.get("CURRENT2").toString()));
				monitorLte.setCurrents3(Double.parseDouble(loadData.get("CURRENT3").toString()));
				monitorLte.setCurrents4(Double.parseDouble(loadData.get("CURRENT4").toString()));
				monitorLte.setRelay1(Integer.parseInt(relayData.get("RELAY1").toString()));
				monitorLte.setRelay2(Integer.parseInt(relayData.get("RELAY2").toString()));
				monitorLte.setRelay3(Integer.parseInt(relayData.get("RELAY3").toString()));
				monitorLte.setRelay4(Integer.parseInt(relayData.get("RELAY4").toString()));
				monitorLte.setIp(lteData.get("IP").toString());
				monitorLte.setLteNum(lteData.get("NUM").toString());
				monitorLte.setImei(lteData.get("IMEI").toString());
				monitorLte.setSw(lteData.get("SW").toString());
				monitorLte.setFw(lteData.get("HW").toString());
				monitorLte.setHw(lteData.get("FW").toString());
				
				// 기본 로그 저장
				int result = monitorLteDao.insertNodeReceivelogLte(monitorLte);
				if (result  <= 0) { // 저장 오류 처리 
					throw new ValidException(ResponseUtil.DATA_SAVE_FAIL);
				}
				
				// device relay update
				if(monitorLte.getDeviceSeq() > 0) {
					
					if( "1".equals(relayData.get("RELAY1").toString()) 
							|| "1".equals(relayData.get("RELAY2").toString())
							|| "1".equals(relayData.get("RELAY3").toString())
							|| "1".equals(relayData.get("RELAY4").toString()) ) {
						// 전원 ON
						monitorLte.setRelay1(1);
					} else {
						// 전원 OFF
						monitorLte.setRelay1(0);
					}
					
					result = monitorLteDao.updateNodeReceivelogLteDeviceRelay(monitorLte);
				}
			} else {
				throw new ValidException(ResponseUtil.DATA_COMMAND_FAIL);
			}
		} else {
			throw new ValidException(ResponseUtil.DATA_JSON_FAIL);
		}
		
		return true;
	}
	
	/**
	 * LTE 장비 컨트롤 세팅
	 * @return boolean
	 */
	public int setLteDeviceControl(String part, MonitorLoRa parameter) throws Exception {
		
		int stat = 0;
		
		int seq = parameter.getDeviceSeq();
		MonitorLte info = new MonitorLte();
		
		info = monitorLteDao.selectLteDeviceInfo(seq);
		
		if(info != null) {
			// API 주소
			String apiInternalLogUrl = "http://" + info.getIp(); 
			
			part = part.toUpperCase();
			
			// josn data 세팅
			JSONObject ob = new JSONObject();
			JSONObject cmd = new JSONObject();
			JSONObject lte = new JSONObject();
			
			if("RELAY1".equals(part) || "RELAY2".equals(part) || "RELAY3".equals(part) || "RELAY4".equals(part)) {
				cmd.put("COMMAND", "WRITE");
				cmd.put("DATA", part);
				
				JSONObject relay = new JSONObject();
				relay.put("part", parameter.getRelay());
				
				ob.put("RELAY", relay);
				
			} else if("RESET".equals(part)) {
				cmd.put("COMMAND", "RESET");
				cmd.put("DATA", "ALL");
			} else if("FWUP".equals(part)) {
				cmd.put("COMMAND", "FWUP");
				cmd.put("DATA", "ALL");
			}

			lte.put("IMEI", info.getImei());
			
			ob.put("CMD", cmd);
			ob.put("LTE", lte);
			
			int i = sendLteDeviceControl(apiInternalLogUrl, ob);
			
			if(i > 0) { // 실행 결과 세팅
				stat ++;
			}
		}

		return stat;
	}
	
	/**
	 * LTE 장비 컨트롤 전송
	 * @return int
	 */
	public int sendLteDeviceControl(String url, JSONObject ob) {

		MappingJacksonValue value = new MappingJacksonValue(ob);
		HttpEntity<MappingJacksonValue> entry = new HttpEntity<MappingJacksonValue>(value); 
		RestTemplate rest = new RestTemplate();
		
		String respones = "";
		
		try {
			// 설정 전달
			respones = rest.postForObject(url, value, String.class);
			 
			JSONParser jsonParser = new JSONParser();
			JSONObject obj = (JSONObject) jsonParser.parse(respones);

		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		
		return 0;
	}
}
