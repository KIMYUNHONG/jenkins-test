/**
 * Create By Lee DoWon on 2020-04-02 / 오후 2:25
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.maintainType;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MaintainType extends BaseDomain {

	// 장비 키
	private int deviceSeq;

	// 유지보수 키
	private int maintainSeq;

	// 유지보수 타입
	private String type;

	public MaintainType(int deviceSeq, int maintainSeq){
		this.deviceSeq = deviceSeq;
		this.maintainSeq = maintainSeq;
	}
	public MaintainType(int deviceSeq, int maintainSeq, String type, String regId){
		this.deviceSeq = deviceSeq;
		this.maintainSeq = maintainSeq;
		this.type = type;
		this.setRegId(regId);
	}
}
