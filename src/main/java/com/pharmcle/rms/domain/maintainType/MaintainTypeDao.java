/**
 * Create By Lee DoWon on 2020-04-02 / 오후 2:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.maintainType;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MaintainTypeDao {

	List<MaintainType> selectList(MaintainType parameter);

	int insert(MaintainType parameter);
	int delete(MaintainType parameter);
}
