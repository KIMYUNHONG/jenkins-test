/**
 * Create By Lee DoWon on 2020-03-16 / 오후 5:02
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menuAuth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class MenuAuthParameter extends BaseDomain {

	// TUI 그리드 등록/수정용 리스트
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<MenuAuth> updatedRows;

	private int menu1Seq;	// 검색조건 대메뉴
	private int menu2Seq;	// 검색조건 중메뉴


}

