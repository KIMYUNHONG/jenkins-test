/**
 * Create By Lee DoWon on 2020-03-16 / 오후 5:04
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menuAuth;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MenuAuthDao {

	List<MenuAuth> selectList(MenuAuthParameter parameter);
	int insert(MenuAuth parameter);
	int update(MenuAuth parameter);

}
