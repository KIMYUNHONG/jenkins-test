/**
 * Create By Lee DoWon on 2020-03-16 / 오후 5:10
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menuAuth;

import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.StringUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MenuAuthService {

	@Autowired
	MenuAuthDao menuAuthDao;

	@Autowired
	UserService userService;

	public List<MenuAuth> getMenuAuthList(MenuAuthParameter menuParameter) throws Exception{
		return menuAuthDao.selectList(menuParameter);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean setMenuAuthList(MenuAuthParameter parameter) throws Exception{

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		if (parameter.getUpdatedRows() != null) {
			for (MenuAuth menuAuth : parameter.getUpdatedRows()) {

				ValidStringUtil.codeChk(menuAuth.getMenuAuth1(), "팜클(관리자) 권한");
				ValidStringUtil.codeChk(menuAuth.getMenuAuth2(), "팜클(직원) 권한");
				ValidStringUtil.codeChk(menuAuth.getMenuAuth3(), "유지보수담당 권한");
				ValidStringUtil.codeChk(menuAuth.getMenuAuth4(), "거래처담당 권한");
				ValidStringUtil.codeChk(menuAuth.getMenuAuth1(), "영업담당 권한");

				if(StringUtil.isNotEmpty(menuAuth.getRegId())){
					menuAuthDao.update(new MenuAuth(menuAuth, menuAuth.getMenuAuth1(), userId, "SC020001"));
					menuAuthDao.update(new MenuAuth(menuAuth, menuAuth.getMenuAuth2(), userId, "SC020002"));
					menuAuthDao.update(new MenuAuth(menuAuth, menuAuth.getMenuAuth3(), userId, "SC020003"));
					menuAuthDao.update(new MenuAuth(menuAuth, menuAuth.getMenuAuth4(), userId, "SC020004"));
					menuAuthDao.update(new MenuAuth(menuAuth, menuAuth.getMenuAuth5(), userId, "SC020005"));
				} else {
					menuAuthDao.insert(new MenuAuth(menuAuth, menuAuth.getMenuAuth1(), userId, "SC020001"));
					menuAuthDao.insert(new MenuAuth(menuAuth, menuAuth.getMenuAuth2(), userId, "SC020002"));
					menuAuthDao.insert(new MenuAuth(menuAuth, menuAuth.getMenuAuth3(), userId, "SC020003"));
					menuAuthDao.insert(new MenuAuth(menuAuth, menuAuth.getMenuAuth4(), userId, "SC020004"));
					menuAuthDao.insert(new MenuAuth(menuAuth, menuAuth.getMenuAuth5(), userId, "SC020005"));
				}
				menuAuth.setRegId(userId);
			}
		}
		return true;
	}
}