/**
 * Create By Lee DoWon on 2020-03-16 / 오후 5:02
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.menuAuth;

import com.pharmcle.rms.annotation.AutoIncrement;
import com.pharmcle.rms.annotation.PK;
import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MenuAuth extends BaseDomain {

	// 메뉴 SEQ
	@PK
	@AutoIncrement
	private int menuSeq;
	private String userAuth;
	private String menuAuth;


	private String m1MenuName;
	private String m2MenuName;

	private String menuAuth1;
	private String menuAuth2;
	private String menuAuth3;
	private String menuAuth4;
	private String menuAuth5;

	private int menu1Seq;	// 검색조건 대메뉴
	private int menu2Seq;	// 검색조건 중메뉴

	public MenuAuth (MenuAuth param, String menuAuth, String userId, String userAuth){
		this.menuSeq = param.menuSeq;
		this.userAuth = userAuth;
		this.menuAuth = menuAuth;
		setRegId(userId);
	}
}

