/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.code;

import com.pharmcle.rms.base.BaseDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 코드
 */
@NoArgsConstructor
@Getter
@Setter
public class Code extends BaseDomain {

	// 코드
	private String code;

	// 코드명
	private String codeName;

	// 그룹 코드
	private String codeGroup;

	// 상위그룹코드
	private String parentCode;

	// 코드 순서
	private BigDecimal orderSeq;

	// 비고
	private String etc;

	// 그룹코드를 따기위한 필드 (그 외에 사용 X)
	private String grpCd;

	//--------------------------
	// 아래서부터는 조회용 필드

	// 소수점이 .00 인경우 자동으로 자꾸 절삭되는 문제가 생겨 조회용으로 text추가
	private String orderSeqText;
	public String getOrderSeqText(){
		if(orderSeqText != null) return orderSeqText;
		if(orderSeq == null) return "";

		orderSeq = orderSeq.setScale(2, RoundingMode.FLOOR);
		return orderSeq.toString();
	}
}
