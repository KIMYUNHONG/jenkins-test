/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.code;

import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.NumberUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CodeService {

	@Autowired
	CodeDao codeDao;

	@Autowired
	UserService userService;


	/**
	 * 그룹코드 한개로 조회
	 * @param parameter
	 * @return
	 */
	public List<Code> getCodeListByGroupCode(CodeParameter parameter){
		return codeDao.selectList(parameter);
	}

	/**
	 * 그룹코드 리스트로 조회하는경우
	 * @param parameter
	 * @return
	 */
	public Map<String, Object> getCodeListByGroupCodeList(CodeParameter parameter){

		Map<String, Object> result = new HashMap<>();
		for(String codeGroup : parameter.getCodeGroupList()){
			List<Code> codeList = getCodeListByGroupCode(new CodeParameter(codeGroup));
			result.put(codeGroup, codeList);
		}

		return result;
	}

	/**
	 * 코드(key)를 통해 코드 조회
	 * @param parameter
	 * @return
	 */
	public Code getCodeByCode(CodeParameter parameter){
		return codeDao.select(parameter);
	}


	/**
	 * 코드 등록/수정 [기준정보/기초코드]
	 * @param parameter
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setCode(CodeParameter parameter) throws Exception{

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());

		if (parameter.getCreatedRows() != null) {

			for (Code code : parameter.getCreatedRows()) {

				chkCode(code);
				// 4. GROUP_CODE 생성
				code.setCode(codeDao.selectNewCode(code.getCodeGroup()));
				code.setOrderSeq(NumberUtil.stringToBigDecimal(code.getOrderSeqText(), 2));
				code.setRegId(userId);

				// 5. GROUP_CODE insert
				codeDao.insert(code);
			}
		}
		if (parameter.getUpdatedRows() != null) {
			for (Code code : parameter.getUpdatedRows()) {

				chkCode(code);
				code.setUptId(userId);
				code.setOrderSeq(NumberUtil.stringToBigDecimal(code.getOrderSeqText(), 2));

				// 2. GROUP_CODE update
				codeDao.update(code);
			}
		}
		return true;
	}

	private void chkCode(Code code) throws Exception{

		ValidStringUtil.nullChk(code.getUseYn(), "사용여부");
		ValidStringUtil.nullChk(code.getCodeGroup(), "코드그룹");
		ValidStringUtil.nullChk(code.getCodeName(), "코드명");
		ValidStringUtil.nullChk(code.getOrderSeqText(), "순서");
	}

	/**
	 * 코드를 코드 이름과 그룹코드로 찾기
	 * 시/도, 시/군구 찾을때 사용
	 *
	 * @return
	 */
	public String getCodeByCodeName(String codeName, String groupCd, String parentCode) throws Exception {

		CodeParameter codeParameter = new CodeParameter();
		codeParameter.setCodeName(codeName);
		codeParameter.setCodeGroup(groupCd);
		codeParameter.setParentCode(parentCode);

		Code areaCode = codeDao.selectByCodeName(codeParameter);

		return areaCode != null ? areaCode.getCode() : "";
	}
}
