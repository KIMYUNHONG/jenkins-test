/**
 * Create By Lee DoWon on 2020-02-18 / 오후 5:49
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.domain.code;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CodeDao {

	List<Code> selectList(CodeParameter parameter);
	Code select(CodeParameter parameter);
	Code selectByCodeName(CodeParameter parameter);
	int insert(Code parameter);
	int update(Code parameter);
	String selectNewCode(String groupCode);

}
