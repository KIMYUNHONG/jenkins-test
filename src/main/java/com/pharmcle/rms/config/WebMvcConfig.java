package com.pharmcle.rms.config;

import com.pharmcle.rms.intercepter.PostHandleInterceptor;
import com.pharmcle.rms.intercepter.PreHandleInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	@Qualifier(value = "postHandleInterceptor")
	private PostHandleInterceptor postInterceptor;
	@Autowired
	@Qualifier(value = "preHandleInterceptor")
	private PreHandleInterceptor preInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		List<String> excludeList = new ArrayList<>();
		excludeList.add("/css/**");
		excludeList.add("/js/**");
		excludeList.add("/fonts/**");
		excludeList.add("/img/**");
		excludeList.add("/lib/**");
		excludeList.add("/temp/**");
		excludeList.add("/app/**");

		registry.addInterceptor(preInterceptor)
			.addPathPatterns("/**").excludePathPatterns(excludeList);
		registry.addInterceptor(postInterceptor)
			.addPathPatterns("/**").excludePathPatterns(excludeList);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/fonts/**")
			.setCachePeriod(86400)
			.addResourceLocations("/fonts/")
			.resourceChain(true);
		registry.addResourceHandler("/img/**")
			.setCachePeriod(86400)
			.addResourceLocations("/img/")
			.resourceChain(true);;
		registry.addResourceHandler("/lib/**")
			.setCachePeriod(86400)
			.addResourceLocations("/lib/")
			.resourceChain(true);;
	}
}
