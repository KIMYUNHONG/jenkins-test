package com.pharmcle.rms.config;

import java.io.IOException;
import java.util.Collections;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestfulConfig {

	private final int READ_TIMEOUT = 300;
	private final int CONNECT_TIMEOUT = 300;
	private final int MAX_CONN_TOTAL = 120;
	private final int MAX_CONN_PER_ROUTE = 60;
	
	@Bean
	public RestTemplate restTemplate() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(READ_TIMEOUT); 
		factory.setConnectTimeout(CONNECT_TIMEOUT); 
		
		HttpClient httpClient = HttpClientBuilder.create() 
				.setMaxConnTotal(MAX_CONN_TOTAL) 
				.setMaxConnPerRoute(MAX_CONN_PER_ROUTE) 
				.build();
 
		factory.setHttpClient(httpClient);
		
		RestTemplate restTemplate = new RestTemplate(factory);
		
		restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor(){
			@Override
	        public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
	        	request.getHeaders().set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
	        	request.getHeaders().setContentType(MediaType.APPLICATION_JSON);
	        	request.getHeaders().setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	        	return execution.execute(request, body);
	        }
		}); 
		
		return restTemplate;
	}
}
