/**
 * Create By Lee DoWon on 2020-03-27 / 오전 9:31
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;

@Configuration
public class TemplateConfig {
	@Bean
	public SpringResourceTemplateResolver htmlTemplateResolver() {
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setPrefix("/html/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		templateResolver.setOrder(0);
		templateResolver.setCheckExistence(true);
		return templateResolver;
	}
	@Bean
	public SpringResourceTemplateResolver xmlTemplateResolver() {
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setPrefix("/html/");
		templateResolver.setSuffix(".xml");
		templateResolver.setTemplateMode(TemplateMode.XML);
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		templateResolver.setOrder(0);
		templateResolver.setCheckExistence(true);
		return templateResolver;
	}
}