/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("IndexController")
public class IndexController {

	@Autowired
	UserService userService;

	@GuestAccess
	@GetMapping("/")
	public String index(Model model) {
		if(StringUtil.isEmpty(SessionUtil.getUserId())){
			return "redirect:/login";
		}
		return "redirect:/home/dashboard/dashboard";
	}
	@GetMapping("/test")
	public String test(Model model) {
		return "/test";
	}

	@GuestAccess
	@GetMapping("/terms")
	public String terms(Model model) {
		return "/terms";
	}
}
