/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.user.UserLoginService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("UserLoginController")
public class UserLoginController {

	@Autowired
	UserLoginService userLoginService;
	
	@Autowired
	CodeService codeService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	UserService userService;

	/**
	 * 로그인 화면 연결
	 * @return
	 */
	@GuestAccess
	@GetMapping("/login")
	public String userLogin() {
		return "/login";
	}
	
	/**
	 * 회원가입 화면 연결
	 * @return
	 */
	@GuestAccess
	@GetMapping("/regUser")
	public String regUser(HttpServletResponse response, Model model) {
		
		try {
			// 직급
			model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));
			// 이메일 도메인
			model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));
			// 휴대폰 앞자리
			model.addAttribute("SC06", codeService.getCodeListByGroupCode(new CodeParameter("SC06")));	// 휴대폰 앞자리

		}catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}
		
		return "/regUser";
	}
	
	/**
	 * 회사명 검색 팝업
	 * @return
	 */
	@GuestAccess
	@GetMapping("/customerListPopup")
	public String customerListPopup(Model model) {

		return "/customerListPopup";
	}
	
}
