/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.board.Board;
import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@Controller("MobileFaqController")
@RequestMapping("/mobile")
public class MobileFaqController {

	@Autowired
	UploadFileService uploadFileService;
	
	@Autowired
	BoardService boardService;
	
	@Autowired
	CodeService codeService;

	private final static String BASE_URL = "/mobile";

	@GetMapping("/faq/faqList")
	public String faqList(Model model) {
		List<Board> boardList = null;
		try {
			boardList = boardService.getFaqList(new BoardParameter());
		} catch (ValidException e) {
			return "";
		} catch (Exception e) {
			log.error(e.getMessage());
			return "";
		}
		model.addAttribute("SC07", codeService.getCodeListByGroupCode(new CodeParameter("SC07")));	// 자료실 구분
		model.addAttribute("boardList", boardList);
		
		return BASE_URL + "/faq/faqList";
	}
}

