/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@Controller("MobileTodoController")
@RequestMapping("/mobile")
public class MobileTodoController {

	@Autowired
	UploadFileService uploadFileService;
	
	@Autowired
	BoardService boardService;
	
	@Autowired
	TodoService todoService;

	private final static String BASE_URL = "/mobile";

	@GetMapping("/todo/todoList")
	public String faqList(Model model, String token) {
		List<Todo> todoList = null;
		try {
			todoList = todoService.getTodoList(new Todo());
		} catch (ValidException e) {
			return "";
		} catch (Exception e) {
			log.error(e.getMessage());
			return "";
		}
		model.addAttribute("todoList", todoList);
		model.addAttribute("token", token);

		return BASE_URL + "/todo/todoList";
	}
}

