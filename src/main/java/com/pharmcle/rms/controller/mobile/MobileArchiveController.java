/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.board.Board;
import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("MobileArchiveController")
@RequestMapping("/mobile")
public class MobileArchiveController {

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	BoardService boardService;

	private final static String BASE_URL = "/mobile";

	@GetMapping("/archive/archiveList")
	public String archiveList(Model model) {
		List<Board> boardList = null;
		try {
			boardList = boardService.getArchiveList(new BoardParameter());
		} catch (ValidException e) {
			return "";
		} catch (Exception e) {
			log.error(e.getMessage());
			return "";
		}
		
		model.addAttribute("boardList", boardList);
		
		return BASE_URL + "/archive/archiveList";
	}

	@GetMapping("/archive/archiveDetail")
	public String archiveDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board;
		List<UploadFile> uploadFileList;
		try {
			boolean boardCnt  = boardService.updateCnt(new BoardParameter(boardSeq));
			board = boardService.getArchive(new BoardParameter(boardSeq));
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.ARCHIVE, boardSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;
			if (board == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}
		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);
		
		return BASE_URL + "/archive/archiveDetail";
	}
}

