/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.monitor.Monitor;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

@Slf4j
@Controller("MobileMonitorController")
@RequestMapping("/mobile")
public class MobileMonitorController {

	@Autowired
	CodeService codeService;

	@Autowired
	AppService appService;

	@Autowired
	LocationService locationService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	MonitorService monitorService;

	private final static String BASE_URL = "/mobile";

	/**
	 * 모니터링 - 리스트 뷰
	 * @param model
	 * @return
	 */
	@GetMapping("/monitor/monitorList")
	public String monitorList(Model model,
		@RequestParam(defaultValue = "false") String token,
		@RequestParam(defaultValue = "10") int level,
		@RequestParam(defaultValue = "37.53105539245015") BigDecimal lat,
		@RequestParam(defaultValue = "126.98392330433975") BigDecimal lng ) {
		model.addAttribute("token", token);
		model.addAttribute("level", level);
		model.addAttribute("lat", lat);
		model.addAttribute("lng", lng);
		return BASE_URL + "/monitor/monitorList";
	}

	/**
	 * 풀팝업 - 지역선택
	 * @param model
	 * @return
	 */
	@GetMapping("/monitor/areaLocationList")
	public String areaLocationList(HttpServletResponse response, Model model,
								@RequestParam(required = false, defaultValue = "0") int locationSeq,
								@RequestParam(required = false, defaultValue = "") String locationArea,
								@RequestParam(defaultValue = "false") String token) {
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("token", token);
		return BASE_URL + "/monitor/areaLocationList";
	}

	/**
	 * 풀팝업 - 장비전원 on/off 리스트
	 * @param model
	 * @return
	 */
	@GetMapping("/monitor/devicePowerList")
	public String devicePowerList(Model model,
								@RequestParam(defaultValue = "false") String token) {
		model.addAttribute("token", token);
		return BASE_URL + "/monitor/devicePowerList";
	}

	/**
	 * 모니터링 상세
	 * @param model
	 * @return
	 */
	@GetMapping("/monitor/monitorDetail")
	public String monitorDetail(HttpServletResponse response, Model model, 
			@RequestParam(defaultValue = "0") int deviceSeq,
			@RequestParam(defaultValue = "10") int level,
			@RequestParam(defaultValue = "37.53105539245015") BigDecimal lat,
			@RequestParam(defaultValue = "126.98392330433975") BigDecimal lng,
			@RequestParam(defaultValue = "false") String token) {
		Monitor monitor = null;
		Device device = null;

		try {
			monitor = monitorService.getMonitorDetail(new Monitor(deviceSeq));
			device = deviceService.getDevice(new DeviceParameter(deviceSeq));


		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("device", device);
		model.addAttribute("monitor", monitor);
		model.addAttribute("token", token);

		model.addAttribute("level", level);
		model.addAttribute("lat", lat);
		model.addAttribute("lng", lng);
		return BASE_URL + "/monitor/monitorDetail";
	}

	/**
	 * 모니터링 - 장비 기준정보변경
	 * @param model
	 * @return
	 */
	@GetMapping("/monitor/addProtocolPopup")
	public String addProtocolPopup(Model model, HttpServletResponse response,
								@RequestParam(required = false, defaultValue = "SD020001") String deviceClassify,
								@RequestParam(required = false, defaultValue = "0") int deviceSeq,
								@RequestParam(defaultValue = "false") String token) {
		
		Monitor monitor;
		String url = "";

		//SD020001 : 포충기 , SD020002 : 기피제 분사함
		if("SD020001".equals(deviceClassify)){
			url = "/monitor/addProtocol1Popup";
		}else{
			url = "/monitor/addProtocol2Popup";
		}
		try {
			monitor = monitorService.getDeviceProtocol(new Monitor(deviceSeq));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}
		
		model.addAttribute("protocol", monitor);    // 점등시간
		model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
		model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간
		model.addAttribute("token", token);
		
		return BASE_URL + url;
	}

}

