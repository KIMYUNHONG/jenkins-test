/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.complain.ComplainParameter;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.dashboard.Dashboard;
import com.pharmcle.rms.domain.dashboard.DashboardService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.installRequest.InstallRequestParameter;
import com.pharmcle.rms.domain.installRequest.InstallRequestService;
import com.pharmcle.rms.domain.location.Location;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.monitor.Monitor;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("MobileDashboardController")
@RequestMapping("/mobile")
public class MobileDashboardController {

	@Autowired
	DashboardService dashboardService;

	@Autowired
	MonitorService monitorService;

	@Autowired
	UserService userService;

	@Autowired
	ComplainService complainService;

	@Autowired
	MaintainService maintainService;

	@Autowired
	CustomerService customerService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	LocationService locationService;

	@Autowired
	InstallRequestService installRequestService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	BoardService boardService;

	private final static String BASE_URL = "/mobile";

	/**
	 * 모바일 대시보드 - 거래처 를 제외한 권한
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/dashboard/dashboard")
	public String monitorList(Model model, String token) {

		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			// 1. 장비설치 요청 리스트 받기 (권한이 팜클(관리자, 직원)이 아닌경우 본인 아이디를 파라미터로 셋팅)
			// * 설치완료를 제외한 데이터만 받아야함

			// 1-1. 파라미터 생성
			InstallRequestParameter installRequestParameter = new InstallRequestParameter();
			List<String> searchStatusList = new ArrayList<>();
			ComplainParameter complainParameter = new ComplainParameter();
			Dashboard dashboardParameter = new Dashboard();
			Monitor monitorParameter = new Monitor();

			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				complainParameter.setAuthCustomerSeq(user.getCustomerSeq());
				complainParameter.setAuthCustomerType(user.getCustomerType());
				dashboardParameter.setAuthCustomerSeq(user.getCustomerSeq());
				dashboardParameter.setAuthCustomerType(user.getCustomerType());
				monitorParameter.setAuthCustomerSeq(user.getCustomerSeq());
				monitorParameter.setAuthCustomerType(user.getCustomerType());
				installRequestParameter.setAuthCustomerSeq(user.getCustomerSeq());
				installRequestParameter.setAuthCustomerType(user.getCustomerType());

			}
			// 1-2. 설치요청 상태 추가
			for (int i = 1; i <= 4; i++) { searchStatusList.add("IM05000" + i);} // 확인완료를 제외한 데이터 셋팅
			installRequestParameter.setStatusList(searchStatusList);

			// 1-4. 설치요청 리스트 조회후 model로 내림
			model.addAttribute("installRequestList", installRequestService.getInstallRequestList(installRequestParameter));

			// 2. 모니터링 카운트, 모니터링 최신일자 데이터 조회

			// 2-2. 모니터링 상태별 카운트 조회
			model.addAttribute("monitorStatCount", monitorService.getMonitorStatCount(monitorParameter));
			// 2-3. 모니터링 최신 데이터 수신일자 조회
			model.addAttribute("latestMonitorDate", monitorService.getLatestMontiorDate());

			model.addAttribute("token", token);

			// 3. 민원 리스트 및 카운트 조회

			// 3-2. 민원 상태별 카운트 조회
			model.addAttribute("complainStatusCount", complainService.getComplainStatusCount(complainParameter));
			// 3-3. 민원 리스트 조회
			model.addAttribute("complainList", complainService.getComplainList(complainParameter));

			// 4-1. 공지사항 1개 조회
			BoardParameter boardParameter = new BoardParameter();
			boardParameter.setStartIndex(0);
			boardParameter.setIndexCount(1);
			model.addAttribute("noticeList", boardService.getNoticeList(boardParameter));

			// 5. 전체 카운트 조회(거래처, 설치장소, 설치대수)

			model.addAttribute("totalCnt", dashboardService.getTotalCnt(dashboardParameter));

			if ("SC020004".equals(user.getAuthRole())) {
				LocationParameter locationParameter = new LocationParameter();
				locationParameter.setCustomerSeq(user.getCustomerSeq());

				if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
					locationParameter.setAuthCustomerSeq(user.getCustomerSeq());
					locationParameter.setAuthCustomerType(user.getCustomerType());
				}
				List<Location> locationList = locationService.getLocationList(locationParameter);
				model.addAttribute("locationList", locationList);

				return BASE_URL + "/dashboard/customerDashboard";
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return BASE_URL + "/dashboard/dashboard";
	}

	/**
	 * 거래처 - 설치장소별 장비뷰
	 */
	@GetMapping("/dashboard/customerLocationDevicePopup")
	public String customerLocationDevicePopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "999999") int locationSeq, String token) {

		try {
			List<Device> deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));
			if (deviceList != null) {
				char index = 65;
				for (Device item : deviceList) {
					item.setAlphabet(String.valueOf(index));
					index++;
				}
			}

			model.addAttribute("deviceList", deviceList);
			model.addAttribute("token", token);
			model.addAttribute("location", locationService.getLocation(locationSeq));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/dashboard/customerLocationDevicePopup";
	}

	/**
	 * 거래처 - 유지보수 보고서
	 */
	@GetMapping("/dashboard/customerMaintainPopup")
	public String customerMaintainPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "999999") int locationSeq, String token) {

		try {
			List<Device> deviceList;
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));

			for (Device item : deviceList) {
				item.setMaintainList(maintainService.getMaintainList(new Maintain(item.getDeviceSeq())));
			}

			model.addAttribute("deviceList", deviceList);
			model.addAttribute("token", token);
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/dashboard/customerMaintainPopup";
	}
	/**
	 * 일별업무내역
	 */
	@GetMapping("/dashboard/todayList")
	public String todayList(HttpServletResponse response, Model model, @RequestParam(defaultValue = "999999") int locationSeq, String token) {

		try {
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("token", token);
		return BASE_URL + "/dashboard/todayList";
	}
}

