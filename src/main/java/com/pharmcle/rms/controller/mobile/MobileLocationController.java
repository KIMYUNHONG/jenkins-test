/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.mobile;

import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller("MobileLocationController")
@RequestMapping("/mobile")
public class MobileLocationController {

	@Autowired
	CodeService codeService;

	@Autowired
	AppService appService;

	@Autowired
	LocationService locationService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	MonitorService monitorService;

	private final static String BASE_URL = "/mobile";

	/**
	 * 페이지 - 설치장소 등록
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/location/addLocation")
	public String areaLocationList(HttpServletResponse response, Model model, @RequestParam(defaultValue = "false") String token) {
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("token", token);
		return BASE_URL + "/location/addLocation";
	}

	/**
	 * 풀팝업 - 거래처 리스트
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/location/customerListPopup")
	public String customerListPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "false") String token) {
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("token", token);
		return BASE_URL + "/location/customerListPopup";
	}
	/**
	 * 풀팝업 - 유저 리스트
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/location/userListPopup")
	public String userListPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "false") String token) {
		model.addAttribute("token", token);
		return BASE_URL + "/location/userListPopup";
	}

	/**
	 * 풀팝업 - 주소 선택
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/location/daumAddressPopup")
	public String daumAddressPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "false") String token) {
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("token", token);
		return BASE_URL + "/location/daumAddressPopup";
	}
}

