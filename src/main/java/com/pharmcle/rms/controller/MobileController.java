/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller;

import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("MobileController")
@RequestMapping("/mobile")
public class MobileController {

	@Autowired
	CodeService codeService;

	@Autowired
	AppService appService;

	@Autowired
	LocationService locationService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	DeviceService deviceService;

	private final static String BASE_URL = "/mobile";

	@GetMapping("/noticeList")
	public String noticeList(Model model) {
		return BASE_URL + "/noticeList";
	}

	@GetMapping("/noticeDetail")
	public String noticeDetail(Model model) {
		return BASE_URL + "/noticeDetail";
	}


}

