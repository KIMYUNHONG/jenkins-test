/**
 * Create By Lee DoWon on 2020-03-23 / 오후 2:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.modem.Modem;
import com.pharmcle.rms.domain.modem.ModemService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/standard")
public class ModemApiController {

	@Autowired
	ModemService modemService;
	@Autowired
	UserService userService;

	/**
	 * 모뎀 리스트 조회
	 * @return
	 */
	@PostMapping("/modem/api/getModemList")
	public ResponseData<Object> getModemList(HttpServletRequest request, @RequestBody Modem parameter) {
		List<Modem> modemList;
		try {
			modemList = modemService.getModemList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, modemList, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 모뎀 리스트 삭제
	 * @return
	 */
	@PostMapping("/modem/api/deleteModemList")
	public ResponseData<Object> deleteModemList(HttpServletRequest request, @RequestBody Modem parameter) {
		int result = 0;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			result = modemService.deleteModem(parameter);
			if(result == 0){
				throw new Exception();
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 모뎀 리스트 조회 - 엑셀다운로드
	 * @return
	 */
	@PostMapping("/modem/excel/getModemList")
	public void downloadModemList(HttpServletResponse response, @RequestBody Modem parameter) {
		List<Modem> modemList;
		try {
			modemList = modemService.getModemList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), modemList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * 모뎀 리스트 조회 - 엑셀다운로드
	 * @return
	 */
	@PostMapping("/modem/api/setModemListByExcel")
	public ResponseData<Object> setModemListByExcel(HttpServletResponse response, @RequestBody Modem parameter) {
		Map<String, Object> result;
		try {
			result = modemService.setModemListByExcel(parameter.getUploadFileKey());

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}
}
