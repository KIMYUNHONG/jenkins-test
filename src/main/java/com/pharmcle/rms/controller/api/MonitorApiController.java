/**
 * Create By jwh on 2020-04-07 / 오후 5:30
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.monitor.Monitor;
import com.pharmcle.rms.domain.monitor.MonitorAlarm;
import com.pharmcle.rms.domain.monitor.MonitorLoRa;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.monitorLte.MonitorLteService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/monitor")
public class MonitorApiController {

	@Autowired
	MonitorService monitorService;
	
	@Autowired
	MonitorLteService monitorLteService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	UserService userService;

	/**
	 * 모니터링 리스트 조회
	 *
	 * @return
	 */
	@ResponseBody
	@PostMapping("/monitor/api/getMonitorList")
	public ResponseData<Object> getMonitorList(@RequestBody Monitor parameter) {
		
		List<Monitor> monitorList = new ArrayList<Monitor>();
		
		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			monitorList =  monitorService.getMonitorList(parameter);
		}catch (ValidException e) {
			e.printStackTrace();
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}

		return new ResponseData<Object>(true, monitorList, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 모니터링 리스트 데쉬보드 조회
	 *
	 * @return
	 */
	@ResponseBody
	@PostMapping("/monitor/api/getMonitorStatusData")
	public ResponseData<Object> getMonitorStatusData(@RequestBody Monitor parameter) {
		
		Map<String, Object> result = new HashMap<>();
		Monitor monitor = new Monitor();
		
		try {
			log.info("getMonitorStatusData[viewNomalFlag] : " + parameter.getUseNomalFlag());

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			monitor =  monitorService.getMonitorStatCount(parameter);
			
			result.put("GOOD", monitor.getStateGood());
			result.put("BAD", monitor.getStateBad());
			result.put("STOP", monitor.getStateStop());
			result.put("RELAYON", monitor.getConnectionOn());
			result.put("RELAYOFF", monitor.getConnectionOff());
			result.put("NOMAL", monitor.getNomal());
			result.put("TOTAL", monitor.getTotal());
			result.put("TOTAL2", monitor.getTotal()); // 임시 화면 테스트용용 
			
			log.info("getMonitorStatusData[TOTAL2] : " + monitor.getTotal());
			
		}catch (ValidException e) {
			e.printStackTrace();
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 모니터링 리스트 데쉬보드 조회
	 *
	 * @return
	 */
	@ResponseBody
	@PostMapping("/monitor/api/getLastReceiveDate")
	public ResponseData<Object> getLastReceiveDate(@RequestBody Monitor parameter) {
		
		Map<String, Object> result = new HashMap<>();
		try {
			result.put("lastReceiveDate", monitorService.getLastReceiveDate(parameter));
		}catch (ValidException e) {
			e.printStackTrace();
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 모니터링 리스트 조회 - 엑셀다운로드
	 *
	 * 
	 */
	@PostMapping("/monitor/excel/getMonitorList")
	@ResponseBody
	public void downloadMonitorList(HttpServletResponse response, @RequestBody Monitor parameter) {
		List<Monitor> monitorList = new ArrayList<Monitor>();
		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}

			monitorList =  monitorService.getMonitorList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), monitorList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	/**
	 * 모니터링 LoRa 전원 컨트롤 (로라 전용)
	 *
	 * 
	 */
	@PostMapping("/monitor/api/setLoraPowerStat")
	@ResponseBody
	public ResponseData<Object> setSystemPowerStat(HttpServletResponse response, @RequestBody MonitorLoRa parameter) {
		int result = 0;

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			int stat = monitorService.setLoraPowerStat(parameter);
			
			result += stat;
		}catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 모니터링 전원 컨트롤 (범용(로라, LTE 전용)
	 * @return ResponseData<Object>
	 * 
	 */
	@PostMapping("/monitor/api/setPowerStat")
	@ResponseBody
	public ResponseData<Object> setDevicePowerStat(HttpServletResponse response, @RequestBody MonitorLoRa parameter) throws Exception {
		
		ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
		
		int result = 0;

		List<String> protocolList =  parameter.getProtocolList();
		List<String> devEUIList =  parameter.getDevEUIList();
		List<Integer> seqList =  parameter.getSeqList();
		
		for (int i = 0; i < protocolList.size(); i++) {
			String protocol = protocolList.get(i);
			String devEUI = devEUIList.get(i);
			int seq = seqList.get(i);
			
			int stat = 0;
			if("LTE".equals(protocol)) {
				parameter.setDeviceSeq(seq);
				stat = monitorLteService.setLteDeviceControl("RELAY1", parameter);
			} else {
				stat = monitorService.setLoraPowerStat(parameter);
			}
			
			result += stat;
		}
		
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}
	

	/**
	 * 모니터링 통신기준정보관리 저장
	 * @return
	 */
	@PostMapping("/monitor/api/setDeviceProtocol")
	public ResponseData<Object> setDeviceProtocol(@RequestBody Monitor parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = monitorService.setDeviceProtocol(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}

		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);

	}


	/**
	 * 모니터링 상세 REPORT 조회
	 *
	 * @return
	 */
	@ResponseBody
	@PostMapping("/monitor/api/getMonitorReportData")
	public ResponseData<Object> getMonitorReportData(@RequestBody Monitor parameter) {

		Map<String, Object> result = new HashMap<>();
		List<Monitor> monitor = new ArrayList<Monitor>();
		List<MonitorAlarm> MonitorAlarm = new ArrayList<MonitorAlarm>();
		
		try {
			monitor =  monitorService.getMonitorReportData(parameter);
			MonitorAlarm = monitorService.getMonitorConditionList(parameter);
			
			result.put("data", monitor);
			result.put("alarm", MonitorAlarm);
		}catch (ValidException e) {
			e.printStackTrace();
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}

}