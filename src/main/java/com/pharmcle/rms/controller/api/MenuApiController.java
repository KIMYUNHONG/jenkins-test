/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.domain.menu.Menu;
import com.pharmcle.rms.domain.menu.MenuParameter;
import com.pharmcle.rms.domain.menu.MenuService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class MenuApiController {

	@Autowired
	MenuService menuService;

	/**
	 * 현재 세션에 접근가능한 메뉴 리스트 조회(header, side용)
	 * @return
	 */
	@PostMapping("/menu/api/getMyMenuList")
	public ResponseData<Object> getMenuList(@RequestBody MenuParameter parameter) {
		List<Menu> menuList;
		try {
			menuList = menuService.getMyMenuList(parameter);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, menuList, ResponseUtil.DATA_LOAD_SUC);
	}
}
