/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.*;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatus;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatusService;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/device")
public class DeviceApiController {

	@Autowired
	DeviceService deviceService;

	@Autowired
	DeviceStatusService deviceStatusService;

	@Autowired
	MaintainService maintainService;

	@Autowired
	UserService userService;
	/**
	 * 장비 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/device/api/getDeviceList")
	public ResponseData<Object> getDeviceList(@RequestBody DeviceParameter parameter) {
		boolean result = true;
		List<DeviceListModel> deviceList = null;
		try {
			deviceList = deviceService.getDeviceList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비 조회(단건)
	 *
	 * @return
	 */
	@PostMapping("/device/api/getDevice")
	public ResponseData<Object> getDevice(@RequestBody DeviceParameter parameter) {

		Device device;
		try {
			if (parameter.getDeviceSeq() == 0 && StringUtil.isEmpty(parameter.getDeviceId())) {
				throw new Exception("조회할 대상이 없습니다.");
			}
			device = deviceService.getDevice(parameter);
			List<String> maintainRegYearList = maintainService.getMaintainAndComplainRegYear(new Maintain(parameter.getDeviceSeq()));

			String defaultYear = (maintainRegYearList != null && maintainRegYearList.size() > 0) ?
			maintainRegYearList.get(0) : String.valueOf(LocalDate.now().getYear());

			parameter.setSearchYear(StringUtil.nvl(parameter.getSearchYear(), defaultYear));
			parameter.setSearchType(StringUtil.nvl(parameter.getSearchType(), Maintain.ORDER_BY_DESC));

			device.setDeviceStatusList(deviceStatusService.getDeviceStatusList(new DeviceStatus(parameter.getDeviceSeq())));
			device.setMaintainList(maintainService.getMaintainAndComplainList(new Maintain(parameter.getDeviceSeq(), parameter.getSearchYear(), parameter.getSearchType())));
			device.setMaintainRegYearList(maintainRegYearList);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, device, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비 조회(단건) - 바코드를 통한 조회
	 *
	 * @return
	 */
	@PostMapping("/device/api/getDeviceByBarcode")
	public ResponseData<Object> getDeviceByBarcode(@RequestBody DeviceParameter parameter) {

		Device device = null;
		try {
			if (StringUtil.isEmpty(parameter.getDeviceId())) {
				throw new Exception("조회할 대상이 없습니다.");
			}
			device = deviceService.getDevice(parameter);

			if (device == null) {
				throw new Exception("바코드를 다시 조회해주세요.");
			} else if (!"DM020001".equals(device.getStatus())) {
				throw new Exception("이미 설치된 장비입니다.");
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, device, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비 리스트 조회(상태별)
	 *
	 * @return
	 */
	@ResponseBody
	@PostMapping("/device/api/getDeviceStatusData")
	public ResponseData<Object> getDeviceStatusData(@RequestBody DeviceParameter parameter) {
		boolean result = true;
		Map<String, Object> statusData = null;
		try {
			statusData = deviceService.getDeviceStatusData(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, statusData, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비 저장 기능
	 *
	 * @return
	 */
	@PostMapping("/device/api/setDevice")
	public ResponseData<Object> setDevice(@RequestBody Device parameter, Errors error) {

		String result = "";

		if (error.hasErrors()) {
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			result = deviceService.setDevice(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비 삭제(단건)
	 *
	 * @return
	 */
	@PostMapping("/device/api/deleteDevice")
	public ResponseData<Object> deleteDevice(@RequestBody Device parameter, Errors error) {

		boolean queryResult;

		if (error.hasErrors()) {
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = deviceService.deleteDevice(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_DELETE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_DELETE_SUC);
	}


	/**
	 * 설치장비 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/device/api/getInstallDeviceList")
	@ResponseBody
	public ResponseData<Object> getInstallDeviceList(@RequestBody SelectInstallDeviceParameter parameter) {
		int listCount = 0;
		List<Device> deviceList = null;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isCustomerAuth(user) || ValidAuthUtil.isAgencyAuth(user)){
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
			}
			deviceList = deviceService.getInstallDeviceList(parameter);
			listCount = deviceService.getInstallDeviceListCount(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceList, ResponseUtil.DATA_LOAD_SUC, listCount);
	}

	/**
	 * 설치장비등록
	 *
	 * @return
	 */
	@PostMapping("/device/api/setInstallDevice")
	public ResponseData<Object> setInstallDevice(@RequestBody InstallDeviceParameter parameter) {

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
            parameter.setStatus(Device.STATUS_INSTALL); // DM020002 : 설치
			if (!deviceService.installDevice(parameter)) {
				throw new Exception();
			}
			result.put("deviceSeq", parameter.getDeviceSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 설치정보 수정
	 *
	 * @return
	 */
	@PostMapping("/device/api/modifyInstallDevice")
	public ResponseData<Object> modifyInstallDevice(@RequestBody InstallDeviceParameter parameter) {

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			if (!deviceService.modifyInstallDevice(parameter)) {
				throw new Exception();
			}
			result.put("deviceSeq", parameter.getDeviceSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 설치장비 리스트 조회 by 설치장소
	 *
	 * @return
	 */
	@PostMapping("/device/api/getInstallDeviceListByLocation")
	public ResponseData<Object> getInstallDeviceListByLocation(@RequestBody LocationParameter parameter) {
		boolean result = true;
		List<Device> deviceList = null;
		try {
			SelectInstallDeviceParameter deviceParameter = new SelectInstallDeviceParameter(parameter.getLocationSeq());
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isCustomerAuth(user) || ValidAuthUtil.isAgencyAuth(user)){
				deviceParameter.setAuthCustomerType(user.getCustomerType());
				deviceParameter.setAuthCustomerSeq(user.getCustomerSeq());
			}
			deviceList = deviceService.getInstallDeviceList(deviceParameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 설치장비 변경상태 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/device/api/getDeviceStatusList")
	public ResponseData<Object> getDeviceStatusList(@RequestBody DeviceStatus parameter) {
		boolean result = true;
		List<DeviceStatus> deviceStatusList = null;
		try {
			deviceStatusList = deviceStatusService.getDeviceStatusList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceStatusList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 설치장비 변경상태 등록
	 *
	 * @return
	 */
	@PostMapping("/device/api/setDeviceStatus")
	public ResponseData<Object> setDeviceStatus(@RequestBody DeviceStatus parameter, Errors error) {

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
		}

		boolean result = true;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			deviceStatusService.setDeviceStatus(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_LOAD_SUC);
	}


	/**
	 * 장비 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/device/excel/getDeviceList")
	public void downloadDeviceList(@RequestBody DeviceParameter parameter, HttpServletResponse response) {
		boolean result = true;
		List<DeviceListModel> deviceList = null;
		try {
			deviceList = deviceService.getDeviceList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), deviceList,  response);
		} catch (Exception e){
			log.error(e.getMessage());
		}
	}

	/**
	 * 설치장비 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/device/excel/getInstallDeviceGridList")
	@ResponseBody
	public void downloadInstallDeviceList(@RequestBody SelectInstallDeviceParameter parameter, HttpServletResponse response) {

		List<InstallDeviceListModel> deviceList = null;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isCustomerAuth(user) || ValidAuthUtil.isAgencyAuth(user)){
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
			}
			deviceList = deviceService.getInstallDeviceGridList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), deviceList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * 설치장비 삭제
	 *
	 * @return
	 */
	@PostMapping("/device/api/deleteInstallDevice")
	public ResponseData<Object> deleteInstallDevice(@RequestBody DeviceParameter parameter) {

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			if (!deviceService.deleteInstallDevice(parameter)) {
				throw new Exception();
			}

			result.put("deviceSeq", parameter.getDeviceSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}


	/**
	 * 설치장비 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/device/api/getInstallDeviceGridList")
	@ResponseBody
	public ResponseData<Object> getInstallDeviceGridList(@RequestBody SelectInstallDeviceParameter parameter) {
		int listCount;
		List<InstallDeviceListModel> deviceList;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isCustomerAuth(user) || ValidAuthUtil.isAgencyAuth(user)){
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
			}
			deviceList = deviceService.getInstallDeviceGridList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceList, ResponseUtil.DATA_LOAD_SUC);
	}
}
