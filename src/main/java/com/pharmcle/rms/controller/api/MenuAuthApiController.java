/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.menuAuth.MenuAuth;
import com.pharmcle.rms.domain.menuAuth.MenuAuthParameter;
import com.pharmcle.rms.domain.menuAuth.MenuAuthService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/standard")
public class MenuAuthApiController {

	@Autowired
	MenuAuthService menuAuthService;
	@Autowired
	UserService userService;


	/**
	 * 메뉴권한 리스트 조회
	 * @return
	 */
	@PostMapping("/menuAuth/api/getMenuAuthList")
	public ResponseData<Object> getMenuAuthList(@RequestBody MenuAuthParameter parameter) {
		List<MenuAuth> menuList;
		try {
			menuList = menuAuthService.getMenuAuthList(parameter);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, menuList, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 메뉴권한 리스트 저장
	 * @return
	 */
	@PostMapping("/menuAuth/api/setMenuAuthList")
	public ResponseData<Object> setMenuAuthList(@RequestBody MenuAuthParameter parameter) {
		boolean result;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			result = menuAuthService.setMenuAuthList(parameter);
		} catch (ValidException e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}
}
