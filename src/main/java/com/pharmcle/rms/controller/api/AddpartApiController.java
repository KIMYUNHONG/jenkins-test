/**
 * Create By Lee DoWon on 2020-03-23 / 오후 2:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/manage")
public class AddpartApiController {

	@Autowired
	AddpartService addpartService;

	/**
	 * 추가부품 리스트 조회
	 * @return
	 */
	@PostMapping("/addpart/api/getAddpartList")
	public ResponseData<Object> getAddpartList(HttpServletRequest request, @RequestBody Addpart parameter) {
		List<Addpart> addpartList;
		try {
			addpartList = addpartService.getAddpartList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, addpartList, ResponseUtil.DATA_LOAD_SUC);
	}

}
