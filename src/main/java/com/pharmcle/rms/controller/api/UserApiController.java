/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.user.*;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("/standard")
public class UserApiController {

	@Autowired
	UserService userService;

	@Autowired
	UserLoginService userLoginService;

	/**
	 * 사용자관리 리스트 조회
	 * @return
	 */
	@PostMapping("/user/api/getUserList")
	public ResponseData<Object> getUserList(@RequestBody UserParameter parameter) {
		boolean result = true;
		List<User> userList = null;
		try {
			userList = userService.getUserList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, userList,ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 사용자관리 리스트 조회(단건)
	 * @return
	 */
	@PostMapping("/user/api/getUser")
	public ResponseData<Object> getUser(@RequestBody UserParameter parameter) {

		User user;
		try {
			if(parameter.getUserSeq() == 0 ){
				throw new Exception("조회할 대상이 없습니다.");
			}
			user = userService.getUser(parameter.getUserSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, user,ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 사용자관리 userId 중복체크
	 * @return
	 */
	@GuestAccess
	@PostMapping("/user/api/getUserChk")
	public ResponseData<Object> getUserChk(@RequestBody UserParameter parameter) {

		String userId = parameter.getUserId();
		int userChk;
		String message;
		boolean result = false;

		Map<String, Object> data = new HashMap<>();
		try {
			userChk = userService.getUserChk(userId);
		// 중복되지 않은 아이디 인경우
		if(userChk == 0){
			// 아이디 형식 체크
			message = userLoginService.isAllowedPattern(userId);
			if("".equals(message)){
				result = true;
				message = userId + "는 사용가능한 아이디입니다.";
			}
		}else{
			message = userId + "는 이미 사용중인 아이디입니다.";
		}
		data.put("result", result);
		data.put("msg", message);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, data,ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 사용자관리 저장 기능
	 * @return
	 */
	@GuestAccess
	@PostMapping("/user/api/setUser")
	public ResponseData<Object> setUser(@RequestBody User parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = userService.setUser(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null,ResponseUtil.DATA_SAVE_SUC);
	}
	
	/**
	 * 사용자관리 저장 기능
	 * @return
	 */
	@GuestAccess
	@PostMapping("/user/api/regUser")
	public ResponseData<Object> regUser(@RequestBody User parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			queryResult = userService.regUser(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null,ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 사용자관리 비밀번호 초기화 기능
	 * @return
	 */
	@PostMapping("/user/api/resetUserPw")
	public ResponseData<Object> resetPw(@RequestBody User parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = userService.resetUserPw(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null,ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 영업담당자 리스트 조회
	 * @return
	 */
	@PostMapping("/user/api/getSalesUserList")
	public ResponseData<Object> getSalesUserList(@RequestBody User parameter) {

		String name = parameter.getName();
		String customerName = parameter.getCustomerName();

		List<User> salesUserList;
		UserParameter userParameter = new UserParameter();
		userParameter.setName(name);
		userParameter.setCustomerName(customerName);

		userParameter.setNotCustomer(true);
		try {
			salesUserList = userService.getUserList(userParameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, salesUserList,ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * AS 담당자 리스트 조회
	 * @return
	 */
	@PostMapping("/user/api/getAsUserList")
	public ResponseData<Object> getAsUserList(@RequestBody User parameter) {

		String name = parameter.getName();
		String customerName = parameter.getCustomerName();

		List<User> asUserList;
		UserParameter userParameter = new UserParameter();
		userParameter.setName(name);
		userParameter.setCustomerName(customerName);

		// 유지보수 권한
		userParameter.setNotCustomer(true);
		try {
			asUserList = userService.getUserList(userParameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, asUserList,ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 사용자관리 비밀번호 변경 기능
	 * @return
	 */
	@PostMapping("/user/api/editUserPw")
	public ResponseData<Object> editUserPw(@RequestBody EditUserParameter parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			queryResult = userService.editUserPw(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null,ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 토큰을 통해 사용자정보 조회
	 * @return
	 */
	@PostMapping("/user/api/getUserByToken")
	public ResponseData<Object> editUserPw(@RequestBody User parameter) {
		User user;
		try {
			user = userService.getUserBySessionOrToken(parameter.getToken());

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		user.setPassword("");
		return new ResponseData<Object>(true, user,ResponseUtil.DATA_LOAD_SUC);
	}


	/**
	 * ID 찾기
	 * @return
	 */
	@PostMapping("/user/api/userFindId")
	@GuestAccess
	public ResponseData<Object> userFindId(@RequestBody User parameter) {
		User user;
		try {
			user = userService.getUserFindId(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, user , ResponseUtil.DATA_LOAD_SUC);
	}


	/**
	 * 사용자관리 리스트 조회
	 * @return
	 */
	@PostMapping("/user/excel/getUserList")
	public void downloadUserList(HttpServletResponse response, @RequestBody UserParameter parameter) {
		List<User> userList = null;
		try {
			userList = userService.getUserList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), userList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * 비밀번호 변경
	 * @return
	 */
	@PostMapping("/user/api/changePassword")
	public ResponseData<Object> changePassword(@RequestBody User parameter) {
		try {
			boolean result = userLoginService.changePassword(parameter);
			if(!result){
				throw new Exception();
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null , ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 마이페이지 - 회원정보 수정
	 */
	@PostMapping("/user/api/modifyMyInfo")
	public ResponseData<Object> modifyMyInfo(@RequestBody User parameter, Errors error) {

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			// 사용자마스터랑 같은 로직을 사용하기위해서 값 셋팅해줌--------------------------------
			// (해당값들을 업데이트하거나 체크를 하기때문에, 이 셋팅으로 변경되거나 하는건 없음)
			User user= userService.getUserBySessionOrToken(SessionUtil.getToken());
			parameter.setUseYn(user.getUseYn());
			parameter.setEtc(user.getEtc());
			parameter.setUserSeq(user.getUserSeq());
			ValidStringUtil.phoneNumberFormatChk(parameter.getPhone(), "휴대폰번호");
			//--------------------------------------------------------------------------------------

			boolean result = userService.setUser(parameter);
			if(!result){
				throw new Exception();
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null,ResponseUtil.DATA_SAVE_SUC);
	}


}
