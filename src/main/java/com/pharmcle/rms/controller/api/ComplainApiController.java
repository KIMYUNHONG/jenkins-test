/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.complain.Complain;
import com.pharmcle.rms.domain.complain.ComplainHandle;
import com.pharmcle.rms.domain.complain.ComplainParameter;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/manage")
public class ComplainApiController {

	@Autowired
	ComplainService complainService;
	@Autowired
	UserService userService;
	@Autowired
	UploadFileService uploadFileService;

	/**
	 * 민원 리스트 조회
	 * @return
	 */
	@PostMapping("/complain/api/getComplainList")
	public ResponseData<Object> getComplainList(@RequestBody ComplainParameter parameter) {
		int listCount;
		List<Complain> complainList;
		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}

			complainList = complainService.getComplainList(parameter);
			listCount = complainService.getComplainListCount(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, complainList, ResponseUtil.DATA_LOAD_SUC,listCount);
	}

	/**
	 * 민원조회(단건)
	 * @return
	 */
	@PostMapping("/complain/api/getComplain")
	public ResponseData<Object> getComplain(@RequestBody ComplainParameter parameter) {

		Complain complain;

		try {
			if(parameter.getComplainSeq() == 0 ){
				throw new ValidException("조회할 대상이 없습니다.");
			}
			complain = complainService.getComplain(parameter);
			List<String> fileList = uploadFileService.getUploadFileKeyListByDomain(UploadFileService.COMPLAIN, parameter.getComplainSeq());
			List<ComplainHandle> handleList = complainService.getComplainHandle(parameter);
			complain.setHandlingList(handleList);
			complain.setUploadFileKey(fileList);

		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, complain, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 민원 저장
	 * @return
	 */
	@PostMapping("/complain/api/setComplain")
	public ResponseData<Object> setComplain(@RequestBody Complain parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;
		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			queryResult = complainService.setComplain(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("complainSeq", parameter.getComplainSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 민원 접수
	 * @return
	 */
	@PostMapping("/complain/api/setReceiptComplain")
	public ResponseData<Object> setReceiptComplain(@RequestBody Complain parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = complainService.setReceiptComplain(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("complainSeq", parameter.getComplainSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 민원 처리완료
	 * @return
	 */
	@PostMapping("/complain/api/setFinishComplain")
	public ResponseData<Object> setFinishComplain(@RequestBody Complain parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = complainService.setFinishComplain(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("complainSeq", parameter.getComplainSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 민원처리 등록
	 * @return
	 */
	@PostMapping("/complain/api/setComplainHandle")
	public ResponseData<Object> setComplainHandle(@RequestBody Complain parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = complainService.setComplainHandle(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("complainSeq", parameter.getComplainSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 민원 리스트 조회 - 엑셀다운로드
	 * @return
	 */
	@PostMapping("/complain/excel/getComplainList")
	public void downloadComplainList(HttpServletResponse response, @RequestBody ComplainParameter parameter) {
		List<Complain> complainList;
		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}

			complainList = complainService.getComplainList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), complainList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
