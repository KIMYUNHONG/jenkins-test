/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatusService;
import com.pharmcle.rms.domain.installRequest.*;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("install")
public class InstallRequestApiController {

	@Autowired
	DeviceService deviceService;

	@Autowired
	InstallRequestService installRequestService;

	@Autowired
	DeviceStatusService deviceStatusService;

	@Autowired
	UserService userService;

	/**
	 * 장비설치요청 리스트 조회
	 * @return
	 */
	@PostMapping("/installRequest/api/getInstallRequestList")
	public ResponseData<Object> getInstallRequestList(@RequestBody InstallRequestParameter parameter) {

		int listCount;
		List<InstallRequest> requestList;

		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthUserId(user.getUserId());
			}

			requestList = installRequestService.getInstallRequestList(parameter);
			listCount = installRequestService.getInstallRequestListCount(parameter);

		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, requestList, ResponseUtil.DATA_LOAD_SUC,listCount);
	}

	/**
	 * 장비설치요청 리스트 조회(모바일)
	 * @return
	 */
	@PostMapping("/installRequest/api/getMobileInstallRequestList")
	public ResponseData<Object> getMobileInstallRequestList(@RequestBody InstallRequestParameter parameter) {

		int listCount;
		List<InstallRequestListMobile> requestList;

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthUserId(user.getUserId());
			}

			requestList = installRequestService.getMobileInstallRequestList(parameter);
			listCount = installRequestService.getInstallRequestListCount(parameter);

		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, requestList, ResponseUtil.DATA_LOAD_SUC,listCount);
	}

	/**
	 * 장비설치요청 상세
	 * @param
	 * @return
	 */
	@PostMapping("/installRequest/api/getInstallRequestDetail")
	public ResponseData<Object> getInstallRequestDetail(@RequestBody InstallRequestParameter parameter) {
		InstallRequest request;

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthUserId(user.getUserId());
			}

			request = installRequestService.getInstallRequestDetail(parameter);
			List<Device> deviceList = deviceService.getInstallRequestDeviceList(new InstallRequestParameter(parameter.getInstallRequestSeq()));
			request.setDeviceList(deviceList);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, request, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비설치요청 상세(모바일)
	 * @param
	 * @return
	 */
	@PostMapping("/installRequest/api/getMobileInstallRequestDetail")
	public ResponseData<Object> getMobileInstallRequestDetail(@RequestBody InstallRequestParameter parameter) {
		InstallRequestDetailMobile request;

		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			request = installRequestService.getMobileInstallRequestDetail(parameter);
			List<Device> deviceList = deviceService.getInstallRequestDeviceList(new InstallRequestParameter(parameter.getInstallRequestSeq()));
			request.setDeviceList(deviceList);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, request, ResponseUtil.DATA_LOAD_SUC);
	}


	/**
	 * 장비설치요청 저장
	 *
	 *  @return
	 */
	@PostMapping("/installRequest/api/setInstallRequest")
	public ResponseData<Object> setInstallRequest(@RequestBody InstallRequest parameter, Errors errors) {
		if (errors.hasErrors()) {
			String errorMessage = errors.getAllErrors().get(0).getDefaultMessage();
			System.out.println(errorMessage);
		}

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			int installRequestSeq = installRequestService.setInstallRequest(parameter);
			result.put("installRequestSeq", installRequestSeq);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 설치요청접수 처리
	 *
	 *  @return
	 */
	@PostMapping("/installRequest/api/setReceiptInstallRequest")
	public ResponseData<Object> setReceiptInstallRequest(@RequestBody InstallRequest parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = installRequestService.setReceiptInstallRequest(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("installRequestSeq", parameter.getInstallRequestSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 설치완료처리
	 *
	 *  @return
	 */
	@PostMapping("/installRequest/api/setCompleteInstallRequest")
	public ResponseData<Object> setCompleteInstallRequest(@RequestBody InstallRequest parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = installRequestService.setCompleteInstallRequest(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("installRequestSeq", parameter.getInstallRequestSeq());

		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 확인완료처리
	 *
	 *  @return
	 */
	@PostMapping("/installRequest/api/setFinishInstallRequest")
	public ResponseData<Object> setFinishInstallRequest(@RequestBody InstallRequest parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = installRequestService.setFinishInstallRequest(parameter);
			if(!queryResult){
				throw new Exception();
			}
			result.put("installRequestSeq", parameter.getInstallRequestSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 - 설치장비등록
	 * @return
	 */
	@PostMapping("/installRequest/api/setInstallRequestDevice")
	public ResponseData<Object> setInstallRequestDevice(@RequestBody Device parameter) {

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			parameter.setStatus(Device.STATUS_INSTALLING); // DM020007 : 설치중(장비상태)
			parameter.setInstallRequestStatus(InstallRequest.STATUS_INSTALLING); // IM050003 : 설치중(설치요청상태)
			if (!deviceService.installRequestDevice(parameter)) {
				throw new Exception();
			}
			result.put("installRequestSeq", parameter.getInstallRequestSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 - 장비설치요청 삭제처리
	 *
	 *  @return
	 */
	@PostMapping("/installRequest/api/deleteInstallRequest")
	public ResponseData<Object> deleteInstallRequest(@RequestBody InstallRequest parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = installRequestService.deleteInstallRequest(parameter);
			if(!queryResult){
				throw new Exception();
			}
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 - 설치장비 삭제처리
	 *  @return
	 */
	@PostMapping("/installRequest/api/deleteInstallRequestDevice")
	public ResponseData<Object> deleteInstallRequestDevice(@RequestBody Device parameter, Errors error) {
		Map<String, Object> result = new HashMap<>();
		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = deviceService.deleteInstallRequestDevice(parameter);
			result.put("installRequestSeq",parameter.getInstallRequestSeq());
			if(!queryResult){
				throw new Exception();
			}
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비설치요청 리스트 조회 - 엑셀다운로드
	 * @return
	 */
	@PostMapping("/installRequest/excel/getInstallRequestList")
	public void downloadInstallRequestList(@RequestBody InstallRequestParameter parameter, HttpServletResponse response) {
		boolean result = true;
		List<InstallRequest> requestList;

		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthUserId(user.getUserId());
			}
			requestList = installRequestService.getInstallRequestList(parameter);

			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), requestList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
