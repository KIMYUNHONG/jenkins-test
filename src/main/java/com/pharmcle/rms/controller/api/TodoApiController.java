/**
 * Create By Lee DoWon on 2020-03-23 / 오후 2:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/todo")
public class TodoApiController {

	@Autowired
	TodoService todoService;

	@Autowired
	UserService userService;

	/**
	 * TO-DO 리스트 조회
	 * @return
	 */
	@PostMapping("/api/getTodoList")
	public ResponseData<Object> getTodoList(HttpServletRequest request, @RequestBody Todo parameter) {
		List<Todo> todoList;
		try {
			todoList = todoService.getTodoList(parameter);
		} catch (ValidException e) {
			log.info(e.getMessage());
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, todoList, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * TO-DO 확인 처리
	 * @return
	 */
	@PostMapping("/api/setTodoViewUserId")
	public ResponseData<Object> setTodoViewUserId(HttpServletRequest request, @RequestBody Todo parameter) {
		boolean result;
		try {
			result = todoService.setViewUserId(parameter.getDomainStatus(), parameter.getDomainSeq());
			if(result == false){
				return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, true, ResponseUtil.DATA_SAVE_SUC);
	}

}
