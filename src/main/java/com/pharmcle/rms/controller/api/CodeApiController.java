/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.Code;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.codeGroup.CodeGroup;
import com.pharmcle.rms.domain.codeGroup.CodeGroupParameter;
import com.pharmcle.rms.domain.codeGroup.CodeGroupService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/standard")
public class CodeApiController {

	@Autowired
	CodeGroupService codeGroupService;

	@Autowired
	CodeService codeService;

	@Autowired
	UserService userService;

	/**
	 * 코드 그룹 리스트 조회
	 * @return
	 */
	@PostMapping("/code/api/getCodeGroupList")
	public ResponseData<Object> getCodeGroupList(@RequestBody CodeGroupParameter parameter) {
		List<CodeGroup> codeGroupList = null;
		try {
			codeGroupList = codeGroupService.getCodeGroupList(parameter);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, codeGroupList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 코드그룹을 통한 코드 조회
	 * @return
	 */
	@GuestAccess
	@PostMapping("/code/api/getCodeListByGroupCode")
	public ResponseData<Object> getCodeListByGroupCode(@RequestBody CodeParameter parameter) {
		List<Code> codeGroupList = null;
		try {
			codeGroupList = codeService.getCodeListByGroupCode(parameter);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, codeGroupList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 코드그룹 리스트를 통한 코드 조회
	 * @return
	 */
	@GuestAccess
	@PostMapping("/code/api/getCodeListByGroupCodeList")
	public ResponseData<Object> getCodeListByGroupCodeList(HttpServletRequest request, @RequestBody CodeParameter parameter) {
		Map<String, Object> codeList = null;
		try {
			codeList = codeService.getCodeListByGroupCodeList(parameter);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, codeList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 코드그룹 저장 기능
	 * @return
	 */
	@PostMapping("/code/api/setCodeGroup")
	public ResponseData<Object> setCodeGroup(@RequestBody CodeGroupParameter parameter, Errors error) {
		boolean queryResult;
		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = codeGroupService.setCodeGroup(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 코드 저장 기능
	 * @return
	 */
	@PostMapping("/code/api/setCode")
	public ResponseData<Object> setCode(@RequestBody CodeParameter parameter, Errors error) {
		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		boolean queryResult = false;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = codeService.setCode(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}
}
