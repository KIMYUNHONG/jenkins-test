/**
 * Create By Lee DoWon on 2020-03-23 / 오후 2:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/manage")
public class MaintainApiController {

	@Autowired
	MaintainService maintainService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	UserService userService;

	/**
	 * 유지보수 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/maintain/api/getMaintainList")
	public ResponseData<Object> getMaintainList(HttpServletRequest request, @RequestBody Maintain parameter) {
		List<Maintain> maintainList;
		int maintainListCount;
		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			maintainList = maintainService.getMaintainList(parameter);
			maintainListCount = maintainService.getMaintainListCount(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, maintainList, ResponseUtil.DATA_LOAD_SUC, maintainListCount);
	}

	/**
	 * 유지보수 등록/수정
	 *
	 * @return
	 */
	@PostMapping("/maintain/api/setMaintain")
	public ResponseData<Object> setMaintain(HttpServletRequest request, @RequestBody Maintain parameter) {
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			boolean result = maintainService.setMaintain(parameter);
			if (!result) {
				throw new Exception();
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 유지보수 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/maintain/api/getMaintainListByDevice")
	public ResponseData<Object> getMaintainListByDevice(HttpServletRequest request, @RequestBody Maintain parameter) {

		List<Maintain> maintainList;
		try {
			List<String> maintainRegYearList = maintainService.getMaintainAndComplainRegYear(parameter);
			String defaultYear = (maintainRegYearList != null && maintainRegYearList.size() > 0) ?
				maintainRegYearList.get(0) : String.valueOf(LocalDate.now().getYear());
			parameter.setSearchYear(StringUtil.nvl(parameter.getSearchYear(), defaultYear));
			parameter.setSearchType(StringUtil.nvl(parameter.getSearchType(), Maintain.ORDER_BY_DESC));

			maintainList = maintainService.getMaintainAndComplainList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, maintainList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 유지보수 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/maintain/excel/getMaintainList")
	@ResponseBody
	public void downloadMaintainList(HttpServletResponse response, @RequestBody Maintain parameter) {
		List<Maintain> maintainList;
		try {
			maintainList = maintainService.getMaintainList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), maintainList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * 유지보수 보고서 메일발송
	 *
	 * @return
	 */
	@PostMapping("/maintain/api/maintainReportSendMail")
	public ResponseData<Object> maintainReportSendMail(HttpServletRequest request, @RequestBody Maintain parameter) {
		try {

			boolean result = maintainService.maintainReportSendMail(parameter);

			if (!result) {
				throw new Exception();
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}


}
