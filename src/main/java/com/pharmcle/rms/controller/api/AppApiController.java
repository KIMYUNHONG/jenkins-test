/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.app.App;
import com.pharmcle.rms.domain.app.AppParameter;
import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/standard")
public class AppApiController {

	@Autowired
	AppService appService;
	@Autowired
	UserService userService;

	/**
	 * APP 관리 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/app/api/getAppList")
	public ResponseData<Object> getAppList(@RequestBody AppParameter parameter) {
		List<App> appList = null;
		try {
			appList = appService.getAppList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, appList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * App 관리 조회(단건)
	 *
	 * @return
	 */
	@PostMapping("/app/api/getApp")
	public ResponseData<Object> getApp(@RequestBody AppParameter parameter) {

		App app = null;
		try {
			if (parameter.getAppSeq() == 0) {
				throw new Exception("조회할 대상이 없습니다.");
			}
			app = appService.getApp(parameter.getAppSeq());
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, app, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * App 관리 저장 기능
	 *
	 * @return
	 */
	@PostMapping("/app/api/setApp")
	public ResponseData<Object> setApp(@RequestBody App parameter, Errors error) {

		boolean queryResult;

		if (error.hasErrors()) {
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = appService.setApp(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}


}


