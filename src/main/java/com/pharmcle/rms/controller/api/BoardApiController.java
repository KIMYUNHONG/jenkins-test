/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.board.Board;
import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/home")
public class BoardApiController {

	@Autowired
	BoardService boardService;
	@Autowired
	UserService userService;

	/**
	 * 공지사항 리스트 조회
	 * @return
	 */
	@PostMapping("/notice/api/getNoticeList")
	public ResponseData<Object> getNoticeList(@RequestBody BoardParameter parameter) {
		boolean result = true;
		List<Board> boardList = null;

		try {
			boardList = boardService.getNoticeList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, boardList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 공지사항 상세조회
	 * @return
	 */
	@PostMapping("/notice/api/getNotice")
	public ResponseData<Object> getNotice(@RequestBody BoardParameter parameter) {

		Board board = null;
		try {
			if(parameter.getBoardSeq() == 0 ){
				throw new Exception("조회할 대상이 없습니다.");
			}
			board = boardService.getNotice(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, board, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 공지사항 저장 기능
	 * @return
	 */
	@PostMapping("/notice/api/setNotice")
	public ResponseData<Object> setNotice(@RequestBody Board parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = boardService.setNotice(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * board 삭제(단건)
	 * @return
	 */
	@PostMapping("/board/api/deleteBoard")
	public ResponseData<Object> deleteBoard(@RequestBody Board parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = boardService.deleteBoard(parameter);

		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_DELETE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_DELETE_SUC);
	}


	/**
	 * 자료실 리스트 조회
	 * @return
	 */
	@PostMapping("/archive/api/getArchiveList")
	public ResponseData<Object> getArchiveList(@RequestBody BoardParameter parameter) {
		boolean result = true;
		List<Board> boardList = null;

		try {
			boardList = boardService.getArchiveList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, boardList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 자료실 상세조회
	 * @return
	 */
	@PostMapping("/archive/api/getArchive")
	public ResponseData<Object> getArchive(@RequestBody BoardParameter parameter) {

		Board board = null;
		try {
			if(parameter.getBoardSeq() == 0 ){
				throw new Exception("조회할 대상이 없습니다.");
			}
			board = boardService.getArchive(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, board, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 자료실 저장 기능
	 * @return
	 */
	@PostMapping("/archive/api/setArchive")
	public ResponseData<Object> setArchive(@RequestBody Board parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = boardService.setArchive(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}


	/**
	 * FAQ 리스트 조회
	 * @return
	 */
	@PostMapping("/faq/api/getFaqList")
	public ResponseData<Object> getFaqList(@RequestBody BoardParameter parameter) {
		boolean result = true;
		List<Board> boardList = null;

		int listCount = 0;

		try {
			boardList = boardService.getFaqList(parameter);
			listCount = boardService.getFaqListCount(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, boardList, ResponseUtil.DATA_LOAD_SUC,listCount);
	}

	/**
	 * FAQ 상세조회
	 * @return
	 */
	@PostMapping("/faq/api/getFaq")
	public ResponseData<Object> getFaq(@RequestBody BoardParameter parameter) {

		Board board = null;
		try {
			if(parameter.getBoardSeq() == 0 ){
				throw new Exception("조회할 대상이 없습니다.");
			}
			board = boardService.getFaq(parameter);

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, board, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * FAQ 저장 기능
	 * @return
	 */
	@PostMapping("/faq/api/setFaq")
	public ResponseData<Object> setFaq(@RequestBody Board parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = boardService.setFaq(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

}
