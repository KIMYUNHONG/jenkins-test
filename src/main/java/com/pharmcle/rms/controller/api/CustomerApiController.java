/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.customer.Customer;
import com.pharmcle.rms.domain.customer.CustomerParameter;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("/customer")
public class CustomerApiController {

	@Autowired
	CustomerService customerService;

	@Autowired
	UserService userService;

	/**
	 * 전체 거래처 리스트 조회
	 *
	 * @return
	 */
	@GuestAccess
	@PostMapping("/customer/api/getAllCustomerList")
	public ResponseData<Object> getAllCustomerList(@RequestBody CustomerParameter parameter) {
		List<Customer> customerList;
		try {
			customerList = customerService.getCustomerList(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, customerList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 거래처 리스트 조회
	 *
	 * @return
	 */
	@GuestAccess
	@PostMapping("/customer/api/getCustomerList")
	public ResponseData<Object> getCustomerList(@RequestBody CustomerParameter parameter) {
		List<Customer> customerList;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			customerList = customerService.getCustomerList(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, customerList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 거래처 담당자 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/customer/api/getChargeList")
	public ResponseData<Object> getChargeList(@RequestBody CustomerParameter parameter) {
		List<User> chargeList;
		try {
			chargeList = customerService.getChargeList(parameter.getCustomerSeq());
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, chargeList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 거래처/ 거래처 담당자 등록
	 *
	 * @return
	 */
	@PostMapping("/customer/api/setCustomer")
	public ResponseData<Object> setCustomer(@RequestBody Customer customer, Errors errors) {
		if (errors.hasErrors()) {
			String errorMessage = errors.getAllErrors().get(0).getDefaultMessage();
			log.error(errorMessage);
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}

		Map<String, Object> result = new HashMap<>();

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			int customerSeq = customerService.setCustomer(customer);
			result.put("customerSeq", customerSeq);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_SAVE_SUC);
	}
	/**
	 * 거래처 담당자 가입승인
	 *
	 * @return
	 */
	@PostMapping("/customer/api/setChargeConfirm")
	public ResponseData<Object> setChargeConfirm(@RequestBody User parameter, Errors errors) {
		if (errors.hasErrors()) {
			String errorMessage = errors.getAllErrors().get(0).getDefaultMessage();
			log.error(errorMessage);
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}

		boolean result;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			result = userService.setUserConfirm(parameter.getUserSeq());
			if(!result){
				throw new Exception();
			}
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 거래처 리스트 엑셀 다운로드
	 *
	 * @return
	 */
	@PostMapping("/customer/excel/getCustomerList")
	public void excelDownload(@RequestBody CustomerParameter parameter, HttpServletResponse response) {
		List<Customer> customerList;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			customerList = customerService.getCustomerList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), customerList, response);
		} catch (Exception e){
			log.error("excelDownload", e);
		}
	}

	/**
	 * 거래처 수정 - 마이페이지
	 *
	 * @return
	 */
	@PostMapping("/customer/api/setMyCustomer")
	public ResponseData<Object> setMyCustomer(@RequestBody Customer customer, Errors errors) {
		if (errors.hasErrors()) {
			String errorMessage = errors.getAllErrors().get(0).getDefaultMessage();
			log.error(errorMessage);
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			customer.setCustomerSeq(user.getCustomerSeq());
			boolean result = customerService.setMyCustomer(customer);
			if(!result){
				throw new Exception();
			}
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}
}
