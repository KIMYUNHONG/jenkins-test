/**
 * Create By Lee DoWon on 2020-03-23 / 오후 2:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.app.App;
import com.pharmcle.rms.domain.app.AppParameter;
import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@Slf4j
@RestController
@RequestMapping("/common")
public class CommonApiController {

	@Autowired
	AppService appService;
	@Autowired
	UploadFileService uploadFileService;


	/**
	 * APP 버전 조회
	 * @return
	 */
	@GuestAccess
	@PostMapping("/app/api/versionCheck")
	public ResponseData<Object> getAppList(@RequestBody AppParameter parameter) {
		App app = null;
		try {
			app = appService.getAppVersionCheck(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, app, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * APP 버전 조회
	 * @return
	 */
	@GuestAccess
	@GetMapping("/app/api/iosAppDownload")
	public ResponseEntity<Resource> iosAppDownload(HttpServletRequest request) {
		try {
			App app = appService.getAppVersionCheck(new AppParameter("SD090002"));
			UploadFile uploadFile = uploadFileService.getUploadFile(app.getAppFileKey());
			File target = new File(uploadFile.getFilePath());

			// Try to determine file's conte	nt type
			String contentType = null;
			Resource resource = new UrlResource(target.toURI());
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());    // Fallback to the default content type if type could not be determined
			if (contentType == null) {
				contentType = "application/octet-stream";
			}
			HttpHeaders header = new HttpHeaders();
			header.setContentType(MediaType.parseMediaType(contentType));
			header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+ app.getAppFileName() +"\"");
			return new ResponseEntity<Resource>(resource, header, HttpStatus.OK);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return null;
		}
	}
}
