/**
 * Create By Lee DoWon on 2020-03-31 / 오후 1:13
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.areaCharge.AreaCharge;
import com.pharmcle.rms.domain.areaCharge.AreaChargeService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/standard")
public class AreaChargeApiController {

	@Autowired
	AreaChargeService areaChargeService;
	@Autowired
	UserService userService;


	/**
	 * 지역별 담당자 리스트 조회 (지역별담당자관리 메뉴)
	 * @param parameter
	 * @return
	 */
	@PostMapping("/areaCharge/api/getAreaChargeList")
	public ResponseData<Object> getAreaChargeList(@RequestBody AreaCharge parameter) {
		List<AreaCharge> areaChargeList;

		try {
			areaChargeList = areaChargeService.getAreaChargeList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, areaChargeList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 지역별 담당자 등록/수정 (지역별담당자관리 메뉴)
	 * @param parameter
	 * @return
	 */
	@PostMapping("/areaCharge/api/setAreaChargeList")
	public ResponseData<Object> setAreaChargeList(@RequestBody AreaCharge parameter) {

		int result = 0;
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			result = areaChargeService.setAreaChargeList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, result, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 지역명(시/도, 시/군/구)을 통한 해당 지역 담당자 조회
	 * parameter
	 * - areaText : 시/도명
	 * - areaDetailText : 시/군/구명
 	 */

	@PostMapping("/areaCharge/api/getAreaChargeByAreaName")
	public ResponseData<Object> getAreaChargeByAreaName(@RequestBody AreaCharge parameter) {

		AreaCharge areaCharge;
		try {
			areaCharge = areaChargeService.getAreaChargeByAreaName(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, areaCharge, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 지역별 담당자 리스트 조회 (지역별담당자관리 메뉴) - 엑셀다운로드
	 * @param parameter
	 * @return
	 */
	@PostMapping("/areaCharge/excel/getAreaChargeList")
	public void downloadAreaChargeList(HttpServletResponse response, @RequestBody AreaCharge parameter) {
		List<AreaCharge> areaChargeList;

		try {
			areaChargeList = areaChargeService.getAreaChargeList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), areaChargeList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
