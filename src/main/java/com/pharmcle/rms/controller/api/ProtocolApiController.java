/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.protocol.Protocol;
import com.pharmcle.rms.domain.protocol.ProtocolParameter;
import com.pharmcle.rms.domain.protocol.ProtocolService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/standard")
public class ProtocolApiController {

	@Autowired
	ProtocolService protocolService;
	@Autowired
	UserService userService;

	/**
	 * 통신기준정보관리 리스트 조회
	 * @return
	 */
	@PostMapping("/protocol/api/getProtocolList")
	public ResponseData<Object> getProtocolList(@RequestBody ProtocolParameter parameter) {
		boolean result = true;
		List<Protocol> protocolList;

		try {
			protocolList = protocolService.getProtocolList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, protocolList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 통신기준정보관리 저장 기능
	 * @return
	 */
	@PostMapping("/protocol/api/setProtocol")
	public ResponseData<Object> setProtocol(@RequestBody Protocol parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}
		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = protocolService.setProtocol(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

}
