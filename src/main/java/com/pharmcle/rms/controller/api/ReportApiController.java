/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.report.Report;
import com.pharmcle.rms.domain.report.ReportParameter;
import com.pharmcle.rms.domain.report.ReportService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/report")
public class ReportApiController {

	@Autowired
	ReportService reportService;
	@Autowired
	UserService userService;

	/**
	 * 장비재고분석 리스트 조회
	 * @return
	 */
	@PostMapping("/deviceStock/api/getDeviceStockList")
	public ResponseData<Object> getDeviceStockList(@RequestBody ReportParameter parameter) {
		List<Report> reportList;
		try {
			reportList = reportService.getDeviceStockList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, reportList, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 거래처분석 리스트 조회
	 * @return
	 */
	@PostMapping("/customer/api/getCustomerList")
	public ResponseData<Object> getCustomerList(@RequestBody ReportParameter parameter) {
		List<Report> reportList;
		try {
			reportList = reportService.getCustomerList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, reportList, ResponseUtil.DATA_LOAD_SUC);
	}
	
	/**
	 * 유지보수분석 리스트 조회
	 * @return
	 */
	@PostMapping("/maintain/api/getMaintainList")
	public ResponseData<Object> getMaintainList(@RequestBody ReportParameter parameter) {
		List<Report> reportList;

		try {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}

			reportList = reportService.getMaintainList(parameter);
			for(Report rp : reportList) {
				rp.setComplainText(rp.getComplainHandleCnt() + "/" + rp.getComplainTotalCnt());
			}
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, reportList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 일별업무내역
	 * @return
	 */
	@PostMapping("/today/api/getTodayList")
	public ResponseData<Object> getTodayList(@RequestBody ReportParameter parameter) {
		List<Report> reportList;
		try {
			reportList = reportService.selectTodayList(parameter);
			if(reportList != null && reportList.size() > 0){
				for(Report rp : reportList) {
					rp.setComplainText(rp.getComplainHandleCnt() + "/" + rp.getComplainTotalCnt());
				}
			}

		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, reportList, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 일별업무내역 상태별 카운트
	 * @return
	 */
	@PostMapping("/today/api/getTodayStatusCount")
	public ResponseData<Object> getTodayStatusCount(@RequestBody ReportParameter parameter) {
		Report report;
		try {
			report = reportService.getTodayStatusCount(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, report, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비재고분석 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/deviceStock/excel/getDeviceStockList")
	public void downloadDeviceStockList(@RequestBody ReportParameter parameter, HttpServletResponse response) {
		List<Report> reportList;
		try {
			reportList = reportService.getDeviceStockList(parameter);
			Boolean mergedRegionYn = true;
			int groupValue[] = {3, 12, 7};	// RowSpan개수, ColSpan개수, ColSpan당 Cell개수
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), reportList,  response, mergedRegionYn, groupValue);
		} catch (Exception e){
			log.error(e.getMessage());
		}
	}
	
	/**
	 * 거래처분석 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/customer/excel/getCustomerList")
	public void downloadCustomerList(@RequestBody ReportParameter parameter, HttpServletResponse response) {
		List<Report> reportList;
		try {
			reportList = reportService.getCustomerList(parameter);
			Boolean mergedRegionYn = true;
			int tabType = Integer.parseInt(parameter.getTabType());
			int[] groupValue = {tabType, 12, 5};	// RowSpan개수, ColSpan개수, ColSpan당 Cell개수
			
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), reportList,  response, mergedRegionYn, groupValue);
		} catch (Exception e){
			log.error(e.getMessage());
		}
	}
	
	/**
	 * 유지보수분석 리스트 조회 - 엑셀다운로드
	 *
	 * @return
	 */
	@PostMapping("/maintain/excel/getMaintainList")
	public void downloadMainTainList(@RequestBody ReportParameter parameter, HttpServletResponse response) {
		List<Report> reportList;
		try {
			reportList = reportService.getMaintainList(parameter);
			for(Report rp : reportList) {
				rp.setComplainText(rp.getComplainHandleCnt() + "/" + rp.getComplainTotalCnt());
			}
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), reportList,  response);
		} catch (Exception e){
			log.error(e.getMessage());
		}
	}
	
}
