/**
 * Create By Lee DoWon on 2020-03-07 / 오전 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileInsertParameter;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.FileUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.ValidStringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class UploadFileApiController {

	@Value("${file.upload-dir}")
	private String uploadDir;

	@Autowired
	UploadFileService uploadFileService;

	@PostMapping("/uploadFile/api/upload")
	public ResponseData<Object> upload(UploadFileInsertParameter parameter) {
		UploadFile uploadFile;
		try {
			uploadFile = uploadFileService.upload(parameter, false);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, uploadFile, ResponseUtil.DATA_LOAD_SUC);
	}

	@PostMapping("/uploadFile/api/photoUpload")
	public ResponseData<Object> photoUpload(UploadFileInsertParameter parameter) {
		UploadFile uploadFile;
		try {
			uploadFile = uploadFileService.upload(parameter, true);
			FileUtil.makeThumbnail((uploadDir + DateUtil.localDateToString(LocalDate.now(), DateUtil.DATE_NO_FORMAT)), uploadFile.getFilePath(), uploadFile.getServerFileName(), FileUtil.getFileFormat(uploadFile.getServerFileName()));
			FileUtil.imageResizing(uploadFile.getFilePath(), FileUtil.getFileFormat(uploadFile.getServerFileName()));
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, uploadFile, ResponseUtil.DATA_LOAD_SUC);
	}

	@PostMapping("/uploadFile/api/uploadList")
	public ResponseData<Object> uploadList(UploadFileInsertParameter parameter, HttpServletRequest request) {
		List<UploadFile> uploadFileList = new ArrayList<>();
		try {
			for(MultipartFile multipartFile : parameter.getFileList()){
				ValidStringUtil.pictureFileChk(multipartFile.getOriginalFilename());
				UploadFile uploadFile = uploadFileService.upload(new UploadFileInsertParameter(multipartFile), true);
				uploadFileList.add(uploadFile);
				FileUtil.makeThumbnail((uploadDir + DateUtil.localDateToString(LocalDate.now(), DateUtil.DATE_NO_FORMAT)), uploadFile.getFilePath(), uploadFile.getServerFileName(), FileUtil.getFileFormat(uploadFile.getServerFileName()));
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.FILE_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, uploadFileList, ResponseUtil.FILE_SAVE_SUC);
	}

	@GuestAccess
	@GetMapping("/uploadFile/api/download/{uploadFileKey}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String uploadFileKey, HttpServletRequest request){
		try {
			UploadFile uploadFile = uploadFileService.getUploadFile(uploadFileKey);
			File target = new File(uploadFile.getFilePath());

			// Try to determine file's conte	nt type
			String contentType = null;
			Resource resource = new UrlResource(target.toURI());
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());	// Fallback to the default content type if type could not be determined
			if(contentType == null) {
				contentType = "application/octet-stream";
			}
			HttpHeaders header = new HttpHeaders();
			header.setContentType(MediaType.parseMediaType(contentType));
			header.set("Content-Disposition", "attachment;filename=\"" + uploadFile.getOriginalFileName() + "\";");
			header.set("Content-Transfer-Encoding", "binary");
			return new ResponseEntity<Resource>(resource, header, HttpStatus.OK);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return null;
		}
	}
	@GuestAccess
	@GetMapping("/uploadFile/api/thumbnailDownload/{uploadFileKey}")
	public ResponseEntity<Resource> downloadThumbnailFile(@PathVariable String uploadFileKey, HttpServletRequest request){
		try {
			UploadFile uploadFile = uploadFileService.getUploadFile(uploadFileKey);
			File target = new File(uploadFile.getThumbnailFilePath());

			// Try to determine file's conte	nt type
			String contentType = null;
			Resource resource = new UrlResource(target.toURI());
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());	// Fallback to the default content type if type could not be determined
			if(contentType == null) {
				contentType = "application/octet-stream";
			}
			HttpHeaders header = new HttpHeaders();
			String mimeType = Files.probeContentType(Paths.get(target.getAbsolutePath()));
			header.setContentType(MediaType.parseMediaType(mimeType));
			return new ResponseEntity<Resource>(resource, header, HttpStatus.OK);
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return null;
		}
	}

}
