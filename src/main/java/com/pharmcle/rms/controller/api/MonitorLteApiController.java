/**
 * Create By jwh on 2020-04-07 / 오후 5:30
 * Email : jwh.19@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.monitorLte.MonitorLte;
import com.pharmcle.rms.domain.monitorLte.MonitorLteService;
import com.pharmcle.rms.util.ResponseUtil;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/monitor")
public class MonitorLteApiController {

	@Autowired
	MonitorLteService monitorLteService; 
	
	/**
	 * 모니터 Lte 장비 로그 저장
	 * @return
	 */
	@GuestAccess
	@PostMapping("/monitorLte/api/setLteDeviceLog")
	public ResponseData<Object> setLteDeviceLog(@RequestBody Map<String, Object> parameter, Errors error) {
		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			queryResult = monitorLteService.setLteData(parameter);
		} catch (ValidException e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}

		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}
}

