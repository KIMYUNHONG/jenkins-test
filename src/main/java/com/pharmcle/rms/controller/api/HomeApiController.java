/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.dashboard.Area;
import com.pharmcle.rms.domain.dashboard.AreaParameter;
import com.pharmcle.rms.domain.dashboard.Dashboard;
import com.pharmcle.rms.domain.dashboard.DashboardService;
import com.pharmcle.rms.domain.device.DashboardDeviceModel;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/home")
public class HomeApiController {

	@Autowired
	DashboardService dashboardService;

	@Autowired
	UserService userService;

	/**
	 * 웹 대시보드 - 재고현황 차트 데이터 조회
	 * @return
	 */
	@PostMapping("/dashboard/api/getDeviceStatusCnt")
	public ResponseData<Object> getDeviceStatusCnt(@RequestBody Dashboard parameter) {

		List<Dashboard> dashboardList = null;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			dashboardList = dashboardService.getDeviceStatusCnt(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, dashboardList, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 웹 대시보드 - 거래처별 현황 차트 데이터 조회
	 * @return
	 */
	@PostMapping("/dashboard/api/getCustomerCnt")
	public ResponseData<Object> getCustomerCnt(@RequestBody Dashboard parameter) {

		List<Dashboard> dashboard = null;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			dashboard = dashboardService.getCustomerCnt(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, dashboard, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 다음맵용 디바이스 리스트 조회
	 * @return
	 */
	@PostMapping("/dashboard/api/getDashboardDeviceList")
	public ResponseData<Object> getDashboardDeviceList(@RequestBody SelectInstallDeviceParameter parameter) {

		List<DashboardDeviceModel> deviceList;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			deviceList = dashboardService.getDashboardDeviceList(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 다음맵용 디바이스 리스트 전체 카운트 조회
	 * @return
	 */
	@PostMapping("/dashboard/api/getDashboardDeviceListCnt")
	public ResponseData<Object> getDashboardDeviceListCnt(@RequestBody SelectInstallDeviceParameter parameter) {

		int deviceListCnt = 0;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			deviceListCnt = dashboardService.getDashboardDeviceListCnt(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceListCnt, ResponseUtil.DATA_LOAD_SUC);
	}
	/**
	 * 다음맵용 시/도, 구군별 카운트 조회
	 * @return
	 */
	@PostMapping("/dashboard/api/getDeviceCntByArea")
	public ResponseData<Object> getDeviceCntByArea(@RequestBody AreaParameter parameter) {

		List<Area> areaList = null;
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}
			areaList = dashboardService.getDeviceCntByArea(parameter);
		} catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, areaList, ResponseUtil.DATA_LOAD_SUC);
	}
}
