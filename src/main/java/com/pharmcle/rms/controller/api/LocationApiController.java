/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.location.Location;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/customer")
public class LocationApiController {

	@Autowired
	LocationService locationService;

	@Autowired
	UserService userService;

	/**
	 * 설치장소 리스트 조회
	 *
	 * @return
	 */
	@PostMapping("/location/api/getLocationList")
	public ResponseData<Object> getLocationList(@RequestBody LocationParameter parameter) {
		List<Location> locationList = null;
		try {
			locationList = locationService.getLocationList(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, locationList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 거래처 담당자 리스트 조회
	 * @param parameter 검색조건
	 * @param errors 에러
	 * @return
	 */
	@PostMapping("/location/api/setLocation")
	public ResponseData<Object> setLocation(@RequestBody Location parameter, Errors errors) {
		if (errors.hasErrors()) {
			String errorMessage = errors.getAllErrors().get(0).getDefaultMessage();
			System.out.println(errorMessage);
		}

		boolean result;
		try {
			result = locationService.setLocation(parameter);
			if(!result) throw new Exception();
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 설치장소 리스트 엑셀 다운로드
	 *
	 * @return
	 */
	@PostMapping("/location/excel/getLocationList")
	public void downloadLocationList(@RequestBody LocationParameter parameter, HttpServletResponse response) {
		List<Location> locationList;
		try {
			locationList = locationService.getLocationList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), locationList,  response);
		} catch (Exception e){
			log.error(e.getMessage());
		}
	}
}
