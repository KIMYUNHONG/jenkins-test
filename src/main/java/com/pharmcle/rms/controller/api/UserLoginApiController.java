/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.loginLog.LoginLogService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserLoginService;
import com.pharmcle.rms.domain.user.UserParameter;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.ServletUtil;
import com.pharmcle.rms.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Slf4j
@RestController
public class UserLoginApiController {

	@Autowired
	UserLoginService userLoginService;

	@Autowired
	LoginLogService loginLogService;

	@Autowired
	UserService userService;

	@GuestAccess
	@PostMapping("/api/login")
	public ResponseData<Object> userLoginPost(
		Model model, HttpServletRequest request,
		@RequestBody UserParameter userParameter,
		HttpSession session) {
		
		String referer = SessionUtil.getReferer(); // 로그인 이전페이지 확인
		
		UserParameter parameter = new UserParameter(userParameter.getUserId(), userParameter.getPassword());

		boolean result = true;
		User user;
		try {
			user = userLoginService.getUserByUserIdAndPassword(parameter);
		} catch (ValidException e) {
			return new ResponseData<>(false, null, e.getMessage());
		} catch (Exception e ){
			return new ResponseData<>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		HashMap<String, Object> body = new HashMap<>();

		// 모바일인경우 토큰도 발급
		if(ServletUtil.isMobile(request)){
			String token = SessionUtil.generateToken();
			body.put("token", token);
			parameter.setToken(token);
			user.setToken(token);
			userLoginService.updateToken(parameter);
		}
		// 세션에 로그인 계정 정보 저장
		ServletUtil.getRequest().getSession().invalidate();
		session.setMaxInactiveInterval(86400);
		SessionUtil.setUserId(user.getUserId());
		SessionUtil.setUser(user);

		try {
			// 접속 로그 추가
			loginLogService.addLoginLog(parameter);
		} catch (Exception e){
			log.error(e.getMessage());
		}
		// 패스워드 틀린횟수 0으로 초기화
		parameter.setPasswordFailCnt(0);
		userLoginService.setPasswordFailCnt(parameter);
		
		if(referer != null) { // 이전페이지 있을 경우 
			body.put("returnUrl", referer);
		} else {
			body.put("returnUrl", "/");
		}
		
		return new ResponseData<Object>(true, body, ResponseUtil.DATA_LOGIN_SUC);
	}

	@PostMapping("/api/loginLog")
	public ResponseData<Object> userLoginPost() {
		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(user == null){
				throw new Exception();
			}
			UserParameter userParameter = new UserParameter();
			userParameter.setUserId(user.getUserId());
			userParameter.setToken(user.getToken());
			loginLogService.addLoginLog(userParameter);
		} catch (ValidException e) {
			return new ResponseData<>(true, null, ResponseUtil.DATA_SAVE_SUC);
		} catch (Exception e ){
			log.error(e.getMessage());
			return new ResponseData<>(true, null, ResponseUtil.DATA_SAVE_SUC);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

}
