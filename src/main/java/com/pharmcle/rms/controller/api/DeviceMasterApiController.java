/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.api;

import com.pharmcle.rms.base.ResponseData;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.deviceMaster.DeviceMaster;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterParameter;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ExcelUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/standard")
public class DeviceMasterApiController {

	@Autowired
	DeviceMasterService deviceMasterService;

	@Autowired
	UserService userService;

	/**
	 * 장비마스터 리스트 조회
	 * @return
	 */
	@PostMapping("/deviceMaster/api/getDeviceMasterList")
	public ResponseData<Object> getDeviceMasterList(@RequestBody DeviceMasterParameter parameter) {
		boolean result = true;
		List<DeviceMaster> deviceMasterList = null;
		try {
			deviceMasterList = deviceMasterService.getDeviceList(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, e.getMessage());
		}
		return new ResponseData<Object>(true, deviceMasterList, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비마스터 조회(단건)
	 * @return
	 */
	@PostMapping("/deviceMaster/api/getDeviceMaster")
	public ResponseData<Object> getDeviceMaster(@RequestBody DeviceMasterParameter parameter) {

		int deviceMstSeq = parameter.getDeviceMstSeq();

		DeviceMaster deviceMaster = null;
		try {
			if(deviceMstSeq == 0 ){
				throw new Exception("조회할 대상이 없습니다.");
			}
			deviceMaster = deviceMasterService.getDevice(deviceMstSeq);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_LOAD_FAIL);
		}
		return new ResponseData<Object>(true, deviceMaster, ResponseUtil.DATA_LOAD_SUC);
	}

	/**
	 * 장비마스터 저장 기능
	 * @return
	 */
	@PostMapping("/deviceMaster/api/setDeviceMaster")
	public ResponseData<Object> setDeviceMaster(@RequestBody DeviceMaster parameter, Errors error) {

		boolean queryResult;

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
			return new ResponseData<Object>(false, null, errorMessage);
		}

		try {
			ValidAuthUtil.validNotAccessCustomer(userService.getAuthroleBySessionOrToken(SessionUtil.getToken()));
			queryResult = deviceMasterService.setDeviceMaster(parameter);
		}catch (ValidException e) {
			return new ResponseData<Object>(false, null, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseData<Object>(false, null, ResponseUtil.DATA_SAVE_FAIL);
		}
		return new ResponseData<Object>(true, null, ResponseUtil.DATA_SAVE_SUC);
	}

	/**
	 * 장비마스터 리스트 조회 - 엑셀다운로드
	 * @return
	 */
	@PostMapping("/deviceMaster/excel/getDeviceMasterList")
	public void downloadDeviceMasterList(HttpServletResponse response, @RequestBody DeviceMasterParameter parameter) {
		List<DeviceMaster> deviceMasterList = null;
		try {
			deviceMasterList = deviceMasterService.getDeviceList(parameter);
			ExcelUtil.downloadExcelByData(parameter.getGridColumnList(), deviceMasterList, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
