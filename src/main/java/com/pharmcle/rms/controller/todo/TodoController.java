/**
 * Create By Lee DoWon on 2020-04-22 / 오후 3:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.todo;

import com.pharmcle.rms.domain.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("TodoController")
@RequestMapping("/todo")
public class TodoController {


	@Autowired
	CustomerService customerService;

	private final static String BASE_URI = "/todo";

	// TO-DO 리스트
	@GetMapping("/todo/todoList")
	public String todoList(Model model) {

		return BASE_URI + "/todo/todoList";
	}

}
