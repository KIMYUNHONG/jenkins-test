/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.monitor;

import com.pharmcle.rms.domain.dashboard.Dashboard;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterParameter;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("MapController")
@RequestMapping("/monitor")
public class MapController {

	@Autowired
	UserService userService;

	@Autowired
	LocationService locationService;

	@Autowired
	DeviceMasterService deviceMasterService;

	private final static String BASE_URL = "/monitor";

	@GetMapping("/map/monitorMapList")
	public String monitorMapList(Model model , HttpServletResponse response) {

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			LocationParameter locationParameter = new LocationParameter();
			Dashboard parameter = new Dashboard();

			// 대리점인경우 대리점에 해당하는 데이터만 받기위해서 체크
			if (ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)) {
				locationParameter.setAuthCustomerSeq(user.getCustomerSeq());
				locationParameter.setAuthCustomerType(user.getCustomerType());
				parameter.setAuthCustomerSeq(user.getCustomerSeq());
				parameter.setAuthCustomerType(user.getCustomerType());
			}

			model.addAttribute("locationList", locationService.getLocationList(locationParameter));
			model.addAttribute("deviceMasterList", deviceMasterService.getDeviceList(new DeviceMasterParameter()));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/map/monitorMapList";
	}
}
