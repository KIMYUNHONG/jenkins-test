/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.monitor;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.monitor.Monitor;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("MonitorController")
@RequestMapping("/monitor")
public class MonitorController {
	
	@Autowired
	CodeService codeService;

	@Autowired
	MonitorService monitorService;

	@Autowired
	DeviceService deviceService;

	private final static String BASE_URL = "/monitor";

	@GetMapping("/monitor/monitorList")
	public String monitorList(Model model , Monitor parameter) {
		
		log.debug("monitorList : Start");
		
		// 지역
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));
		// 거래처
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));
		// 통신방식
		model.addAttribute("DM01", codeService.getCodeListByGroupCode(new CodeParameter("DM01")));
		// 전원
		//model.addAttribute("MT01", codeService.getCodeListByGroupCode(new CodeParameter("MT01")));
		// 상태
		model.addAttribute("MT02", codeService.getCodeListByGroupCode(new CodeParameter("MT02")));
		// 통신
		//model.addAttribute("MT03", codeService.getCodeListByGroupCode(new CodeParameter("MT03")));
		//검색조건
		model.addAttribute("parameter", parameter);

		return BASE_URL + "/monitor/monitorList";
	}

	@GetMapping("/monitor/monitorDetail")
	public String monitorDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "") int deviceSeq) {

		Monitor monitor = null;
		Device device = null;

		try {
			monitor = monitorService.getMonitorDetail(new Monitor(deviceSeq));
			device = deviceService.getDevice(new DeviceParameter(deviceSeq));

			// 일반 장비 일경우
			if (monitor.getReceiveDate() == null) { // 일반장비는 마지막 데이터 수신을 인스톨 일자로 설정
				log.info("일반 장비 일경우");
				String installDt = device.getInstallDtText();
				monitor.setReceiveDateText(installDt);
				monitor.setNomal(1);
			}

		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("device", device);
		model.addAttribute("monitor", monitor);

		return BASE_URL + "/monitor/monitorDetail";
	}

	// 통신기준정보관리 등록 팝업
	@GetMapping("/monitor/addProtocolPopup")
	public String addProtocolPopup(Model model, HttpServletResponse response,
								   @RequestParam(required = false, defaultValue = "SD020001") String deviceClassify,
								   @RequestParam(required = false, defaultValue = "0") int deviceSeq) {

		Monitor monitor;
		Device device;
		String url = "";

		//SD020001 : 포충기 , SD020002 : 기피제 분사함
		if("SD020001".equals(deviceClassify)){
			url = "/monitor/addProtocol1Popup";
		}else{
			url = "/monitor/addProtocol2Popup";
		}

		try {
			monitor = monitorService.getDeviceProtocol(new Monitor(deviceSeq));
			device = deviceService.getDevice(new DeviceParameter(deviceSeq));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("protocol", monitor); // protocol
		model.addAttribute("device", device);    // device
		model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
		model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간

		return BASE_URL + url;
	}

	// 테스트
	@GetMapping("/monitor/setLoraLogUpdate")
	public String setLoraTest1(Model model) throws Exception {
		
		log.info("setLoraTest1 ");
		
		try {
			monitorService.setLoRaData("T");
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return BASE_URL + "/monitor/monitorLog";
	}
}
