/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.home;

import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.complain.ComplainParameter;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.dashboard.Dashboard;
import com.pharmcle.rms.domain.dashboard.DashboardService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterParameter;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("DashboardController")
@RequestMapping("/home")
public class DashboardController {

	@Autowired
	UserService userService;

	@Autowired
	CustomerService customerService;

	@Autowired
	LocationService locationService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	DeviceMasterService deviceMasterService;

	@Autowired
	MaintainService maintainService;

	@Autowired
	CodeService codeService;

	@Autowired
	BoardService boardService;

	@Autowired
	ComplainService complainService;

	@Autowired
	DashboardService dashboardService;

	private final static String BASE_URI = "/home";

	@GetMapping("/dashboard/dashboard")
	public String dashboard(Model model, HttpServletResponse response) {

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());

			// 거래처용 대시보드 (로그인 유저 권한이 '거래처담당')
			if(ValidAuthUtil.isCustomerAuth(user)){

				// 1. 파라미터 생성
				LocationParameter locationParameter = new LocationParameter();
				locationParameter.setCustomerSeq(user.getCustomerSeq());
				ComplainParameter complainParameter = new ComplainParameter();

				BoardParameter boardParameter = new BoardParameter();
				boardParameter.setStartIndex(0);
				boardParameter.setIndexCount(3);

				Dashboard dashboardParameter = new Dashboard();
				dashboardParameter.setAuthCustomerSeq(user.getCustomerSeq());
				dashboardParameter.setAuthCustomerType(user.getCustomerType());

				// 2. 데이터 담기

				// 2-1. 민원신청 전체개수
				model.addAttribute("complainTotalCnt", complainService.getComplainListCount(complainParameter));

				// 2-2. 민원신청 완료개수
				List<String> status = new ArrayList<>();
				status.add("MR030003");
				complainParameter.setStatusList(status);
				model.addAttribute("complainHandleCnt", complainService.getComplainListCount(complainParameter));

				// 2-3. 설치장소 리스트
				model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
				model.addAttribute("locationList", locationService.getLocationList(locationParameter));

				// 2-4. 본인 거래처정보
				model.addAttribute("customer", customerService.getCustomer(user.getCustomerSeq()));

				// 2-5. 대시보드 거래처수, 설치장소, 설치장비수 민원신청/처리 토탈개수
				model.addAttribute("totalCnt", dashboardService.getTotalCnt(dashboardParameter));

				// 2-6. 공지사항 3개, 지식인 3개
				model.addAttribute("noticeList", boardService.getNoticeList(boardParameter));
				model.addAttribute("archiveList", boardService.getArchiveList(boardParameter));

				model.addAttribute("deviceMasterList", deviceMasterService.getDeviceList(new DeviceMasterParameter()));
				return BASE_URI + "/dashboard/customerDashboard";

				// 그 외 팜클, 대리점인경우
			} else {

				// 1. 파라미터 생성
				BoardParameter boardParameter = new BoardParameter();
				boardParameter.setStartIndex(0);
				boardParameter.setIndexCount(3);

				LocationParameter locationParameter = new LocationParameter();
				Dashboard parameter = new Dashboard();

				// 대리점인경우 대리점에 해당하는 데이터만 받기위해서 체크
				if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
					locationParameter.setAuthCustomerSeq(user.getCustomerSeq());
					locationParameter.setAuthCustomerType(user.getCustomerType());
					parameter.setAuthCustomerSeq(user.getCustomerSeq());
					parameter.setAuthCustomerType(user.getCustomerType());
				}

				model.addAttribute("noticeList", boardService.getNoticeList(boardParameter));
				model.addAttribute("areaCntList", dashboardService.getAreaCnt(parameter));
				model.addAttribute("totalCnt", dashboardService.getTotalCnt(parameter));
				model.addAttribute("userList", dashboardService.getUserList(parameter));
				model.addAttribute("archiveList", boardService.getArchiveList(boardParameter));
				model.addAttribute("locationList", locationService.getLocationList(locationParameter));
				model.addAttribute("deviceMasterList", deviceMasterService.getDeviceList(new DeviceMasterParameter()));
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URI + "/dashboard/dashboard";
	}


	@GetMapping("/dashboard/maintainReportPopup")
	public String maintainReportPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int locationSeq) {
		try {

			List<Device> deviceList;
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));

			for (Device item : deviceList) {
				item.setMaintainList(maintainService.getMaintainAndComplainList(new Maintain(item.getDeviceSeq())));
			}

			model.addAttribute("deviceList", deviceList);
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URI + "/dashboard/maintainReportPopup";
	}

	@GetMapping("/dashboard/maintainReportSendMailPopup")
	public String maintainReportSendMailPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int locationSeq) {

		try {
			List<Device> deviceList;
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));

			for (Device item : deviceList) {
				item.setMaintainList(maintainService.getMaintainAndComplainList(new Maintain(item.getDeviceSeq())));
			}

			model.addAttribute("deviceList", deviceList);
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URI + "/dashboard/maintainReportSendMailPopup";
	}

	@GetMapping("/dashboard/locationDeviceListPopup")
	public String locationDeviceListPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int locationSeq) {
		try {
			model.addAttribute("location", locationService.getLocation(locationSeq));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URI + "/dashboard/locationDeviceListPopup";
	}

}
