/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.home;

import com.pharmcle.rms.domain.board.Board;
import com.pharmcle.rms.domain.board.BoardParameter;
import com.pharmcle.rms.domain.board.BoardService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("BoardController")
@RequestMapping("/home")
public class BoardController {

	@Autowired
	BoardService boardService;
	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	CodeService codeService;

	private final static String BASE_URI = "/home";



	@GetMapping("/notice/noticeList")
	public String noticeList(Model model, BoardParameter parameter) {
		model.addAttribute("parameter", parameter); // 검색조건
		return BASE_URI + "/notice/noticeList";
	}

	@GetMapping("/notice/noticeWrite")
		public String noticeWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board = new Board();
		List<UploadFile> uploadFileList = new ArrayList<>();

		try {

			if(boardSeq > 0) {
				board = boardService.getNotice(new BoardParameter(boardSeq));
			}

			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.NOTICE, boardSeq);
			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);

		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 공지대상
		return BASE_URI + "/notice/noticeWrite";
	}



	@GetMapping("/notice/noticeDetail")
	public String noticeDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board;
		List<UploadFile> uploadFileList;

		try {

			boolean boardCnt  = boardService.updateCnt(new BoardParameter(boardSeq));

			board = boardService.getNotice(new BoardParameter(boardSeq));
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.NOTICE, boardSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

			if (board == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 공지대상
		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);
		return BASE_URI + "/notice/noticeDetail";
	}

	@GetMapping("/archive/archiveList")
	public String archiveList(Model model , BoardParameter parameter) {
		model.addAttribute("SC07", codeService.getCodeListByGroupCode(new CodeParameter("SC07")));	// 자료실 구분
		model.addAttribute("parameter", parameter);	// 검색조건
		return BASE_URI + "/archive/archiveList";
	}

	@GetMapping("/archive/archiveWrite")
	public String archiveWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board = new Board();
		List<UploadFile> uploadFileList = new ArrayList<>();

		try {
			if(boardSeq > 0) {
				board = boardService.getArchive(new BoardParameter(boardSeq));
			}
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.ARCHIVE, boardSeq);
			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);

		model.addAttribute("SC07", codeService.getCodeListByGroupCode(new CodeParameter("SC07")));	// 자료실 구분
		return BASE_URI + "/archive/archiveWrite";
	}



	@GetMapping("/archive/archiveDetail")
	public String archiveDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board;
		List<UploadFile> uploadFileList;

		try {

			boolean boardCnt  = boardService.updateCnt(new BoardParameter(boardSeq));

			board = boardService.getArchive(new BoardParameter(boardSeq));
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.ARCHIVE, boardSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

			if (board == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);
		return BASE_URI + "/archive/archiveDetail";
	}



	@GetMapping("/faq/faqList")
	public String faqList(Model model) {
		model.addAttribute("SC07", codeService.getCodeListByGroupCode(new CodeParameter("SC07")));	// 자료실 구분
		return BASE_URI + "/faq/faqList";
	}

	@GetMapping("/faq/faqWrite")
	public String faqWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board = new Board();
		List<UploadFile> uploadFileList = new ArrayList<>();

		try {
			if(boardSeq > 0) {
				board = boardService.getFaq(new BoardParameter(boardSeq));
			}
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.FAQ, boardSeq);
			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);

		model.addAttribute("SC07", codeService.getCodeListByGroupCode(new CodeParameter("SC07")));	// 자료실 구분
		return BASE_URI + "/faq/faqWrite";
	}



	@GetMapping("/faq/faqDetail")
	public String faqDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int boardSeq) {
		Board board;
		List<UploadFile> uploadFileList;

		try {

			boolean boardCnt  = boardService.updateCnt(new BoardParameter(boardSeq));

			board = boardService.getFaq(new BoardParameter(boardSeq));
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.FAQ, boardSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

			if (board == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("board", board);
		model.addAttribute("uploadFileList", uploadFileList);
		return BASE_URI + "/faq/faqDetail";
	}
}
