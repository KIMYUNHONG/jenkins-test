/**
 * Create By Lee DoWon on 2020-02-16 / 오후 10:07
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.app.App;
import com.pharmcle.rms.domain.app.AppParameter;
import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.location.Location;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("CommonController")
@RequestMapping("/common")
public class CommonController {

	@Value("${site.url}")
	private String siteUrl;

	@Value("${manifest.uri}")
	private String manifestUri;

	@Autowired
	CodeService codeService;

	@Autowired
	AppService appService;

	@Autowired
	LocationService locationService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	DeviceService deviceService;

	private final static String BASE_URL = "/common";

	@GuestAccess
	@GetMapping("/plist")
	public String plist(Model model) {

		App ios = new App();
		try {
			ios = appService.getAppVersionCheck(new AppParameter("SD090002"));
		} catch (Exception e){
			log.error(e.getMessage());
		}
		model.addAttribute("version", ios.getVersionText());    // 거래처 구분
		model.addAttribute("downloadUrl", siteUrl + ios.getAppFilePath());    // 거래처 구분
		return BASE_URL + "/plist";
	}

	@GuestAccess
	@GetMapping("/error")
	public String error(Model model) {
		return BASE_URL + "/error";
	}

	@GetMapping("/searchAddressPopup")
	public String searchAddressPopup(Model model) {
		return BASE_URL + "/searchAddressPopup";
	}

	@GetMapping("/salesUserListPopup")
	public String salesUserListPopup(Model model) {
		return BASE_URL + "/salesUserListPopup";
	}

	@GetMapping("/asUserListPopup")
	public String asUserListPopup(Model model) {
		return BASE_URL + "/asUserListPopup";
	}

	@GetMapping("/imageSliderPopup")
	public String imageSliderPopup(Model model, @RequestParam String domain, @RequestParam int key,  @RequestParam(defaultValue = "0") int key2, @RequestParam(defaultValue = "0") int index) {

		try {
			List<UploadFile> uploadFileList = uploadFileService.getUploadFileListByDomain(domain, key, key2);

			model.addAttribute("uploadFileList", uploadFileList);
			model.addAttribute("defaultIndex", index);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return BASE_URL + "/imageSliderPopup";
	}

	@GetMapping("/deviceListPopup")
	public String deviceListPopup(Model model) {

		model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));    // 장비구분
		model.addAttribute("SD05", codeService.getCodeListByGroupCode(new CodeParameter("SD05")));    // 제조사

		return BASE_URL + "/deviceListPopup";
	}

	@GetMapping("/customerListPopup")
	public String customerListPopup(Model model) {

		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
		return BASE_URL + "/customerListPopup";
	}

	@GuestAccess
	@GetMapping("/appDownload")
	public String appDownload(Model model) {
		App android = new App();
		App ios = new App();
		try {
			android = appService.getAppVersionCheck(new AppParameter("SD090001"));
		} catch (Exception e){
			log.error(e.getMessage());
		}
		model.addAttribute("android", android);
		model.addAttribute("ios", ios);
		model.addAttribute("plistUrl", siteUrl + manifestUri);

		return BASE_URL + "/appDownload";
	}

	@GetMapping("/locationListPopup")
	public String locationListPopup(Model model) {

		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처구분

		return BASE_URL + "/locationListPopup";
	}

	@GetMapping("/mobileMarkerMap")
	public String mobileMarkerMap(Model model,
								  @RequestParam(value = "lat", defaultValue = "37.53105539245015") BigDecimal lat,
								  @RequestParam(value = "lng", defaultValue = "126.98392330433975") BigDecimal lng,
								  @RequestParam(value = "viewFlag", defaultValue = "false") boolean viewFlag,
								  @RequestParam(value = "token", defaultValue = "false") String token
	) {
		model.addAttribute("lat", lat);
		model.addAttribute("lng", lng);
		model.addAttribute("token", token);
		model.addAttribute("viewFlag", viewFlag);

		return BASE_URL + "/mobileMarkerMap";
	}

	/**
	 * @param model
	 * @param lat
	 * @param lng
	 * @return
	 */
	@GetMapping("/addressMarkerMapPopup")
	public String addressMarkerMapPopup(Model model,
										@RequestParam(value = "lat", defaultValue = "37.53105539245015") BigDecimal lat,
										@RequestParam(value = "lng", defaultValue = "126.98392330433975") BigDecimal lng,
										@RequestParam(value = "address", defaultValue = "") String address
	) {
		model.addAttribute("lat", lat);
		model.addAttribute("lng", lng);
		model.addAttribute("address", address);

		return BASE_URL + "/addressMarkerMapPopup";
	}

	/**
	 * @param model
	 * @param lat
	 * @param lng
	 * @return
	 */
	@GetMapping("/latlngMarkerMapPopup")
	public String latlngMarkerMapPopup(Model model,
									   @RequestParam(value = "lat", defaultValue = "37.53105539245015") BigDecimal lat,
									   @RequestParam(value = "lng", defaultValue = "126.98392330433975") BigDecimal lng
	) {
		model.addAttribute("lat", lat);
		model.addAttribute("lng", lng);
		return BASE_URL + "/latlngMarkerMapPopup";
	}

	@GetMapping("/mobileClustererMap")
	public String mobileClustererMap(Model model, HttpServletResponse response, @RequestParam(defaultValue = "0") int locationSeq,
									 @RequestParam(defaultValue = "0") int deviceSeq,
									 @RequestParam(defaultValue = "") String token
	) {
		String address = "서울시 중구";
		try {
			Location location = locationService.getLocation(locationSeq);
			address = location != null ? location.getAddress() : "서울시 중구";
		} catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("locationSeq", locationSeq);
		model.addAttribute("deviceSeq", deviceSeq);
		model.addAttribute("address", address);
		model.addAttribute("token", token);
		return BASE_URL + "/mobileClustererMap";
	}

	@GetMapping("/mobileLocationDevice")
	public String mobileLocationDevice(Model model, HttpServletResponse response, @RequestParam(defaultValue = "0") int locationSeq, @RequestParam(defaultValue = "") String token) {

		if (StringUtil.isEmpty(token)) {
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		if (locationSeq == 0) {
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		List<Device> deviceList;
		try {
			deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));
			if (deviceList != null) {
				char index = 65;
				for (Device item : deviceList) {
					item.setAlphabet(String.valueOf(index));
					index++;
				}
			}
			model.addAttribute("deviceList", deviceList);
			model.addAttribute("token", token);
			model.addAttribute("location", locationService.getLocation(locationSeq));
		} catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/mobileLocationDevice";
	}


	@GetMapping("/downloadFilePopup")
	public String downloadFilePopup(HttpServletResponse response, Model model, String domain, int domainSeq, @RequestParam(defaultValue = "0") int domainSeq2) {

		List<UploadFile> uploadFileList = new ArrayList<>();
		try {

 			uploadFileList = uploadFileService.getUploadFileListByDomain(domain, domainSeq, domainSeq2);
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("uploadFileList", uploadFileList);
		return "/common/downloadFilePopup";
	}

}

