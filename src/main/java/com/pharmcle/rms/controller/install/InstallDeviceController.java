/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:33
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.install;

import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.*;
import com.pharmcle.rms.domain.deviceMaster.DeviceMaster;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatus;
import com.pharmcle.rms.domain.deviceStatus.DeviceStatusService;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("InstallDeviceController")
@RequestMapping("/install")
public class InstallDeviceController {

	@Autowired
	CodeService codeService;

	@Autowired
	DeviceMasterService deviceMasterService;

	@Autowired
	DeviceService deviceService;
	@Autowired
	DeviceStatusService deviceStatusService;

	@Autowired
	LocationService locationService;

	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	AddpartService addpartService;

	private final static String BASE_URL = "/install";

	@GetMapping("/installDevice/installDeviceList")
	public String installDeviceList(Model model , SelectInstallDeviceParameter parameter) {

		model.addAttribute("IM01", codeService.getCodeListByGroupCode(new CodeParameter("IM01")));    // 기간구분
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처구분
		model.addAttribute("DM01", codeService.getCodeListByGroupCode(new CodeParameter("DM01")));    // 통신방식
		model.addAttribute("DM02", codeService.getCodeListByGroupCode(new CodeParameter("DM02")));    // 장비상태
		model.addAttribute("IM02", codeService.getCodeListByGroupCode(new CodeParameter("IM02")));    // 사용자구분
		model.addAttribute("parameter", parameter);

		return BASE_URL + "/installDevice/installDeviceList";
	}

	@GetMapping("/installDevice/installDeviceDetail")
	public String installDeviceDetail(HttpServletResponse response, Model model, @RequestParam(value = "deviceSeq", required = false, defaultValue = "0") int deviceSeq) {

		try {
			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));
			List<UploadFile> beforeFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, deviceSeq);
			List<UploadFile> afterFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, deviceSeq);

			model.addAttribute("device", device);
			model.addAttribute("beforeFileList", beforeFileList);
			model.addAttribute("afterFileList", afterFileList);
			model.addAttribute("deviceStatusList", deviceStatusService.getDeviceStatusList(new DeviceStatus(deviceSeq)));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/installDevice/installDeviceDetail";
	}

	//장비규격 팝업
	@GetMapping("/installDevice/deviceStandardPopup")
	public String deviceStandardPopup(HttpServletResponse response , Model model, @RequestParam(value = "deviceMstSeq", required = false, defaultValue = "0") int deviceMstSeq,
									  @RequestParam(value = "deviceSeq", required = false, defaultValue = "0") int deviceSeq) {

		try {
			DeviceMaster deviceMaster = deviceMasterService.getDevice(deviceMstSeq);

			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));

			deviceMaster = deviceMaster == null ? new DeviceMaster() : deviceMaster;

			device = device == null ? new Device() : device;

			model.addAttribute("deviceMaster", deviceMaster);
			model.addAttribute("device", device);
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/installDevice/deviceStandardPopup";
	}

	@GetMapping("/installDevice/deviceViewAllPopup")
	public String deviceViewAllPopup(HttpServletResponse response,Model model, @RequestParam int locationSeq) {

		try {
			List<Device> deviceList = deviceService.getInstallDeviceList(new SelectInstallDeviceParameter(locationSeq));
			if (deviceList != null) {
				char index = 65;
				for (Device item : deviceList) {
					item.setAlphabet(String.valueOf(index));
					index++;
				}
			}

			model.addAttribute("deviceList", deviceList);
			model.addAttribute("location", locationService.getLocation(locationSeq));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/installDevice/deviceViewAllPopup";
	}

	@GetMapping("/installDevice/installDeviceWrite")
	public String installDeviceWrite(HttpServletResponse response, Model model, @RequestParam(value = "deviceSeq", required = false, defaultValue = "0") int deviceSeq) {

		Device device = new Device();
		List<UploadFile> beforeFileList = new ArrayList<>();
		List<UploadFile> afterFileList = new ArrayList<>();

		//수정일 경우
		if(deviceSeq > 0){
			try{
				device = deviceService.getDevice(new DeviceParameter(deviceSeq));
				beforeFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, deviceSeq);
				afterFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, deviceSeq);
			}catch (Exception e){
				log.error(e.getMessage());
				ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			}
		}

		model.addAttribute("device", device);
		model.addAttribute("beforeFileList", beforeFileList);
		model.addAttribute("afterFileList", afterFileList);
		model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
		model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간
		return BASE_URL + "/installDevice/installDeviceWrite";
	}

	@GetMapping("/installDevice/installedDeviceWritePopup")
	public String installedDeviceWritePopup(HttpServletResponse response,Model model, @RequestParam(required = true, defaultValue = "0") int deviceSeq) {

		try {
			model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
			model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간
			model.addAttribute("device", deviceService.getDevice(new DeviceParameter(deviceSeq)));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/installDevice/installedDeviceWritePopup";
	}

	@GetMapping("/installDevice/changeStatusPopup")
	public String changeStatusPopup(HttpServletResponse response,Model model, @RequestParam(required = true, defaultValue = "0") int deviceSeq) {

		try {
			model.addAttribute("device", deviceService.getDevice(new DeviceParameter(deviceSeq)));
			model.addAttribute("DM02", codeService.getCodeListByGroupCode(new CodeParameter("DM02")));    // 장비상태
			model.addAttribute("DM03", codeService.getCodeListByGroupCode(new CodeParameter("DM03")));    // 보관장소
			model.addAttribute("IM07", codeService.getCodeListByGroupCode(new CodeParameter("IM07")));    // 폐기종류
			model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
			model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간


		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/installDevice/changeStatusPopup";
	}

	@GetMapping("/installDevice/addPartListPopup")
	public String addPartListPopup(Model model, HttpServletResponse response,
								   @RequestParam String domainName,
								   @RequestParam(defaultValue="0") int domainSeq,
								   @RequestParam(defaultValue="0") int domainSeq2) {

		List<Addpart> addpartList = new ArrayList<>();

		try {
			addpartList = addpartService.getAddpartList(new Addpart(domainName, domainSeq, domainSeq2, Addpart.INSTALL_ADDPART_IM03));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("addpartList", addpartList);

		return BASE_URL + "/installDevice/addPartListPopup";
	}
}
