/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:33
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.install;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.installRequest.InstallRequest;
import com.pharmcle.rms.domain.installRequest.InstallRequestParameter;
import com.pharmcle.rms.domain.installRequest.InstallRequestService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.DateUtil;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("InstallRequestController")
@RequestMapping("/install")
public class InstallRequestController {

	private final static String BASE_URL = "/install";

	@Autowired
	CodeService codeService;
	@Autowired
	DeviceService deviceService;
	@Autowired
	InstallRequestService installRequestService;

	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	AddpartService addpartService;

	/**
	 * 장비설치요청 리스트
	 * @param model
	 * @return
	 */
	@GetMapping("/installRequest/installRequestList")
	public String installRequestList(Model model , InstallRequestParameter parameter) {

		model.addAttribute("IM04", codeService.getCodeListByGroupCode(new CodeParameter("IM04")));    // 기간구분
		model.addAttribute("IM05", codeService.getCodeListByGroupCode(new CodeParameter("IM05")));    // 설치상태
		model.addAttribute("IM06", codeService.getCodeListByGroupCode(new CodeParameter("IM06")));    // 설치구분
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
		model.addAttribute("IM02", codeService.getCodeListByGroupCode(new CodeParameter("IM02")));    // 설치관리 담당자
		model.addAttribute("parameter", parameter);    // 검색조건

		return BASE_URL + "/installRequest/installRequestList";
	}

	/**
	 * 장비설치요청 상세
	 * @param model
	 * @return
	 */
	@GetMapping("/installRequest/installRequestDetail")
	public String installRequestDetail(HttpServletResponse response, Model model, @RequestParam(value = "installRequestSeq", required = false, defaultValue = "0") int installRequestSeq) {

		try {
			InstallRequest request = installRequestService.getInstallRequestDetail(new InstallRequestParameter(installRequestSeq));
			List<Device> deviceList = deviceService.getInstallRequestDeviceList(new InstallRequestParameter(installRequestSeq));

			model.addAttribute("request", request);
			model.addAttribute("deviceList", deviceList);

		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/installRequest/installRequestDetail";
	}

	/**
	 * 장비설치요청 등록/수정
	 * @param model
	 * @return
	 */
	@GetMapping("/installRequest/installRequestWrite")
	public String installRequestWrite(HttpServletResponse response, Model model, @RequestParam(value = "installRequestSeq", required = false, defaultValue = "0") int installRequestSeq) {
		InstallRequest request = new InstallRequest();
		if(installRequestSeq > 0){
//			수정시 설치요정 DATA 조회해서 return
			try {
				request = installRequestService.getInstallRequestDetail(new InstallRequestParameter(installRequestSeq));
			} catch (Exception e) {
				log.error(e.getMessage());
				ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			}
		}

		model.addAttribute("request", request);
		model.addAttribute("IM06", codeService.getCodeListByGroupCode(new CodeParameter("IM06")));    // 설치구분

		return BASE_URL + "/installRequest/installRequestWrite";

	}

	/**
	 * 장비설치요청 설치장비 등록/수정
	 * @param model
	 * @return
	 */
	@GetMapping("/installRequest/installRequestDeviceWrite")
	public String installDeviceWrite(HttpServletResponse response, Model model,
									 @RequestParam(value = "installRequestSeq", required = false, defaultValue = "0") int installRequestSeq,
									 @RequestParam(value = "deviceSeq", required = false, defaultValue = "0") int deviceSeq,
									@RequestParam(value = "gbn", required = false ,defaultValue = "") String gbn) {

		List<UploadFile> beforeFileList = new ArrayList<>();
		List<UploadFile> afterFileList = new ArrayList<>();
		InstallRequest installRequest = new InstallRequest();
		Device device = new Device();
//			수정시 설치요정 DATA 조회해서 return
		try {
			installRequest = installRequestService.getInstallRequest(new InstallRequestParameter(installRequestSeq,gbn));
			if (deviceSeq > 0) {
				device = deviceService.getInstallRequestDevice(new InstallRequestParameter(installRequestSeq, deviceSeq));
				beforeFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, deviceSeq);
				afterFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, deviceSeq);

			}
		} catch(ValidException e){
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		}
		catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		model.addAttribute("request", installRequest);
		model.addAttribute("device", device);
		model.addAttribute("beforeFileList", beforeFileList);
		model.addAttribute("afterFileList", afterFileList);
		model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
		model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간

		return BASE_URL + "/installRequest/installRequestDeviceWrite";
	}

	@GetMapping("/installRequest/addPartListPopup")
	public String addPartListPopup(Model model, HttpServletResponse response,
								   @RequestParam String domainName,
								   @RequestParam(defaultValue="0") int domainSeq,
								   @RequestParam(defaultValue="0") int domainSeq2
	) {

		List<Addpart> addpartList = new ArrayList<>();

		try {
			addpartList = addpartService.getAddpartList(new Addpart(domainName, domainSeq, domainSeq2, Addpart.INSTALL_ADDPART_IM03));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("addpartList", addpartList);

		return BASE_URL + "/installRequest/addPartListPopup";
	}
}
