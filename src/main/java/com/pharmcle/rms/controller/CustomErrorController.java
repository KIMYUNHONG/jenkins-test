/**
 * Create By Lee DoWon on 2020-05-11 / 오후 6:20
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.util.ServletUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@Slf4j
public class CustomErrorController implements ErrorController {

	private static final String ERROR_PATH = "/error";

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

	@GuestAccess
	@RequestMapping("/error")
	public String handleError(HttpServletRequest request, Model model) {
		model.addAttribute("isMobile", ServletUtil.isMobile(request));
		return "error/error";
	}
	@GuestAccess
	@RequestMapping("/error/login")
	public String loginError(HttpServletRequest request, Model model) {
		return "error/login";
	}

}

