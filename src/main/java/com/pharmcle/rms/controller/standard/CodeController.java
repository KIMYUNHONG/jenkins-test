/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller("CodeController")
@RequestMapping("/standard")
public class CodeController {

	private final static String BASE_URL = "/standard";

	@GetMapping("/code/codeList")
	public String codeList(Model model) {
		return BASE_URL + "/code/codeList";
	}

}
