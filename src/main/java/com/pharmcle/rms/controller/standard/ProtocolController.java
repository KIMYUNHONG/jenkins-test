/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.protocol.ProtocolService;
import com.pharmcle.rms.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;


@Controller("ProtocolController")
@RequestMapping("/standard")
public class ProtocolController {

	private final static String BASE_URL = "/standard";


	@Autowired
	CodeService codeService;
	@Autowired
	ProtocolService protocolService;

	@GetMapping("/protocol/protocolList")
	public String protocolList(Model model) {
		return BASE_URL + "/protocol/protocolList";
	}


	// 통신기준정보관리 등록 팝업
	@GetMapping("/protocol/addProtocolPopup")
	public String addProtocolPopup(Model model, HttpServletResponse response,
									   @RequestParam(required = false, defaultValue = "SD020001") String deviceClassify) {

		String url = "";
			//SD020001 : 포충기 , SD020002 : 기피제 분사함
			if("SD020001".equals(deviceClassify)){
				url = "/protocol/addProtocol1Popup";
			}else{
				url = "/protocol/addProtocol2Popup";
			}

		model.addAttribute("startHourList", DateUtil.getHourMinuteList("17", "24"));    // 점등시간
		model.addAttribute("endHourList", DateUtil.getHourMinuteList("0", "8"));    // 소등시간

		return BASE_URL + url;
	}




}
