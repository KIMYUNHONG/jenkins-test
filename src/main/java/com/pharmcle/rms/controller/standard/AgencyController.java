/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.Customer;
import com.pharmcle.rms.domain.customer.CustomerParameter;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("AgencyController")
@RequestMapping("/standard")
public class AgencyController {

	@Autowired
	CustomerService customerService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	private final static String BASE_URI = "/standard";

	// 거래처관리 > 거래처 > 거래처리스트
	@GetMapping("/agency/agencyList")
	public String customerList(Model model , CustomerParameter parameter) {

		model.addAttribute("CM02", codeService.getCodeListByGroupCode(new CodeParameter("CM02")));	// 기간구분
		model.addAttribute("parameter", parameter);	// 검색조건
		return BASE_URI + "/agency/agencyList";
	}

	@GetMapping("/agency/agencyDetail")
	public String customerDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int customerSeq) {
		Customer customer;
		List<UploadFile> uploadFileList;
		try {
			customer = customerService.getCustomer(customerSeq);
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.CUSTOMER, customerSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;
			if (customer == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일
		model.addAttribute("customer", customer);
		model.addAttribute("uploadFileList", uploadFileList);
		return BASE_URI + "/agency/agencyDetail";
	}

	@GetMapping("/agency/agencyWrite")
	public String customerWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int customerSeq) {
		Customer customer;
		List<UploadFile> uploadFileList;

		try {
			customer = customerService.getCustomer(customerSeq);

			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.CUSTOMER, customerSeq);
			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;

			if(customer == null){
				customer = new Customer();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("customer", customer);
		model.addAttribute("uploadFileList", uploadFileList);

		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일
		return BASE_URI + "/agency/agencyWrite";
	}

	@GetMapping("/agency/agencyChargeWritePopup")
	public String customerChargeWritePopup(HttpServletResponse response, Model model, User charge, Errors error) {

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
		}

		try {
			if(charge == null){
				charge = new User();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("charge", charge);

//		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 거래처구분
		model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));	// 직책
		return BASE_URI + "/agency/agencyChargeWritePopup";
	}
}
