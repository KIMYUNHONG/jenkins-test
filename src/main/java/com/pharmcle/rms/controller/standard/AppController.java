/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.domain.app.App;
import com.pharmcle.rms.domain.app.AppService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("AppController")
@RequestMapping("/standard")
public class AppController {

	@Autowired
	CodeService codeService;

	@Autowired
	AppService appService;

	private final static String BASE_URL = "/standard";

	@GetMapping("/app/appList")
	public String appList(Model model) {

		model.addAttribute("SD09", codeService.getCodeListByGroupCode(new CodeParameter("SD09")));

		return BASE_URL + "/app/appList";
	}

	@GetMapping("/app/addAppPopup")
	public String addAppPopup(HttpServletResponse response, Model model, @RequestParam(value = "appSeq", required = false, defaultValue = "0") int appSeq) {

		try {
			App app = appService.getApp(appSeq);
			app = app == null ? new App() : app;
			model.addAttribute("app", app);
		} catch(Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("SD09", codeService.getCodeListByGroupCode(new CodeParameter("SD09")));
		model.addAttribute("SD10", codeService.getCodeListByGroupCode(new CodeParameter("SD10")));
		return BASE_URL + "/app/addAppPopup";
	}
}
