/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserParameter;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller("AreaChargeController")
@RequestMapping("/standard")
public class AreaChargeController {

	private final static String BASE_URL = "/standard";

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	@GetMapping("/areaCharge/areaChargeList")
	public String areaChargeList(Model model) {

		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));
		return BASE_URL + "/areaCharge/areaChargeList";
	}

	@GetMapping("/areaCharge/modifyAreaChargePopup")
	public String modifyAreaChargePopup(Model model, HttpServletResponse response) {

		UserParameter userParameter = new UserParameter();
		List<User> userList = new ArrayList<>();

		try {

			// 유지보수 권한
			userParameter.setNotCustomer(true);
			userList = userService.getUserList(userParameter);
		} catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		model.addAttribute("userList", userList);
		return BASE_URL + "/areaCharge/modifyAreaChargePopup";
	}
}
