/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.domain.menu.MenuParameter;
import com.pharmcle.rms.domain.menu.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller("AuthController")
@RequestMapping("/standard")
public class AuthController {

	@Autowired
	MenuService menuService;

	private final static String BASE_URL = "/standard";

	@GetMapping("/auth/authList")
	public String authList(Model model) {


		model.addAttribute("menu1List", menuService.getMenuList(new MenuParameter(0)));
		model.addAttribute("menu2List", menuService.getMenuList(new MenuParameter(1)));
		return BASE_URL + "/auth/authList";
	}

}
