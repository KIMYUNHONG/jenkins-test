/**
 * Create By Lee DoWon on 2020-03-25 / 오후 5:18
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.domain.code.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("ModemController")
@RequestMapping("/standard")
public class ModemController {

	@Autowired
	CodeService codeService;

	private final static String BASE_URL = "/standard";

	// 모뎀 리스트
	@GetMapping("/modem/modemList")
	public String modemList(Model model) {
		return BASE_URL + "/modem/modemList";
	}

	// 모뎀 엑셀 업로드 팝업
	@GetMapping("/modem/modemUploadPopup")
	public String modemUploadPopup(Model model) {
		return BASE_URL + "/modem/modemUploadPopup";
	}

}