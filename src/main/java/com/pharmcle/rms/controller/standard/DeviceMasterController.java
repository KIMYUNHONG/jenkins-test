/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.deviceMaster.DeviceMaster;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("DeviceMasterController")
@RequestMapping("/standard")
public class DeviceMasterController {

	@Autowired
	DeviceMasterService deviceMasterService;

	@Autowired
	CodeService codeService;

	private final static String BASE_URL = "/standard";

	@GetMapping("/deviceMaster/deviceMasterList")
	public String deviceMasterList(Model model) {

		model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));
		model.addAttribute("SD03", codeService.getCodeListByGroupCode(new CodeParameter("SD03")));
		model.addAttribute("SD06", codeService.getCodeListByGroupCode(new CodeParameter("SD06")));

		return BASE_URL + "/deviceMaster/deviceMasterList";
	}

	@GetMapping("/deviceMaster/addDeviceMasterPopup")
	public String addDeviceMasterPopup(Model model, HttpServletResponse response,
		@RequestParam(required = false, defaultValue = "0") int deviceMstSeq) {

		try {
			DeviceMaster deviceMaster = deviceMasterService.getDevice(deviceMstSeq);
			deviceMaster = deviceMaster == null ? new DeviceMaster() : deviceMaster;
			model.addAttribute("deviceMaster", deviceMaster);
			model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));
			model.addAttribute("SD03", codeService.getCodeListByGroupCode(new CodeParameter("SD03")));
			model.addAttribute("SD04", codeService.getCodeListByGroupCode(new CodeParameter("SD04")));
			model.addAttribute("SD05", codeService.getCodeListByGroupCode(new CodeParameter("SD05")));
		} catch (ValidException e){
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/deviceMaster/addDeviceMasterPopup";
	}

	@GetMapping("/deviceMaster/addDeviceMasterPopup3")
	public String addDeviceMasterPopup2(Model model) {	return BASE_URL + "/deviceMaster/addDeviceMasterPopup";
	}
}
