/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.standard;

import com.pharmcle.rms.annotation.GuestAccess;
import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.CustomerParameter;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("UserController")
@RequestMapping("/standard")
public class UserController {

	private final static String BASE_URL = "/standard";

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;
	@Autowired
	CustomerService customerService;

	@GetMapping("/user/userList")
	public String userList(HttpServletResponse response, Model model) {
		try {
			//사용권한
			model.addAttribute("SC02", codeService.getCodeListByGroupCode(new CodeParameter("SC02")));
			// 소속
			CustomerParameter customerParameter = new CustomerParameter();
			// 사용자관리 검색조건 소속 전체 거래처가 나오도록 N 처리
			customerParameter.setIsAgencyMenu("ALL");

			model.addAttribute("customerList", customerService.getCustomerList(customerParameter));
		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		}catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/user/userList";
	}

	@GetMapping("/user/addUserPopup")
	public String addUserPopup(HttpServletResponse response, Model model, @RequestParam(value = "userSeq", required = false, defaultValue = "0") int userSeq) {

		try {
			User user = userService.getUser(userSeq);
			user = user == null ? new User() : user;

			model.addAttribute("user", user);
			//사용자권한
			model.addAttribute("SC02", codeService.getCodeListByGroupCode(new CodeParameter("SC02")));
			//활동여부
			model.addAttribute("SD08", codeService.getCodeListByGroupCode(new CodeParameter("SD08")));
			// 직급
			model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));
			// 이메일 도메인
			model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));
			//소속
			model.addAttribute("customerList", customerService.getCustomerList(new CustomerParameter()));
		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
			return "";
		}catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}
		return BASE_URL + "/user/addUserPopup";
	}

	@GuestAccess
	@GetMapping("/user/findUserIdPopup")
	public String findUserIdPopup(HttpServletResponse response, Model model, @RequestParam(value = "userSeq", required = false, defaultValue = "0") int userSeq) {
		try {
			//이메일 도메인
			model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));

		}catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/user/findIdPopup.html";
	}

}
