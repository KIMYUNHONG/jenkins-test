/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.manage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller("AsManagerController")
@RequestMapping("/manage")
public class AsManagerController {

	private final static String BASE_URL = "/manage";

	@GetMapping("/asManager/asManagerList")
	public String asManagerList(Model model) {
		return BASE_URL + "/asManager/asManagerList";
	}

}
