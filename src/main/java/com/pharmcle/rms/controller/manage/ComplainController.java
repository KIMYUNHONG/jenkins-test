/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.manage;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.addpart.Addpart;
import com.pharmcle.rms.domain.addpart.AddpartService;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.complain.Complain;
import com.pharmcle.rms.domain.complain.ComplainHandle;
import com.pharmcle.rms.domain.complain.ComplainParameter;
import com.pharmcle.rms.domain.complain.ComplainService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.device.SelectInstallDeviceParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Controller("ComplainController")
@RequestMapping("/manage")
public class ComplainController {

	@Autowired
	CodeService codeService;

	@Autowired
	ComplainService complainService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;

	@Autowired
	LocationService locationService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	AddpartService addpartService;

	private final static String BASE_URL = "/manage";

	@GetMapping("/complain/complainList")
	public String complainList(Model model , ComplainParameter parameter) {

		model.addAttribute("MR01", codeService.getCodeListByGroupCode(new CodeParameter("MR01")));    // 민원유형
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
		model.addAttribute("MR03", codeService.getCodeListByGroupCode(new CodeParameter("MR03")));    // 민원상태
		model.addAttribute("parameter", parameter);    // 검색조건

		return BASE_URL + "/complain/complainList";
	}

	@GetMapping("/complain/complainDetail")
	public String complainDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int complainSeq) {
		Complain complain = new Complain();
		List<ComplainHandle> complainHandle = new ArrayList<>();
		List<UploadFile> fileList = new ArrayList<>();

		try {
			complain = complainService.getComplain(new ComplainParameter(complainSeq));
			if (complain == null) {
				throw new Exception();
			}
			fileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN, complainSeq);
			complainHandle = complainService.getComplainHandle(new ComplainParameter(complainSeq));
		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch(Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("complain", complain);
		model.addAttribute("fileList", fileList);
		model.addAttribute("complainHandle", complainHandle);

		return BASE_URL + "/complain/complainDetail";
	}

	@GetMapping("/complain/complainWrite")
	public String complainWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int complainSeq) {
		Complain complain = new Complain();
		List<UploadFile> uploadFileList = new ArrayList<>();

		try {
			complain = complainService.getComplain(new ComplainParameter(complainSeq));
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.COMPLAIN, complainSeq);
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("complain", complain == null ? new Complain() : complain);
		model.addAttribute("fileList", uploadFileList);

		model.addAttribute("MR01", codeService.getCodeListByGroupCode(new CodeParameter("MR01")));    // 민원유형
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
		model.addAttribute("MR03", codeService.getCodeListByGroupCode(new CodeParameter("MR03")));    // 민원상태
		model.addAttribute("MR02", codeService.getCodeListByGroupCode(new CodeParameter("MR02")));    // 대리신청

		return BASE_URL + "/complain/complainWrite";
	}

	@GetMapping("/complain/complainDeviceListPopup")
	public String complainDeviceListPopup(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int locationSeq) {

		try {
			model.addAttribute("location", locationService.getLocation(locationSeq));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/complain/complainDeviceListPopup";
	}

	@GetMapping("/complain/complainHandling")
	public String complainHandling(HttpServletResponse response, Model model,
								   @RequestParam(defaultValue = "") List<String> deviceSeqList,
								   @RequestParam(defaultValue = "0") int complainSeq) {

		Complain complain = new Complain();

		try {
			List<Device> deviceList = new ArrayList<>();
			List<ComplainHandle> handleList = new ArrayList<>();

			complain = complainService.getComplain(new ComplainParameter(complainSeq));
			if (deviceSeqList != null && deviceSeqList.size() > 0) {
				SelectInstallDeviceParameter parameter = new SelectInstallDeviceParameter();
				parameter.setDeviceSeqList(deviceSeqList);
				deviceList = deviceService.getInstallDeviceList(parameter);
				model.addAttribute("deviceList", deviceList);
			} else {
				handleList = complainService.getComplainHandle(new ComplainParameter(complainSeq));
				if(handleList == null || handleList.size() == 0){
					ResponseUtil.htmlAlertAndHistoryback(response, "등록된 처리내역이 없습니다.");
				}
				model.addAttribute("deviceList", handleList);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("complainSeq", complainSeq);
		model.addAttribute("complain", complain);
		model.addAttribute("MR04", codeService.getCodeListByGroupCode(new CodeParameter("MR04")));    // 처리결과
		return BASE_URL + "/complain/complainHandling";
	}

	@GetMapping("/complain/addPartListPopup")
	public String addPartListPopup(Model model, HttpServletResponse response,
								   @RequestParam String domainName,
								   @RequestParam(defaultValue="0") int domainSeq,
								   @RequestParam(defaultValue="0") int domainSeq2
								   ) {

 		List<Addpart> addpartList = new ArrayList<>();

		try {
			addpartList = addpartService.getAddpartList(new Addpart(domainName, domainSeq, domainSeq2, Addpart.MANAGE_ADDPART_MR07));
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("addpartList", addpartList);

		return BASE_URL + "/complain/addPartListPopup";
	}


}
