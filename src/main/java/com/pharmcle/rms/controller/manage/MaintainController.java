/**
 * Create By Lee DoWon on 2020-02-18 / 오전 10:43
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.manage;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.maintain.Maintain;
import com.pharmcle.rms.domain.maintain.MaintainService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Controller("MaintainController")
@RequestMapping("/manage")
public class MaintainController {

	@Autowired
	CodeService codeService;

	@Autowired
	MaintainService maintainService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	DeviceService deviceService;

	private final static String BASE_URL = "/manage";

	@GetMapping("/maintain/maintainList")
	public String maintainList(Model model , Maintain parameter) {

		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
		model.addAttribute("MR06", codeService.getCodeListByGroupCode(new CodeParameter("MR06")));    // 유지보수 유형
		model.addAttribute("parameter", parameter);    // 검색조건

		return BASE_URL + "/maintain/maintainList";
	}
	@GetMapping("/maintain/maintainDetail")
	public String maintainDetail(HttpServletResponse response, Model model,
								 @RequestParam(defaultValue = "0") int deviceSeq,
								 @RequestParam(required = false) String searchYear,
								 @RequestParam(required = false) String searchType) {
		try {
			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));


			List<UploadFile> beforeFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_BEFORE, deviceSeq);
			List<UploadFile> afterFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.INSTALL_DEVICE_AFTER, deviceSeq);
			Maintain parameter = new Maintain(deviceSeq);

			List<String> maintainRegYearList = maintainService.getMaintainAndComplainRegYear(parameter);

			String defaultYear = (maintainRegYearList != null && maintainRegYearList.size() > 0) ?
				maintainRegYearList.get(0) : String.valueOf(LocalDate.now().getYear());

			parameter.setSearchYear(StringUtil.nvl(searchYear, defaultYear));
			parameter.setSearchType(StringUtil.nvl(searchType, Maintain.ORDER_BY_DESC));
//			List<Maintain> maintainList = maintainService.getMaintainList(parameter);
			List<Maintain> maintainList = maintainService.getMaintainAndComplainList(parameter);



			if(maintainList == null || maintainList.size() == 0 ){
				throw new ValidException("등록된 유지보수가 없습니다.");
			}


			model.addAttribute("device", device);
			model.addAttribute("beforeFileList", beforeFileList);
			model.addAttribute("afterFileList", afterFileList);
			model.addAttribute("maintainList", maintainList);
			model.addAttribute("parameter", parameter);
			model.addAttribute("maintainRegYearList", maintainRegYearList);
			model.addAttribute("MR09", codeService.getCodeListByGroupCode(new CodeParameter("MR09")));    // 유지보수 정렬

		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/maintain/maintainDetail";
	}


	@GetMapping("/maintain/maintainReportPopup")
	public String maintainReportPopup(HttpServletResponse response, Model model,
									  @RequestParam(defaultValue = "0") int deviceSeq,
									  @RequestParam(required = false) String searchYear,
									  @RequestParam(required = false) String searchType) {
		try {

			Maintain parameter = new Maintain(deviceSeq);
			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));
			List<String> maintainRegYearList = maintainService.getMaintainAndComplainRegYear(parameter);

			String defaultYear = (maintainRegYearList != null && maintainRegYearList.size() > 0) ?
				maintainRegYearList.get(0) : String.valueOf(LocalDate.now().getYear());

			parameter.setSearchYear(StringUtil.nvl(searchYear, defaultYear));
			parameter.setSearchType(StringUtil.nvl(searchType, Maintain.ORDER_BY_DESC));
			List<Maintain> maintainList = maintainService.getMaintainAndComplainList(parameter);

			if(maintainList == null || maintainList.size() == 0 ){
				throw new ValidException("등록된 유지보수가 없습니다.");
			}

			model.addAttribute("device", device);
			model.addAttribute("maintainList", maintainList);
			model.addAttribute("parameter", parameter);
			model.addAttribute("maintainRegYearList", maintainRegYearList);
			model.addAttribute("MR09", codeService.getCodeListByGroupCode(new CodeParameter("MR09")));    // 유지보수 정렬

		}catch (ValidException e) {
			ResponseUtil.htmlAlertClosePopup(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/maintain/maintainReportPopup";
	}


	@GetMapping("/maintain/maintainReportSendMailPopup")
	public String maintainReportSendMailPopup(HttpServletResponse response, Model model,
	                                          @RequestParam(defaultValue = "0") int deviceSeq,
											  @RequestParam(required = false) String searchYear,
											  @RequestParam(required = false) String searchType) {

		try {

			Maintain parameter = new Maintain(deviceSeq);
			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));
			List<String> maintainRegYearList = maintainService.getMaintainAndComplainRegYear(parameter);

			String defaultYear = (maintainRegYearList != null && maintainRegYearList.size() > 0) ?
				maintainRegYearList.get(0) : String.valueOf(LocalDate.now().getYear());

			parameter.setSearchYear(StringUtil.nvl(searchYear, defaultYear));
			parameter.setSearchType(StringUtil.nvl(searchType, Maintain.ORDER_BY_DESC));
			List<Maintain> maintainList = maintainService.getMaintainAndComplainList(parameter);

			if(maintainList == null || maintainList.size() == 0 ){
				throw new ValidException("등록된 유지보수가 없습니다.");
			}

			model.addAttribute("device", device);
			model.addAttribute("maintainList", maintainList);

		}catch (ValidException e) {
			ResponseUtil.htmlAlertClosePopup(response, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URL + "/maintain/maintainReportSendMailPopup";
	}


}
