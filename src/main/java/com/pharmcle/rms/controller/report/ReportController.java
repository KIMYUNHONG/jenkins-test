/**
 * Create By Lee DoWon on 2020-04-17 / 오후 1:12
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.report;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.CustomerParameter;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.ValidAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/*
* 일단 레포트 기획이 안나와서 페이지 연결로 만들어논 컨트롤러
* */
@Slf4j
@Controller("ReportController")
@RequestMapping("/report")
public class ReportController {
	
	@Autowired
	CodeService codeService;

	@Autowired
	CustomerService customerService;

	@Autowired
	UserService userService;
	
	private final static String BASE_URL = "/report";

	@GetMapping("/deviceStock/deviceStockList")
	public String deviceStockList(Model model) {
		// 장비구분
		model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));

		return BASE_URL + "/deviceStock/deviceStockList";
	}
	@GetMapping("/customer/customerList")
	public String customerList(Model model) {
		// 거래처 구분
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));
				
		return BASE_URL + "/customer/customerList";
	}
	@GetMapping("/maintain/maintainList")
	public String maintainList(HttpServletResponse response,Model model) {
		try {
			// 소속
			CustomerParameter customerParameter = new CustomerParameter();
			// 사용자관리 검색조건 소속 전체 거래처가 나오도록 N 처리
			customerParameter.setIsAgencyMenu("Y");

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			if(ValidAuthUtil.isAgencyAuth(user) || ValidAuthUtil.isCustomerAuth(user)){
				customerParameter.setAuthCustomerSeq(user.getCustomerSeq());
				customerParameter.setAuthCustomerType(user.getCustomerType());
			}

			model.addAttribute("customerList", customerService.getCustomerList(customerParameter));

			model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처 구분
			model.addAttribute("SC02", codeService.getCodeListByGroupCode(new CodeParameter("SC02")));    // 담당자 구분
		}catch (ValidException e) {
			ResponseUtil.htmlAlertAndHistoryback(response, e.getMessage());
		}catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/maintain/maintainList";
	}

	@GetMapping("/today/todayList")
	public String todayList(HttpServletResponse response,Model model) {
		return BASE_URL + "/today/todayList";
	}
}
