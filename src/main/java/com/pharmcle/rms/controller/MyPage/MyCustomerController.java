/**
 * Create By Lee DoWon on 2020-04-10 / 오후 5:37
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.myPage;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("MyCustomerController")
@RequestMapping("/myPage")
public class MyCustomerController {

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	@Autowired
	CustomerService customerService;

	private final static String BASE_URL = "/myPage";

	@GetMapping("/myCustomer/myCustomer")
	public String myCustomer(Model model, HttpServletResponse response){

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			model.addAttribute("customer", customerService.getCustomer(user.getCustomerSeq()));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URL + "/myCustomer/myCustomer";
	}

	@GetMapping("/myCustomer/modifyMyCustomer")
	public String modifyMyCustomer(Model model, HttpServletResponse response){

		try {
			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			model.addAttribute("customer", customerService.getCustomer(user.getCustomerSeq()));
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));	// 직급
		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일도메인
		model.addAttribute("SC06", codeService.getCodeListByGroupCode(new CodeParameter("SC06")));	// 휴대폰 앞자리
		return BASE_URL + "/myCustomer/modifyMyCustomer";
	}
}
