/**
 * Create By Lee DoWon on 2020-04-10 / 오후 5:37
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.myPage;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("MyInfoController")
@RequestMapping("/myPage")
public class MyInfoController {

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	private final static String BASE_URL = "/myPage";

	@GetMapping("/myInfo/myInfo")
	public String myInfo(Model model){
		return BASE_URL + "/myInfo/myInfo";
	}

	@GetMapping("/myInfo/modifyMyInfo")
	public String modifyInfo(Model model){

		model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));	// 직급
		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일도메인
		model.addAttribute("SC06", codeService.getCodeListByGroupCode(new CodeParameter("SC06")));	// 휴대폰 앞자리
		return BASE_URL + "/myInfo/modifyMyInfo";
	}
}
