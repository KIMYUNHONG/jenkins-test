/**
 * Create By Lee DoWon on 2020-04-10 / 오후 5:37
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.myPage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("ChangePasswordController")
@RequestMapping("/myPage")
public class ChangePasswordController {

	private final static String BASE_URL = "/myPage";

	@GetMapping("/changePassword/changePassword")
	public String changePassword(){
		return BASE_URL + "/changePassword/changePassword";
	}

}
