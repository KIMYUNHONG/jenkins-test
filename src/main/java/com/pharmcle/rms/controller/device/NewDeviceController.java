/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.device;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("NewDeviceController")
@RequestMapping("/device")
public class NewDeviceController {

	private final static String BASE_URI = "/device";

	@GetMapping("/newDevice/newDeviceRegist")
	public String newDeviceList(Model model) {
		return BASE_URI + "/newDevice/newDeviceRegist";
	}
}
