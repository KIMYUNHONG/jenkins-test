/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.device;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceParameter;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.deviceMaster.DeviceMaster;
import com.pharmcle.rms.domain.deviceMaster.DeviceMasterService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller("DeviceController")
@RequestMapping("/device")
public class DeviceController {

	@Autowired
	CodeService codeService;
	@Autowired
	DeviceService deviceService;
	@Autowired
	DeviceMasterService deviceMasterService;


	private final static String BASE_URI = "/device";

	@GetMapping("/device/deviceList")
	public String deviceList(Model model , DeviceParameter parameter) {

		model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));// 장비구분
		model.addAttribute("SD05", codeService.getCodeListByGroupCode(new CodeParameter("SD05")));//	 제조사
		model.addAttribute("DM02", codeService.getCodeListByGroupCode(new CodeParameter("DM02")));// 장비상태
		model.addAttribute("parameter",parameter);

		return BASE_URI + "/device/deviceList";
	}

	@GetMapping("/device/deviceDetail")
	public String deviceDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int deviceSeq) {
		Device device;
		try {
			device = deviceService.getDevice(new DeviceParameter(deviceSeq));

			if (device == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("device", device);
		return BASE_URI + "/device/deviceDetail";
	}

	@GetMapping("/device/detailDeviceMasterPopup")
	public String detailDeviceMasterPopup(HttpServletResponse response,Model model, @RequestParam(value = "deviceMstSeq", required = false, defaultValue = "0") int deviceMstSeq) {

		try {
			DeviceMaster deviceMaster = deviceMasterService.getDevice(deviceMstSeq);

			deviceMaster = deviceMaster == null ? new DeviceMaster() : deviceMaster;

			model.addAttribute("deviceMaster", deviceMaster);
		} catch(Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		return BASE_URI+"/device/detailDeviceMasterPopup.html";
	}

	@GetMapping(value = {"/device/deviceWrite", "/newDevice/deviceWrite"})
	public String deviceWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int deviceSeq) {

		//통신방식
		model.addAttribute("DM01", codeService.getCodeListByGroupCode(new CodeParameter("DM01")));
		//보관장소
		model.addAttribute("DM03", codeService.getCodeListByGroupCode(new CodeParameter("DM03")));

		try {
			Device device = deviceService.getDevice(new DeviceParameter(deviceSeq));

			device = device == null ? new Device() : device;

			model.addAttribute("device", device);
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}
		return BASE_URI + "/device/deviceWrite";
	}

	@GetMapping("/device/selectDeviceMasterPopup")
	public String selectDeviceMasterPopup(Model model)  {
		//장비구분
		model.addAttribute("SD02", codeService.getCodeListByGroupCode(new CodeParameter("SD02")));
		return BASE_URI+"/device/selectDeviceMasterPopup";
	}
	@GetMapping("/device/modemListPopup")
	public String modemListPopup(Model model)  {
		//장비구분
		return BASE_URI+"/device/modemListPopup";
	}


}
