/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.customer;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.device.Device;
import com.pharmcle.rms.domain.device.DeviceDao;
import com.pharmcle.rms.domain.device.DeviceService;
import com.pharmcle.rms.domain.location.Location;
import com.pharmcle.rms.domain.location.LocationParameter;
import com.pharmcle.rms.domain.location.LocationService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@Controller("LocationController")
public class LocationController {

	@Autowired
	CodeService codeService;
	@Autowired
	DeviceDao deviceDao;

	@Autowired
	CustomerService customerService;

	@Autowired
	LocationService locationService;

	@Autowired
	DeviceService deviceService;

	private final static String BASE_URI = "/customer";

	/**
	 * 설치장소 리스트
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/customer/location/locationList")
	public String locationList(Model model, LocationParameter parameter) {

		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));    // 거래처구분
		model.addAttribute("CM05", codeService.getCodeListByGroupCode(new CodeParameter("CM05")));    // 설치장소기간
		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));    // 지역
		model.addAttribute("CM04", codeService.getCodeListByGroupCode(new CodeParameter("CM04")));    // 계약상태
		model.addAttribute("parameter", parameter);	// 검색 조건
		return BASE_URI + "/location/locationList";

	}

	/**
	 * 설치장소 등록/수정 팝업
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/customer/location/locationWritePopup")
	public String locationWritePopup(Model model, HttpServletResponse response, @RequestParam(defaultValue = "0") int locationSeq) {

		Location location;
		try {
			location = locationSeq > 0 ? locationService.getLocation(locationSeq) : new Location();
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("location", location);
		return BASE_URI + "/location/locationWritePopup";
	}

	/**
	 * 설치장소 등록/수정 팝업
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/customer/location/locationMapPopup")
	public String locationMapPopup(Model model, HttpServletResponse response,
								   @RequestParam(defaultValue = "0") int locationSeq) {

		String address = "서울시 중구";
		try {
			Location location = locationService.getLocation(locationSeq);
			address = location != null ? location.getAddress() : "서울시 중구";
		} catch (Exception e) {
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("locationSeq", locationSeq);
		model.addAttribute("address", address);
		return BASE_URI + "/location/locationMapPopup";
	}

	/**
	 * 설치장소 리스트
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/customer/migration/locationList")
	public String migrationLocationList(Model model, Location parameter) {

		try {
			List<Device> deviceList = deviceDao.selectTempDevice(new Device());
			model.addAttribute("deviceList", deviceList);
//			for(int i = 5832 ; i < 5933; i++) {
//				deviceDao.insertTempDevice(new Device(i));
//			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return "";
		}

		return "/test";

	}

}

