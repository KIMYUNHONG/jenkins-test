/**
 * Create By Lee DoWon on 2020-02-17 / 오후 5:48
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.controller.customer;

import com.pharmcle.rms.domain.code.CodeParameter;
import com.pharmcle.rms.domain.code.CodeService;
import com.pharmcle.rms.domain.customer.Customer;
import com.pharmcle.rms.domain.customer.CustomerParameter;
import com.pharmcle.rms.domain.customer.CustomerService;
import com.pharmcle.rms.domain.uploadFile.UploadFile;
import com.pharmcle.rms.domain.uploadFile.UploadFileService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@Autowired
	UploadFileService uploadFileService;

	@Autowired
	UserService userService;

	@Autowired
	CodeService codeService;

	private final static String BASE_URI = "/customer";

	// 거래처관리 > 거래처 > 거래처리스트
	@GetMapping("/customer/customer/customerList")
	public String customerList(Model model, CustomerParameter parameter) {

		model.addAttribute("SC03", codeService.getCodeListByGroupCode(new CodeParameter("SC03")));	// 지역
		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 거래처구분
		model.addAttribute("CM02", codeService.getCodeListByGroupCode(new CodeParameter("CM02")));	// 기간구분
		model.addAttribute("CM04", codeService.getCodeListByGroupCode(new CodeParameter("CM04")));	// 계약상태
		model.addAttribute("parameter", parameter);	// 검색 파라미터

		return BASE_URI + "/customer/customerList";
	}

	@GetMapping("/customer/customer/customerDetail")
	public String customerDetail(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int customerSeq, CustomerParameter parameter) {
		Customer customer;
		List<UploadFile> uploadFileList;
		try {
			customer = customerService.getCustomer(customerSeq);
			uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.CUSTOMER, customerSeq);

			uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;
			if (customer == null) {
				throw new Exception();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
			return "";
		}

		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일
		model.addAttribute("customer", customer);
		model.addAttribute("uploadFileList", uploadFileList);
		model.addAttribute("parameter", parameter);	// 검색 파라미터

		return BASE_URI + "/customer/customerDetail";
	}

	@GetMapping("/customer/customer/customerWrite")
	public String customerWrite(HttpServletResponse response, Model model, @RequestParam(defaultValue = "0") int customerSeq, CustomerParameter parameter) {
		Customer customer = new Customer();
		List<UploadFile> uploadFileList = new ArrayList<>();

		try {
			if(customerSeq > 0) {
				customer = customerService.getCustomer(customerSeq);

				uploadFileList = uploadFileService.getUploadFileListByDomain(UploadFileService.CUSTOMER, customerSeq);
				uploadFileList = uploadFileList == null ? new ArrayList<UploadFile>() : uploadFileList;
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_SAVE_FAIL);
		}

		model.addAttribute("customer", customer);
		model.addAttribute("uploadFileList", uploadFileList);

		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 거래처구분
		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일
		model.addAttribute("parameter", parameter);	// 검색 파라미터

		return BASE_URI + "/customer/customerWrite";
	}

	@GetMapping("/customer/customer/customerChargeWritePopup")
	public String customerChargeWritePopup(HttpServletResponse response, Model model, User charge, Errors error) {

		if(error.hasErrors()){
			String errorMessage = error.getAllErrors().get(0).getDefaultMessage();
		}
		try {
			if(charge == null){
				charge = new User();
			}
		} catch (Exception e){
			log.error(e.getMessage());
			ResponseUtil.htmlAlertAndHistoryback(response, ResponseUtil.DATA_LOAD_FAIL);
		}

		model.addAttribute("charge", charge);

//		model.addAttribute("CM01", codeService.getCodeListByGroupCode(new CodeParameter("CM01")));	// 거래처구분
		model.addAttribute("SD11", codeService.getCodeListByGroupCode(new CodeParameter("SD11")));	// 직책
		model.addAttribute("SC04", codeService.getCodeListByGroupCode(new CodeParameter("SC04")));	// 이메일
		model.addAttribute("SC06", codeService.getCodeListByGroupCode(new CodeParameter("SC06")));	// 휴대폰번호 앞자리

		return BASE_URI + "/customer/customerChargeWritePopup";
	}
}
