/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtil {

	public final static String DEFAULT_DATE_FORMAT = "yyyy.MM.dd";
	public final static String DASH_DATE_FORMAT = "yyyy-MM-dd";
	public final static String DEFAULT_DATE_FORMAT_KOREAN = "yyyy년 MM월 dd일";
	public final static String DATE_NO_FORMAT = "yyyyMMdd";
	public final static String DEFAULT_LONG_DATE_FORMAT = "yyyy.MM.dd HH:mm";

	/**
	 * LocalDateTime 객체를 String으로 문자열화
	 *
	 * @param dateTime
	 * @param format
	 * @return
	 */
	public static String localDatetimeToString(LocalDateTime dateTime, String format) {
		if (dateTime == null) { return ""; }
		return dateTime.format(DateTimeFormatter.ofPattern(format));
	}

	/**
	 * @param dateTime
	 * @param format
	 * @return
	 */
	public static String localDateToString(LocalDate dateTime, String format) {
		if (dateTime == null) { return ""; }
		return dateTime.format(DateTimeFormatter.ofPattern(format));
	}

	public static List<String> getHourMinuteList(String startHour, String endHour) {
		List<String> list = new ArrayList<>();

		int start = Integer.parseInt(startHour);
		int end = Integer.parseInt(endHour);
		for (int i = start; i < end; i++) {
			list.add(i >= 10 ? i + "00" : ("0" + i) + "00");
			list.add(i >= 10 ? i + "30" : ("0" + i) + "30");
		}
		list.add(end >= 10 ? end + "00" :  ("0" + end) + "00");
		return list;
	}

	/**
	 * 로컬 데이트 Between 함수
	 * @param localDate 비교대상
	 * @param startDate 시작날짜
	 * @param endDate 끝나는날짜
	 * @return
	 */
	public static boolean isLocalDateBetween(LocalDate localDate, LocalDate startDate, LocalDate endDate){
		if(localDate == null || startDate == null || endDate == null){
			return false;
		}
		return (localDate.isEqual(startDate) || localDate.isAfter(startDate)) && (localDate.isEqual(endDate) || localDate.isBefore(endDate));

	}
	
	/**
	 * 특정일자시간 UNIX Timestemp 변환
	 * @param date 일자(0000-00-00 00:00:00)
	 * @return
	 */
	public static long getUnixDate(String date) {
		long timeStamp = 0;
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("GMT+9")); // 유닉스 시간

		try {
			Date dateTime = format.parse(date);
			
			timeStamp = dateTime.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return timeStamp;
	}
}
