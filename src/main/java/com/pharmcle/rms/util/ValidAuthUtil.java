/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import com.pharmcle.rms.base.ValidException;
import com.pharmcle.rms.domain.customer.Customer;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidAuthUtil {

	private final static String SCRIPT = "alert|confirm|prompt|onabort|onactivate|onafterprint|onafterupdate|onbeforeactivate|onbeforecopy|onbeforecut|onbeforedeactivate|onbeforeeditfocus|onbeforepaste|onbeforeprint|onbeforeunload|onbeforeupdate|onbegin|onblur|onbounce|oncellchange|onchange|onchange|onclick|oncontentready|oncontentsave|oncontextmenu|oncontrolselect|oncopy|oncut|ondataavailable|ondatasetchanged|ondatasetcomplete|ondblclick|ondeactivate|ondetach|ondocumentready|ondrag|ondragdrop|ondragend|ondragenter|ondragleave|ondragover|ondragstart|ondrop|onend|onerror|onerrorupdate|onfilterchange|onfinish|onfocus|onfocusin|onfocusout|onhelp|onhide|onkeydown|onkeypress|onkeyup|onlayoutcomplete|onload|onlosecapture|onmediacomplete|onmediaerror|onmedialoadfailed|onmousedown|onmouseenter|onmouseleave|onmousemove|onmouseout|onmouseover|onmouseup|onmousewheel|onmove|onmoveend|onmovestart|onopenstatechange|onoutofsync|onpaste|onpause|onplaystatechange|onpropertychange|onreadystatechange|onrepeat|onreset|onreset|onresize|onresizeend|onresizestart|onresume|onreverse|onrowclick|onrowenter|onrowexit|onrowout|onrowover|onrowsdelete|onrowsinserted|onsave|onscroll|onseek|onselect|onselectionchange|onselectstart|onshow|onstart|onstop|onsubmit|onsyncrestored|ontimeerror|ontrackchange|onunload|onurlflip";


	@Autowired
	UserService userService;

	public static void validNotAccessCustomer(String authRole) throws ValidException{
		if(User.AUTH_CUSTOMER.equals(authRole)){
			throw new ValidException("권한이 없습니다.");
		}
	}
	public static boolean isCustomerAuth(User user){
		return User.AUTH_CUSTOMER.equals(user.getAuthRole()) && !Customer.CUSTOMER_TYPE_AGANCY.equals(user.getCustomerType());
	}
	public static boolean isAgencyAuth(User user){
		return Customer.CUSTOMER_TYPE_AGANCY.equals(user.getCustomerType());
	}


	public static boolean isIncludeScript(String value){
		if(value == null){
			return true;
		}
		if (value.contains("script")) {
			return false;
		}
		if (value.contains(SCRIPT)) {
			return false;
		}
		return true;
	}

	public static boolean isIncludeInequality(String value){
		if(value == null){
			return true;
		}

		return value.contains(">") || value.contains("<");
	}
}
