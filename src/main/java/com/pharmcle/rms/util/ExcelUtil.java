package com.pharmcle.rms.util;

import com.pharmcle.rms.base.BaseDomain;
import com.pharmcle.rms.base.GridColumn;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.List;

public class ExcelUtil {

	public static <T> T downloadExcelByData(List<GridColumn> excelParameter, List<T> data, HttpServletResponse response) throws Exception {
		return downloadExcelByData(excelParameter, data, response, false, null);
	}

	public static <T> T downloadExcelByData(List<GridColumn> excelParameter, List<T> data, HttpServletResponse response, Boolean mergedRegionYn, int[] groupValue) throws Exception {

		Workbook workbook = new SXSSFWorkbook(100);
		Sheet sheet = workbook.createSheet("Sheet1");
		if (mergedRegionYn) {
			createMergedTitleRow(workbook, sheet, excelParameter, groupValue);
		} else {
			createTitleRow(workbook, sheet, excelParameter);
		}
		appendDataRow(workbook, sheet, excelParameter, data, mergedRegionYn);

		response.setContentType("application/download;charset=utf-8");
		response.setHeader("Content-Transfer-Encoding", "binary");
		OutputStream outputStream = null;

		try {
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			workbook.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();

			outputStream = response.getOutputStream();
			outputStream.write(outArray);
			outputStream.flush();

		} catch (Exception e) {
			throw e;
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
					((SXSSFWorkbook) workbook).dispose();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	// 엑셀 헤더 만들기
	private static void createTitleRow(Workbook workbook, Sheet sheet, List<GridColumn> excelParameter) {

		Row row = sheet.createRow(0);
		Cell cell = null;

		// 폰트 설정
		Font headerFont = workbook.createFont();
		headerFont.setFontName("고딕");
		headerFont.setBold(true);
		row.setHeight((short) 350);

		CellStyle headerStyle = workbook.createCellStyle();

		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		int idx = 0;
		for (GridColumn column : excelParameter) {
			cell = row.createCell(idx);
			cell.setCellValue(column.getHeader());
			sheet.setColumnWidth(idx, (column.getWidth() == 0 ? column.getMinWidth() : column.getWidth()) * 40);
			cell.setCellStyle(headerStyle);
			idx++;
		}
	}

	// 2열 엑셀 헤더 만들기
	private static void createMergedTitleRow(Workbook workbook, Sheet sheet, List<GridColumn> excelParameter, int[] groupValue) {

		Row row = sheet.createRow(0);
		Cell cell = null;

		// 폰트 설정
		Font headerFont = workbook.createFont();
		headerFont.setFontName("고딕");
		headerFont.setBold(true);
		row.setHeight((short) 350);

		CellStyle headerStyle = workbook.createCellStyle();

		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		// 1열 헤더 셀입력
		int idx = 0;
		String prevId = "";
		for (GridColumn column : excelParameter) {
			cell = row.createCell(idx);
			if (column.getGroupName() != null) {
				if (!prevId.equals(column.getGroupName())) {
					cell.setCellValue(column.getGroupName());
					prevId = column.getGroupName();
				}
			} else {
				cell.setCellValue(column.getHeader());
			}
			sheet.setColumnWidth(idx, (column.getWidth() == 0 ? column.getMinWidth() : column.getWidth()) * 40);
			cell.setCellStyle(headerStyle);
			idx++;
		}

		// 2열 헤더 셀입력
		row = sheet.createRow(1);
		idx = 0;
		for (GridColumn column : excelParameter) {
			cell = row.createCell(idx);
			if (column.getGroupName() != null) {
				cell.setCellValue(column.getHeader());
			}
			sheet.setColumnWidth(idx, (column.getWidth() == 0 ? column.getMinWidth() : column.getWidth()) * 40);
			cell.setCellStyle(headerStyle);
			idx++;
		}

		// 셀병합 처리
		if (groupValue != null) {
			int rowNum = groupValue[0];
			int colNum = groupValue[1];
			int cellNumPerCol = groupValue[2];
			for (int i = 0; i < rowNum; i++) {
				sheet.addMergedRegion(new CellRangeAddress(0, 1, i, i));
			}
			for (int j = 0; j <= colNum; j++) {
				sheet.addMergedRegion(new CellRangeAddress(0, 0, rowNum + cellNumPerCol * j, (rowNum + cellNumPerCol * j) + (cellNumPerCol - 1)));
			}
		}
	}

	private static <T> T appendDataRow(Workbook workbook, Sheet sheet, List<GridColumn> excelParameter, List<T> data, Boolean mergedRegionYn) {

		CellStyle leftStyle = createCellStyle(workbook, "left");
		CellStyle centerStyle = createCellStyle(workbook, "center");
		CellStyle rightStyle = createCellStyle(workbook, "right");

		try {
			int index = 1;
			if (mergedRegionYn) { index++;}
			if (excelParameter.size() <= 0 || data.size() <= 0) {
				return null;
			}

			// 필드 순서를 미리 캐치하여 매번 비교안하고 한번 잡은것으로 데이터 넣기위해 생성
			int[] fieldOrderList = new int[excelParameter.size()];
			int fieldOrderListIndex = 0;
			T idxItem = data.get(0);
			Field[] idxFieldList = idxItem.getClass().getDeclaredFields();

			for (GridColumn column : excelParameter) {
				int idx = 0;
				for (Field field : idxFieldList) {
					if (column.getName().equals(field.getName())) {
						fieldOrderList[fieldOrderListIndex] = idx;
						idx = 9999;
						break;
					}
					idx++;
				}
				if (idx != 9999) {
					fieldOrderList[fieldOrderListIndex] = 9999;
				}
				fieldOrderListIndex++;
			}
			int i = 0;
			for (T item : data) {
				Class itemClass = idxItem.getClass();
				BaseDomain baseDomainClass = (BaseDomain) item;
				Row row = sheet.createRow(index++);
				Field[] fieldList = item.getClass().getDeclaredFields();
				Field[] baseDomainFieldList = baseDomainClass.getClass().getFields();

				int cellIndex = 0;
				for (GridColumn column : excelParameter) {
					if (fieldOrderList[cellIndex] != 9999) {
						Object object = new PropertyDescriptor(fieldList[fieldOrderList[cellIndex]].getName(), itemClass).getReadMethod().invoke(item);
						Cell cell = row.createCell(cellIndex);
						String value = object == null ? "" : object.toString();
						cell.setCellValue(value);
						if (column.isNumberFormat()) {
//								cell.setCellType(CellType.NUMERIC);
						}
						if ("center".equals(column.getAlign())) {
							cell.setCellStyle(centerStyle);
						} else if ("left".equals(column.getAlign())) {
							cell.setCellStyle(leftStyle);
						} else {
							cell.setCellStyle(rightStyle);
						}
						cellIndex++;
					} else {
						// 클래스에서 못찾은경우 베이스도메인에 있는 필드인지 다시 찾음.(등록자, 등록일 등)
						for (Field field : baseDomainFieldList) {
							if (column.getName().equals(field.getName())) {
								Object object = new PropertyDescriptor(field.getName(), itemClass).getReadMethod().invoke(item);
								Cell cell = row.createCell(cellIndex);
								String value = object == null ? "" : object.toString();
								cell.setCellValue(value);
								cell.setCellStyle(createCellStyle(workbook, column.getAlign()));
								cellIndex++;
								break;
							}
						}
					}

				}


			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return null;
	}

	private static HorizontalAlignment getHorizontalAlignmentByAlign(String align) {
		if ("left".equals(align)) {
			return HorizontalAlignment.LEFT;
		} else if ("right".equals(align)) {
			return HorizontalAlignment.RIGHT;
		} else {
			return HorizontalAlignment.CENTER;
		}
	}

	private static CellStyle createCellStyle(Workbook workbook, String align) {

		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setAlignment(getHorizontalAlignmentByAlign(align));

		return cellStyle;
	}

	/**
	 * 서버 파일 주소로 업로드된 엑셀 파일을 접근해서 엑셀 시트 얻기
	 *
	 * @param filePath - uploaded file ServerPath
	 * @param index    - sheet index
	 * @return - Excel Sheet
	 * @throws Exception
	 */
	public static Sheet getExcelSheetByExcelFile(String filePath, int index) throws Exception {

		try {

			Workbook wb = new XSSFWorkbook(new FileInputStream(filePath));
			Sheet sheet = wb.getSheetAt(index); //첫번째 Sheet만

			return sheet;
		} catch (Exception e) {
			throw new Exception();
		}
	}
}