package com.pharmcle.rms.util;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Slf4j
public class ResponseUtil {

	//private static final Logger logger = LoggerFactory.getLogger(ResponseUtils.class);

	private static final String ENCODING_UTF8 = "UTF-8";

	private static final String HTML_TITLE = "TITLE";
	private static final String HTML_TEMPLATE_1 = "<!doctype html><html lang=\"ko\"><head><title>" + HTML_TITLE + "</title><script>%s</script></head><body>%s</body></html>";
	public static final String DATA_LOAD_FAIL = "데이터를 불러오는중 문제가 발생했습니다. 관리자에게 문의해주세요.";
	public static final String DATA_LOAD_SUC = "정상적으로 조회가 완료되었습니다.";
	public static final String DATA_SAVE_FAIL = "데이터를 저장중 문제가 발생했습니다. 관리자에게 문의해주세요.";
	public static final String DATA_SAVE_SUC = "저장이 완료되었습니다.";
	public static final String DATA_DELETE_SUC = "삭제가 완료되었습니다.";
	public static final String DATA_DELETE_FAIL = "삭제중 문제가 발생 했습니다";
	public static final String DATA_LOGIN_SUC = "로그인에 성공했습니다.";
	public static final String FILE_SAVE_SUC = "파일 저장이 완료되었습니다.";
	public static final String FILE_SAVE_FAIL = "파일 저장중 문제가 발생 했습니다.";
	public static final String FILE_AUTH_FAIL = "인증 중 오류가 발생했습니다.";
	public static final String DATA_COMMAND_FAIL = "데이터 형식이 맞지 않습니다.";
	public static final String DATA_JSON_FAIL = "JSON 데이터 없습니다.";


	public static void responseHtml(HttpServletResponse response, String script) {
		responseHtml(response, script, "");
	}

	public static void responseHtml(HttpServletResponse response, String script, String body) {
		try {
			response.setCharacterEncoding(ENCODING_UTF8);
			response.setContentType("text/html;charset=" + ENCODING_UTF8 + "");

			PrintWriter printWriter = response.getWriter();
			printWriter.write(String.format(HTML_TEMPLATE_1, script, body));
			printWriter.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/*
	private static String getResponseHtmlByAlertAndHistoryback(String msg) {
		String script = String.format(JS_ALERT, msg) + JS_HISTORYBACK;
		return String.format(HTML_TEMPLATE_1, script, "");
	}
	*/

	private static final String JS_HISTORYBACK = "history.back();";
	private static final String JS_ALERT = " setTimeout(function() { alert(\"%s\"); }, 10); ";
	private static final String JS_LOCATION = "location.href = \"%s\";";

	private static final String JS_ALERT_AND_CLOSE_POPUP = " setTimeout(function() { alert(\"%s\"); }, 10); document.getElementById('popupList').innerHTML = '';";


	public static void htmlAlert(HttpServletResponse response, String msg) {
		String script = String.format(JS_ALERT, msg);
		responseHtml(response, script);
	}

	public static void htmlAlertClosePopup(HttpServletResponse response, String msg) {
		String script = String.format(JS_ALERT_AND_CLOSE_POPUP, msg);
		responseHtml(response, script);
	}


	public static void htmlLocation(HttpServletResponse response, String url) {
		String script = String.format(JS_LOCATION, url);
		responseHtml(response, script);
	}

	public static void htmlAlertAndHistoryback(HttpServletResponse response, String msg) {
		String script = String.format(JS_ALERT, msg) + JS_HISTORYBACK;
		responseHtml(response, script);
	}

	public static String htmlAlertAndLocation(HttpServletResponse response, String msg, String url) {
		String script = String.format(JS_ALERT, msg) + String.format(JS_LOCATION, url);
		responseHtml(response, script);
		return "";
	}

	public static void responseJsonFailMessage(HttpServletResponse response, String resultCode, String message) {

		String jsonString = "";

		try {
			JSONObject header = new JSONObject();
			header.put("result", false);
			header.put("resultCode", !(resultCode == null || "".equals(resultCode)) ? resultCode : "");
			header.put("message", message);

			JSONObject json = new JSONObject();
			json.put("header", header);
			json.put("body", "");

			jsonString = json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		responseJson(response, jsonString);
	}

	public static void responseJson(HttpServletResponse response, String json) {
		try {
			response.reset();
			response.setCharacterEncoding(ENCODING_UTF8);
			response.setContentType("application/json;charset=" + ENCODING_UTF8 + "");

			PrintWriter printWriter = response.getWriter();
			printWriter.write(json);
			printWriter.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
