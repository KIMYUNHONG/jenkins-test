/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtil {

	public final static String DEFAULT_DATE_FORMAT = "yyyy.MM.dd";

	/**
	 * 문자열 숫자를 BigDecimal로 변환
	 * @param number 변환할 숫자
	 * @param scale 소수점 자리수
	 * @return BigDecimal로 변환된 숫자
	 */
	public static BigDecimal stringToBigDecimal(String number, int scale){
		if(number == null || "".equals(number)){
			return null;
		}
		BigDecimal bigDecimal = new BigDecimal(number);
		bigDecimal = bigDecimal.setScale(scale, RoundingMode.FLOOR);

		return bigDecimal;
	}
}
