/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class FileUtil {

	// 파일 확장자 추출
	public static String getFileFormat(String fileName) {

		if (StringUtil.isEmpty(fileName)) {
			return "";
		}

		int index = fileName.lastIndexOf(".");
		if (index == -1) {
			return "";
		}
		return fileName.substring(index);
	}

	// 썸네일 생성
	public static void makeThumbnail(String uploadPath, String filePath, String fileName, String fileExt) throws Exception {
		BufferedImage srcImg = ImageIO.read(new File(filePath));

		int dw = 80, dh = 80;

		int ow = srcImg.getWidth();
		int oh = srcImg.getHeight();
		int nw = ow;
		int nh = (ow * dh) / dw;

		if (nh > oh) {
			nw = (oh * dw) / dh;
			nh = oh;
		}
		String path = uploadPath;

		BufferedImage cropImg = Scalr.crop(srcImg, (ow - nw) / 2, (oh - nh) / 2, nw, nh);
		BufferedImage destImg = Scalr.resize(cropImg, dw, dh);
		String thumbName = path + "/t_" + fileName;
		File thumbFile = new File(thumbName);
 		ImageIO.write(destImg, fileExt.toLowerCase().replace(".", ""), thumbFile);
	}

	// 이미지 리사이징
	public static void imageResizing(String filePath, String fileExt) throws Exception {
		BufferedImage srcImg = ImageIO.read(new File(filePath));
		String mainPosition = "W";

		int imageWidth = srcImg.getWidth();
		int imageHeight = srcImg.getHeight();

		int standardSize = Math.min(imageWidth, 1280);

		int resizeWidth = 1280;
		int resizeHeight = 720;
		if(imageHeight > imageWidth){
			mainPosition = "H";
			standardSize = Math.min(imageHeight, 720);
		}
		double ratio = 0;


		if(mainPosition.equals("W")){    // 넓이기준

			ratio = (double)standardSize/(double)imageWidth;
			resizeWidth = (int)(imageWidth * ratio);
			resizeHeight = (int)(imageHeight * ratio);

		}else if(mainPosition.equals("H")){ // 높이기준

			ratio = (double)standardSize/(double)imageHeight;
			resizeWidth = (int)(imageWidth * ratio);
			resizeHeight = (int)(imageHeight * ratio);

		}

		BufferedImage destImg = Scalr.resize(srcImg, resizeWidth, resizeHeight);
		String thumbName = filePath;
		File thumbFile = new File(thumbName);
 		ImageIO.write(destImg, fileExt.toLowerCase().replace(".", ""), thumbFile);
	}
}
