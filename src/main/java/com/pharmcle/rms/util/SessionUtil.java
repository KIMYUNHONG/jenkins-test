/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import com.pharmcle.rms.domain.user.User;

import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.util.Map;
import java.util.UUID;

public class SessionUtil {

	private final static String SESSION_KEY_USER_ID = "USER_ID";
	private final static String SESSION_KEY_USER = "USER";
	private final static String SESSION_KEY_REFERER = "REFERER";

	// 세션에 로그인 유저 아이디 저장
	public static boolean setUserId(String userId){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null || ("".equals(userId) || userId == null)) {
			return false;
		}
		session.setAttribute(SESSION_KEY_USER_ID, userId);
		return true;
	}

	// 세션에 로그인 유저 정보 저장
	public static boolean setUser(User user){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null || user == null) {
			return false;
		}
		session.setAttribute(SESSION_KEY_USER, user);
		return true;
	}
	
	// 세션에 로그인오류 페이지 저장
	public static boolean setReferer(String url){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null || url == null) {
			return false;
		}
		session.setAttribute(SESSION_KEY_REFERER, url);
		return true;
	}

	// 로그인한 세션의 유저아이디 조회
	public static String getUserId(){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null) {
			return "";
		}
		return (String) session.getAttribute(SESSION_KEY_USER_ID);
	}

	// 로그인한 세션의 유저 객체 조회
	public static User getUser(){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null) {
			return null;
		}
		return (User) session.getAttribute(SESSION_KEY_USER);
	}
	
	// 로그인오류 페이지 조회
	public static String getReferer(){
		HttpSession session = ServletUtil.getRequest().getSession();
		if(session == null) {
			return null;
		}
		return (String) session.getAttribute(SESSION_KEY_REFERER);
	}

	/**
	 * 클라이언트 IP 조회
	 * @return Client IP
	 */
	public static String getClientIp(){
		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			return inetAddress.getHostAddress();

		} catch (Exception e) {
			return "";
		}
	}

	public static String generateToken(){
		return UUID.randomUUID().toString();
	}




	public static String getToken(){
		Map<String, String[]> o = ServletUtil.getRequest().getParameterMap();
		String token = (o.get("token") != null && o.get("token").length > 0) ? o.get("token")[0] : null;

		return token;
	}
}
