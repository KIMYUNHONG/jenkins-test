package com.pharmcle.rms.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class CryptoUtil {
	static String enKey = "_keysystemcryptphc";
	
	/**
	 * MD5 로 해시 한다.
	 * 
	 * @param msg
	 * @return
	 */
	public static String md5(String msg) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(msg.getBytes());
		return CryptoUtil.byteToHexString(md.digest());
	}
	
	/**
	 * BCrypt 로 변환하기
	 * 
	 * @param String
	 * @return String
	 */
	public String getBCryptDecrypt(String str) {
		return new BCryptPasswordEncoder().encode(str);
	}
	
	/**
	 * SHA-256으로 해시한다.
	 * 
	 * @param msg
	 * @return
	 */
	public static String sha256(String msg)  throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(msg.getBytes());
		return CryptoUtil.byteToHexString(md.digest());
	}
	
	/**
	 * 바이트 배열을 HEX 문자열로 변환한다.
	 * @param data
	 * @return
	 */
	public static String byteToHexString(byte[] data) {
		StringBuilder sb = new StringBuilder();
		for(byte b : data) {
			sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	
	/**
	 * 공통 > 암호화 하기
	 * @param String : 암호화 키, 엄호화할 문자열
	 * @return String
	 */
	public static String getAESEncrypt(String str) {
		String returnStr = "";
		if(StringUtil.isEmpty(str)) {
			return returnStr;
		}
		
		try {
			SecretKeySpec ks = new SecretKeySpec(generateKey(enKey), "AES");
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(1, ks);
	        byte encryptedBytes[] = cipher.doFinal(str.getBytes());
	        returnStr = new String(Base64Coder.encode(encryptedBytes));
	    }
	    catch(Exception e) {
	        e.printStackTrace();
	    }
		return returnStr;
	}
	
	/**
	 * 공통 > 암호화 하기
	 * @param String : 암호화 키
	 * @return String
	 */
	public static byte[] generateKey(String key) {
		byte desKey[] = new byte[16];
		byte bkey[] = key.getBytes();
		if(bkey.length < desKey.length) {
			System.arraycopy(bkey, 0, desKey, 0, bkey.length);
			for(int i = bkey.length; i < desKey.length; i++)
				desKey[i] = 0;

		} else {
			System.arraycopy(bkey, 0, desKey, 0, desKey.length);
		}
		return desKey;
	}
	
	/**
	 * 공통 > 복호화 하기
	 * @param String : 복호화 키, 복호화할 문자열
	 * @return String
	 */
	public static String getAESDecrypt(String str) {
		String returnStr = "";
		if(StringUtil.isEmpty(str)) {
			return returnStr;
		}
		
		try {
	        SecretKeySpec ks = new SecretKeySpec(generateKey(enKey), "AES");
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(2, ks);
	        byte decryptedBytes[] = cipher.doFinal(Base64Coder.decode(str));
	        returnStr = new String(decryptedBytes).toString();
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    }
		return returnStr;
	}
}
