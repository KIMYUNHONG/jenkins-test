/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import com.pharmcle.rms.base.ValidException;

import java.time.LocalDate;

public class ValidDateUtil {

	public static void nullChk(LocalDate date, String dateName) throws ValidException{

		if(date == null){
			throw new ValidException(dateName + "를 입력해주세요");
		}
	}

}
