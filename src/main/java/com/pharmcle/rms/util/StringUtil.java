/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

public class StringUtil {

	public static String nvl(String value, String nullValue) {
		return value == null || "".equals(value) ? nullValue : value;
	}

	public static boolean isEmpty(String value) {
		return value == null || "".equals(value);
	}

	public static boolean isNotEmpty(String value) {
		return !(value == null || "".equals(value));
	}

	public static String ListToString(String toStringData){
		if(toStringData.contains("[") && toStringData.contains("]")){
			toStringData = toStringData.substring(1, toStringData.length() -1);
		}
		return toStringData;
	}
}
