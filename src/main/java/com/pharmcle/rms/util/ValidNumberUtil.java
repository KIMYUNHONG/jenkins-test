/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import com.pharmcle.rms.base.ValidException;

import java.math.BigDecimal;

public class ValidNumberUtil {

	public static void nullChk(Number number, String numberName) throws ValidException{

		if(number == null){
			throw new ValidException(numberName + "를 입력해주세요");
		}

	}
	public static void zeroChk(Number number, String numberName) throws ValidException{

		nullChk(number, numberName);

		BigDecimal bigDecimal = new BigDecimal(number.doubleValue());
		if(bigDecimal.equals(BigDecimal.ZERO)){
			throw new ValidException(numberName + "를 입력해주세요");
		}
	}
	public static void rangeChk(Number number, String numberName, Number start, Number end) throws ValidException{

		nullChk(number, numberName);

		BigDecimal bigDecimal = new BigDecimal(number.doubleValue());
		BigDecimal startDecimal = new BigDecimal(start.doubleValue());
		BigDecimal endDecimal = new BigDecimal(end.doubleValue());
		if(bigDecimal.compareTo(startDecimal) < 0){
			throw new ValidException(numberName + "를 " + startDecimal + "이상으로 입력해주세요.");
		}
		if(bigDecimal.compareTo(endDecimal) > 0){
			throw new ValidException(numberName + "를 " + endDecimal + "이하로 입력해주세요.");
		}
	}
}
