package com.pharmcle.rms.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;

import java.lang.reflect.Method;

@Slf4j
public class InterceptorUtil {

	public static Method getExecuteMethod(Object handler) {

		Method executeMethod = null;

		if (handler instanceof HandlerMethod) {
			HandlerMethod handlerMethod = (HandlerMethod) handler;
			executeMethod = handlerMethod.getMethod();
		}

		return executeMethod;
	}

}
