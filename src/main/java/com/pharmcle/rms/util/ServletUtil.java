/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class ServletUtil {

	public static final String IS_MOBILE = "ANDROID";
	private static final String IS_PHONE = "PHONE";
	public static final String IS_TABLET = "TABLET";
	public static final String IS_PC = "PC";

	public static HttpServletRequest getRequest() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes != null) {
			return ((ServletRequestAttributes) requestAttributes).getRequest();
		}
		return null;
	}
	/**
	 * 모바일인지 체크
	 * @param req
	 * @return
	 */
	public static boolean  isMobile(HttpServletRequest req) {
		if(req == null){
			return false;
		}
		if(getUserAgent() == null){
			return false;
		}
		String userAgent = getUserAgent().toUpperCase();
		return userAgent.contains(IS_MOBILE) || userAgent.contains("IOS") || userAgent.contains("IPHONE");
	}

	public static String getUserAgent() {
		try {
			return getRequest().getHeader("User-Agent");
		} catch (Exception e){
			return "";
		}
	}

	/**
	 * api경로인지 여부
	 */
	public static boolean isApiCall(HttpServletRequest request) {
		return request.getRequestURI().contains("/api/");
	}
}
