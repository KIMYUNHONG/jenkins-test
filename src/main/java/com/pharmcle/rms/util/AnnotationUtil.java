package com.pharmcle.rms.util;


import com.pharmcle.rms.annotation.GuestAccess;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationUtil {

	public static boolean existGuestAccess(Method method) {

		if (method != null) {
			Annotation[] annotations = method.getAnnotations();

			if (annotations != null) {
				for (Annotation annotation : annotations) {
					if (annotation instanceof GuestAccess) {
						return true;
					}
				}
			}
		}

		return false;
	}


}
