/**
 * Create By Lee DoWon on 2020-02-18 / 오후 12:44
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.util;

import com.pharmcle.rms.base.ValidException;

public class ValidStringUtil {

	public static void nullChk(String str, String strName) throws ValidException{

		if(StringUtil.isEmpty(str)){
			throw new ValidException(strName + "를(을) 입력해주세요");
		}
	}

	public static void lengthChk(String str, String strName, int minLength, int maxLength) throws ValidException{

		nullChk(str, strName);
		if(str.length() < minLength){
			throw new ValidException(strName + "는 " + minLength + "자리 이상 입력해주세요.");
		} else if(str.length() > maxLength){
			throw new ValidException(strName + "는 " + maxLength + "자리 이하로 입력해주세요.");
		}
	}
	public static void codeChk(String str, String strName) throws ValidException{

		nullChk(str, strName);
		if(str.length() != 8){
			throw new ValidException(strName + "는 코드 데이터를 입력해주세요.");
		}
	}

	public static void phoneNumberFormatChk(String str, String strName) throws ValidException{

		nullChk(str, strName);
		// 전화번호,핸드폰의 형태 인지 체크
		String regExp = "^\\d{2,3}-\\d{3,4}-\\d{4}$";
		if(!str.matches(regExp)){
			throw new ValidException(strName + "를(을) 형식에 맞게 입력해주세요.");
		}
	}
	public static void emailFormatChk(String str, String strName) throws ValidException{
		nullChk(str, strName);

		String regExp = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";

		if(!str.matches(regExp)){
			throw new ValidException(strName + "를(을) 형식에 맞게 입력해주세요.");
		}


	}

	// 사업자등록번호 같이 ('xxx-xx-xxxxx') 숫자와 dash만 있는지 체크
	public static void bisNumberFormatChk(String str, String strName) throws ValidException{
		nullChk(str, strName);

		String regExp = "^\\d{3}-\\d{2}-\\d{5}$";
		if(!str.matches(regExp)){
			throw new ValidException(strName + "를(을) 형식에 맞게 입력해주세요.");
		}
	}

	public static void pictureFileChk(String str) throws ValidException{
		nullChk(str, "파일 이름");
		str = str.toLowerCase();
		String regExp = "(.*?)\\.(jpg|jpeg|png|gif|bmp)$";
		str = FileUtil.getFileFormat(str);
		if(!str.matches((regExp))){
			throw new ValidException("이미지 파일만 등록이 가능합니다.");
		}
	}
	public static void userIdChk(String str, String strName) throws ValidException{
		nullChk(str, strName);
		String regExp =  "^[a-z0-9]*$";
		if(!str.matches(regExp)){
			throw new ValidException(strName + "를(을) 형식에 맞게 입력해주세요.");
		}
	}
	public static void emailIdChk(String str, String strName) throws ValidException{
		nullChk(str, strName);
		String regExp =  "^[a-z0-9_\\.]*$";
		System.out.println(str.matches(regExp));
		if(!str.matches(regExp)){
			throw new ValidException(strName + "를(을) 형식에 맞게 입력해주세요.");
		}
	}
}
