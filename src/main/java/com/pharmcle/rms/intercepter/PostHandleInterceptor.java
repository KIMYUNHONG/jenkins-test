package com.pharmcle.rms.intercepter;

import com.pharmcle.rms.base.SiteInfo;
import com.pharmcle.rms.domain.menu.Menu;
import com.pharmcle.rms.domain.menu.MenuParameter;
import com.pharmcle.rms.domain.menu.MenuService;
import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.todo.Todo;
import com.pharmcle.rms.domain.todo.TodoService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class PostHandleInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	MenuService menuService;

	@Autowired
	UserService userService;

	@Autowired
	TodoService todoService;

	@Autowired
	MonitorService monitorService;


	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
		ModelAndView modelAndView) throws Exception {
		Method executeMethod = InterceptorUtil.getExecuteMethod(handler);
		boolean isGuessPage = AnnotationUtil.existGuestAccess(executeMethod);
		boolean isMobile = ServletUtil.isMobile(request);

		if(modelAndView != null && !ServletUtil.isApiCall(request)){
			modelAndView.addObject("isMobile", isMobile);
		}
		// 웹 메뉴 받아오는 부분
		if (modelAndView != null && !isGuessPage && !ServletUtil.isApiCall(request)) {

			User user = userService.getUserBySessionOrToken(SessionUtil.getToken());
			String userAuth = null;
			if(user != null) {
				userAuth = user.getAuthRole();
				
				String phone = user.getPhone();
				String telephone = user.getTelephone();
				
				user.setPhone(CryptoUtil.getAESDecrypt(phone));
				user.setTelephone(CryptoUtil.getAESDecrypt(telephone));
			}
			modelAndView.addObject("timestamp", LocalDateTime.now());
			modelAndView.addObject("_userInfo", user);

			// 팝업의 경우에는 메뉴 정보가 필요없음.
			if(!request.getRequestURI().contains("Popup") && !isMobile){
				List<Menu> menuList = menuService.getMyMenuList(new MenuParameter(userAuth, request.getRequestURI()));
				// 현재 메뉴가 뭔지 체크
				SiteInfo siteInfo = new SiteInfo(request.getRequestURI(), menuList);
				if(siteInfo.getCurrentMenuName() == null){
					ResponseUtil.htmlAlertAndHistoryback(response, "접근 권한이 없습니다.");
				}
				modelAndView.addObject("_siteInfo", siteInfo);
				modelAndView.addObject("_menuList", menuList);
				modelAndView.addObject("todoCount", todoService.getTodoCount(new Todo(user.getUserId())));
				modelAndView.addObject("latestMonitorDate", monitorService.getLatestMontiorDate());
			}
		}
		super.postHandle(request, response, handler, modelAndView);
	}
}
