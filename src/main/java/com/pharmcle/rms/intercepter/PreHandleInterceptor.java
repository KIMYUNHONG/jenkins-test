package com.pharmcle.rms.intercepter;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.pharmcle.rms.domain.user.UserLoginService;
import com.pharmcle.rms.domain.user.UserService;
import com.pharmcle.rms.util.AnnotationUtil;
import com.pharmcle.rms.util.InterceptorUtil;
import com.pharmcle.rms.util.ResponseUtil;
import com.pharmcle.rms.util.ServletUtil;
import com.pharmcle.rms.util.SessionUtil;
import com.pharmcle.rms.util.StringUtil;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class PreHandleInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	UserLoginService userLoginService;
	@Autowired
	UserService userService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String userId = userService.getUserIdBySessionOrToken(SessionUtil.getToken());
		Method executeMethod = InterceptorUtil.getExecuteMethod(handler);

		boolean isApi = ServletUtil.isApiCall(request);
		boolean isGuessPage = AnnotationUtil.existGuestAccess(executeMethod);
		// 웹에서 로그인이 필요한 페이지에서 로그인 했는지 체크
		if (!isApi && StringUtil.isEmpty(userId) && !isGuessPage) {
			if(ServletUtil.isMobile(request)){
				log.info("current Login ID : " + userId + ", token = " + SessionUtil.getToken());
				log.info("URI : " + ServletUtil.getRequest().getRequestURI());
				log.info("isMobile ? " + ServletUtil.isMobile(ServletUtil.getRequest()));
				ResponseUtil.htmlLocation(response, "/error/login");
			}else {
				String refererUrl = "";
				
				if((ServletUtil.getRequest().getRequestURI()).toLowerCase().indexOf("robots.txt") < 0) { 
					if(ServletUtil.getRequest().getQueryString() == null) {
						refererUrl = ServletUtil.getRequest().getRequestURI();
					} else {
						refererUrl = ServletUtil.getRequest().getRequestURI() + "?" + ServletUtil.getRequest().getQueryString();
					}
					
					SessionUtil.setReferer(refererUrl);
				}

				ResponseUtil.htmlAlertAndLocation(response, "세션이 만료되었습니다. 다시 로그인 후 이용해주세요. ", "/login");
			}
			return false;
		}

		// api 요청인경우 세션 또는 토큰 체크
		if (isApi && !isGuessPage) {
			log.info("current Login ID : " + userId + ", token = " + SessionUtil.getToken());
			log.info("URI : " + ServletUtil.getRequest().getRequestURI());
			log.info("isMobile ? " + ServletUtil.isMobile(ServletUtil.getRequest()));
			// 세션이 없는경우
			if (userId == null) {
				ResponseUtil.responseJsonFailMessage(response, "GO_LOGIN", "세션이 만료되었습니다. 다시 로그인 후 이용해주세요. ");
				return false;
			}
		}
		return super.preHandle(request, response, handler);
	}
}
