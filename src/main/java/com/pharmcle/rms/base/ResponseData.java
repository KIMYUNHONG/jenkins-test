package com.pharmcle.rms.base;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ResponseData<T> {

	private Header header;
	private T body;

	public ResponseData() {
		this(true, null, "", "");
	}

	public ResponseData(boolean result, T body) {
		this(result, body, "", "");
	}

	public ResponseData(boolean result, T body, String message) {
		this(result, body, "", message);
	}

	public ResponseData(boolean result, T body, String resultCode, String message) {
		header = new Header(result, resultCode, message);
		this.body = body;
	}

	public ResponseData(boolean result, T body, String message, int totalCount) {
		header = new Header(result, "", message, totalCount);
		this.body = body;
	}
	public ResponseData(boolean result, String message) {
		this(result, (T) "", "", message);
	}

}
