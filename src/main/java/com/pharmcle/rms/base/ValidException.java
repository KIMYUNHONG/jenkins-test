/**
 * Create By Lee DoWon on 2020-03-19 / 오후 4:28
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.base;

public class ValidException extends Exception {
	public ValidException(String message, Throwable cause){
		super(message, cause);
	}
	public ValidException(String message){
		super(message);
	}
}
