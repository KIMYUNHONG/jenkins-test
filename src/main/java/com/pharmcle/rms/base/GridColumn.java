/**
 * Create By Lee DoWon on 2020-04-10 / 오전 9:05
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GridColumn {

	private String header;
	private String name;
	private int width;
	private int minWidth;
	private String align;
	private boolean numberFormat;

	private String groupName;
}
