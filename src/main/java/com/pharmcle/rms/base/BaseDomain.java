/**
 * Create By Lee DoWon on 2020-02-17 / 오후 1:51
 * Email : vin4195@uniwiz.co.kr
 **/

package com.pharmcle.rms.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pharmcle.rms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BaseDomain {

	// 등록일시
	@JsonIgnore
	private LocalDateTime regDt;

	// 등록자 아이디
	private String regId;

	// 수정일시
	@JsonIgnore
	private LocalDateTime uptDt;

	// 수정자 아이디
	private String uptId;

	// 수정자 이름
	public String uptName;
	public String regName;

	// 사용여부 Y/N
	public String useYn;
	public String useYnText;
	public String getUseYnText() { return "Y".equals(useYn) ? "O" : "X";}


	public String regDtText;
	public String getRegDtText() {
		return DateUtil.localDatetimeToString(regDt, DateUtil.DEFAULT_DATE_FORMAT);
	}


	public String regDtLongText;
	public String getRegDtLongText() {
		return DateUtil.localDatetimeToString(regDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}

	public String uptDtText;
	public String getUptDtText() {
		return DateUtil.localDatetimeToString(uptDt, DateUtil.DEFAULT_DATE_FORMAT);
	}

	public String uptDtLongText;
	public String getUptDtLongText() {
		return DateUtil.localDatetimeToString(uptDt, DateUtil.DEFAULT_LONG_DATE_FORMAT);
	}

}
