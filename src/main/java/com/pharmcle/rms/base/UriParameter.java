package com.pharmcle.rms.base;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UriParameter {

    String headerMenu;
    String sideMenu;
}
