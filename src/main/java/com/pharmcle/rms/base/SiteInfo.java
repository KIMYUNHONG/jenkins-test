package com.pharmcle.rms.base;

import com.pharmcle.rms.domain.menu.Menu;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SiteInfo {

	private String headerMenuName;
	private String sideMenuName;
	private String currentMenuName;
	private String currentUrl;
	private String currentUri;
	private String menuAuth;

	public SiteInfo(String uri, List<Menu> menuList){

		for(Menu headerMenu : menuList){
			for(Menu sideMenu : headerMenu.getChildMenuList()){

				// 현재 페이지 권한 체크를 위해서 받아온 메뉴리스트중 현재 URI가 있는지 체크
				if(uri.equals(sideMenu.getMenuUri())){
					this.currentMenuName = sideMenu.getMenuName();
				}
				// 현재 페이지 권한 체크를 위해서 받아온 메뉴리스트중 현재 URI가 있는지 체크
				if(sideMenu.getChildMenuList() != null && sideMenu.getChildMenuList().size() > 0){
					for(Menu sideMenu2 : sideMenu.getChildMenuList()){
						if(uri.equals(sideMenu2.getMenuUri())){
							this.currentMenuName = sideMenu.getMenuName();
						}
					}
				}

				String[] splitUri = uri.split("/");
				try {
					if (splitUri.length >= 2 && sideMenu.getMenuUri().contains("/" + splitUri[1] + "/") && sideMenu.getMenuUri().contains("/" + splitUri[1] + "/" + splitUri[2] + "/")) {
						this.headerMenuName = headerMenu.getMenuName();
						this.sideMenuName = sideMenu.getMenuName();
						this.currentUri = uri;
						this.menuAuth = sideMenu.getMenuAuth();
						sideMenu.setCurrentMenu(true);
						headerMenu.setCurrentMenu(true);
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
