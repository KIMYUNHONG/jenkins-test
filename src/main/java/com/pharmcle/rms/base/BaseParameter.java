package com.pharmcle.rms.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseParameter {
    Integer startIndex;
    Integer indexCount;
}
