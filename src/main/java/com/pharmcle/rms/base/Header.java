package com.pharmcle.rms.base;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Header {

	private boolean result;
	private String resultCode;
	private String message;
	private int totalCount;

	public Header(boolean result, String resultCode, String message) {
		this.result = result;
		this.resultCode = resultCode;
		this.message = message;
	}
	public Header(boolean result, String resultCode, String message, int totalCount) {
		this.result = result;
		this.resultCode = resultCode;
		this.message = message;
		this.totalCount = totalCount;
	}
	public Header(boolean result) {
		this.result = result;
	}

}
