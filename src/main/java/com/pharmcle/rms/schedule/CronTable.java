package com.pharmcle.rms.schedule;

import com.pharmcle.rms.domain.monitor.MonitorService;
import com.pharmcle.rms.domain.user.User;
import com.pharmcle.rms.domain.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
* 배치 설정 
* @author : jwh
* @since  : 2020-02-13
* @desc   : 배치 프로세스 설정
*/

@Slf4j
@Component
public class CronTable {

	@Autowired
	MonitorService monitorService;

	@Autowired
	UserService userService;

	/**
	 * influx DB 저장
	 * 매시 10분에 실행
	 */
	@Scheduled(cron = "0 5 * * * *")
	public void responseLotteTracking() {
    	try {
			monitorService.setLoRaData("");
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * 모니터 알람 로그 저장 배치
	 * 매일 1시 반에 실행
	 */
	@Scheduled(cron = "0 30 1 * * *")
	//@Scheduled(cron = "0 4 * * * *")
	public void setMonitorAlarmLog() {
    	try {
			monitorService.setMonitorAlarmUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
	 * 휴면계정 전환처리
	 * 매일 4시에 실행
	 */
	@Scheduled(cron = "0 0 4 * * *")
	public void setUserState() {
    	try {
			log.info("################################################## 휴면계정 처리 스케줄러 START - " + LocalDateTime.now());
			List<User> userList = userService.getRestUserList();
			log.info("# 휴면계정 수 : " + userList.size());
			for(User user : userList) {
				log.info("[소속]이름 : " + "[" + user.getCustomerName() + "]" + user.getName() + ", 아이디 : " + user.getUserId());
				userService.setRestUser(user.getUserSeq());
			}
			log.info("################################################## 휴면계정 처리 스케줄러 END - " + LocalDateTime.now());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
