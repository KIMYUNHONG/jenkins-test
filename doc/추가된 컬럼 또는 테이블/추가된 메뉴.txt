
------------------------------------------------------
-- 2019.04.01 권한 정리 - 개발 반영 O , 운영 X

UPDATE pharmcle.menu_auth SET MENU_AUTH = 'SD010002' WHERE MENU_AUTH ='SD010004';
UPDATE pharmcle.menu_auth SET MENU_AUTH = 'SD010003' WHERE MENU_AUTH ='SD010005';

DELETE FROM pharmcle.code WHERE CODE LIKE 'SD010004' ESCAPE '#';
DELETE FROM pharmcle.code WHERE CODE LIKE 'SD010003' ESCAPE '#';

UPDATE pharmcle.code t SET t.CODE_NAME = '조회/수정' WHERE t.CODE LIKE 'SD010002' ESCAPE '#';
UPDATE pharmcle.code t SET t.CODE = 'SD010003' WHERE t.CODE LIKE 'SD010005' ESCAPE '#;'

------------------------------------------------------

------------------------------------------------------
-- 2019.04.02 유지보수 관련 테이블 추가
create table maintain - 개발 반영 O , 운영 X
(
	DEVICE_SEQ int not null comment '장비_키',
	MAINTAIN_SEQ int not null comment '유지보수_차수',
	MAINTAIN_RESULT char(8) null comment '유지보수_결과',
	CONTENT varchar(2000) null comment '내용',
	LAST_LATITUDE decimal(20,17) null comment '변경전_위도',
	LAST_LONGITUDE decimal(20,17) null comment '변경전_경도',
	LAST_DEVICE_ADDRESS varchar(200) null comment '변경전_주소',
	LAST_MANAGE_NO varchar(40) null comment '변경전_관리번호',
	LAST_LAMP_NO varchar(40) null comment '변경전_가로등번호',
	REG_DT datetime default current_timestamp() null,
	REG_ID varchar(30) null,
	UPT_DT datetime null,
	UPT_ID varchar(30) null,
	VOLUME int default 0 null comment '포집량',
	primary key (DEVICE_SEQ, MAINTAIN_SEQ)
)
comment '유지보수';

create table maintain_type
(
	DEVICE_SEQ int not null comment '장비_시퀀스',
	MAINTAIN_SEQ int not null comment '유지보수_시퀀스',
	TYPE char(8) not null comment '유지보수_타입',
	REG_DT datetime default current_timestamp() null,
	REG_ID varchar(30) null,
	primary key (DEVICE_SEQ, MAINTAIN_SEQ, TYPE)
)
comment '유지보수_타입';

------------------------------------------------------
