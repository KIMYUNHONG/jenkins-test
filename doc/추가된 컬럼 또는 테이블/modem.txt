 - 개발 반영 O , 운영 X

create table modem
(
	MODEM_ID varchar(20) not null comment '모뎀_ID',
	REG_DT datetime default current_timestamp() null comment '등록일',
	REG_ID varchar(50) null comment '등록자',
	DEVICE_ID varchar(20) null comment '장비_아이디',
	constraint device_modem_MODEM_ID_uindex
		unique (MODEM_ID)
)
	comment '기기_모뎀';

alter table modem
	add primary key (MODEM_ID);

